__version__ = '0.1'




import logging
slog = logging.getLogger('solpspy')
if not slog.handlers: #Prevents from adding repeated streamhandlers
    formatter = logging.Formatter(
            '%(name)s -- %(levelname)s: %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    slog.addHandler(ch)
    slog.propagate = False #Otherwise, root logger also prints





from .factory import SolpsData

from .classes.mds import MdsData
from .classes.rundir import RundirData

#from .classes.family import Family

from .tools.tools import get_rhopol
