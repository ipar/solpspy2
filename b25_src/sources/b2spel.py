import numpy as np


#Auxiliary functions
def trimg(t0,t1,tt):
    """trimg = lambda (t0,t1,tt): np.max(t0,np.min(t1,tt))"""
    return max(t0, min(t1,tt))

def compute_coefficients(rtlsa, fxne, fxte, rlte, rtlt, ite, ine, si, ltst0):
    t0 = ((1.0 - fxne)*rtlsa[ite,ine,si] +
                 fxne*rtlsa[ite,ine+1,si])
    t1 = ((1.0 - fxne)*rtlsa[ite+1,ine,si] +
                 fxne*rtlsa[ite+1,ine+1,si])
    tt = (t1-t0)/(rtlt[ite+1] - rtlt[ite])
    if ltst0:
        tmp1 = trimg(-20.0, 20.0, tt)
    else:
        tmp1 = trimg(-20.0, 20.0, tt)
    tmp2 = (1.0 - fxte)*t0 + fxte*t1 - tmp1*rlte
    return tmp2, tmp1 #Correct order for 0:1




def b2spel(mother):
    """
    Code extracted from sources/b2xpen.F of SOLPS-ITER

        B2SPEL computes on all cells of the mesh a log-log representation
    of the rate coefficients for ionisation, recombination, radiation
    and electron heat loss.

        This routine uses table interpolation to compute on each cell
    and for each atomic species a local representation of the rate
    coefficients for ionisation, recombination, electron heat
    loss and radiation. The representation is linear in the log of
    the rate coefficient versus the log of the temperature.
    The tables store the log of the rate coefficients as a function
    of log(te/ev) and log(ne). Linear interpolation is used.
    See the appropriate routines elsewhere in the code for a
    description of the computation of the table entries.

        Linear interpolation in the table is employed to represent the
    log of each rate coefficient as a function of log(te/ev) and
    log(ne). If log(te/ev) falls outside the domain of the table
    then linear extrapolation in that variable is used; if log(ne)
    falls outside the domain of the table then a prudent zeroth-order
    extrapolation is used.

    See also routine b2cdca):



    Input
    -----
    mother (solps object):
        From mother, the electron density, temperature and charge are extracted.
        Also all the components rtl*.

    b2cmrc (table):
        Contains all the requred rtl* for calculations of rl*

    Output
    ------

    rlsa (nx, ny, 2, ns):
        Coefficients in the representation
            >> rsa = exp(rlsa(,,0,)+rlsa(,,1,)*log(te(ix,iy)/ev))
        that specifies the rate coefficient for ionisation from atomic
        species (is) to species (is+1).

    rlra (nx, ny, 2, ns):
        Coefficients in the representation
            >> rra = exp(rlra(,,0,)+rlra(,,1,)*log(te(ix,iy)/ev))
        that specifies the rate coefficient for recombination from atomic
        species (is) to species (is-1).

    rlqa (nx, ny, 2, ns):
        Coefficients in the representation
            >> rqa = ev*exp(rlqa(,,0,)+rlqa(,,1,)*log(te(ix,iy)/ev))
        that specifies the rate coefficient for electron heat loss terms
        that are proportional to the density of the atomic species (is).

    rlrd (nx, ny, 2, ns):
        Coefficients for line radiation. Not described in the manual

    rlbr (nx, ny, 2, ns):
        Coefficients for bremsstrahlung radiation. Not described in the manual

    rlza (nx, ny, 2, ns):
        Coefficients in the representation
            >> rza = rlza(,,0,)+rlza(,,1,)*log(te(ix,iy)/ev)
        that specifies the effective charge of stage (is).

    rlz2 (nx, ny, 2, ns):
        Coefficients in the representation
            >> rz2 = rlz2(,,0,)+rlz2(,,1,)*log(te(ix,iy)/ev)
        that specifies the effective square charge of stage (is).

    rlpt (nx, ny, 2, ns):
        Coefficients in the representation
            >> rpt = rlpt(,,0,)+rlpt(,,1,)*log(te(ix,iy)/ev)
        that specifies the cumulative ionisation potential to stage (is).

    rlpi (nx, ny, 2, ns):
        Coefficients in the representation
            >> rpi = rlpi(,,0,)+rlpi(,,1,)*log(te(ix,iy)/ev)
        that specifies the effective ionisation potential to stage (is)
        from stage (is-1).


    Notes
    -----
        Internal parameters described in driver/b2mod_driver.F

    rtnt (int):
        Number of intervals in the discretization of temperature range.
        The table will contain (rtnt+1) values.

    rtnn (int):
        Number of intervals in the discretization of density range.
        The table will contain (rtnn+1) values.
        Described in driver/b2mod_driver.F

    rtt [rtnt]:
        Specifies the sequence of values of the temperature variable,
        expressed in ev, for which the rate coefficients are evaluated.
        Will be positive and increasing.

    rtn [rtnn]:
        Specifies the sequence of values of the density variable,
        expressed in m^-3, for which the rate coefficients are evaluated.
        Will be positive and increasing.

    rtlt [rtnt]:
        rtlt[:] = log(rtt[:])

    rtln [rtnn]:
        rtln[:] = log(rtn[:])

    """

    # Unpacking inputs for less verbose code
    ny = mother.ny
    nx = mother.nx
    ns = mother.ns
    ev = mother.ev

    rtnt = mother.rtnt -1
    rtnn = mother.rtnn -1

    ne = mother.ne.copy()
    te = mother.te.copy()*ev #Inside solps, temperatures are stored in J.

    rtlt = mother.rtlt.copy()
    rtln = mother.rtln.copy()

    rtlsa = mother.rtlsa.copy()
    rtlra = mother.rtlra.copy()
    rtlqa = mother.rtlqa.copy()
    rtlrd = mother.rtlrd.copy()
    rtlbr = mother.rtlbr.copy()
    rtlza = mother.rtlza.copy()
    rtlz2 = mother.rtlz2.copy()
    rtlpt = mother.rtlpt.copy()
    rtlpi = mother.rtlpi.copy()

    # Defining arrays of outputs
    rlsa = np.zeros((nx,ny,2,ns))
    rlra = np.zeros((nx,ny,2,ns))
    rlqa = np.zeros((nx,ny,2,ns))
    rlrd = np.zeros((nx,ny,2,ns))
    rlbr = np.zeros((nx,ny,2,ns))
    rlza = np.zeros((nx,ny,2,ns))
    rlz2 = np.zeros((nx,ny,2,ns))
    rlpt = np.zeros((nx,ny,2,ns))
    rlpi = np.zeros((nx,ny,2,ns))


    # Begining of calcultion in each cell
    ite = 0
    ine = 0


    for yi in xrange(ny):
        for xi in xrange(nx):
            # Set ltst0
            ltst0 = (rtlt[0] <= np.log(te[xi,yi]/ev))

            # Compute abscissae.
            #rlte = np.max(rtlt[0], np.min(rtlt[rtnt], np.log(te[xi,yi]/ev))
            #rlne = np.max(rtln[0], np.min(rtln[rtnn], np.log(ne[xi,yi]))
            rlte = trimg(rtlt[0], rtlt[rtnt], np.log(te[xi,yi]/ev))
            rlne = trimg(rtln[0], rtln[rtnn], np.log(ne[xi,yi]))


            # Find ite
            while True:
                if (ite < rtnt-1):
                    if (rtlt[ite+1] <= rlte):
                        ite += 1
                    else:
                        break
                else:
                    break
            while True:
                if (0.0 < ite): #Correct for Python indexing??
                    if (rlte < rtlt[ite]):
                        ite -= 1
                    else:
                        break
                else:
                    break
            # Compute coefficient for linear interpolation
            fxte = (rlte - rtlt[ite]) / (rtlt[ite+1] - rtlt[ite])

            # Find ine
            while True:
                if (ine < rtnn-1):
                    if (rtln[ine+1] <= rlne):
                        ine += 1
                    else:
                        break
                else:
                    break
            while True:
                if (0.0 < ine): #Correct for Python indexing??
                    if (rlne < rtln[ine]):
                        ine -= 1
                    else:
                        break
                else:
                    break
            # Compute coefficient for linear interpolation
            fxne = (rlne - rtln[ine]) / (rtln[ine+1] - rtln[ine])


            # Compute log-log rate coefficients
            for si in xrange(ns):

                #Compute ionisation rate coefficient
                rlsa[xi,yi,:,si] = compute_coefficients(rtlsa,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)

                #Compute recombination rate coefficient
                rlra[xi,yi,:,si] = compute_coefficients(rtlra,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)

                #Compute heat loss rate coefficient
                rlqa[xi,yi,:,si] = compute_coefficients(rtlqa,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)

                #Compute line radiation rate coefficient
                rlrd[xi,yi,:,si] = compute_coefficients(rtlrd,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)

                #Compute bremsstrahlung radiation rate coefficient
                rlbr[xi,yi,:,si] = compute_coefficients(rtlbr,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)

                #Compute effective charge state
                rlza[xi,yi,:,si] = compute_coefficients(rtlza,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)

                #Compute effective square charge
                rlz2[xi,yi,:,si] = compute_coefficients(rtlz2,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)

                #Compute cumulative ionisation potential
                rlpt[xi,yi,:,si] = compute_coefficients(rtlpt,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)

                #Compute effective ionisation potential
                rlpi[xi,yi,:,si] = compute_coefficients(rtlpi,
                        fxne, fxte, rlte, rtlt, ite, ine, si, ltst0)


    return rlsa, rlra, rlqa, rlrd, rlbr, rlza, rlz2, rlpt, rlpi


if __name__ == '__main__':
    from solpspy import *
    import os
    from IPython import embed
    #run = SolpsData('../../tests/rundir_src/119603/run/')
    run = SolpsData('/ptmp1/work/ipar/solps-iter-develop/runs/aug_ddnu_34303/newer_feedback/for_debugging/', maggeom='ddnu')
    allvars = b2spel(run)

    allvars2 = []
    path = '/ptmp1/work/ipar/solps-iter-develop/runs/aug_ddnu_34303/newer_feedback/for_debugging/testings/'
    for i, typ in enumerate(['rlsa', 'rlra', 'rlqa', 'rlrd', 'rlbr', 'rlza', 'rlz2', 'rlpt', 'rlpi']):
        allvars2.append(np.load(os.path.join(path,typ+'.npy')))
        print typ + " is " + str(np.allclose(allvars[i],allvars2[i]))


    embed()
