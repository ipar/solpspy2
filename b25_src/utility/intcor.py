import os
import numpy as np
from IPython import embed

def intcor(mother, centre):
    """ INTerpolation of quantity from cell centre to cell CORners.
    Which corner?? Most likely bottom left corner.

    From utility/intcor.F, line 10:
            call intcor(nx,ny,vol,centre(-1,-1,is),corner(-1,-1,is))

    Copied almost verbatim (with adapatation of indices and ()<->[].
    """
    nx = mother.nx
    ny = mother.ny
    #vol = mother.vol
    vol = np.ones((nx,ny)) #vol is, in fact, =1.0 (converts to matrix of ones)
    leftix   = mother.leftix
    leftiy   = mother.leftiy
    rightix  = mother.rightix
    rightiy  = mother.rightiy
    topix    = mother.topix
    topiy    = mother.topiy
    bottomix = mother.bottomix
    bottomiy = mother.bottomiy


    ix1 =-99
    iy1 =-99
    ix2 =-99
    iy2 =-99

    corner = np.zeros_like(centre)
    for iy in xrange(ny):
        for ix in xrange(nx):

            if (leftix[ix,iy] != -1 and bottomiy[ix,iy] != -1):
                corner[ix,iy]=((
                    centre[ix,iy]*
                     vol[bottomix[leftix[ix,iy],leftiy[ix,iy]],
                         bottomiy[leftix[ix,iy],leftiy[ix,iy]]]+
                    centre[leftix[ix,iy],leftiy[ix,iy]]*
                     vol[bottomix[ix,iy],bottomiy[ix,iy]]+
                    centre[bottomix[leftix[ix,iy],leftiy[ix,iy]],
                         bottomiy[leftix[ix,iy],leftiy[ix,iy]]]*
                     vol[ix,iy]+
                    centre[bottomix[ix,iy],bottomiy[ix,iy]]*
                     vol[leftix[ix,iy],leftiy[ix,iy]])/
                    (vol[bottomix[leftix[ix,iy],leftiy[ix,iy]],
                       bottomiy[leftix[ix,iy],leftiy[ix,iy]]]+
                     vol[bottomix[ix,iy],bottomiy[ix,iy]]+
                     vol[ix,iy]+
                     vol[leftix[ix,iy],leftiy[ix,iy]]))

            elif (leftix[ix,iy] == -1 and bottomiy[ix,iy] != -1):
                corner[ix,iy]=((
                    centre[ix,iy]*
                     vol[bottomix[ix,iy],bottomiy[ix,iy]]+
                    centre[bottomix[ix,iy],bottomiy[ix,iy]]*vol[ix,iy])/
                    (vol[bottomix[ix,iy],bottomiy[ix,iy]]+
                     vol[ix,iy]))

            elif (leftix[ix,iy] != -1 and bottomiy[ix,iy] == -1):
                corner[ix,iy]=((
                    centre[ix,iy]*vol[leftix[ix,iy],leftiy[ix,iy]]+
                    centre[leftix[ix,iy],leftiy[ix,iy]]*vol[ix,iy])/
                    (vol[ix,iy]+vol[leftix[ix,iy],leftiy[ix,iy]]))

            elif (leftix[ix,iy] == -1 and bottomiy[ix,iy] == -1):
                corner[ix,iy]=centre[ix,iy]


    # special treatment of standard X-points
    for iy in xrange(1,ny-1):
        for ix in xrange(1,nx-1):
            if(leftix[ix,iy] != -1  and  bottomiy[ix,iy] != -1):
                if(leftix[bottomix[ix,iy],bottomiy[ix,iy]] !=
                   bottomix[leftix[ix,iy],leftiy[ix,iy]]):
                    ix1=ix
                    ix2=rightix[topix[leftix[bottomix[ix,iy],bottomiy[ix,iy]],
                                      leftiy[bottomix[ix,iy],bottomiy[ix,iy]]],
                                topiy[leftix[bottomix[ix,iy],bottomiy[ix,iy]],
                                      leftiy[bottomix[ix,iy],bottomiy[ix,iy]]]]
    # Check that this is a standard X-point
            if (ix2 >=1  and  ix2 <= nx-1):
                corner[ix1,iy]=(centre[ix1,iy]*vol[ix2,iy]+
                     centre[bottomix[ix1,iy],bottomiy[ix1,iy]]*
                     vol[bottomix[ix2,iy],bottomiy[ix2,iy]]+
                     centre[leftix[bottomix[ix1,iy],bottomiy[ix1,iy]],
                            leftiy[bottomix[ix1,iy],bottomiy[ix1,iy]]]*
                     vol[leftix[bottomix[ix2,iy],bottomiy[ix2,iy]],
                         leftiy[bottomix[ix2,iy],bottomiy[ix2,iy]]]+
                     centre[leftix[ix1,iy],leftiy[ix1,iy]]*
                     vol[leftix[ix2,iy],leftiy[ix2,iy]]+
                     centre[ix2,iy]*vol[ix1,iy]+
                     centre[bottomix[ix2,iy],bottomiy[ix2,iy]]*
                     vol[bottomix[ix1,iy],bottomiy[ix2,iy]]+
                     centre[leftix[bottomix[ix2,iy],bottomiy[ix2,iy]],
                            leftiy[bottomix[ix2,iy],bottomiy[ix2,iy]]]*
                     vol[leftix[bottomix[ix1,iy],bottomiy[ix1,iy]],
                         leftiy[bottomix[ix1,iy],bottomiy[ix1,iy]]]+
                     centre[leftix[ix2,iy],leftiy[ix2,iy]]*
                     vol[leftix[ix1,iy],leftiy[ix1,iy]])

                corner[ix1,iy]=(corner[ix1,iy]/
                    (vol[ix2,iy]+vol[bottomix[ix2,iy],bottomiy[ix2,iy]]+
                     vol[leftix[bottomix[ix2,iy],bottomiy[ix2,iy]],
                         leftiy[bottomix[ix2,iy],bottomiy[ix2,iy]]]+
                     vol[leftix[ix2,iy],leftiy[ix2,iy]]+
                     vol[ix1,iy]+vol[bottomix[ix1,iy],bottomiy[ix1,iy]]+
                     vol[leftix[bottomix[ix1,iy],bottomiy[ix1,iy]],
                         leftiy[bottomix[ix1,iy],bottomiy[ix1,iy]]]+
                     vol[leftix[ix1,iy],leftiy[ix1,iy]]))

                corner[ix2,iy]=corner[ix1,iy]




    return corner

    #embed()















if __name__ == '__main__':
    from solpspy import *
    shot_path = '/toks/scratch/ipar/solps-iter/runs/aug_ddnu_34311_b2standalone/BCCON=16_BCENI=1_BCENE=1_shifted_profs06_leak=1e-2_kaveeva=1e-2/nesepm=1.00_tecore=500eV_ticore=450eV_coreflux=1.00e20_with_outputs/'
    shot = SolpsData(shot_path)
    tmp = intcor(shot, shot.po)
