import numpy as np
from IPython import embed

def read_csv(csv_file):
    with open(csv_file) as cf:
        ndims = len(cf.readline().split(','))-1
        mxdims = np.zeros(ndims)
        val = []
        for k,line in enumerate(cf):
            ln=line.split(',')
            if k==0:
                save_line = ln
            val.append(float(ln[-1]))
            for i in xrange(ndims):
                if ln[i]>mxdims[i]:
                    mxdims[i]=ln[i]

        mndims = []
        for mnind in save_line[:-1]:
            mndims.append(int(mnind))


        dims = mxdims.astype(np.int)
        for i in xrange(ndims):
            if mndims[i] == -1:
                dims[i]+=2
            elif mndims[i] ==0:
                dims[i]+=1

        return np.reshape(np.array(val), dims, order='F')



if __name__ == '__main__':
    read_csv('/afs/ipp-garching.mpg.de/home/i/ipar/repository/python/solpspy/b25_src/transport/testing_b2tfed/te_i.csv')
    #read_csv('/afs/ipp-garching.mpg.de/home/i/ipar/repository/python/solpspy/b25_src/transport/testing_b2tfed/te_test.csv')

    embed()
