def b2ar(mother):
    """
    Prepares the call to b2ardr, where the computation is carried out.

    The input units are:
    ninp(0): formatted; provides species parameters.
    ninp(1): formatted; provides input data for STRAHL calculation.
    ninp(2): formatted; provides input data from Weisheit code.
    ninp(3): un*formatted; provides default physics parameters.
    ninp(4): un*formatted; provides the geometry.

    The output units are:
    nout(0): formatted; provides printed summary of rate coefs.
    nout(1): un*formatted; provides the table of rate coefficients.
    """
    ninp = []
    ninp.append(mother.avail['b2ar.dat'])
    ninp.append(mother.avail['stra.dat'])
    ninp.append(mother.avail['weis.dat'])
    ninp.append(mother.avail['b2fpardf'])
    ninp.append(mother.avail['b2fgmtry'])

    nout = []
    nout.append(mother.avail['b2ar.prt'])
    nout.append(mother.avail['b2frates'])



