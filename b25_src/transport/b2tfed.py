import numpy as np
from solpspy.b25_src.utility.intcor import intcor
from solpspy.b25_src.tools_dtt import read_csv
from IPython import embed
import matplotlib.pyplot as plt


def b2tfed(mother):
    qe = mother.qe
    nx = mother.nx
    ny = mother.ny
    hx = mother.hx
    hy = mother.hy
    hy1 = mother.hy1
    vol = mother.vol
    leftix   = mother.leftix
    leftiy   = mother.leftiy
    rightix  = mother.rightix
    rightiy  = mother.rightiy
    topix    = mother.topix
    topiy    = mother.topiy
    bottomix = mother.bottomix
    bottomiy = mother.bottomiy

    po = mother.po
    bb = mother.bb
    te = mother.te*mother.qe # Inside equations, always in Joules.

    weight = 1.0 ## The heck is that?? Vol = np.ones??
    poleldr = 1.0
    poldidr = 1.0
    radeldr = 1.0
    raddidr = 1.0

    facdrift = np.ones((nx,ny))
    fac_ExB = np.ones((nx,ny))

    poc = intcor(mother, po)
    OnedBsqc = intcor(mother, 1/np.power(mother.bb[...,3],2)) ##CORRECT??

    veecrb = np.zeros((nx,ny,2))
    vedia = np.zeros((nx,ny,2))

    for iy in xrange(1,ny-1):
        for ix in xrange(1,nx):
            if (leftix[ix,iy] != -1):
                veecrb[ix,iy,0] = (- poleldr*fac_ExB[ix,iy]/
                    (hy1[ix,iy]+hy1[leftix[ix,iy],leftiy[ix,iy]])*
                    (bb[ix,iy,2]/bb[ix,iy,3]**2
                    +bb[leftix[ix,iy],leftiy[ix,iy],2]/
                     bb[leftix[ix,iy],leftiy[ix,iy],3]**2)*
                    (poc[topix[ix,iy],topiy[ix,iy]]-poc[ix,iy]))

                vedia[ix,iy,0] = (-poldidr*facdrift[ix,iy]*
                     (te[leftix[ix,iy],leftiy[ix,iy]]
                     *bb[leftix[ix,iy],leftiy[ix,iy],2]
                    /(qe*hy1[leftix[ix,iy],leftiy[ix,iy]])
                     +te[ix,iy]*bb[ix,iy,2]/[qe*hy1[ix,iy]])*0.5
                    *(OnedBsqc[topix[ix,iy],topiy[ix,iy]]-OnedBsqc[ix,iy]))


    for iy in xrange(1,ny):
        for ix in xrange(1,nx-1):
            if (leftix[ix,iy] !=-1 and rightix[ix,iy] != nx):
                veecrb[ix,iy,1] = (radeldr*fac_ExB[ix,iy]/
                    (hx[ix,iy]+hx[bottomix[ix,iy],bottomiy[ix,iy]])*
                    (bb[ix,iy,2]/bb[ix,iy,3]**2
                    +bb[bottomix[ix,iy],bottomiy[ix,iy],2]/
                     bb[bottomix[ix,iy],bottomiy[ix,iy],3]**2)*
                    (poc[rightix[ix,iy],rightiy[ix,iy]]-poc[ix,iy]))

                vedia[ix,iy,1] =  (raddidr*facdrift[ix,iy]*
                     (te[bottomix[ix,iy],bottomiy[ix,iy]]
                     *bb[bottomix[ix,iy],bottomiy[ix,iy],2]
                    /(qe*hx[bottomix[ix,iy],bottomiy[ix,iy]])
                     +te[ix,iy]*bb[ix,iy,2]/(qe*hx[ix,iy]))*0.5
                    *(OnedBsqc[rightix[ix,iy],rightiy[ix,iy]]-
                      OnedBsqc[ix,iy]))
    return veecrb, vedia




def calculate_wedia(mother):
    qe = mother.qe
    nx = mother.nx
    ny = mother.ny
    hx = mother.hx
    hy = mother.hy
    hy1 = mother.hy1
    vol = mother.vol
    leftix   = mother.leftix
    leftiy   = mother.leftiy
    rightix  = mother.rightix
    rightiy  = mother.rightiy
    topix    = mother.topix
    topiy    = mother.topiy
    bottomix = mother.bottomix
    bottomiy = mother.bottomiy

    po = mother.po
    bb = mother.bb
    te = mother.te
    ne = mother.ne

    weight = 1.0 ## The heck is that?? Vol = np.ones??
    poleldr = 1.0
    poldidr = 1.0
    radeldr = 1.0
    raddidr = 1.0

    facdrift = np.ones((nx,ny))
    fac_ExB = np.ones((nx,ny))

    poc = intcor(mother, po)
    nbtic = intcor(mother, ne*te*qe)
    nbzbc = intcor(mother, ne) ## From equation C.104 vs C.14

    wbdia = np.zeros((nx,ny,2))

    for iy in xrange(1, ny-1):
        for ix in xrange(1, nx):
            if leftix[ix,iy] != -1:
                wbdia[ix,iy,0] =   (facdrift[ix,iy]/
                   (hy1[ix,iy]+hy1[leftix[ix,iy],leftiy[ix,iy]])*
                   (bb[ix,iy,2]/bb[ix,iy,3]**2
                   +bb[leftix[ix,iy],leftiy[ix,iy],2]/
                    bb[leftix[ix,iy],leftiy[ix,iy],3]**2)*
                    (nbtic[topix[ix,iy],topiy[ix,iy]]-nbtic[ix,iy])/
                   (nbzbc[topix[ix,iy],topiy[ix,iy]]+nbzbc[ix,iy])*2.0/qe)


    ## For some fucking stupid reason, rightix[49,1] = 100
    ## it goes like 48, 49, 100, 51, 52. Me cago en todo hostia puta ya joder.
    for iy in xrange(1, ny):
        for ix in xrange(1, nx-1):
            if leftix[ix,iy] != -1 and rightix[ix,iy] != nx: # Otherwise, it fails
                wbdia[ix,iy,1] =   (-facdrift[ix,iy]/
                   (hx[ix,iy]+hx[bottomix[ix,iy],bottomiy[ix,iy]])*
                   (bb[ix,iy,2]/bb[ix,iy,3]**2
                   +bb[bottomix[ix,iy],bottomiy[ix,iy],2]/
                    bb[bottomix[ix,iy],bottomiy[ix,iy],3]**2)*
                    (nbtic[rightix[ix,iy],rightiy[ix,iy]]-nbtic[ix,iy])/
                   (nbzbc[rightix[ix,iy],rightiy[ix,iy]]+nbzbc[ix,iy])*2.0/qe)


    return wbdia













if __name__ == '__main__':
    from solpspy import *
    #shot_path = '/toks/scratch/ipar/solps-iter/runs/aug_ddnu_34311_b2standalone/BCCON=16_BCENI=1_BCENE=1_shifted_profs06_leak=1e-2_kaveeva=1e-2/nesepm=1.00_tecore=500eV_ticore=450eV_coreflux=1.00e20_with_outputs/'
    shot_path = '/toks/scratch/ipar/solps-iter/runs/aug_ddnu_34311_b2standalone/BCCON=16_BCENI=1_BCENE=1_shifted_profs13_leak=1e-2_kaveeva=1e-2/nesepm=0.80_tecore=500eV_ticore=450eV_coreflux=1.00e20_TO_TEST'

    shot = SolpsData(shot_path)
    veecrb, vedia = b2tfed(shot)
    wedia = calculate_wedia(shot)
    x = shot.ds[shot.omp]



    vaecrbx = shot.eqout.b2tfnb_vbecrbx[...,1]
    vaecrby = shot.eqout.b2tfnb_vbecrby[...,1]
    #vaecrbx2 = shot.eqout.b2tfnb_vaecrbnax[...,1]*(shot.hx/shot.vol/shot.ne) #Wrong mapping

    vadiax = shot.eqout.b2tfnb_vbdiax[...,1]
    wadiax = shot.eqout.b2tfnb_wbdiax[...,1]
    wadiay = shot.eqout.b2tfnb_wbdiay[...,1]


    po_ddt = read_csv('testing_b2tfed/po_i.csv')
    veecrb_ddt = read_csv('testing_b2tfed/veecrb_ii.csv')
    vedia_ddt = read_csv('testing_b2tfed/vedia_ii.csv')

    ##THEY ARE NOT EQUIVALENT APPARENTLY
    divvedia =shot.eqout.div_flux(vedia_ddt)
    svedia = np.sum(divvedia,2)
    divwedia =shot.eqout.div_flux(wedia)
    swedia = np.sum(divwedia,2)

    plt.figure()
    plt.plot(x,veecrb[shot.iout,:,0], color='r')
    plt.plot(x,vaecrbx[shot.iout], color='g')
    #plt.plot(vaecrbx2[shot.iout], color='b')
    plt.plot(x,veecrb_ddt[shot.iout,:,0], color='orange')

    plt.figure()
    plt.plot(x,veecrb[shot.omp,:,0],'r')
    plt.plot(x,vaecrbx[shot.omp],'g')
    #plt.plot(vaecrbx2[shot.omp],'b')
    plt.plot(x,veecrb_ddt[shot.omp,:,0], color='orange')
    #plt.show()



    plt.figure()
    plt.plot(x,veecrb[shot.iout,:,1], color='r')
    plt.plot(x,vaecrby[shot.iout], color='g')
    plt.plot(x,veecrb_ddt[shot.iout,:,1], color='orange')

    plt.figure()
    plt.plot(x,veecrb[shot.omp,:,1],'r')
    plt.plot(x,vaecrby[shot.omp],'g')
    plt.plot(x,veecrb_ddt[shot.omp,:,1], color='orange')
    #plt.show()




    plt.figure()
    plt.plot(x,vedia[shot.iout,:,0], color='r')
    plt.plot(x,-vadiax[shot.iout], color='g')
    plt.plot(x,vedia_ddt[shot.iout,:,0], color='orange')

    plt.figure()
    plt.plot(x,vedia[shot.omp,:,0],'r')
    plt.plot(x,-vadiax[shot.omp],'g')
    plt.plot(x,vedia_ddt[shot.omp,:,0], color='orange')





    plt.figure()
    plt.plot(x,-wedia[shot.iout,:,0], color='r')
    plt.plot(x,wadiax[shot.iout], color='g')

    plt.figure()
    plt.plot(x,-wedia[shot.omp,:,0],'r')
    plt.plot(x,wadiax[shot.omp],'g')


    plt.figure()
    plt.plot(x,-wedia[shot.iout,:,1], color='r')
    plt.plot(x,wadiay[shot.iout], color='g')

    plt.figure()
    plt.plot(x,-wedia[shot.omp,:,1],'r')
    plt.plot(x,wadiay[shot.omp],'g')




    plt.show()
    embed()
