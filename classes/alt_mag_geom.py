import numpy as np
from solpspy.classes.mds import MdsData
from solpspy.classes.rundir import RundirData

import mdstt
import rundirtt

from solpspy.tools.tools import try_block, solps_property, module_path

# =============================================================================
class MdsDataUSN(MdsData):
    """
    Mds+ class for Upper Single-Null configurations.
    """
    def __init__(self, *args, **kwargs):
        super(MdsDataUSN, self).__init__(*args,**kwargs)
        self._type = 'MdsDataUSN'
        self._magnetic_geometry = 'usn'

        self.target1 = mdstt.TargetData(self,'target1')
        self.target4 = mdstt.TargetData(self,'target4')

        self.out = self.target1
        self.inn = self.target4

        self.li = None
        self.ui = self.target4
        self.uo = self.target1
        self.lo = None

    #ATTENTION: Should it be guarding cell and thus iout-1?
    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = 1

    #ATTENTION: Should it be guarding cell?
    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = self.nx-2

class RundirDataUSN(RundirData):
    """
    Rundir class for Upper Single-null configurations.
    """
    def __init__(self, *args, **kwargs):
        super(RundirDataUSN, self).__init__(*args,**kwargs)
        self._type = 'RundirDataUSN'
        self._magnetic_geometry = 'usn'

        self.target1 = rundirtt.TargetData(self,'target1')
        self.target4 = rundirtt.TargetData(self,'target4')

        self.out = self.target1
        self.inn = self.target4

        self.li = None
        self.ui = self.target4
        self.uo = self.target1
        self.lo = None

    #ATTENTION: Should it be guarding cell and thus iout-1?
    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = 1

    #ATTENTION: Should it be guarding cell?
    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = self.nx-2

# =============================================================================



# =============================================================================
class MdsDataDDN(MdsData):
    """
    Mds+ class for Disconnected Double Null configurations.
    """


class RundirDataDDN(RundirData):
    """
    Rundir class for Disconnected Double Null configurations
    """


# =============================================================================





# =============================================================================
class MdsDataDDNU(MdsData):
    """
    Mds+ class for Disconnected Double Null Up configurations.
    """
    def __init__(self, *args, **kwargs):
        super(MdsDataDDNU, self).__init__(*args,**kwargs)
        ##ATTENTION: This should be before super, but gives problems!
        self._type = 'MdsDataDDNU'
        self._magnetic_geometry = 'ddnu'

        self.target1 = mdstt.TargetData(self,'target1')
        self.target2 = mdstt.TargetData(self,'target2')
        self.target3 = mdstt.TargetData(self,'target3')
        self.target4 = mdstt.TargetData(self,'target4')

        self.inn2 = self.target1
        self.inn  = self.target2
        self.out  = self.target3
        self.out2 = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target3



    #Add all the cuts
    @solps_property
    def nncut(self):
        self._nncut = np.array([2])

    @solps_property
    def leftcut(self):
        self._leftcut = np.zeros(2,dtype=int)
        self._leftcut[0] = np.where(self.region[...,1]==2)[0][0]-1
        self._leftcut[1] = np.where(self.region[...,1]==3)[0][0]-1

    @solps_property
    def rightcut(self):
        self._rightcut = np.zeros(2,dtype=int)
        self._rightcut[0] = np.where(self.region[...,1]==7)[0][0]-1
        self._rightcut[1] = np.where(self.region[...,1]==6)[0][0]-1

    @solps_property
    def topcut(self):
        """
        The logic is somewhat hacked and might only be correct for a certain
        relation between radial flux surfaces in different regions.
        """
        self._topcut = np.zeros(2,dtype=int)

        tmpl = np.where(self.region[...,1]==2)[1][0]
        tmpr = np.where(self.region[...,1]==7)[1][0]
        if tmpl == tmpl:
            self._topcut[0] = tmpl-1

        self._topcut[1] = np.where(self.region[...,2]==4)[1][0]-1

    @solps_property
    def bottomcut(self):
        self._bottomcut = np.array([-1, -1])


    @solps_property
    def sep(self):
        """int: Index of the first flux surface in the inner SOL"""
        self._sep = np.min(self.topcut) + 1

    @solps_property
    def sep2(self):
        """int: Index of the first flux surface in the outer SOL"""
        self._sep2 = np.max(self.topcut) + 1


    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = np.where(self.region[...,1] == 5)[0][0]

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 4)[0][0]-1


    @solps_property
    def iout2(self):
        """int: Index of the secondary outer target (not guarding cell)."""
        self._iout2 = np.where(self.region[...,1] == 8)[0][0]-1

    @solps_property
    def iinn2(self):
        """int: Index of the secondary inner target (not guarding cell)."""
        self._iinn2 = np.where(self.region[...,1] == 1)[0][0]

    @property
    def oxp(self):
        """ Outer index of main x-point, first pol. cell inside divertor."""
        return self.rightcut[1]

    @property
    def oxp2(self):
        """ Outer index of secondary x-point, first pol. cell inside divertor."""
        return self.rightcut[0]+1

    @property
    def ixp(self):
        """ Inner index of main x-point, first pol. cell inside divertor."""
        return self.leftcut[1]+1

    @property
    def ixp2(self):
        """ Inner index of secondary x-point, first pol. cell inside divertor."""
        return self.leftcut[0]


class RundirDataDDNU(RundirData):
    """
    Rundir class for Disconnected Double Null Up configurations.
    """
    def __init__(self, *args, **kwargs):
        super(RundirDataDDNU, self).__init__(*args,**kwargs)
        ##ATTENTION: This should be before super, but gives problems!
        self._type = 'RundirDataDDNU'
        self._magnetic_geometry = 'ddnu'

        self.target1 = rundirtt.TargetData(self,'target1')
        self.target2 = rundirtt.TargetData(self,'target2')
        self.target3 = rundirtt.TargetData(self,'target3')
        self.target4 = rundirtt.TargetData(self,'target4')

        self.inn2 = self.target1
        self.inn  = self.target2
        self.out  = self.target3
        self.out2 = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target3

        self._find_file('dstr')
        self._find_file('dstl')

    @solps_property
    def nncut(self):
        self._nncut = np.array([2])

    @solps_property
    def sep(self):
        """int: Index of the first flux surface in the inner SOL"""
        self._sep = np.min(self.topcut) + 1

    @solps_property
    def sep2(self):
        """int: Index of the first flux surface in the outer SOL"""
        self._sep2 = np.max(self.topcut) + 1

    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = np.where(self.region[...,1] == 5)[0][0]

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 4)[0][0]-1


    @solps_property
    def iout2(self):
        """int: Index of the secondary outer target (not guarding cell)."""
        self._iout2 = np.where(self.region[...,1] == 8)[0][0]-1

    @solps_property
    def iinn2(self):
        """int: Index of the secondary inner target (not guarding cell)."""
        self._iinn2 = np.where(self.region[...,1] == 1)[0][0]

    @property
    def oxp(self):
        """ Outer index of main x-point, first pol. cell inside divertor."""
        return self.rightcut[1]

    @property
    def oxp2(self):
        """ Outer index of secondary x-point, first pol. cell inside divertor."""
        return self.rightcut[0]+1

    @property
    def ixp(self):
        """ Inner index of main x-point, first pol. cell inside divertor."""
        return self.leftcut[1]+1

    @property
    def ixp2(self):
        """ Inner index of secondary x-point, first pol. cell inside divertor."""
        return self.leftcut[0]


    #In test phase yet:
    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = MasksDDNU(self)


    #ATTENTION: Improve docstrig
    #ATTENTION: Review logic
    def _extend(self, field):
        """
        Similar to b2plot/extend.F but only for grid dimensions.
        Should be similar to, but not implemented (why waste time right now?)
        For now, just use it for the standard case of connected spaces
        """
        # As for SN configurations
        ndim = list(field.shape)
        ndim[0], ndim[1] = ndim[0]+2, ndim[1]+2
        ndim = tuple(ndim)
        new_field = np.zeros(ndim)
        new_field[1:-1,1:-1] = field
        new_field[0] = new_field[1]
        new_field[-1] = new_field[-2]
        new_field[:,0] = new_field[:,1]
        new_field[:,-1] = new_field[:,-2]

        # Include 'internal' guarding cells too
        in1 = np.where(self.region[...,1] == 4)[0][0]
        in2 = np.where(self.region[...,1] == 5)[0][0]
        new_field[in1] = new_field[in1-1]
        new_field[in2-1] = new_field[in2]

        return new_field

# =============================================================================




#ATTENTION: Improve docstring in all of the class
#ATTENTION:
#Found an error about how masks are implemented. If outdiv_nolim is used
#before masks.outdiv, the result is different because the guarding cell
#is either accepted or not. That is wrong and has to be fixed.
class MasksDDNU(object):
    """Class containing different grid masks."""

    def __init__(self,mother):
        self._mother = mother
        self.log = self._mother.log
        self.irlcl = True

    ## ATTENTION!
    @solps_property
    def gdcl(self):
        """(nx,ny): Guard cells."""
        self._gdcl = np.zeros((self._mother.nx,self._mother.ny))
        ## WRONG???
        #self._gdcl[0,:] = 1
        #self._gdcl[:,0] = 1
        #self._gdcl[self._mother.nx-1,:] = 1
        #self._gdcl[:,self._mother.ny-1] = 1

        self._gdcl[0] = 1
        self._gdcl[-1] = 1
        self._gdcl[:,0] = 1
        self._gdcl[:,-1] = 1
        self._gdcl[self._mother.iout-1,:] = 1
        self._gdcl[self._mother.iinn+1,:] = 1

    @property
    def guard_cells(self):
        """Alias for self.gdcl"""
        return self.gdcl

    @solps_property
    def rlcl(self):
        """(nx,ny): Real cells."""
        #self._rlcl = np.ones((self._mother.nx,self._mother.ny)) - self.gdcl
        self._rlcl = np.logical_not(self.gdcl)

    @property
    def real_cells(self):
        """Alias for self.rlcl"""
        return self.rlcl

    @solps_property
    def lfs(self):
        """ Low field side region of grid.
        For DDN is quite simple, as it is naturally partitioned.
        """
        self._lfs = self.outdiv + self.outdiv2 + self.outcore + self.outmainsol + self.outmainsol2


    def _regions(self,regions, mregions = None, rlcl=True):
        """
        regions: dict
        """
        str_to_reg = {'v':0, 'x':1, 'y':2}

        # 'AND' regions
        mask = np.ones((self._mother.nx, self._mother.ny))
        for d, inds in regions.iteritems():
            if isinstance(inds,int) or isinstance(inds,float):
                inds = [int(inds)]
            if isinstance(d,str):
                d = str_to_reg[d]
            for ind in inds:
                tmp = self._mother.region[...,d] == ind
                mask = np.logical_and(mask, tmp)

        if rlcl:
            return np.logical_and(mask,self.rlcl)
        else:
            return mask


    ## Region definitions
    @solps_property
    def outdiv(self):
        """ (nx,ny): Main outer divertor region."""
        self._outdiv = self._regions({'v':7}, rlcl=self.irlcl)

    @solps_property
    def inndiv(self):
        """ (nx,ny): Main inner divertor region."""
        self._inndiv = self._regions({'v':4}, rlcl=self.irlcl)

    @solps_property
    def outdiv2(self):
        """ (nx,ny): Main outer divertor region."""
        self._outdiv2 = self._regions({'v':8}, rlcl=self.irlcl)

    @solps_property
    def inndiv2(self):
        """ (nx,ny): Main inner divertor region."""
        self._inndiv2 = self._regions({'v':3}, rlcl=self.irlcl)

    @solps_property
    def outsol(self):
        """ (nx,ny): Outer main chamber SOL region."""
        self._outsol = self._regions({'v':6}, rlcl=self.irlcl)

    @solps_property
    def innsol(self):
        """ (nx,ny): Inner main chamber SOL region."""
        self._innsol = self._regions({'v':2}, rlcl=self.irlcl)


    @solps_property
    def outcore(self):
        """ (nx,ny): Outer core region."""
        self._outcore = self._regions({'v':5}, rlcl=self.irlcl)

    @solps_property
    def inncore(self):
        """ (nx,ny): Inner core region."""
        self._inncore = self._regions({'v':1}, rlcl=self.irlcl)











    @solps_property
    def core(self):
        self._core = np.logical_and(self.outcore, self.inncore)

    @solps_property
    def notcore(self):
        self._notcore = self.rlcl*(1-self.core)


    @solps_property
    def pfr(self):
        self._pfr = (1-self.sol) * self.notcore * self.rlcl



    @solps_property
    def div(self):
        """ Main Inner + Outer divertors.
        Guard cells are excluded.
        """
        self._div = self.inndiv + self.outdiv
    @property
    def divertor(self):
        """Alias of self.div"""
        return self.div

    @solps_property
    def notdiv(self):
        """ Everything except inner+outer divertors.
        Guard cells are excluded.
        """
        self._notdiv = self.rlcl*(np.ones((self._mother.nx,self._mother.ny))-self.div)


    @solps_property
    def div2(self):
        """ Secondary Inner + Outer divertors.
        Guard cells are excluded.
        """
        self._div2 = self.inndiv2 + self.outdiv2
    @property
    def divertor2(self):
        """Alias of self.div"""
        return self.div2

    @solps_property
    def notdiv2(self):
        """ Everything except inner+outer divertors.
        Guard cells are excluded.
        """
        self._notdiv2 = self.rlcl*(np.ones((self._mother.nx,self._mother.ny))-self.div2)


    # Not valid without region definitions but this is ok anyway.
    @solps_property
    def innertarget(self):
        self._innertarget = np.zeros((self._mother.nx,self._mother.ny))
        self._innertarget[self._mother.iinn,:] = 1 # nx=1 (rlcl)

    @solps_property
    def outertarget(self):
        self._outertarget = np.zeros((self._mother.nx,self._mother.ny))
        self._outertarget[self._mother.iout,:] = 1

    @solps_property
    def innertarget2(self):
        self._innertarget2 = np.zeros((self._mother.nx,self._mother.ny))
        self._innertarget2[self._mother.iinn2,:] = 1 # nx=1 (rlcl)

    @solps_property
    def outertarget2(self):
        self._outertarget2 = np.zeros((self._mother.nx,self._mother.ny))
        self._outertarget2[self._mother.iout2,:] = 1







    @property
    @try_block
    def xpoint(self):
        """Grid mask for the grid cells surrounding the x-point."""
        self._xpoint = np.zeros((self._mother.nx,self._mother.ny))
        for nx in range(self._mother.nx):
            for ny in range(self._mother.ny):
                cell = self._mother.grid[nx,ny] # [nvertex,R|z]
                for v in range(len(cell)):
                    if (cell[v][0] == self._mother.xpoint[0] and
                        cell[v][1] == self._mother.xpoint[1]):
                        self._xpoint[nx,ny] = 1





    @property
    @try_block
    def sol_without_div(self):
        self._sol_without_div = ( self.sol*(1-self.inndiv)*(1-self.outdiv)
                                  * self.rlcl )

    @property
    @try_block
    def sol_lfs(self):
        # Find highest vertex to separate hfs and lfs:
        zmax = -np.inf
        xmax = 0
        for nx in range(self._mother.nx):
            vmax = max(self._mother.grid[nx,self._mother.ny-1,:,1])
            if vmax > zmax:
                xmax = nx
                zmax = vmax
        self._sol_lfs = self.sol_without_div.copy()*self.rlcl
        self._sol_lfs[0:xmax+1,:] = 0

    @property
    @try_block
    def sol_hfs(self):
        self._sol_hfs = self.sol_without_div - self.sol_lfs



    def __call__(self, **kwargs):
        return generate(**kwargs)


    def generate(self, **kwargs):
        """ Generates mask with given description.

        Parameters
        ----------
        region : str or list(str)
            Name or list of names of predefined grid regions.
            Can be 'outer divertor', 'inner divertor', 'sol' or 'not core'
            or a list with any of them.
            Default to None
        ix : int or list(int)
            If region is None, then ix represent the selected poloidal indexes
            of the (unbound) total radial range.
            If both ix and iy have valid values, then the mask is the
            interection of both.
            Default to []
        iy : int or list(int)
            If region is None, then iy represent the selected radial indexes
            of the (unbound) total poloidal range.
            If both ix and iy have valid values, then the mask is the
            interection of both.
            Default to []
        extra_dims : int or iterable(int)
            Expands mask from (nx,ny) to (nx,ny,extra_dims).
            Default to None.

        Returns
        -------
        mask: numpy.array with dtype=numpy.bool
            Array with True in the cells that dwell inside the desired
            subdivision of the grid.
            It has dimensions (nx,ny,extra_dims)
        """

        region = priority('region', kwargs, None)
        ix = priority(['ix','xrange'], kwargs, [])
        iy = priority(['iy','yrange'], kwargs, [])
        extra_dims = priority(['extra_dims', 'extra_dim', 'dims', 'dim'],
            kwargs, None)

        nx = self._mother.nx
        ny = self._mother.ny
        ns = self._mother.ns
        sep = self._mother.sep
        #oul = self._mother.oul
        #odl = self._mother.odl
        #iul = self._mother.iul
        #idl = self._mother.idl
        oxp = self._mother.oxp
        ixp = self._mother.ixp
        oxp2 = self._mother.oxp2
        ixp2 = self._mother.ixp2

        if region:

            mask = np.zeros((nx,ny), dtype=np.bool)
            if type(region) is str:
                region=[region.lower()]
            elif type(region) is list:
                region = [reg.lower() for reg in region]

            for reg in region:
                if ('outer divertor' in reg or 'outdiv' in reg or
                    'out div' in reg or 'lfs div' in reg):
                    mask[oul:odl] = True

                elif ('inner divertor' in reg or 'indiv' in  reg or
                      'inndiv' in reg or 'hfs div' in reg):
                    mask[idl:iul+1] = True

                elif 'sol' in reg:
                    mask[:,sep:ny] = True

                elif 'not core' in reg or 'notcore' in reg:
                    mask[idl:iul+1] = True
                    mask[oul:odl] = True
                    mask[:,sep:ny] = True

        elif ix is not []  or iy is not []:
            if type(ix) is not list: ix = [ix]
            if type(iy) is not list: iy = [iy]
            mask1 = np.zeros((nx,ny), dtype=np.bool)
            mask2 = np.zeros((nx,ny), dtype=np.bool)
            for i in xrange(nx):
                for k in xrange(ny):
                    if (i in ix):
                        mask1[i,k] = True
                    if (k in iy):
                        mask2[i,k] = True
            if ix and iy:
                mask = np.logical_and(mask1, mask2)
            elif ix:
                mask = mask1
            elif iy:
                mask = mask2

        if extra_dims:
            if '__iter__' not in extra_dims.__class__.__dict__:
                extra_dims = [extra_dims]

            for dm in extra_dims:
                mask = np.repeat(mask.T[np.newaxis], dm, axis=0).T

        return mask
