#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    Class containing the time traces stored e.g. in b2time.nc and others.
"""

# Common modules
import os
import logging
import numpy as np
import functools

from scipy import exp
from scipy import  special, optimize, integrate
from scipy.special import erf, erfc
#from scipy.optimize import fmin
#from scipy.integrate import trapz

# Special modules
from solpspy.tools.tools import solps_property, priority, read_config
from solpspy.extensions.heatflux import HeatFlux


# =============================================================================
class BaseTimeTraces(object):
    """Base class for quantites with time traces for MDS+ SolpsData.

    There are two types of time traces in SOLPS: 1D and 2D.
    The 1D store a single quantity on a single position on every time step.
    The 2D store profiles of quantities on every time step.

    The nomenclature of 1D time traces is implemented as follows:
    Prefixes:
    fe- : Energy flux
    fc- : Current

    Middle:
    -t- : total
    -e- : electronic
    -i- : ionic

    Sufixes:
    -x : poloidal
    -y : radial

    Terminations:
    _sep : At the separatrix
    _int : Integral
    _max : Maximum

    For example: the integrated total energy flux on the outerboard divertor
    (fetxap) receives te name fetx_int (fe-t-x-_int)
    """



    def __init__(self, mother, wh):
        """
        Parameters
        ----------
        mother : object
            SolpsData instance
        wh : str
            Special location out of which the data are being retrieved.
            Must be one of {'target1','target2','target3','target4','imp','omp'}
        """

        self._which = wh.lower()
        self.log = mother.log
        self.mag = mother.magnetic_geometry

        #mds
        self.conn = mother.conn
        self.conn_open = mother.conn_open
        self.shot = mother.shot


    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------
    @solps_property
    def time(self):
        """(time) : distance from the separatrix [s]"""
        self._read_signal('_time', 'time')


    @solps_property
    def ds(self):
        """(ny) : distance from the separatrix [m]"""
        self._read_signal('_ds', 'ds{}')

    @solps_property
    def an(self):
        """(time,ny) : atomic density [m^-3]"""
        self._read_signal('_an', 'an3d{}')

    @solps_property
    def mn(self):
        """(time,ny) : molecular density [m^-3]"""
        self._read_signal('_mn', 'mn3d{}')

    @solps_property
    def po(self):
        """(time,ny) : potential [V]"""
        self._read_signal('_po', 'po3d{}')

    @solps_property
    def te(self):
        """(time,ny) : electron temperature [eV]"""
        self._read_signal('_te', 'te3d{}')

    @solps_property
    def ti(self):
        """(time,ny) : ion temperature [eV]"""
        self._read_signal('_ti', 'ti3d{}')

    @solps_property
    def ne(self):
        """(time,ny) : electron density [m^-3]"""
        self._read_signal('_ne', 'ne3d{}')


    # -------------------- Singleton quantities -------------------------------
    @solps_property
    def ne_sep(self):
        """(time) : separatrix electron density [m^-3]"""
        self._read_signal('_ne_sep', 'nesep{}')

    #@solps_property
    #def ne_sep(self):
    #    """(time) : separatrix electron density [m^-3]"""
    #    if self._which == 'omp':
    #        self._read_signal('_ne_sep', 'nesep{}')
    #        if self.mag == 'ddn' or self.mag == 'ddnu':
    #            self._ne_sep = self._ne_sep[:,0]

    #    elif self._which =='imp':
    #        if self.mag == 'sn' or self.mag == 'usn':
    #            self._ne_sep = (self.ne[:, self.mother.sep-1] +
    #                            self.ne[:, self.mother.sep])/2.0
    #        elif self.mag == 'ddn' or self.mag == 'ddnu':
    #            self._read_signal('_ne_sep', 'nesep{}')
    #            self._ne_sep = self._ne_sep[:,1]

    @solps_property
    def po_sep(self):
        """(time) : separatrix potential [V]"""
        self._read_signal('_po_sep', 'posep{}')

    @solps_property
    def te_sep(self):
        """(time) : separatrix electron temperature [eV]"""
        self._read_signal('_te_sep', 'tesep{}')

    @solps_property
    def ti_sep(self):
        """(time) : separatrix electron temperature [eV]"""
        self._read_signal('_ti_sep', 'tisep{}')





    #    =============== Methods ==========================
    def _read_signal(self,var_name, signal_name, attach_to=None):
        """ Retrieve data from the MDS+, given tree link and signal name.

        read_signal does not return any value. It automatically attach
        the signal as an attribute with a given name to the given object
        or self.

        Parameters
        ----------
        var_name : str
            Storage name of the variable for the retrieve signal.
        signal_name : str
            Name to retrieve signal.
            Only the stem of signal_name is given.
        attach_to : object, optional
            signal_name will be attached as attribute to object
            attach_to. Default to self.

        Returns
        -------
        None
        """
        if not self.conn_open:
            self.conn.openTree('solps', self.shot)
            self.conn_open=True

        if not attach_to:
            attach_to = self

        #Not symmetrical wrt rundirtt, as parser gives array there
        signal_link = self._parser(signal_name)
        if signal_link:
            try:
                #Introduce here DN logic for singleton variables, by deciding
                #which of the tuple in tmp is given back
                tmp = self.conn.get(signal_link).value.T
                setattr(attach_to, var_name, tmp)
            except:
                self.log.warning("error reading '{1}' for '{0}'".format(signal_name,
                                                                     signal_link))
                setattr(attach_to, var_name, None)
        else:
            setattr(attach_to, var_name, None)

        if self.conn_open:
            self.conn.closeTree('solps', self.shot)
            self.conn_open = False


    def _parser(self, signal_name):
        """
        signal_name only contains the stem. The complete signal name must be
        evaluated here with the correct mixture of (signal_name + self._psym)

        In the case of MDS+ signals, the location and name are formatted
        directly into the MDS roots.

        completing the formating will be only use for the parser of Rundir.
        """

        _whl = self._which.upper()

        #Needed because singleton quantities use TARG instead
        if _whl[:-1] == 'TARGET':
            _wh = 'TARG'+_whl[-1]
        else:
            _wh = _whl

        #No need to complete name
        signal_name = signal_name.format("")

        #For profiles
        tprof = '\TIMEDEP::TOP.{0}:{1}'

        #For singletons
        tint = '\TIMEDEP::TOP.{1}:{0}INT'
        tsep = '\TIMEDEP::TOP.{1}:{0}SEP'
        tmax = '\TIMEDEP::TOP.{1}:{0}MAX'


        mdstree_common ={
                #Profile quantities
                'an3d':tprof.format(_whl, 'AN'),
                'ds':tprof.format(_whl, 'DS'),
                'mn3d':tprof.format(_whl, 'MN'),
                'ne3d':tprof.format(_whl, 'NE'),
                'po3d':tprof.format(_whl, 'PO'),
                'te3d':tprof.format(_whl, 'TE'),
                'ti3d':tprof.format(_whl, 'TI'),

                #Singleton quantities
                'tesep':tsep.format(_wh,'TE'),
                'tisep':tsep.format(_wh,'TI'),
                'nesep':tsep.format(_wh,'NE'),
                'posep':tsep.format(_wh,'PO'),
                }


        mdstree_midplanes ={
                #Profile quantities
                'dn3d':tprof.format(_whl, 'DN'),
                'dp3d':tprof.format(_whl, 'DP'),
                'ke3d':tprof.format(_whl, 'KE'),
                'ki3d':tprof.format(_whl, 'KI'),
                'vs3d':tprof.format(_whl, 'VS'),
                'vx3d':tprof.format(_whl, 'VX'),
                'vy3d':tprof.format(_whl, 'VY'),

                #Singleton quantities
                'dnsep':tsep.format(_wh,'DN'),
                'dpsep':tsep.format(_wh,'DP'),
                'kesep':tsep.format(_wh,'KE'),
                'kisep':tsep.format(_wh,'KI'),
                'vssep':tsep.format(_wh,'VS'),
                'vxsep':tsep.format(_wh,'VX'),
                'vysep':tsep.format(_wh,'VY'),
                 }


        mdstree_targets ={
                #Profile quantities
                'da':tprof.format(_whl, 'DA'),
                'fc3d':tprof.format(_whl, 'FC'),
                'fe3d':tprof.format(_whl, 'FE'),
                'fi3d':tprof.format(_whl, 'FI'),
                'fl3d':tprof.format(_whl, 'FL'),
                'fn3d':tprof.format(_whl, 'FN'),
                'fo3d':tprof.format(_whl, 'FO'),
                'ft3d':tprof.format(_whl, 'FT'),

                #Singleton quantities
                'temxp':tmax.format(_wh,'TE'),
                'timxp':tmax.format(_wh,'TI'),
                'nemxp':tmax.format(_wh,'NE'),
                'pomxp':tmax.format(_wh,'PO'),

                'fchxp':tint.format(_wh,'FCH'),
                'feexp':tint.format(_wh,'FEE'),
                'feixp':tint.format(_wh,'FEI'),
                'fetxp':tint.format(_wh,'FET'),
                'fnixp':tint.format(_wh,'FNI'),
                }


        if _whl[:-1] == 'TARGET':
            try:
                mdstree_common.update(mdstree_targets)
                return mdstree_common[signal_name]
            except:
                self.log.warning(
                    "'{0}' not found/implemented for '{1}' MDSTREE".format(
                                                    signal_name, self._which))
                return None


        elif _whl[-2:] == 'MP':
            try:
                mdstree_common.update(mdstree_midplanes)
                return mdstree_common[signal_name]
            except:
                self.log.warning(
                    "'{0}' not found/implemented for '{1}' MDSTREE".format(
                                                    signal_name, self._which))
                return None



    def _symbols(self, var_type):
        """ Returns character of the solps nomenclature that identify  the
        corresponding position.

        It uses self._which to determine

        Parameters
        ----------
        var_type: str
           Type of variable. It can be one of {'profile', 'singleton'}

        Returns
        -------
        str of single character
        """
        #Move to function symbols
        _profiles_symbols = {'target1':'l',
                             'imp':'i',
                             'omp':'a',
                             'target4':'r',
                             'target2':'tl',
                             'target3':'tr'}
        _singletons_symbols_LSN = {'omp':'m',
                                   'imp':'', #Not defined, apparently
                                   'target1':'i',
                                   'target2':'i',
                                   'target3':'a',
                                   'target4':'a'}

        ##In case of LSN
        #_targ_to_loc_LSN = {'target1':'lhfs',
        #                    'target4':'llfs'}

        #_targ_to_loc_USN = {'target1':'llfs',
        #                    'target4':'lhfs'}

        #_loc_to_targ = {v: k for k, v in _num_to_loc.iteritems()}

        if var_type == 'profile':
            return _profiles_symbols[self._which]
        elif var_type == 'singleton':
            ##ATTENTION!!!!!!
            if self.magnetic_geometry.lower == 'lsn':
                return _singletons_symbols_LSN[self._which]
            else:
                self.log.warning("Other magnetic geometries not yet implemented")
                return None
        else:
            self.log.error("Type '{0}' is not valid".format(var_type))
            return None









# =============================================================================
class TargetData(BaseTimeTraces):
    """ Mds Target Data Time Traces.
    For DDN cases, information is given in pairs.
    """
    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------
    @solps_property
    def da(self):
        self._read_signal('_da','da')

    @solps_property
    def fc(self):
        """ (time,ny): poloidal current density [A/m^2].
        In netcdf A, but in MDS+ saved with 1/da
        """
        self._read_signal('_fc', 'fc3d{}')

#    @property
#    def fcd(self):
#        """ (time,ny): poloidal current density [A/m^2]"""
#        return self.fc/self.da

    @solps_property
    def fe(self):
        """(time,ny) : poloidal electron energy flux density [W/m^2]
        In netcdf W, but in MDS+ saved with 1/da
        """
        self._read_signal('_fe', 'fe3d{}')

#    @property
#    def fed(self):
#        """(time,ny) : poloidal electron energy flux density [W/m^2]"""
#        return self.fe/self.da

    @solps_property
    def fi(self):
        """(time,ny) : poloidal ion energy flux  density [W/m^2]
        In netcdf W, but in MDS+ saved with 1/da
        """
        self._read_signal('_fi', 'fi3d{}')

#    @property
#    def fid(self):
#        """(time,ny) : poloidal ion energy flux  density [W/m^2]"""
#        return self.fi/self.da

    @solps_property
    def ft(self):
        """(time,ny) : poloidal total energy flux [W/m^2].
        In netcdf W, but in MDS+ saved with 1/da
        """
        self._read_signal('_ft', 'ft3d{}')

#    @property
#    def ftd(self):
#        """(time,ny) : poloidal total energy flux density [W/m^2]"""
#        return self.ft/self.da

    @solps_property
    def fl(self):
        """(time,ny) : poloidal electron flux [m^-2 s^-1]
        In netcdf s^-1, but in MDS+ saved with 1/da
        """
        self._read_signal('_fl', 'fl3d{}')

    @solps_property
    def fn(self):
        """(time,ny) : poloidal spec one flux density[m^-2 s^-1]
        In netcdf s^-1, but in MDS+ saved with 1/da
        """
        self._read_signal('_fn', 'fn3d{}')

#    @property
#    def fnd(self):
#        """(time,ny) : poloidal spec one flux density[m^-2 s^-1]"""
#        return self.fn/self.da

    @solps_property
    def fo(self):
        """(time,ny) : poloidal ion flux [m^-2 s^-1]
        In netcdf s^-1, but in MDS+ saved with 1/da
        """
        self._read_signal('_fo', 'fo3d{}')


    # -------------------- Singleton quantities -------------------------------
    @solps_property
    def fch_int(self):
        """(time) : integrated poloidal current [A]"""
        self._read_signal('_fch_int', 'fchx{}p')

    @solps_property
    def fee_int(self):
        """(time) : integrated poloidal electron energy flux [W]"""
        self._read_signal('_fee_int', 'feex{}p')

    @solps_property
    def fei_int(self):
        """(time) : integrated poloidal ion energy flux [W]"""
        self._read_signal('_fei_int', 'feix{}p')

    @solps_property
    def fet_int(self):
        """(time) : integrated poloidal total energy flux [W]"""
        self._read_signal('_fet_int', 'fetx{}p')

    @solps_property
    def fna_int(self):
        """(time) : integrated poloidal particle flux [1/s]"""
        self._read_signal('_fna_int', 'fnix{}p')


    @solps_property
    def ne_max(self):
        """(time) : maximum electron density [m^-3]"""
        self._read_signal('_ne_max', 'nemx{}p')

    @solps_property
    def po_max(self):
        """(time) : maximum potential [V]"""
        self._read_signal('_po_max', 'pomx{}p')

    @solps_property
    def te_max(self):
        """(time) : maximum electron temperature [eV]"""
        self._read_signal('_te_max', 'temx{}p')

    @solps_property
    def ti_max(self):
        """(time) : maximum electron temperature [eV]"""
        self._read_signal('_ti_max', 'timx{}p')


    # -------------------- Extensions -------------------------------
    #Heatflux
    def heatflux(self,x):
        """ x must be in mm"""
        try:
            return self._heatflux(x)
        except:
            self.log.warning("Heat flux is not yet to be trusted")
            hfc = read_config()['HeatFlux']
            yi = np.average(self.ft,0)*hfc['yscale']
            xi = self.ds*hfc['xscale']
            parameters = hfc['initial values']
            if parameters['a'] == 'maxyi':
                parameters['a'] = np.max(yi)
            self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
            self._heatflux_params = self._heatflux.p
            self._lambdaq = self._heatflux.lambdaq
            self._s = self._heatflux.s
            self._bkg = self._heatflux.bkg
            return self._heatflux(x)

    @solps_property
    def lambdaq(self):
        self.log.warning("Heat flux is not yet to be trusted")
        hfc = read_config()['HeatFlux']
        yi = np.average(self.ft,0)*hfc['yscale']
        xi = self.ds*hfc['xscale']
        parameters = hfc['initial values']
        if parameters['a'] == 'maxyi':
            parameters['a'] = np.max(yi)
        self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
        self._heatflux_params = self._heatflux.p
        self._lambdaq = self._heatflux.lambdaq
        self._s = self._heatflux.s
        self._bkg = self._heatflux.bkg

    @property
    def pdl(self):
        return self.lambdaq

    @property
    def power_decay_lenght(self):
        return self.lambdaq

    @solps_property
    def s(self):
        self.log.warning("Heat flux is not yet to be trusted")
        hfc = read_config()['HeatFlux']
        yi = np.average(self.ft,0)*hfc['yscale']
        xi = self.ds*hfc['xscale']
        parameters = hfc['initial values']
        if parameters['a'] == 'maxyi':
            parameters['a'] = np.max(yi)
        self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
        self._heatflux_params = self._heatflux.p
        self._lambdaq = self._heatflux.lambdaq
        self._s = self._heatflux.s
        self._bkg = self._heatflux.bkg

    #ATTENTION: Bad naming ( = parallel force due to pstot in mockup_balance)
    @property
    def psf(self):
        return self.s

    @property
    def power_spread_factor(self):
        return self.s

    @solps_property
    def bkg(self):
        self.log.warning("Heat flux is not yet to be trusted")
        hfc = read_config()['HeatFlux']
        yi = np.average(self.ft,0)*hfc['yscale']
        xi = self.ds*hfc['xscale']
        parameters = hfc['initial values']
        if parameters['a'] == 'maxyi':
            parameters['a'] = np.max(yi)
        self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
        self._heatflux_params = self._heatflux.p
        self._lambdaq = self._heatflux.lambdaq
        self._s = self._heatflux.s
        self._bkg = self._heatflux.bkg

    @solps_property
    def heatflux_params(self):
        self.log.warning("Heat flux is not yet to be trusted")
        hfc = read_config()['HeatFlux']
        yi = np.average(self.ft,0)*hfc['yscale']
        xi = self.ds*hfc['xscale']
        parameters = hfc['initial values']
        if parameters['a'] == 'maxyi':
            parameters['a'] = np.max(yi)
        self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
        self._heatflux_params = self._heatflux.p
        self._lambdaq = self._heatflux.lambdaq
        self._s = self._heatflux.s
        self._bkg = self._heatflux.bkg



    def plot_heatflux(self, **kwargs):
        """ For now, only  out in SN"""
        import matplotlib.pyplot as plt
        which = priority(['which', 'side'], kwargs, 'out')
        canvas = priority(['canvas','fig','figure','ax'], kwargs, None)
        units = priority(['units','u'], kwargs, 'mm')
        color = priority(['color', 'c'], kwargs, 'r')
        try:
            eich = self.heatflux
        except:
            pass
        if canvas is None:
            fig, canvas = plt.subplots(1)
            canvas.set_xlabel('dS ['+units+']')
            canvas.set_ylabel('q$_\perp$ [MW m$^{-2}$]')
            ltext = r'$\lambda_q$ = '+'{0:.2f}  mm\n\n'.format(self.lambdaq)
            stext = r'S = '+'{0:.2f}  mm'.format(self.psf)
            canvas.text(0.70, 0.80, ltext+stext,
                fontsize=15,
                horizontalalignment='left',
                verticalalignment='center',
                bbox={'facecolor':'white', 'pad':10},
                transform= canvas.transAxes)
        xx = np.linspace(self.ds[0], self.ds[-1], 1000)*1e3
        canvas.plot(self.ds*1e3, np.average(self.ft,0)*1e-6, 'o', color = color)
        canvas.plot(xx, eich(xx), color = color)
        #ax.legend(loc = 'upper right')
        plt.draw()
        return



# =============================================================================
class MidplaneData(BaseTimeTraces):
    """ Mds Target Data Time Traces.
    For DDN cases, information is given in pairs.
    """
    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------
    @solps_property
    def dn(self):
        """(time,ny) : diffusion coefficient [m^2/s]"""
        self._read_signal('_dn', 'dn3d{}')

    @solps_property
    def dp(self):
        """(time,ny) : pressure diffusion coefficient [m^2/s]"""
        self._read_signal('_dp', 'dp3d{}')

    @solps_property
    def ke(self):
        """(time,ny) : electron thermal diffusivty [m^2/s]"""
        self._read_signal('_ke', 'ke3d{}')

    @solps_property
    def ki(self):
        """(time,ny) : electron thermal diffusivty [m^2/s]"""
        self._read_signal('_ki', 'ki3d{}')

    @solps_property
    def vs(self):
        """(time,ny) : viscosity coefficient [m kg^-1 s^-1]"""
        self._read_signal('_vs', 'vs3d{}')

    @solps_property
    def vx(self):
        """(time,ny) : poloidal pinch velocity [m/s]"""
        self._read_signal('_vx', 'vx3d{}')

    @solps_property
    def vy(self):
        """(time,ny) : radial pinch velocity [m/s]"""
        self._read_signal('_vy', 'vy3d{}')

    # -------------------- Singleton quantities -------------------------------
    @solps_property
    def dn_sep(self):
        """(time) : separatrix diffusion coefficient [m^2/s]"""
        self._read_signal('_dn_sep', 'dnsep{}')

    @solps_property
    def dp_sep(self):
        """(time) : separatrix pressure diffusion coefficient [m^2/s]"""
        self._read_signal('_dp_sep', 'dpsep{}')

    @solps_property
    def ke_sep(self):
        """(time) : separatrix electron thermal diffusivty [m^2/s]"""
        self._read_signal('_ke_sep', 'kesep{}')

    @solps_property
    def ki_sep(self):
        """(time) : separatrix ion thermal diffusivty [m^2/s]"""
        self._read_signal('_ki_sep', 'kisep{}')

    @solps_property
    def vs_sep(self):
        """(time) : separatrix viscosity coefficient [m kg^-1 s^-1]"""
        self._read_signal('_vs_sep', 'vssep{}')

    @solps_property
    def vx_sep(self):
        """(time) : separatrix poloidal pinch velocity [m/s]"""
        self._read_signal('_vx_sep', 'vxsep{}')

    @solps_property
    def vy_sep(self):
        """(time) : separatrix radial pinch velocity [m/s]"""
        self._read_signal('_vy_sep', 'vysep{}')



















#### =================== GLOBAL TIME TRACES ===================================



class TimeTraces(object):
    """Base class for global quantites with time traces for MDS+ SolpsData.
    """
    def __init__(self, mother, wh):
        """
        Parameters
        ----------
        mother : object
            SolpsData instance
        """

        self.log = mother.log
        self.mag = mother.magnetic_geometry

        #mds
        self.conn = mother.conn
        self.conn_open = mother.conn_open
        self.shot = mother.shot




    def _read_signal(self,var_name, signal_name, attach_to=None):
        """ Retrieve data from the MDS+, given tree link and signal name.

        read_signal does not return any value. It automatically attach
        the signal as an attribute with a given name to the given object
        or self.

        Parameters
        ----------
        var_name : str
            Storage name of the variable for the retrieve signal.
        signal_name : str
            Name to retrieve signal.
            Only the stem of signal_name is given.
        attach_to : object, optional
            signal_name will be attached as attribute to object
            attach_to. Default to self.

        Returns
        -------
        None
        """
        if not self.conn_open:
            self.conn.openTree('solps', self.shot)
            self.conn_open=True

        if not attach_to:
            attach_to = self

        #Not symmetrical wrt rundirtt, as parser gives array there
        signal_link = self._parser(signal_name)
        if signal_link:
            try:
                #Introduce here DN logic for singleton variables, by deciding
                #which of the tuple in tmp is given back
                tmp = self.conn.get(signal_link).value.T
                setattr(attach_to, var_name, tmp)
            except:
                self.log.warning("error reading '{1}' for '{0}'".format(signal_name,
                                                                     signal_link))
                setattr(attach_to, var_name, None)
        else:
            setattr(attach_to, var_name, None)

        if self.conn_open:
            self.conn.closeTree('solps', self.shot)
            self.conn_open = False


    def _parser(self, signal_name):
        """
        signal_name only contains the stem. The complete signal name must be
        evaluated here with the correct mixture of (signal_name + self._psym)

        In the case of MDS+ signals, the location and name are formatted
        directly into the MDS roots.

        completing the formating will be only use for the parser of Rundir.
        """

        _whl = self._which.upper()

        #Needed because singleton quantities use TARG instead
        if _whl[:-1] == 'TARGET':
            _wh = 'TARG'+_whl[-1]
        else:
            _wh = _whl

        #No need to complete name
        signal_name = signal_name.format("")

        #For profiles
        tprof = '\TIMEDEP::TOP.{0}:{1}'

        #For singletons
        tint = '\TIMEDEP::TOP.{1}:{0}INT'
        tsep = '\TIMEDEP::TOP.{1}:{0}SEP'
        tmax = '\TIMEDEP::TOP.{1}:{0}MAX'


        mdstree_common ={
                #Profile quantities
                'an3d':tprof.format(_whl, 'AN'),
                'ds':tprof.format(_whl, 'DS'),
                'mn3d':tprof.format(_whl, 'MN'),
                'ne3d':tprof.format(_whl, 'NE'),
                'po3d':tprof.format(_whl, 'PO'),
                'te3d':tprof.format(_whl, 'TE'),
                'ti3d':tprof.format(_whl, 'TI'),

                #Singleton quantities
                'tesep':tsep.format(_wh,'TE'),
                'tisep':tsep.format(_wh,'TI'),
                'nesep':tsep.format(_wh,'NE'),
                'posep':tsep.format(_wh,'PO'),
                }


        mdstree_midplanes ={
                #Profile quantities
                'dn3d':tprof.format(_whl, 'DN'),
                'dp3d':tprof.format(_whl, 'DP'),
                'ke3d':tprof.format(_whl, 'KE'),
                'ki3d':tprof.format(_whl, 'KI'),
                'vs3d':tprof.format(_whl, 'VS'),
                'vx3d':tprof.format(_whl, 'VX'),
                'vy3d':tprof.format(_whl, 'VY'),

                #Singleton quantities
                'dnsep':tsep.format(_wh,'DN'),
                'dpsep':tsep.format(_wh,'DP'),
                'kesep':tsep.format(_wh,'KE'),
                'kisep':tsep.format(_wh,'KI'),
                'vssep':tsep.format(_wh,'VS'),
                'vxsep':tsep.format(_wh,'VX'),
                'vysep':tsep.format(_wh,'VY'),
                 }


        mdstree_targets ={
                #Profile quantities
                'da':tprof.format(_whl, 'DA'),
                'fc3d':tprof.format(_whl, 'FC'),
                'fe3d':tprof.format(_whl, 'FE'),
                'fi3d':tprof.format(_whl, 'FI'),
                'fl3d':tprof.format(_whl, 'FL'),
                'fn3d':tprof.format(_whl, 'FN'),
                'fo3d':tprof.format(_whl, 'FO'),
                'ft3d':tprof.format(_whl, 'FT'),

                #Singleton quantities
                'temxp':tmax.format(_wh,'TE'),
                'timxp':tmax.format(_wh,'TI'),
                'nemxp':tmax.format(_wh,'NE'),
                'pomxp':tmax.format(_wh,'PO'),

                'fchxp':tint.format(_wh,'FCH'),
                'feexp':tint.format(_wh,'FEE'),
                'feixp':tint.format(_wh,'FEI'),
                'fetxp':tint.format(_wh,'FET'),
                'fnixp':tint.format(_wh,'FNI'),
                }


        if _whl[:-1] == 'TARGET':
            try:
                mdstree_common.update(mdstree_targets)
                return mdstree_common[signal_name]
            except:
                self.log.warning(
                    "'{0}' not found/implemented for '{1}' MDSTREE".format(
                                                    signal_name, self._which))
                return None


        elif _whl[-2:] == 'MP':
            try:
                mdstree_common.update(mdstree_midplanes)
                return mdstree_common[signal_name]
            except:
                self.log.warning(
                    "'{0}' not found/implemented for '{1}' MDSTREE".format(
                                                    signal_name, self._which))
                return None












































# =================OLD========================================================



# ====== b2time.nc: ===========================================================
#The following are not documented in solps manual and need to be tried to test
# if they are uploaded to the MDS server anyway
#    @property
#    @try_block
#    def fchsap(self):
#        """poloidal current, into outboard separatrix throat [A]"""
#        self._fchsap = self._read_ncfile('fchsap')
#
#    @property
#    @try_block
#    def fchsapp(self):
#        """poloidal current, x-pt private flux region [A]"""
#        self._fchsapp = self._read_ncfile('fchsapp')
#
#    @property
#    @try_block
#    def fchsip(self):
#        """poloidal current, into inboard separatrix throat [A]"""
#        self._fchsip = self._read_ncfile('fchsip')
#
#    @property
#    @try_block
#    def fchsipp(self):
#        """poloidal current, core x-pt flux region [A]"""
#        self._fchsipp = self._read_ncfile('fchsipp')
#    @property
#    @try_block
#    def fchyap(self):
#        """integrated perpendicular current, divertor region [A]"""
#        self._fchyap = self._read_ncfile('fchyap')
#
#    @property
#    @try_block
#    def fchyip(self):
#        """integrated perpendicular current, main chamber [A]"""
#        self._fchyip = self._read_ncfile('fchyip')
#
#    @property
#    @try_block
#    def feesap(self):
#        """poloidal electron energy flux, into outboard separatrix throat [W]"""
#        self._feesap = self._read_ncfile('feesap')
#
#    @property
#    @try_block
#    def feesapp(self):
#        """poloidal electron energy flux, x-pt private flux region [W]"""
#        self._feesapp = self._read_ncfile('feesapp')
#
#    @property
#    @try_block
#    def feesip(self):
#        """poloidal electron energy flux, into inboard separatrix throat [W]"""
#        self._feesip = self._read_ncfile('feesip')
#
#    @property
#    @try_block
#    def feesipp(self):
#        """poloidal electron energy flux, core x-pt flux region [W]"""
#        self._feesipp = self._read_ncfile('feesipp')
#    @property
#    @try_block
#    def feeyap(self):
#        """integrated perpendicular electron energy flux, divertor region [W]"""
#        self._feeyap = self._read_ncfile('feeyap')
#
#    @property
#    @try_block
#    def feeyip(self):
#        """integrated perpendicular electron energy flux, main chamber [W]"""
#        self._feeyip = self._read_ncfile('feeyip')
#
#    @property
#    @try_block
#    def feisap(self):
#        """poloidal ion energy flux, into outboard separatrix throat [W]"""
#        self._feisap = self._read_ncfile('feisap')
#
#    @property
#    @try_block
#    def feisapp(self):
#        """poloidal ion energy flux, x-pt private flux region [W]"""
#        self._feisapp = self._read_ncfile('feisapp')
#
#    @property
#    @try_block
#    def feisip(self):
#        """poloidal ion energy flux, into inboard separatrix throat [W]"""
#        self._feisip = self._read_ncfile('feisip')
#
#    @property
#    @try_block
#    def feisipp(self):
#        """poloidal ion energy flux, core x-pt flux region [W]"""
#        self._feisipp = self._read_ncfile('feisipp')
#    @property
#    @try_block
#    def feiyap(self):
#        """integrated perpendicular ion energy flux, divertor region [W]"""
#        self._feiyap = self._read_ncfile('feiyap')
#
#    @property
#    @try_block
#    def feiyip(self):
#        """integrated perpendicular ion energy flux, main chamber [W]"""
#        self._feiyip = self._read_ncfile('feiyip')
#
#    @property
#    @try_block
#    def fetsap(self):
#        """poloidal total energy flux, into outboard separatrix throat [W]"""
#        self._fetsap = self._read_ncfile('fetsap')
#
#    @property
#    @try_block
#    def fetsapp(self):
#        """poloidal total energy flux, x-pt private flux region [W]"""
#        self._fetsapp = self._read_ncfile('fetsapp')
#
#    @property
#    @try_block
#    def fetsip(self):
#        """poloidal total energy flux, into inboard separatrix throat [W]"""
#        self._fetsip = self._read_ncfile('fetsip')
#
#    @property
#    @try_block
#    def fetsipp(self):
#        """poloidal total energy flux, core x-pt flux region [W]"""
#        self._fetsipp = self._read_ncfile('fetsipp')
#
#    @property
#    @try_block
#    def fetyap(self):
#        """integrated perpendicular total energy flux, divertor region [W]"""
#        self._fetyap = self._read_ncfile('fetyap')
#
#    @property
#    @try_block
#    def fetyip(self):
#        """integrated perpendicular total energy flux, main chamber [W]"""
#        self._fetyip = self._read_ncfile('fetyip')
#
#    @property
#    @try_block
#    def fnisap(self):
#        """poloidal particle flux, into outboard separatrix throat [1/s]"""
#        self._fnisap = self._read_ncfile('fnisap')
#
#    @property
#    @try_block
#    def fnisapp(self):
#        """poloidal particle flux, x-pt private flux region [1/s]"""
#        self._fnisapp = self._read_ncfile('fnisapp')
#
#    @property
#    @try_block
#    def fnisip(self):
#        """poloidal particle flux, into inboard separatrix throat [1/s]"""
#        self._fnisip = self._read_ncfile('fnisip')
#
#    @property
#    @try_block
#    def fnisipp(self):
#        """poloidal particle flux, core x-pt flux region [1/s]"""
#        self._fnisipp = self._read_ncfile('fnisipp')
#
#    @property
#    @try_block
#    def fniyap(self):
#        """integrated perpendicular particle flux, divertor region [1/s]"""
#        self._fniyap = self._read_ncfile('fniyap')
#
#    @property
#    @try_block
#    def fniyip(self):
#        """integrated perpendicular particle flux, main chamber [1/s]"""
#        self._fniyip = self._read_ncfile('fniyip')
#
#    @property
#    @try_block
#    def pwmxap(self):
#        """maximum total power flux, outboard divertor [W/m^2]"""
#        self._pwmxap = self._read_ncfile('pwmxap')
#
#    @property
#    @try_block
#    def pwmxip(self):
#        """maximum total power flux, inboard divertor [W/m^2]"""
#        self._pwmxip = self._read_ncfile('pwmxip')
#
#    @property
#    @try_block
#    def tmhacore(self):
#        """H-alpha emissivity, core [photons / (m^2 sr)]"""
#        self._tmhacore = self._read_ncfile('tmhacore')
#
#    @property
#    @try_block
#    def tmhadiv(self):
#        """H-alpha emissivity, divertor region [photons / (m^2 sr)]"""
#        self._tmhadiv = self._read_ncfile('tmhadiv')
#
#    @property
#    @try_block
#    def tmhasol(self):
#        """H-alpha emissivity, SOL above the X-point [photons / (m^2 sr)]"""
#        self._tmhasol = self._read_ncfile('tmhasol')
#
#    @property
#    @try_block
#    def tmne(self):
#        """total number of particles"""
#        self._tmne = self._read_ncfile('tmne')
#
#    @property
#    @try_block
#    def tmte(self):
#        """total electron energy [eV]"""
#        self._tmte = self._read_ncfile('tmte')
#
#    @property
#    @try_block
#    def tmti(self):
#        """total ion energy [eV]"""
#        self._tmti = self._read_ncfile('tmti')
#
#    @property
#    @try_block
#    def tpmxap(self):
#        """maximum target temperature, outboard divertor [K]"""
#        self._tpmxap = self._read_ncfile('tpmxap')
#
#    @property
#    @try_block
#    def tpmxip(self):
#        """maximum target temperature, inboard divertor [K]"""
#        self._tpmxip = self._read_ncfile('tpmxip')
#
#    @property
#    @try_block
#    def tpsepa(self):
#        """separatrix target temperature, outboard divertor [K]"""
#        self._tpsepa = self._read_ncfile('tpsepa')
#
#    @property
#    @try_block
#    def tpsepi(self):
#        """separatrix target temperature, inboard divertor [K]"""
#        self._tpsepi = self._read_ncfile('tpsepi')
# ====== b2tallies.nc: ========================================================
#Implemented in TALLIES: Next step!
#
#    # Electron and Ion x and y power fluxes in different regions:
#    @property
#    @try_block
#    def fhexreg(self):
#        self._fhexreg = self._read_ncfile('fhexreg')
#
#    @property
#    @try_block
#    def fhixreg(self):
#        self._fhixreg = self._read_ncfile('fhixreg')
#
#    @property
#    @try_block
#    def fheyreg(self):
#        self._fheyreg = self._read_ncfile('fheyreg')
#
#    @property
#    @try_block
#    def fhiyreg(self):
#        self._fhiyreg = self._read_ncfile('fhiyreg')
#
#    @property
#    @try_block
#    def fhcore(self):
#        """Power flux crossing the core boundary."""
#        self._fhcore = self.fheyreg[:,2] + self.fhiyreg[:,2]
#
# ====== Special time traces (particle_fluxes.nc): ============================
#Implemented by Ferdinand. Not in MDS
#
#    # Deuterium puff:
#    @property
#    @try_block
#    def D_puff(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._D_puff = self._read_ncfile('D_puff')
#
#    @property
#    @try_block
#    def D_puff_core(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._D_puff_core = self._read_ncfile('D_puff_core')
#
#    @property
#    @try_block
#    def D_puff_total(self):
#        self._D_puff_total = self.D_puff + self.D_puff_core
#
#    # Deuterium pumping:
#    @property
#    @try_block
#    def D_pump(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._D_pump = self._read_ncfile('D_pump')
#
#    @property
#    @try_block
#    def D_pump_core(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._D_pump_core = self._read_ncfile('D_pump_core')
#
#    @property
#    @try_block
#    def D2_pump(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._D2_pump = self._read_ncfile('D2_pump')
#
#    @property
#    @try_block
#    def D2_pump_core(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._D2_pump_core  = self._read_ncfile('D2_pump_core')
#
#    @property
#    @try_block
#    def D_pump_total(self):
#        self._D_pump_total = (self.D_pump + self.D_pump_core
#                              + self.D2_pump + self.D2_pump_core)
#
#    # Nitrogen puff:
#    @property
#    @try_block
#    def N_puff(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._N_puff = self._read_ncfile('N_puff')
#
#    # Nitrogen pumping:
#    @property
#    @try_block
#    def N_pump(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._N_pump = self._read_ncfile('N_pump')
#
#    @property
#    @try_block
#    def N_pump_core(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._N_pump_core = self._read_ncfile('N_pump_core')
#
#    @property
#    @try_block
#    def N2_pump(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._N2_pump = self._read_ncfile('N2_pump')
#
#    @property
#    @try_block
#    def N2_pump_core(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._N2_pump_core = self._read_ncfile('N2_pump_core')
#
#    @property
#    @try_block
#    def N_pump_total(self):
#        self._N_pump_total = (self.N_pump + self.N_pump_core 
#                              + self.N2_pump + self.N2_pump_core)
#
#    # Argon puff:
#    @property
#    @try_block
#    def Ar_puff(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._Ar_puff = self._read_ncfile('Ar_puff')
#
#    # Argon pumping:
#    @property
#    @try_block
#    def Ar_pump(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._Ar_pump = self._read_ncfile('Ar_pump')
#
#    @property
#    @try_block
#    def Ar_pump_core(self):
#        """Not availiable by default. Needs to be written to
#        particle_fluxes.nc by some special postprocessing."""
#        self._Ar_pump_core = self._read_ncfile('Ar_pump_core')
#
#    @property
#    @try_block
#    def Ar_pump_total(self):
#        self._Ar_pump_total = self.Ar_pump + self.Ar_pump_core
#
