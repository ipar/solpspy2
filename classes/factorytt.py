import mdstt
import rundirtt

#Add another if for magnetic geometry, once it is settle how to do it
def TimeTraces(mother, which, **kwargs):
    if mother._type == 'MdsData':
        if which[:-1] == 'target':
            return mdstt.TargetData(mother, which, **kwargs)
        elif which[-2:] == 'mp':
            return mdstt.MidplaneData(mother, which, **kwargs)
        else:
            mother.log.error('SolpsData type not supported in factory method')
            return None
    elif mother._type == 'RundirData':
        if which[:-1] == 'target':
            return rundirtt.TargetData(mother, which, **kwargs)
        elif which[-2:] == 'mp':
            return rundirtt.MidplaneData(mother, which, **kwargs)
        else:
            mother.log.error('SolpsData type not supported in factory method')
            return None
