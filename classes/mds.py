#!/usr/bin/env python

"""
solps_data.py v0.1
      (ex-solps_data.py v1.0)
      (ex-gdata.py v1.0)

Description:
    Read the data of a given shot number and returns an object with the
    requested data. If not possible, returns a zero array of shape and warning.

  ---  IMPORTANT REMARKS: ---
       - self.na only valid for non neutral species when using EIRENE
       - self.sep given is solps.sep + 1, to be the first one in the SOL.
       - self.[io]xp corresponds to the first and last IN CORE pol. index.
         Thus inndiv  = [:ixp) and outdiv = (oxp:]


REMARKS:
    using _wrap_read_signal is twice as slow using the inline form. The last is
    6 times slower than reading the data instead of the property.
    I will use the warping function because it is easier, more readable and
    more modular. And the differences should not be felt anyway:
              This is Python, not C.

"""

from __future__ import division, print_function

# Common modules:
import sys
import os
import re
import numpy as np
from sets import Set
from collections import OrderedDict
import itertools

# Solpspy modules:
from solpspy.tools.tools import try_block, solps_property, module_path
from solpspy.tools.tools import SimpleNamespace, priority, cellface, elements
from solpspy.tools.tools import  update


#Extensions
from solpspy.extensions.chords import Chords
from solpspy.extensions.spectroscopy import Spectroscopy
import mdstt
from solpspy.plots.core import Plots

# Logging setup
import logging
mdslog = logging.getLogger('MdsData')
if not mdslog.handlers: #Prevents from adding repeated streamhandlers
    formatter = logging.Formatter(
            '%(name)s(%(ident)s) -- %(levelname)s: %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    mdslog.addHandler(ch)
    mdslog.propagate = False #Otherwise, root logger also prints


try:
    import MDSplus
except:
    log = logging.getLogger()
    ch = logging.StreamHandler()
    log.addHandler(ch)
    log.error("MDSplus package not found")


from solpspy.tools.tools import read_config, styles
defmpl = read_config()['Matplotlib style']['default']
if type(defmpl) == str and defmpl.lower() != 'none':
    import matplotlib.pyplot as plt
    plt.style.use(styles(read_config()['Matplotlib style']['default']))




# =============================================================================
class MdsData(object):
    """
     MdsData manages simulations uploaded to the MDS+ server.


    Comments:
    Previous definition of pitch angle, which is WRONG (I think):
              self.pitch[i,j] = (np.sqrt(self.b[i,j,0]**2 + self.b[i,j,1]**2)/
                                               np.abs(self.b[i,j,2]))
    Second definition: Bpol/Btor
    Moulton definition: Bpol/Btotal

    Best definition? pitch = arctan(Bpol/Btor)

    For some of the propeties, one could go back to writting the try block
    inline to speed up one order of magnitude the code, but 200 ns is not that
    much, at the end of the day. In this way, one also have both sets of data
    (raw and modified) which has its advantages.

    Change logger from instance specific to class specific and add id by
    adapter instead that coded in name

    """
    #@profile
    def __init__(self,shot, name=None, b2standalone=False, **kwargs):
        self.conn = MDSplus.Connection('solps-mdsplus.aug.ipp.mpg.de:8001')
        self.conn_open = False
        self.shot = shot
        self.nshot = shot # Alias
        if name is None:
            self.name = str(shot)
        else:
            self.name = name
        self._type = 'MdsData'
        self.log = logging.LoggerAdapter(mdslog, {'ident':self.name})
        self.log.info("%d: Reading shot" % self.shot)

        self.b2standalone = b2standalone
        self._get_basic_dimensions()

        self.avail = {}
        self._check_avail()

        self.target1 = mdstt.TargetData(self,'target1')
        self.target4 = mdstt.TargetData(self,'target4')

        self.out = self.target4
        self.inn = self.target1

        self.li = self.target1
        self.ui = None
        self.uo = None
        self.lo = self.target4

        self.outmp = mdstt.MidplaneData(self,'omp')
        self.innmp = mdstt.MidplaneData(self,'imp')

        self.mp = 1.672621777e-27
        self.qe = 1.602176565e-19
        self.ev = self.qe # Alias





# IDENT PROPERTIES ============================================================
    @solps_property
    def user(self):
        """str : Name id of whomever is recorded as author."""
        self._read_signal('_user', 'user')

    @solps_property
    def solpsversion(self):
        """str : SOLPS numerical version string."""
        self._read_signal('_solpsversion', 'solpsversion')

    @solps_property
    def solps_code(self):
        """str : Name of SOLPS code package version."""
        if self.solpsversion[0:2] == '01':
            self._solps_code = 'solps5.0'
        elif self.solpsversion[0:2] == '03':
            self._solps_code = 'solps-iter'
        else:
            self._solps_code = None
    #ATTENTION: Isn't it confusing?
    @property
    def solps_version(self):
        """str : Name of SOLPS code package version.

        Notes
        -----
        Alias for self.solps_code.
        """
        return self.solps_code

    @solps_property
    def directory(self):
        """str: Absolute path pointing towards run directory."""
        self._read_signal('_directory', 'directory')

    @solps_property
    def module_path(self):
        """str: Path to the local solpspy distribution."""
        self._module_path = module_path()

    @solps_property
    def exp(self):
        """str : Exp id of machine on which the simulation is based."""
        self._read_signal('_exp', 'exp')

# SNAPSTHOP PROPERTIES ========================================================
    # -------------------- Dimensions -----------------------------------------
    # nx, ny, ns, natm, nmol and nion are read with self._get_basic_dimensions()


    # -------------------- Indices and locators -------------------------------

    @solps_property
    def regvol(self):
        """(nx,ny):     Volume region indices. """
        self._read_signal('_regvol','regvol')
    @solps_property
    def regflx(self):
        """(nx,ny):     x-directed flux region indices. """
        self._read_signal('_regflx','regflx')
    @solps_property
    def regfly(self):
        """(nx,ny):     y-directed flux region indices. """
        self._read_signal('_regfly','regfly')
    @solps_property
    def region(self):
        """(nx,ny,3):   Index of corresponding region.
        0:  Volume region
        1:  x-region
        2:  y-region
        """
        self._region = np.zeros((self.nx, self.ny, 3), dtype=np.int32)
        self._region[...,0] = self.regvol
        self._region[...,1] = self.regflx
        self._region[...,2] = self.regfly

    @solps_property
    def nnreg(self):
        """(3):     Number of volumetric, x- and y-regions. """
        self._nnreg = np.zeros(3, dtype=np.int32)
        self._nnreg[0] = self.region[...,0].max()
        self._nnreg[1] = self.region[...,1].max()
        self._nnreg[2] = self.region[...,2].max()




    @solps_property
    def jxi(self):
        """int: Inner midplane index."""
        self.read_signal('_jxi', '\SNAPSHOT::TOP.DIMENSIONS:IMP')

    @solps_property
    def jxa(self):
        """int: Outer midplane index."""
        self.read_signal('_jxa', '\SNAPSHOT::TOP.DIMENSIONS:OMP')

    #ATTENTION: Having USN class, is the if necessary anymore?
    @property
    def omp(self):
        """int : Outer midplane index (B2 grid).

        Notes
        -----
        Alias for either jxa or jxi.
        It should take the low field side of either of them.
        """
        if (np.abs(self.b[self.jxi,self.sep,2]) >
            np.abs(self.b[self.jxa,self.sep,2])):
            return self.jxa
        else:
            return self.jxi

    #ATTENTION: Having USN class, is the if necessary anymore?
    @property
    def imp(self):
        """int : Inner midplane index (B2 grid).

        Notes
        -----
        Alias for either jxi or jxa.
        It should take the high field side of either of them.
        """
        if (np.abs(self.b[self.jxi,self.sep,2]) >
            np.abs(self.b[self.jxa,self.sep,2])):
            return self.jxi
        else:
            return self.jxa


    @solps_property
    def sep(self):
        """int : Separatrix index (B2 grid).

        Notes
        -----
        Defined as first flux surface in the SOL.
        It means that there a +1 of difference with standard definition.
        Distance from separatrix defined by this index should always be >0.
        """
        self.read_signal('_sep', '\SNAPSHOT::TOP.DIMENSIONS:SEP')
        self._sep+= 1

    @solps_property
    def targ1(self):
        """int: Index of 'targ1'."""
        self._read_signal('_targ1', 'targ1')

    @solps_property
    def targ2(self):
        """int: Index of 'targ2'."""
        self._read_signal('_targ2', 'targ2')

    @solps_property
    def targ3(self):
        """int: Index of 'targ3'."""
        self._read_signal('_targ3', 'targ3')

    @solps_property
    def targ4(self):
        """int: Index of 'targ4'."""
        self._read_signal('_targ4', 'targ4')

    @solps_property
    def xpoint(self):
        """(float, float) : R and z coordinates of the x-point of B2 grid."""
        # The x-point is the only grid vertex where 8 lines converge, i.e.
        # which is shared by 8 grid cells. => Count the occurrence of all
        # vertices, the one which appears 8 times is the x-point.
        allvertices = {}
        for nx in range(self.nx):
            for ny in range(self.ny):
                cell = self.grid[nx,ny] # [nvertex,R|z]
                for v in range(len(cell)):
                    vertex = str(cell[v,0]) + ' ' + str(cell[v,1])
                    allvertices[vertex] = allvertices.get(vertex,0) + 1

        xpoint = allvertices.keys()[allvertices.values().index(8)]
        xpoint = np.array([float(xpoint.split()[0]),
                           float(xpoint.split()[1])])
        self._xpoint = xpoint


    #Replace both ixp and oxp with the corresponding cuts.
    # Create ixp2 and oxp2 too.
    @solps_property
    def ixp(self):
        """int: Index of the inner side of the x-point."""
        gradte = np.array([self.te[i+1,1]-self.te[i,1]
                           for i in range(self.nx-1)])
        self._ixp = np.argmax(gradte) + 1

    @solps_property
    def oxp(self):
        """int: Index of the outer side of the x-point."""
        gradte = np.array([self.te[i+1,1]-self.te[i,1]
                           for i in range(self.nx-1)])
        self._oxp = np.argmin(gradte)



    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = np.where(self.region[...,1] == 4)[0][0]-1

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 1)[0][0]



    ##ATTENTION: To be removed? Is it still useful?
    @solps_property
    def oul(self):   # Outer divertor upstream limit
        self._oul = self.oxp + 1
    ##ATTENTION: To be removed? Is it still useful?
    @solps_property
    def odl(self):   # Outer divertor downstream limit
        self._odl = self.nx - 1
    ##ATTENTION: To be removed? Is it still useful?
    @solps_property
    def iul(self):   # Outer divertor upstream limit
        self._iul = self.ixp - 1
    ##ATTENTION: To be removed? Is it still useful?
    @solps_property
    def idl(self):   # Outer divertor downstream limit
        self._idl = 1

    #ATTENTION: Create test.
    #ATTENTION: Is the definition correct?
    @solps_property
    def pedestal_top(self):
        """int: Y index of the midplane pedestal top position calculated
        as the minimum of the  second derivative of the plasma pressure."""
        ddyp = self.gradient(self.gradient(self.ptot)[...,1])[...,1][self.omp,:]
        self._pedestal_top = np.argmin(ddyp)

    # -------------------- Neighbours and cuts --------------------------------
    #ATTENTION: Improve docstrings
    @solps_property
    def leftix(self):
        """int: X index of the left neighbour (ix-1,iy).

        Notes:
        -----
            Index in base 0, Python-like"""
        self._read_signal('_leftix','leftix')

    @solps_property
    def leftiy(self):
        """int: Y index of the left neighbour (ix-1,iy).

        Notes:
        -----
            Index in base 0, Python-like"""
        self._read_signal('_leftiy','leftiy')

    @solps_property
    def rightix(self):
        """int: X index of the right neighbour (ix+1,iy).

        Notes:
        -----
            Index in base 0, Python-like"""
        self._read_signal('_rightix','rightix')

    @solps_property
    def rightiy(self):
        """int: Y index of the right neighbour (ix+1,iy).

        Notes:
        -----
            Index in base 0, Python-like"""
        self._read_signal('_rightiy','rightiy')

    @solps_property
    def bottomix(self):
        """int: X index of the bottom neighbour (ix,iy-1).

        Notes:
        -----
            Index in base 0, Python-like"""
        self._read_signal('_bottomix','bottomix')

    @solps_property
    def bottomiy(self):
        """int: Y index of the bottom neighbour (ix,iy-1).

        Notes:
        -----
            Index in base 0, Python-like"""
        self._read_signal('_bottomiy','bottomiy')

    @solps_property
    def topix(self):
        """int: X index of the top neighbour (ix,iy+1).

        Notes:
        -----
            Index in base 0, Python-like"""
        self._read_signal('_topix','topix')

    @solps_property
    def topiy(self):
        """int: Y index of the top neighbour (ix,iy+1).

        Notes:
        -----
            Index in base 0, Python-like"""
        self._read_signal('_topiy','topiy')


    #Add all the cuts
    @solps_property
    def nncut(self):
        """int: Number of grid cuts."""
        self._nncut = np.array([1])
    @solps_property
    def leftcut(self):
        self._leftcut = np.array(
            [np.where(self.region[...,1]==2)[0][0].astype(int) -1])

    @solps_property
    def rightcut(self):
        self._rightcut = np.array(
           [np.where(self.region[...,1]==3)[0][0].astype(int) -1])

    @solps_property
    def topcut(self):
        self._topcut = np.array(
            [np.where(self.region[...,2]==4)[1][0].astype(int) -1])

    @solps_property
    def bottomcut(self):
        self._bottomcut = np.array([-1])



    # -------------------- Grid and vessel geometry ---------------------------
    #ATTENTION: In rundir it is read from input.dat most likely,
    #but in mds, it is read from mesh.extra.
    @solps_property
    def vessel(self):
        """(4, nsurfaces) : Vessel structure."""
        self._read_signal('_vessel', 'vessel')
    @property
    def vessel_mesh(self):
        """ Alias for self.vessel, as in MDS+ only vessel_mesh is saved"""
        return self.vessel

    #ATTENTION: Add docstrig
    @solps_property
    def cr(self):
        """(nx,ny) : R-coordinate of the cell centre [m]"""
        self._read_signal('_cr', 'cr')
    #ATTENTION: Add docstrig
    @solps_property
    def cr_x(self):
        """(nx,ny) : R-coordinate of the cell left x-face [m]"""
        self._read_signal('_cr_x', 'cr_x')
    #ATTENTION: Add docstrig
    @solps_property
    def cr_y(self):
        """(nx,ny) : R-coordinate of the cell bottom y-face [m]"""
        self._read_signal('_cr_y', 'cr_y')

    #ATTENTION: Add docstrig
    @solps_property
    def cz(self):
        """(nx,ny) : Z-coordinate of the cell centre [m]"""
        self._read_signal('_cz', 'cz')
    #ATTENTION: Add docstrig
    @solps_property
    def cz_x(self):
        """(nx,ny) : Z-coordinate of the cell left x-face [m]"""
        self._read_signal('_cz_x', 'cz_x')
    #ATTENTION: Add docstrig
    @solps_property
    def cz_y(self):
        """(nx,ny) : Z-coordinate of the cell bottom y-face [m]"""
        self._read_signal('_cz_y', 'cz_y')

    #ATTENTION: Add docstrig
    @solps_property
    def r(self):
        """(nx,ny,4): R-coordinate of the corners of the B2 grid [m]."""
        self._read_signal('_r', 'r')

    #ATTENTION: Add docstrig
    @solps_property
    def z(self):
        """(nx,ny,4): Z-coordinate of the corners of the B2 grid [m]."""
        self._read_signal('_z', 'z')

    #ATTENTION: Add docstrig
    @solps_property
    def grid(self):
        """(nx,ny,4,2): Corners of B2 grid cells.

        Notes
        -----
        [grid] = nx, ny, vertex, R|z"""
        self._grid = np.zeros((self.nx,self.ny,4,2))
        for i in range(self.nx):
            for k in range(self.ny):
                self._grid[i,k,:,0] = np.array([self.r[i,k,0], self.r[i,k,1],
                                                self.r[i,k,3],self.r[i,k,2]])
                self._grid[i,k,:,1] = np.array([self.z[i,k,0], self.z[i,k,1],
                                                self.z[i,k,3],self.z[i,k,2]])

#    #ATTENTION: Improve docstring
#    #ATTENTION: Needs revision?? Is it even needed? No logic will be used here
#    @solps_property
#    def magnetic_geometry(self):
#        """
#        This function check if the current case is a LSN or an USN.
#        In the future, DN in all the forms should be checked too.
#        It requires certain knowledge about the case:
#        self.z or self.cz
#        self.sep (maybe not required)
#        It will default to None, so users are aware that the check was not
#        successful.
#        If magnetic geometry is passed as a kwarg with creation, then set
#        the _ manually during initialization to prevent the check.
#
#        Another check could be to test self.b[:,:,2] which is Bt:
#        The self.sep target with the smaller abs strengh is the lfs
#        The self.sep target with the higher  abs strengh is the hfs
#
#        Then, if targ1 is hfs and targ4 is hfs, then USN
#
#        This could also serve for more complex DN:
#        Get the four strike points, two lfs two hfs, two up two down.
#
#        Probably, an exclusive and explicit way to obtain cz and z only to
#        test is needed, as try_block will now use LSN or USN information.
#        Also, a new generating function will be needed because it cannot
#        be a try_block property
#
#
#        DDN and DDNU must be valid outputs of magnetic geometry too
#        """
#        try:
#            pos = self.cz[:,self.sep]
#        except:
#            try:
#                pos = self.z[:,self.sep,0]
#            except:
#                self._magnetic_geometry = None
#                return
#
#        if (pos.argmin() == 0) or (pos.argmin() == len(pos)-1):
#            self._magnetic_geometry = 'lsn'
#        else:
#            self._magnetic_geometry = 'usn'

    @solps_property
    def magnetic_geometry(self):
        """str: Corresponding magnetic geometry."""
        self._magnetic_geometry = 'lsn'



    @solps_property
    def hx(self):
        """(nx,ny): Scale factors in the x-direction [m].

        Notes
        -----
            Corresponds to the lenght of the cell."""
        self._read_signal('_hx', 'hx')

    @solps_property
    def hy(self):
        """(nx,ny): Scale factors in the y-direction [m].

        Notes
        -----
            Corresponds to the (uncorrected) width of the cell.
            self.hy1 should be used instead."""
        self._read_signal('_hy', 'hy')

    @solps_property
    def hy1(self):
        """(nx,ny): Corrected scale factors in the y-direction [m].
        """
        self._hy1 = self.hy*self.qz[:,:,1]

    @solps_property
    def dsrad(self):
        """(nx,ny): Radial distance wrt separatrix [m].

        Notes
        -----
            Somewhat different results than self implemented
            self.ds.
        """
        self._read_signal('_dsrad', 'dsrad')




    #ATTENTION: Add docstrig
    @solps_property
    def dspar(self):
        """(nx,ny): Parallel distance with respect to first poloidal cell
        in the computational grid layout [m].

        Notes
        -----
            Almost equal to self calculated self.ipardspar.
            Also defined for closed flux surfaces.
        """
        self._read_signal('_dspar', 'dspar')


    #ATTENTION: should this stay?
    #ATTENTION: Add docstrig
    @solps_property
    def ipardspar(self):
        self._ipardspar = np.zeros((self.nx,self.ny))
        for k in xrange(self.ny):
          self._ipardspar[0,k]=self.hx[0,k]/2
          for j in xrange(1,self.nx):
              self._ipardspar[j,k] = (self._ipardspar[j-1,k]+
                  (self.hx[j-1,k]*self.invpitch[j-1,k]+
                   self.hx[j,k]*self.invpitch[j,k])/2)

    @solps_property
    def ds(self):
        """(nx,ny) : Radial distance from the separatrix [m]"""
        self._ds = np.zeros((self.nx,self.ny))
        proxy = np.zeros((self.nx,self.ny))
        for i in range(self.nx):
            proxy[i,0] = 0
            for k in range(1,self.ny):
                proxy[i,k] = (proxy[i,k-1] +
                              np.sqrt((self.cr[i,k] - self.cr[i,k-1])**2 +
                                      (self.cz[i,k] - self.cz[i,k-1])**2))
            ds_offset = (proxy[i,self.sep-1] + proxy[i,self.sep])/2
            self._ds[i,:] = proxy[i,:] - ds_offset


    #ATTENTION: Required? Wouldn't it be better to have poloidal distance?
    @solps_property
    def domp(self):
        """(nx,ny) : Poloidal distance from the outer midplane
                     along x-direction [m]."""
        # I think this is both more correct and much
        # simpler/more elegant than the old version:
        self._domp = np.array([np.sum(self.hx[0:nx,:],0) + self.hx[nx,:]/2
                               for nx in range(self.nx)])
        self._domp = self._domp - self._domp[self.omp,:]
#   @solps_property
#   def domp_old(self):
#       """Distance from the outer midplane along x-direction."""
#       self._domp_old = np.zeros((self.nx,self.ny))
#       proxy = np.zeros((self.nx,self.ny))
#       for ny in range(self.ny):
#           proxy[0,ny] = 0
#           for nx in range(1,self.nx):
#               a = np.array([self.cr[nx  ,ny],self.cz[nx  ,ny]])
#               b = np.array([self.cr[nx-1,ny],self.cz[nx-1,ny]])
#               d = np.linalg.norm(b-a)
#               proxy[nx,ny] = proxy[nx-1,ny] + d
#           domp_offset = proxy[self.omp,ny]
#           self._domp_old[:,ny] = proxy[:,ny] - domp_offset

    #ATTENTION: Required? Wouldn't it be better to have poloidal distance?
    @solps_property
    def domp_parallel(self):
        """(nx,ny) : Parallel distance from the outer midplane
                     along x-direction (i.e. parallel to B) [m]."""
        hx_par = self.hx*np.abs(self.bb[...,3]/self.bb[...,0])
        dp = np.array([np.sum(hx_par[0:nx,:],0) + hx_par[nx,:]/2
                       for nx in range(self.nx)])
        self._domp_parallel = dp - dp[self.omp,:]





    #ATTENTION: which one is the most correct one??
    #ATTENTION: Add docstrig
    @solps_property
    def pitch(self):
        self._pitch = np.ones((self.nx,self.ny))
        bm = (self.b[:,:,2] != 0)
        for i in xrange(self.nx):
          for j in xrange(self.ny):
            if bm[i,j]:
              self._pitch[i,j] = np.arctan(np.abs(self.b[i,j,0]/self.b[i,j,2]))

    #ATTENTION: which one is the most correct one??
    #ATTENTION: Add docstrig
    @solps_property
    def pitch_pt(self):
        self._pitch_pt = np.ones((self.nx,self.ny))
        bm = (self.b[:,:,2] != 0)
        for i in xrange(self.nx):
          for j in xrange(self.ny):
            if bm[i,j]:
              self._pitch_pt[i,j] = np.abs(self.b[i,j,0]/self.b[i,j,2])

    #ATTENTION: which one is the most correct one??
    #ATTENTION: Add docstrig
    @solps_property
    def pitch_tot(self):
        self._pitch_tot = np.ones((self.nx,self.ny))
        bm = (self.b[:,:,3] != 0)
        for i in xrange(self.nx):
          for j in xrange(self.ny):
            if bm[i,j]:
              self._pitch_tot[i,j] = np.abs(self.b[i,j,0]/self.b[i,j,3])

    #ATTENTION: Add docstrig
    @solps_property
    def invpitch(self):
        """(nx,ny): Inverse of pitch angle (1/alpha)."""
        self._invpitch = 1./self.pitch #Add protection against 1/0

    #ATTENTION: Necessary?
    #ATTENTION: Add docstrig
    @solps_property
    def parallel_surface_m(self):
        """(nx,ny): Parallel surface at cell center [m^2]."""
        self._parallel_surface_m = (self.dv/self.hx)*self.pitch_tot

    @solps_property
    def parallel_surface(self):
        """(nx,ny): Parallel surface at cell center [m^2]."""
        self._parallel_surface = (self.dv/self.hx)*self.pitch_tot



    @solps_property
    def sx(self):
        """(nx,ny): Area of the x-face between cell (ix,iy) and its left
        neighbour (ix-1,iy) [m^2]."""
        self._read_signal('_sx','sx')
        self._sx = np.concatenate((np.zeros((1,self.ny)),
                                        self._sx), axis = 0)

    @solps_property
    def sy(self):
        """(nx,ny): Area of the y-face between cell (ix,iy) and its bottom
        neighbour (ix,iy-1) [m^2]."""
        self._read_signal('_sy','sy')
        self._sy = np.concatenate((np.zeros((self.nx,1)),
                                        self._sy), axis = 1)
    @solps_property
    def gs(self):
        """(nx,ny,3): Area of faces between cell (ix,iy) and its left and
        bottom neighbours [m^2].
        gs[...,0] left x-face (left face).
        gs[...,1] bottom y-face (bottom face).

        Notes
        -----
        gs[...,2] corresponding to SZ is still missing!
        But:
            >>> np.allclose(shot.gs[...,:-1], run.gs[...,:-1])
            >>> True
        """
        self._gs = np.zeros((self.nx,self.ny,3))
        self._gs[...,0] = self.sx
        self._gs[...,1] = self.sy





    # -------------------- Plasma Characterization ----------------------------
    @solps_property
    def am(self):
        """(ns) : Atomic mass [u]"""
        self._read_signal('_am', 'am')

    @solps_property
    def am_kg(self):
        """(ns) : Atomic mass [kg]"""
        amu = 1.660539040e-27
        self._am_kg = amu * self.am

    #ATTENTION: Unnecessary? On the other hand it's used quite often already ...
    @solps_property
    def mi(self):
        """(nx,ny,ns) : Atomic mass [kg]"""
        self._mi = self.am_kg * np.ones((self.nx,self.ny,self.ns))

    #ATTENTION: Better name?
    @solps_property
    def na_eirene(self):
        """(nx,ny,ns): Species densities corrected for Eirene neutrals [m^-3].

        Notes
        -----
        If b2standalone case, then na_eirene = na."""
        self._na_eirene = self.na.copy()

        if self.b2standalone is False:
            iatm = 0
            for i,z in enumerate(self.za):
                if z == 0:
                    self._na_eirene[:,:,i] = self.dab2[:,:,iatm]
                    iatm += 1



    @solps_property
    def zn(self):
        """(ns): Nuclear charge [e-]."""
        self._read_signal('_zn', 'zn')

    @solps_property
    def za(self):
        """(ns): Atom charge [e-]."""
        self._read_signal('_za', 'za')


    @solps_property
    def bb(self):
        """(nx,ny,4) : Magnetic field components [T].

        Notes
        -----
        bb[...,0] : x component (Poloidal)
        bb[...,1] : y component (Radial)
        bb[...,2] : z component (Toroidal)
        bb[...,3] : Total ( =sqrt(x^2+y^2+z^2) )
        """
        self._read_signal('_bb', 'bb')

    @property
    def b(self):
        """ Alias for self.bb"""
        return self.bb

    #ATTENTION: Add docstrig
    @solps_property
    def po(self):
        """(nx,ny): Electric potential [V]."""
        self._read_signal('_po', 'po')

    #ATTENTION: Add docstrig
    @solps_property
    def qz(self):
        """(nx,ny,2): Complementary angle, t, between x- and y-directions.
        qz[...,0] = sin(t) => inner product of x and y basis vectors.
                    It vanishes for orthogonal geometries.
        qz[...,1] = cos(t).
        qz"""
        self._read_signal('_qz', 'qz')

    #ATTENTION: Add docstrig
    @solps_property

    def vol(self):
        """(nx,ny): Cell volume [m^3]."""
        self._read_signal('_vol', 'vol')
    @property
    def dv(self):
        """ Alias for self.vol """
        return self.vol


    # -------------------- Densities ------------------------------------------
    @solps_property
    def na(self):
        """ (nx,ny,ns): Fluid species density [m^-3]"""
        self._read_signal('_na','na')

    @solps_property
    def ne(self):
        """ (nx,ny): Electron density [m^-3]"""
        self._read_signal('_ne','ne')

    @solps_property
    def zeff(self):
        """ (nx,ny): Effective charge [e-]"""
        self._zeff = np.sum(self.na*self.za**2,2)/self.ne


    @solps_property
    def dab2(self):
        """ (nx,ny,natm): Eirene atomic density [m^-3]"""
        self._read_signal('_dab2', 'dab2')

    @solps_property
    def dmb2(self):
        """ (nx,ny,nmol): Eirene molecular density [m^-3]"""
        self._read_signal('_dmb2', 'dmb2')

    @solps_property
    def dib2(self):
        """ (nx,ny,nion): Eirene test ion density [m^-3]"""
        self._read_signal('_dib2', 'dib2')

    #ATTENTION: Not a very good name? Maybe na_isonuclear?
    #ATTENTION: Or isonuclear_density?
    #ATTENTION: Or na_element?
    #ATTENTION: Improve docstring
    @solps_property
    def na_atom(self):
        """(nx,ny): Corrected total density per element [m^-3].

        Notes
        -----
        Sum of all charge states of isonuclear sequence.
        It uses EIRENE densities.
        """
        self._na_atom = self._sum_cs(self.na_eirene)

    @solps_property
    def fractional_abundances(self):
        """Fractional ion abundances as they appear in the simulation"""
        natot = np.zeros((self.nx,self.ny,self.ns))
        natm = 0
        zn = self.zn[0]
        for ns in range(self.ns):
            if zn != self.zn[ns]:
                zn = self.zn[ns]
                natm += 1
            natot[:,:,ns] = self.na_atom[:,:,natm]
        self._fractional_abundances = self.na_eirene / natot


    #ATTENTION: Better name? Alias?
    #ATTENTION: Improve docstring
    @solps_property
    def cimp(self):
        """(nx,ny,ns): Impurity concentration.

        Notes
        -----
        It uses EIRENE densities
        """
        self._cimp = self.na_atom / np.expand_dims(np.sum(self.na_eirene,2),2)


    @solps_property
    def ni(self):
        """ (nx,ny): Total ion density [m^-3]"""
        self._ni = np.sum(shot.na[...,shot.za>0],2)

    # -------------------- Temperatures ---------------------------------------
    @solps_property
    def te(self):
        """(nx,ny) : Electron temperature [eV]"""
        self._read_signal('_te', 'te')

    @solps_property
    def ti(self):
        """(nx,ny) : Ion temperature [eV]"""
        self._read_signal('_ti', 'ti')

    @solps_property
    def tab2(self):
        """(nx,ny,natm) : Eirene atomic temperature [eV]"""
        self._read_signal('_tab2', 'tab2')

    @solps_property
    def tmb2(self):
        """(nx,ny,nmol) : Eirene molecular temperature [eV]"""
        self._read_signal('_tmb2', 'tmb2')

    @solps_property
    def tib2(self):
        """(nx,ny,nion) : Eirene test ion temperature [ev]"""
        self._read_signal('_tib2', 'tib2')

    # -------------------- Collisionalities -----------------------------------
    @solps_property
    def dspar_omp(self):
        self._dspar_omp = np.abs(self.ipardspar[self.omp]-self.ipardspar[self.iout])

    @solps_property
    def dspar_oxp(self):
        self._dspar_oxp = np.abs(self.ipardspar[self.oxp]-self.ipardspar[self.iout])

    @solps_property
    def dspar_imp(self):
        self._dspar_imp = np.abs(self.ipardspar[self.imp]-self.ipardspar[self.iinn])

    @solps_property
    def dspar_ixp(self):
        self._dspar_ixp = np.abs(self.ipardspar[self.ixp]-self.ipardspar[self.iinn])

    @solps_property
    def colle_omp(self):
        self._colle_omp = self.ne[self.omp]*self.dspar_omp/np.power(
                self.te[self.omp],2)*1e-16

    @solps_property
    def colli_omp(self):
        """ Only main ion. """
        self._colli_omp = self.na[self.omp,:,1]*self.dspar_omp/np.power(
                self.ti[self.omp],2)*1e-16

    @solps_property
    def colle_oxp(self):
        self._colle_oxp = self.ne[self.oxp]*self.dspar_oxp/np.power(
                self.te[self.oxp],2)*1e-16

    @solps_property
    def colli_oxp(self):
        """ Only main ion. """
        self._colli_oxp = self.na[self.oxp,:,1]*self.dspar_oxp/np.power(
                self.ti[self.oxp],2)*1e-16

    @solps_property
    def colle_imp(self):
        self._colle_imp = self.ne[self.imp]*self.dspar_imp/np.power(
                self.te[self.imp],2)*1e-16

    @solps_property
    def colli_imp(self):
        """ Only main ion. """
        self._colli_imp = self.na[self.imp,:,1]*self.dspar_imp/np.power(
                self.ti[self.imp],2)*1e-16

    @solps_property
    def colle_ixp(self):
        self._colle_ixp = self.ne[self.ixp]*self.dspar_ixp/np.power(
                self.te[self.ixp],2)*1e-16

    @solps_property
    def colli_ixp(self):
        """ Only main ion. """
        self._colli_ixp = self.na[self.ixp,:,1]*self.dspar_ixp/np.power(
                self.ti[self.ixp],2)*1e-16







    # -------------------- Pressures ------------------------------------------
    @solps_property
    def pse(self):
        """(nx,ny) : Electron static pressure [Pa]"""
        self._pse = self.qe*self.ne*self.te

    @solps_property
    def psi(self):
        """(nx,ny) : Ion static pressure  [Pa]"""
        self._psi = self.qe*np.sum(self.na[:,:,self.za>0],2)*self.ti

    @solps_property
    def pstot(self):
        """(nx,ny) : Total static pressure (electrons + ions) [Pa]"""
        self._pstot = self.pse + self.psi

    @solps_property
    def pdynd(self):
        """(nx,ny) : First ion species (D+) dynamic pressure [Pa]

        See also
        --------
        pdyna: dynamic pressure for all fluid species.
        They are slightly different because 2*mp =/= mi.
        """
        self._pdynd = 2*self.mp*(self.na[:,:,1]*self.ua[:,:,1]**2)

    #ATTENTION: Should this only encompass ions and not neutrals and
    #move the neutral dynamic pressure to other quantity?
    @solps_property
    def pdyna(self):
        """(nx,ny,ns) : Dynamic pressure (m*n*u**2) [Pa]

        Notes
        -----
        EIRENE densities are used"""
        self._pdyna = self.mi*self.na_eirene*self.ua**2

    @solps_property
    def ptot(self):
        """(nx,ny) : Total plasma pressure (static + dynamic) [Pa]"""
        self._ptot = self.pstot + np.sum(self.pdyna[...,self.za>0],2)

    @solps_property
    def pressure_gradient_force(self):
        """(nx,ny,ns) : Parallel pressure gradient force acting on the
                        whole cell volume element [N].

        Notes
        -----
        For the comparison of forces force-densities (force/self.vol)
        should be used since the total force scales with the volume of
        the considered volume element (i.e. with the cell volume)
        """

        # F = M * a
        #   = M * (-1/sum(mi*na) * grad p)
        #   = vol*mi*na * (-1/sum(mi*na) * grad p)
        #   = - vol*mi*na*grad p / sum(mi*na)
        # (with the total mass density rho = sum(mi*na))

        # Only the static pressure has to be used here:
        # (see, e.g., Stangeby (2000), Chapter 1.8.1 and Fig. 1.21)
        dp  = self.gradient(self.pstot)[...,2]     # (nx,ny)
        dp  = np.expand_dims(dp,2)                 # (nx,ny,1)
        vol = np.expand_dims(self.vol,2)           # (nx,ny,1)
        mi  = self.mi                              # (nx,ny,ns)
        na  = self.na#_eirene                      # (nx,ny,ns)
        rho = np.expand_dims(np.sum(mi*na,2),2)    # (nx,ny,1)
        self._pressure_gradient_force = -vol*mi*na*dp/rho

    # -------------------- Velocities -----------------------------------------
    @solps_property
    def ua(self):
        """(nx,ny,ns) : Parallel velocity [m/s]"""
        self._read_signal('_ua', 'ua')



    # -------------------- Fluxes and energies --------------------------------
    @solps_property
    def fnax(self):
        """(nx,ny,ns): Poloidal particle flux of species is through the x-face
        between the cell (ix,iy) and its left neighbour (ix-1,iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny,ns) to complete the dimensions.
        """
        self._read_signal('_fnax', 'fnax')
        self._fnax = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                    self._fnax), axis = 0)

    @solps_property
    def fnax32(self):
        """(nx,ny,ns): Convective poloidal particle flux of species is
        through the x-face between the cell (ix,iy) and its left
        neighbour (ix-1,iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny,ns) to complete the dimensions.
        """
        self._read_signal('_fnax32', 'fnax_32')
        self._fnax32 = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                    self._fnax32), axis = 0)

    @solps_property
    def fnax52(self):
        """(nx,ny,ns): Conductive poloidal particle flux of species is
        through the x-face between the cell (ix,iy) and its left
        neighbour (ix-1,iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny,ns) to complete the dimensions.
        """
        self._read_signal('_fnax52', 'fnax_52')
        self._fnax52 = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                    self._fnax52), axis = 0)

    @solps_property
    def fnay(self):
        """(nx,ny,ns): Radial particle flux of species is through the y-face
        between the cell (ix,iy) and its bottom neighbour (ix,iy-1) [s^-1].

        Notes
        -----
        fnay[:,0] = np.zeros(nx,ns) to complete the dimensions.
        """
        self._read_signal('_fnay', 'fnay')
        self._fnay = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                    self._fnay), axis = 1)

    @solps_property
    def fnay32(self):
        """(nx,ny,ns): Convective radial particle flux of species is
        through the y-face between the cell (ix,iy) and its bottom
        neighbour (ix,iy-1) [s^-1].

        Notes
        -----
        fnay[:,0] = np.zeros(nx,ns) to complete the dimensions.
        """
        self._read_signal('_fnay32', 'fnay_32')
        self._fnay32 = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                    self._fnay32), axis = 1)

    @solps_property
    def fnay52(self):
        """(nx,ny,ns): Conductive radial particle flux of species is
        through the y-face between the cell (ix,iy) and its bottom
        neighbour (ix,iy-1) [s^-1].

        Notes
        -----
        fnay[:,0] = np.zeros(nx,ns) to complete the dimensions.
        """
        self._read_signal('_fnay52', 'fnay_52')
        self._fnay52 = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                    self._fnay52), axis = 1)

    @solps_property
    def fna(self):
        """(nx,ny,2,ns): Particle flux of species is through cell faces [s^-1].

        Notes
        -----
        fna[...,0] poloidal flux through left face. Alias of fnax.
        fna[...,1] radial flux through bottom face. Alias of fnay.
        """
        self._fna = np.zeros((self.nx,self.ny,2,self.ns))
        self._fna[:,:,0,:] = self.fnax
        self._fna[:,:,1,:] = self.fnay


    @solps_property
    def fna32(self):
        """(nx,ny,2,ns): Convective particle flux through cell faces [s^-1].

        Notes
        -----
        fna[...,0] poloidal flux through left face. Alias of fnax32.
        fna[...,1] radial flux through bottom face. Alias of fnay32.
        """
        self._fna32 = np.zeros((self.nx,self.ny,2,self.ns))
        self._fna32[:,:,0,:] = self.fnax32
        self._fna32[:,:,1,:] = self.fnay32

    @solps_property
    def fna52(self):
        """(nx,ny,2,ns): Conductive particle flux through cell faces [s^-1].

        Notes
        -----
        fna[...,0] poloidal flux through left face. Alias of fnax52.
        fna[...,1] radial flux through bottom face. Alias of fnay52.
        """
        self._fna52 = np.zeros((self.nx,self.ny,2,self.ns))
        self._fna52[:,:,0,:] = self.fnax52
        self._fna52[:,:,1,:] = self.fnay52





    ##ATTENTION: Add docstring
    #@solps_property
    #def fnex(self):
    #    """ Definition of the manual, not the implemented b2 src code.
    #    This uses za instead of rza.
    #    Not equal in values to the one from rundir"""
    #    self._fnex = np.sum(self.za*self.fnax,2) + self.fchx/self.qe

    ##ATTENTION: Add docstring
    #@solps_property
    #def fney(self):
    #    """ Definition of the manual, not the implemented b2 src code.
    #    This uses za instead of rza.
    #    Not equal in values to the one from rundir"""
    #    self._fney = np.sum(self.za*self.fnay,2) + self.fchy/self.qe

    ##ATTENTION: Add docstring
    #@solps_property
    #def fne(self):
    #    """ Definition of the manual, not the implemented b2 src code.
    #    This uses za instead of rza.
    #    Not equal in values to the one from rundir"""
    #    self._fne = np.zeros((self.nx,self.ny,2))
    #    self._fne[...,0] = self.fnex
    #    self._fne[...,1] = self.fney


    #ATTENTION STILL NOT WORKING
    @solps_property
    def fne(self):
        """Also not the same as rundir"""
        from solpspy.b25_src.b2aux.b2xpfe import b2xpfe
        self._fne = b2xpfe(self)

    @solps_property
    def fnex(self):
        """Also not the same as rundir"""
        self._fnex = self.fne[...,0]

    @solps_property
    def fney(self):
        """Also not the same as rundir"""
        self._fney = self.fne[...,1]






    @solps_property
    def fhex(self):
        """(nx,ny): Poloidal electron heat flux through the x-face
        between the cell (ix,iy) and its left neighbour (ix-1,iy) [W].

        Notes
        -----
        fhex[0] = np.zeros(ny,ns) to complete the dimensions.
        """
        self._read_signal('_fhex', 'fhex')
        self._fhex = np.concatenate((np.zeros((1,self.ny)),
                                    self._fhex), axis = 0)

    @solps_property
    def fhey(self):
        """(nx,ny): Radial electron heat flux through the y-face
        between the cell (ix,iy) and its bottom neighbour (ix,iy-1) [W].

        Notes
        -----
        fhey[0] = np.zeros(nx,ns) to complete the dimensions.
        """
        self._read_signal('_fhey', 'fhey')
        self._fhey = np.concatenate((np.zeros((self.nx,1)),
                                    self._fhey), axis = 1)

    @solps_property
    def fhe(self):
        """(nx,ny,2): Electron heat flux through cell faces [W].

        Notes
        -----
        fhe[...,0] poloidal flux through left face. Alias of fhex.
        fhe[...,1] radial flux through bottom face. Alias of fhey.
        """
        self._fhe = np.zeros((self.nx,self.ny,2))
        self._fhe[:,:,0] = self.fhex
        self._fhe[:,:,1] = self.fhey

    @solps_property
    def fhix(self):
        """(nx,ny): Poloidal ion heat flux through the x-face
        between the cell (ix,iy) and its left neighbour (ix-1,iy) [W].

        Notes
        -----
        fhix[0] = np.zeros(ny,ns) to complete the dimensions.
        """
        self._read_signal('_fhix', 'fhix')
        self._fhix = np.concatenate((np.zeros((1,self.ny)),
                                    self._fhix), axis = 0)

    @solps_property
    def fhiy(self):
        """(nx,ny): Radial ion heat flux through the y-face
        between the cell (ix,iy) and its bottom neighbour (ix,iy-1) [W].

        Notes
        -----
        fhiy[0] = np.zeros(nx,ns) to complete the dimensions.
        """
        self._read_signal('_fhiy', 'fhiy')
        self._fhiy = np.concatenate((np.zeros((self.nx,1)),
                                    self._fhiy), axis = 1)

    @solps_property
    def fhi(self):
        """(nx,ny,2): Ionn heat flux through cell faces [W].

        Notes
        -----
        fhi[...,0] poloidal flux through left face. Alias of fhix.
        fhi[...,1] radial flux through bottom face. Alias of fhiy.
        """
        self._fhi = np.zeros((self.nx,self.ny,2))
        self._fhi[:,:,0] = self.fhix
        self._fhi[:,:,1] = self.fhiy

    @solps_property
    def fhjx(self):
        """(nx,ny): Poloidal electrostatic energy flux [W]"""
        self._read_signal('_fhjx', 'fhjx')
        self._fhjx = np.concatenate((np.zeros((1,self.ny)),
                                    self._fhjx), axis = 0)

    @solps_property
    def fhjy(self):
        """(nx,ny): Radial electrostatic energy flux [W]"""
        self._read_signal('_fhjy', 'fhjy')
        self._fhjy = np.concatenate((np.zeros((self.nx,1)),
                                    self._fhjy), axis = 1)

    @solps_property
    def fhj(self):
        """(nx,ny,2): Electrostatic energy flux [W]"""
        self._fhj = np.zeros((self.nx,self.ny,2))
        self._fhj[:,:,0] = self.fhjx
        self._fhj[:,:,1] = self.fhjy

    @solps_property
    def fhmx(self):
        """(nx,ny,ns): Poloidal parallel kinetic energy flux [W]"""
        self._read_signal('_fhmx', 'fhmx')
        self._fhmx = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                     self._fhmx), axis = 0)
    @solps_property
    def fhmy(self):
        """(nx,ny,ns): Radial parallel kinetic energy flux [W]"""
        self._read_signal('_fhmy', 'fhmy')
        self._fhmy = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                     self._fhmy), axis = 1)

    @solps_property
    def fhm(self):
        """(nx,ny,2,ns): Parallel kinetic energy flux [W]"""
        self._fhm = np.zeros((self.nx,self.ny,2,self.ns))
        self._fhm[:,:,0,:] = self.fhmx
        self._fhm[:,:,1,:] = self.fhmy

    @solps_property
    def fhpx(self):
        """(nx,ny,ns): Poloidal potential energy flux [W]"""
        self._read_signal('_fhpx', 'fhpx')
        self._fhpx = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                     self._fhpx), axis = 0)
    @solps_property
    def fhpy(self):
        """(nx,ny,ns): Radial potential energy flux [W]"""
        self._read_signal('_fhpy', 'fhpy')
        self._fhpy = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                     self._fhpy), axis = 1)

    @solps_property
    def fhp(self):
        """(nx,ny,2,ns): Potential energy flux [W]"""
        self._fhp = np.zeros((self.nx,self.ny,2,self.ns))
        self._fhp[:,:,0,:] = self.fhpx
        self._fhp[:,:,1,:] = self.fhpy

    @solps_property
    def fhtx(self):
        """(nx,ny): Poloidal total energy flux [W]"""
        self._read_signal('_fhtx', 'fhtx')
        self._fhtx = np.concatenate((np.zeros((1,self.ny)),
                                     self._fhtx), axis = 0)
    @solps_property
    def fhty(self):
        """(nx,ny): Radial total energy flux [W]"""
        self._read_signal('_fhty', 'fhty')
        self._fhty = np.concatenate((np.zeros((self.nx,1)),
                                    self._fhty), axis = 1)

    @solps_property
    def fht(self):
        """(nx,ny,2): Total energy flux [W].

        Notes
        -----
        fht = fhe + fhi (electron and ion heat fluxes)
            + fhj (electrostatic energy flux)
            + fhm (kinectic energy flux)
            + fhp (potential energy flux)
           [+ fnt (addition to heat fluxes, only for SOLPS-ITER)]
        """
        self._fht = np.zeros((self.nx,self.ny,2))
        self._fht[:,:,0] = self.fhtx
        self._fht[:,:,1] = self.fhty


    @solps_property
    def fchx(self):
        """(nx,ny): Poloidal current through left face [A]."""
        self._read_signal('_fchx', 'fchx')
        self._fchx = np.concatenate((np.zeros((1,self.ny)),
                                    self._fchx), axis = 0)

    @solps_property
    def fchy(self):
        """(nx,ny): Radial current through bottom face [A]."""
        self._read_signal('_fchy', 'fchy')
        self._fchy = np.concatenate((np.zeros((self.nx,1)),
                                    self._fchy), axis = 1)

    @solps_property
    def fch(self):
        """(nx,ny,2): Total current [A]."""
        self._fch = np.zeros((self.nx,self.ny,2))
        self._fch[:,:,0] = self.fchx
        self._fch[:,:,1] = self.fchy


    #ATTENTION: Add docstrig
    #ATTENTION: STILL NOT SAME AS RUNDIR
    @solps_property
    def fnt(self):
        """ Not yet tested"""
        fnt = np.zeros((self.nx,self.ny,2))
        for ny in xrange(self.ny):
            for nx in xrange(self.nx):

                # poloidal component
                if self.leftix[nx,ny] >= 0:
                    for ns in range(self.ns):
                        fnt[nx,ny,0] += (0.5*self.fna[nx,ny,0,ns]*
                                self.qe*(self.ti[self.leftix[nx,ny],
                                                  self.leftiy[nx,ny]]
                                        + self.ti[nx,ny]))

                    fnt[nx,ny,0] += (0.5*self.fne[nx,ny,0]*
                               self.qe*(self.te[self.leftix[nx,ny],
                                                self.leftiy[nx,ny]]
                                        + self.te[nx,ny]))
                else:
                    fnt[nx,ny,0] = 0

                # radial component
                if self.bottomiy[nx,ny] >= 0:
                    for ns in range(self.ns):
                        fnt[nx,ny,1] += (0.5*self.fna[nx,ny,1,ns]*
                               self.qe*(self.ti[self.bottomix[nx,ny],
                                                self.bottomiy[nx,ny]]
                                        + self.ti[nx,ny]))

                    fnt[nx,ny,1] += (0.5*self.fne[nx,ny,1]*
                               self.qe*(self.te[self.bottomix[nx,ny],
                                                self.bottomiy[nx,ny]]
                                        + self.te[nx,ny]))
                else:
                    fnt[nx,ny,1] = 0

        self._fnt = fnt



    @solps_property
    def fmox(self):
        """(nx,ny,ns) : Total momentum flux on the left face of (ix,iy) [N]"""
        self._read_signal('_fmox', 'fmox')
        self._fmox =  np.concatenate((np.zeros((1,self.ny,self.ns)),
                                     self._fmox), axis = 0)

    @solps_property
    def fmoy(self):
        """(nx,ny,ns) : Total momentum flux on the bottom face of (ix,iy) [N]"""
        self._read_signal('_fmoy', 'fmoy')
        self._fmoy = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                    self._fmoy), axis = 1)

    #ATTENTION: Was the previous description better?
    ##(nx,ny) : Stored energy per cell volume as given by the kinetic
    ##          energies of ions and electrons [J/m^3].
    @solps_property
    def stored_energy(self):
        """(nx,ny) : Stored kinetic energy of ions and electrons
        per cell volume [J/m^3].

        Notes
        -----

         E_stored = 3/2*k_B*T*n
                  = 3/2*k_B*(Te*ne + Ti*sum(na))
                  = 3/2*p
         (See e.g. "Thomas Puetterich - Control and Diagnostic of High-Z
                                        Impurities in Fusion Plasmas", p.16ff,
               or  "Vladislav Kotov   - Talk Juelich 2016-09-22)
        """
        Ee =  3./2.*self.pse               # = 3/2*ne*te
        Ei = (3./2.*self.psi +             # = 3/2*sum(na*ti)
              1./2.*np.sum(self.pdyna,2))  # = 1/2*sum(mi*na*ua**2)
        self._stored_energy = Ee+Ei



    # -------------------- Coefficients ---------------------------------------
    #ATTENTION: Add docstrig
    @solps_property
    def dpa0(self):
        self._read_signal('_dpa0', 'dpa0')
    @solps_property
    def dna0(self):
        self._read_signal('_dna0', 'dna0')
    @property
    def dperp(self):
        """Alias for dna0"""
        return self.dna0

    #ATTENTION: Add docstrig
    @solps_property
    def kyeperp(self):
        self._read_signal('_kyeperp',  'kye')

    #ATTENTION: Add docstrig
    @solps_property
    def kyiperp(self):
        self._read_signal('_kyiperp',  'kyi')

    #ATTENTION: Add docstrig
    @solps_property
    def rpt(self):
        self._read_signal('_rpt',  'rpt')

    # -------------------- Rates, losses/sources and residuals ----------------
    #ATTENTION: Are they correct??
    @solps_property
    def rqahe(self):
        """(nx,ny,ns) : Electron cooling rate [W]"""
        self._read_signal('_rqahe', 'rqahe')
    @solps_property
    def rqbrm(self):
        """(nx,ny,ns) : Bremsstrahlung radiation rate [W]"""
        self._read_signal('_rqbrm', 'rqbrm')

    @solps_property
    def rqrad(self):
        """(nx,ny,ns) : B2 line radiation rate [W]

        Notes
        -----
        Use self.line_radiation for runs with Eirene.
        """
        self._read_signal('_rqrad', 'rqrad')

    @solps_property
    def line_radiation(self):
        """ (nx,ny,ns) : Corrected line radiation rate [W]"""
        self._line_radiation = self.rqrad.copy()
        natm = 0
        for i, z in enumerate(self.za):
            if z == 0:
                self._line_radiation[:,:,i] = ( self.rqrad[:,:,i] /
                                                self.na[:,:,i] *
                                                self.dab2[:,:,natm] )
                natm += 1

    @solps_property
    def rrahi(self):
        """ (nx,ny,ns) : Recombination ion energy losses [W]"""
        self._read_signal('_rrahi', 'rrahi')

    @solps_property
    def rramo(self):
        """ (nx,ny,ns) : Recombination ion momentum losses [ms^-2]"""
        self._read_signal('_rramo', 'rramo')

    @solps_property
    def rrana(self):
        """ (nx,ny,ns) : Recombination ion particle losses [1/s]"""
        self._read_signal('_rrana', 'rrana')

    @solps_property
    def rsahi(self):
        """ (nx,ny,ns) : Ionization ion energy losses [W]"""
        self._read_signal('_rsahi', 'rsahi')

    @solps_property
    def rsamo(self):
        """ (nx,ny,ns) : Ionization ion momentum losses [ms^-2]"""
        self._read_signal('_rsamo', 'rsamo')

    @solps_property
    def rsana(self):
        """ (nx,ny,ns) : Ionization ion particle losses [1/s]"""
        self._read_signal('_rsana', 'rsana')

    @solps_property
    def rsana_b2stbr(self):
        """ (nx,ny,ns) : Ionization ion and neutral particle losses [1/s]

        Notes
        -----
        For the Ionization rates of the neutrals the recycling sources of
        the singly ionized charge states 'b2stbr_sna' are used."""
        # This should be the correct thing to use I think. Also
        # the particle balance works well with this quantity.
        self._rsana_b2stbr = self.rsana.copy()
        for ns in range(self.ns):
            if (self.za[ns] == 0 
                and ns+1 < self.ns
                and self.zn[ns] == self.zn[ns+1]):
                self._rsana_b2stbr[:,:,ns] = self.b2stbr_sna[:,:,ns+1]

    @solps_property
    def rsana_rna(self):
        """ (nx,ny,ns) : Rescaled ionization ion particle losses [1/s]

        Notes
        -----
        The neutrals are rescaled using the EIRENE neutral density:
        rsana_rna = rsana*dab2/na for the neutrals.
        """
        self._rsana_rna = self.rsana.copy()
        natm = 0
        for i, z in enumerate(self.za):
            if z == 0:
                self._rsana_rna[:,:,i] = ( self.rsana[:,:,i] /
                                           self.na[:,:,i] *
                                           self.dab2[:,:,natm] )
                natm += 1

    @solps_property
    def rsana_rns(self):
        """ (nx,ny,ns) : Rescaled_ionization ion particle losses [1/s]

        Notes
        -----
        This is rescaled via the 'self._rescale_neutrals' method using the
        scaling factor specified in the 'b2mndr_rescale_neutrals_sources'
        switch in b2mn.dat.
        """
        self._rsana_rns = self._rescale_neutrals(self.rsana)

    @solps_property
    def rcxmo(self):
        """(nx,ny,ns) : Ion momentum neutral losses [ms^-2]"""
        # According to the manual it is in [1/s], but probably that's a typo ...
        self._read_signal('_rcxmo','rcxmo')

    #They do not appear on SOLPS manual??
    @solps_property
    def smo(self):
        """(nx,ny,4,ns): Coefficients of the linear expression of the
        parallel momentum source of species is [N].
        """
        self._read_signal('_smo', 'smo')

    @solps_property
    def smq(self):
        """(nx,ny,4,ns): Coefficients of the linear expression of the
        total momentum source of species is [N].

        Notes
        -----
        Apparently, from atomic physics.
        """
        self._read_signal('_smq', 'smq')

    #ATTENTION: Add docstrig
    @solps_property
    def smaf(self):
        self._read_signal('_smaf', 'b2npmo_smaf')

    @solps_property
    def smag(self):
        self._read_signal('_smag', 'b2npmo_smag')

    @solps_property
    def smav(self):
        """(nx,ny,4,ns): Coefficients of the linear expression of the
        parallel momentum source of species is from additional viscosity [N].
        """
        self._read_signal('_smav', 'b2npmo_smav')

    # Residuals
    @solps_property
    def resmo(self):
        """(nx,ny,ns): Residuals of the parallel momentum equation of
        species is [N*s].
        """
        self._read_signal('_resmo', 'resmo')
        if (self._resmo is not None and
            self._resmo.shape ==(self.nx,self.ny-1,self.ns)):
              self._resmo = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                           self._resmo), axis = 1)

    @solps_property
    def respo(self):
        """(nx,ny) : Potential equation residuals [V]"""
        self._read_signal('_respo','respo')


    # -------------------- Names and texts ----------------------------------
    ## Heavily improve this section with more feedback.

    #ATTENTION: Maybe change to fluid_species
    def b2_species(self, ns):
        """ Returns index of species if given a name or name for a given
        index of the B2 fluid species.
        """
        if type(ns) != str:
            return self.species_names[ns]
        else:
            return self.species_names.index(ns)

    #ATTENTION: This only works for atoms, not molecules or test ions.
    #ATTENTION: Maybe change to atom_species.
    #ATTENTION: And add other like mol_species
    def eirene_species(self, ns):
        """ Returns index of species if given a name or name for a given
        index of EIRENE atomic species.
        """
        if type(ns) != str:
            return self.eirene_names[ns]
        else:
            return self.eirene_names.index(ns)


    ## ---------------- REVISION    warning! ------------------------
    #ATTENTION: It gives back H, not D.
    #ATTENTION: change name to eirene_names? neutral_names?
    @solps_property
    def atom_names(self):
        """list(str) : B2 neutrals names."""
        self._atom_names = []
        zn = -1
        for ns in range(self.ns):
            if zn != self.zn[ns]:
                zn = self.zn[ns]
                self._atom_names.append(elements[zn].title())

    #ATTENTION: Add docstrig
    #ATTENTION: Better name?
    @property
    def eirene_names(self):
        return self.atom_names


    #ATTENTION: It gives back H, not D.
    #ATTENTION: change name to b2_names?
    @solps_property
    def species_names(self):
        """list(str): B2 species names."""
        self._species_names = []
        for ns in range(self.ns):
            name = elements[self.zn[ns]].title()
            if   self.za[ns] == 1: name += '+'
            elif self.za[ns] == 2: name += '++'
            elif self.za[ns] >= 3: name += str(int(self.za[ns])) + '+'
            self._species_names.append(name)

    #ATTENTION: Create function instead: It returns name if given an index and
    # index if given a name
    # Maybe insert the logic here completely with the one of species_names
    #Add the logic of species_index and eirene_index too
    # Maybe add alias of fluid_species
    def b2_species(self, id):
        if type(id) != str:
            return self.species_names[id]
        else:
            return self.species_names.index(id)
    def eirene_species(self, id):
        if type(id) != str:
            return self.eirene_names[id]
        else:
            return self.eirene_names.index(id)



    # -------------------- Others (for now) ---------------------------------
    #ATTENTION: Keep?
    @property
    @try_block
    def rqrad_natm(self):
        """(nx,ny,natm) : Total radiation per isonuclear sequence [W]."""
        self._rqrad_natm = self._sum_cs(self.line_radiation)

    @property
    @try_block
    def lz(self):
        """(nx,ny,natm) : Radiation efficiency per isonuclear sequence [Wm^3].
        """

#       This is wrong:
        self._lz = self._sum_cs(self.lz_cs)

#       This is correct (according to Ralph Dux):
        self._lz = np.zeros((self.nx,self.ny,self.natm))
        pv = self.rqrad_natm / np.expand_dims(self.vol,2)
        for natm in range(self.natm):
            self._lz[:,:,natm] = pv[:,:,natm] / self.na_atom[:,:,natm] / self.ne

    @solps_property
    def lz_cs(self):
        """(nx,ny,ns) : Radiation efficiency for every charge state [Wm^3].
        """
        self._lz_cs = np.zeros((self.nx,self.ny,self.ns))
        pv = self.line_radiation / np.expand_dims(self.vol,2)
        for ns in range(self.ns):
            self._lz_cs[:,:,ns] = pv[:,:,ns] / self.na_eirene[:,:,ns] / self.ne
        self._lz_cs[np.where(np.isnan(self._lz_cs))] = 0

    @property
    def lz_adas(self):
        """(nx,ny,natm) : Expected L_z values for each cell according to their
        density and temperature values [Wm^3].
        Calculated according to the the ADAS database.

        Notes
        -----
        Alias of self.spectroscopy.lz_adas
        """
        return self.spectroscopy.lz_adas

    @property
    @try_block
    def lz_deviation(self):
        """(nx,ny,natm) : Deviation of 'real' lz values from expectation [%]."""
        self._lz_deviation = np.zeros((self.nx,self.ny,self.natm))
        for natm in range(self.natm):
            for nx in range(self.nx):
                for ny in range(self.ny):
                    if self.lz_adas[nx,ny,natm] == 0: continue
                    self._lz_deviation[nx,ny,natm] = ((self.lz[nx,ny,natm] -
                                                       self.lz_adas[nx,ny,natm])
                                                      /self.lz_adas[nx,ny,natm])


    @solps_property
    def electric_field(self):
        """(nx,ny,3) : Electric field calculated as the gradient of
                       the electric potential in x-, y- and parallel
                       direction [V/m].

        Notes
        -----
        electric_field[...,0] is the electric_field in x direction
        electric_field[...,1] is the electric_field in y direction
        electric_field[...,2] is the electric_field in parallel direction (to B)
        """
        self._electric_field = -self.gradient(self.po)

    @solps_property
    def electric_force(self):
        """(nx,ny,ns) : Parallel electrostatic force acting on the whole
                        cell volume [N]

        Notes
        -----
        For the comparison of forces force-densities (force/self.vol)
        should be used since the total force scales with the volume of
        the considered volume element (i.e. with the cell volume)
        """
        e = self.masks.rlcl*self.electric_field[...,2]   # (nx,ny)
        e = np.expand_dims(e,2)                          # (nx,ny, 1)
        z = np.expand_dims(np.expand_dims(self.za,0),0)  # ( 1, 1,ns)
        n = np.expand_dims(self.vol,2)*self.na           # (nx,ny,ns)
        q = self.qe*n*z # Charge per species in the whole cell volume
        self._electric_force = q*e                       # (nx,ny,ns)

    # -------------------- Regions -----------------------------------------
    # -------------------- Globals -----------------------------------------
















#Create an avg function: self.avg('dab2', 'outdiv', along/dim='x'/'y'/'all')
#   # ONLY TEMPORAL
    @property
    @try_block
    def outdiv_nolim(self):
        self._outdiv_nolim = self.masks.outdiv.copy()
        self._outdiv_nolim[:,0] = np.zeros(self.nx, dtype=np.bool)
        self._outdiv_nolim[:,-1] = np.zeros(self.nx, dtype=np.bool)

    @property
    def dab2_avg(self):
        return self.average(self.dab2, self.outdiv_nolim)

    @property
    def na_avg(self):
        return self.average(self.na, self.outdiv_nolim)

    @property
    def dmb2_avg(self):
        return self.average(self.dmb2, self.outdiv_nolim)

    @property
    def dab2_line(self):
        return self.average(self.dab2, self.outdiv_nolim, dimension='x')

    @property
    def na_line(self):
        return self.average(self.na, self.outdiv_nolim, dimension='x')

    @property
    def dmb2_line(self):
        return self.average(self.dmb2, self.outdiv_nolim, dimension='x')



#    @property
#    @try_block
#    def dab2_line(self):
#        self._dab2_line = np.zeros((self.ny,self.natm))
#        for k in xrange(self.ny):
#          if self.outdiv_nolim[:,k].any():
#            for l in xrange(self.natm):
#                self._dab2_line[k,l] = (
#                    np.sum(self.dab2[self.outdiv_nolim[:,k],k,l]*
#                               self.dv[self.outdiv_nolim[:,k],k])/
#                    np.sum(self.dv[self.outdiv_nolim[:,k],k]))
#    @property
#    @try_block
#    def na_line(self):
#        self._na_line = np.zeros((self.ny,self.ns))
#        for k in xrange(self.ny):
#          if self.outdiv_nolim[:,k].any():
#            for l in xrange(self.ns):
#                self._na_line[k,l] = (
#                    np.sum(self.na[self.outdiv_nolim[:,k],k,l]*
#                               self.dv[self.outdiv_nolim[:,k],k])/
#                    np.sum(self.dv[self.outdiv_nolim[:,k],k]))
#    @property
#    @try_block
#    def dmb2_line(self):
#        self._dmb2_line = np.zeros((self.ny,self.nmol))
#        for k in xrange(self.ny):
#          if self.outdiv_nolim[:,k].any():
#            for l in xrange(self.nmol):
#                self._dmb2_line[k,l] = (
#                    np.sum(self.dmb2[self.outdiv_nolim[:,k],k,l]*
#                               self.dv[self.outdiv_nolim[:,k],k])/
#                    np.sum(self.dv[self.outdiv_nolim[:,k],k]))
#



#                   ============== EXTENSIONS ================
    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = Masks(self)

    @solps_property
    def plot(self):
        """Class for plot routines."""
        self._plot = Plots(self)

    @solps_property
    def chords(self):
        """A dictionary containing lines of sight as Chords() objects."""
        self._chords = Chords(self)

    @solps_property
    def spectroscopy(self):
        """An object for calculation & plotting of spectroscopic ADAS data."""
        self._spectroscopy = Spectroscopy(self)






#                   ============== FUNCTIONS =================

    @classmethod
    def help(cls, var):
        """
        Returns docstring of requested property

        TODO
        ----
        Add possibility of calling, e.g, help('out.te')
        i.e, another layer
        Also useful for all the other inner classes (balance, etc)
        """
        try:
            print(getattr(cls, var).__doc__)
        except:
            print("No docstring yet.")


    @solps_property
    def docstrings(self):
        """ Dictionary with the docstrings of all methods and properties."""
        _cls = self.__class__
        mro = _cls.mro()
        mro.reverse()
        del(mro[0]) #Delete 'object' docstrings.

        self._docstrings = {}
        for clss in mro:
            clssdict = {var: vobj.__doc__
                for var, vobj in clss.__dict__.iteritems()}
            self._docstrings = update(self._docstrings, clssdict)


    def find(self, regex):
        """
        Match regex pattern(s) against all the docstring.

        TODO
        ----
        Needs to be recursive, i.e., to go deep into extensions, etc.
        """

        if isinstance(regex, str):
            regex = [regex]

        nomatch=True

        for var, doc in self.docstrings.iteritems():
            if doc:
                found = True
                for reg in regex:
                    res = re.search(
                        reg, doc.replace('\n','').replace('   ',''),
                        re.IGNORECASE)
                    if res is None:
                        found = False

                if found:
                    nomatch=False
                    print('-'*20)
                    print(var)
                    print(doc)
                    print('-'*20)

        if nomatch:
            print("No matching docstring found.")

        return




    def read_signal(self, signal_name, signal_link, attach_to = None):
        """ Retrieve data from the MDS+, given tree link and signal name.

        read_signal does not return any value. It automatically attach
        the signal as an attribute with a given name to the given object
        or self.

        Parameters
        ----------
        signal_name : str
            Storage name for the retrieve signal.
        signal_link : str
            SOLPS tree link to signal in the MDS+ directory.
            It is described in the SOLPS* user manuals.
        attach_to : object, optional
            signal_name will be attached as attribute to object
            attach_to. Default to self.

        Returns
        -------
        None
        """
        if not self.conn_open:
            self.conn.openTree('solps', self.shot)
            self.conn_open=True
        if not attach_to:
            attach_to = self

        try:
            setattr(attach_to, signal_name, self.conn.get(signal_link).value.T)
        except:
            self.log.warning("error reading '{1}' for '{0}'".format(signal_name,
                                                                 signal_link))
            setattr(attach_to, signal_name, None)

        if self.conn_open:
            self.conn.closeTree('solps', self.shot)
            self.conn_open = False



    def average(self, var, gridmask=None, dimension='xy', volume_average=True,
            full_array=False):
        """Calculates the average value of var in the region defined
        by the grid mask.

        Parameters
        ----------

        var : (n1,n2) or (n1, n2, n3) array.
            Quantity to be averaged.

        gridmask : (n1,n2) boolean like array, optional.
            Mask defining the average region.
            Default: Entire grid.

        dimension : str, optional.
            Dimension in which the average is applied.
            'x' makes the average in the poloidal direction (at least n2).
            'y' in the radial direction (at least n1).
            'xy' makes the average over the entire grid section.
            Default: 'xy'.

        volume_average: bool, optional.
            If volume_average is True, then the average is weighted by the
            cell volume. Otherwise, a simple average is used.

        full_array: bool, optional.
            If full_array is True, then the return value is always (n1,n2)
            or (n1,n2,n3). If False, then it only returns indexes which had
            at least one True in the direction of average.


        Returns
        -------
        (n3), (n1,n3) or (n2,n3) array, depending on the dimension parameter.


        TODO
        ----
        Think about returning a proper array instead of a list when n3>1.
        """
        nx = var.shape[0]
        ny = var.shape[1]
        if gridmask is None:
            gridmask = np.ones((nx, ny), dtype=np.bool)
        gmask = gridmask.astype(np.int)


        #import pdb; pdb.set_trace()
        if len(var.shape) == 3:
            # If dimensions are not (nx,ny) then use recursivity.
            ns = var.shape[2]
            avr = []
            for n in xrange(ns):
                avr.append(self.average(var[:,:,n],
                    gridmask, dimension, volume_average))
            return avr

        if volume_average:
            # Average weighted with the cell volume:
            if dimension == 'xy':
                return np.sum(gmask*var*self.vol) / np.sum(gmask*self.vol)

            elif dimension == 'x':
                tmp = np.full(ny, np.nan)
                msk = np.any(gridmask,0)
                for iy in xrange(ny):
                    if msk[iy] == False:
                        continue
                    tmp[iy] = (np.sum(gmask[:,iy]*var[:,iy]*self.vol[:,iy])/
                               np.sum(gmask[:,iy]*self.vol[:,iy]))
                return tmp

            elif dimension == 'y':
                tmp = np.full(nx, np.nan)
                msk = np.any(gridmask,1)
                for ix in xrange(nx):
                    if msk[ix] == False:
                        continue
                    tmp[ix] = (np.sum(gmask[ix]*var[ix]*self.vol[ix])/
                               np.sum(gmask[ix]*self.vol[ix]))
                return tmp


        else:
            # Only averaged over cell amount:
            if dimension == 'xy':
                return np.sum(gmask*var) / np.sum(gmask)

            elif dimension == 'x':
                tmp = np.full(ny, np.nan)
                msk = np.any(gridmask,0)
                for iy in xrange(ny):
                    if msk[iy] == False:
                        continue
                    tmp[iy] = np.sum(gmask[:,iy]*var[:,iy])/np.sum(gmask[:,iy])
                return tmp

            elif dimension == 'y':
                tmp = np.full(nx, np.nan)
                msk = np.any(gridmask,1)
                for ix in xrange(nx):
                    if msk[ix] == False:
                        continue
                    tmp[ix] = np.sum(gmask[ix]*var[ix])/np.sum(gmask[ix])
                return tmp



    def sum(self, var, gridmask=None, dimension='xy', volume_average=True,
            full_array=False):
        """Calculates the sum value of var in the region defined
        by the grid mask.

        Parameters
        ----------

        var : (n1,n2) or (n1, n2, n3) array.
            Quantity to be averaged.

        gridmask : (n1,n2) boolean like array, optional.
            Mask defining the average region.
            Default: Entire grid.

        dimension : str, optional.
            Dimension in which the average is applied.
            'x' makes the average in the poloidal direction (at least n2).
            'y' in the radial direction (at least n1).
            'xy' makes the average over the entire grid section.
            Default: 'xy'.

        full_array: bool, optional.
            If full_array is True, then the return value is always (n1,n2)
            or (n1,n2,n3). If False, then it only returns indexes which had
            at least one True in the direction of average.


        Returns
        -------
        (n3), (n1,n3) or (n2,n3) array, depending on the dimension parameter.


        TODO
        ----
        Think about returning a proper array instead of a list when n3>1.
        """
        nx = var.shape[0]
        ny = var.shape[1]
        if gridmask is None:
            gridmask = np.ones((nx, ny), dtype=np.bool)
        gmask = gridmask.astype(np.int)


        #import pdb; pdb.set_trace()
        if len(var.shape) == 3:
            # If dimensions are not (nx,ny) then use recursivity.
            ns = var.shape[2]
            avr = []
            for n in xrange(ns):
                avr.append(self.sum(var[:,:,n],
                    gridmask, dimension))
            return avr

        if True:
            if dimension == 'xy':
                return np.sum(gmask*var)

            elif dimension == 'x':
                tmp = np.full(ny, np.nan)
                msk = np.any(gridmask,0)
                for iy in xrange(ny):
                    if msk[iy] == False:
                        continue
                    tmp[iy] = (np.sum(gmask[:,iy]*var[:,iy]))
                return tmp

            elif dimension == 'y':
                tmp = np.full(nx, np.nan)
                msk = np.any(gridmask,1)
                for ix in xrange(nx):
                    if msk[ix] == False:
                        continue
                    tmp[ix] = (np.sum(gmask[ix]*var[ix]))
                return tmp




    #ATTENTION: Complete when understood the logic of the matlab indexing
    #ATTENTION: DO NOT USE YET.
    #At least it is known that giving x and y or just x give same result.
    def divergence(self, flux_x, flux_y=None):
        """ Calculates the divergences of different fluxes for every grid cell,
        following $SOLPSTOP/scrits/matlab/calc_divergence.m.

        Inputs
        ------
        flux_x: (nx,ny) or (nx,ny,ns) or (nx,ny,2) or (nx,ny,2,ns).
            x component of the flux which divergence is wanted.
            It can also be the merge version of both x and y fluxes.
            In that case, it will be separated inside the function.

        flux_y: (nx,ny) or (nx,ny,ns), optional.
            y component of the flux which divergence is wanted.

        Returns
        -------
        (div_x, div_y, div):  (nx,ny,3) or (nx,ny,ns,3)

            div_x: (nx,ny) or (nx,ny,ns)
                Divergence in the x-direction.
            div_y:
                Divergence in the y-direction.
            div:
                Total divergence in the cell.
        """
        if flux_y is None:
            flux_y = flux_x[:,:,1]
            flux_x = flux_x[:,:,0]

        if flux_x.shape != flux_y.shape:
            self.log.error("Dimensions of flux_x and flux_y must be the same.")

        div_x =  np.zeros_like(flux_x)
        div_y =  np.zeros_like(flux_x)
        div   =  np.zeros_like(flux_x)
        tmp   =  np.zeros(list(itertools.chain(flux_x.shape,[3])))

        for ix in xrange(self.nx):
            for iy in xrange(self.ny):
                div_x[ix,iy] = -flux_x[ix,iy]
                div_y[ix,iy] = -flux_y[ix,iy]
                ## ATTENTION: The following doesn't make sense!!!
                #if self.topiy[ix,iy] != self.ny:
                #    div_y[ix,iy] += flux_y[self.topiy[ix,iy],iy]
                #if self.rightix[ix,iy] != self.nx:
                #    div_x[ix,iy] += flux_x[ix, self.rightix[ix,iy]]

        tmp[...,0] = div_x
        tmp[...,1] = div_y
        tmp[...,2] = div_x + div_y
        return tmp









#    #ATTENTION: Improve docstring
#    def region_average(self,var,gridmask,volume_average=True):
#        """Calculates the average value of var in the region defined
#        by the grid mask.
#        """
#
#        if len(var.shape) == 3:
#            avr = []
#            for n in range(var.shape[2]):
#                avr.append(self.region_average(var[:,:,n],
#                                               gridmask,
#                                               volume_average))
#            return avr
#
#        if volume_average:
#            # Average weighted with the cell volume:
#            return np.sum(gridmask*var*self.vol) / np.sum(gridmask*self.vol)
#        else:
#            # Only averaged over cells:
#            return np.sum(gridmask*var) / np.sum(gridmask)
#
    #ATTENTION: Better name?
    def map_cell_faces_to_center(self,var,direction='xy'):
        """Calculates the average value of 'var' in the cell center,
        e.g. for fluxes (which are stored on the cell faces) as it
        is also done in B2Plot.

        Parameters
        ----------
            var : np.array(nx,ny,...)

            direction : String 'x', 'y' or 'xy' (default)
                Specifies the direction of the mapping; for 'xy' does the
                mapping in both directions if var.shape == (...,2,...)

        Return Value
        ------------
            np.array(nx,ny,...)
        """

        if var.shape[0:2] != (self.nx,self.ny):
            self.log.error('Invalid shape of variable array: %s',var.shape)
            return None

        retval = np.zeros(var.shape)

        if direction == 'xy':
            if var.shape[-2] == 2:
                retval[...,0,:]=self.map_cell_faces_to_center(var[...,0,:],'x')
                retval[...,1,:]=self.map_cell_faces_to_center(var[...,1,:],'y')
            elif var.shape[-1] == 2:
                retval[...,0] = self.map_cell_faces_to_center(var[...,0],'x')
                retval[...,1] = self.map_cell_faces_to_center(var[...,1],'y')
            else:
                self.log.error("Invalid shape of variable array for"
                               + " direction 'xy': %s",var.shape)
                return None
        elif direction == 'x':
            for nx in range(self.nx):
                for ny in range(self.ny):
                    if nx == 0:
                        retval[nx,ny,...] = var[int(self.rightix[nx,ny]),
                                                int(self.rightiy[nx,ny]),...]
                    elif nx == self.nx-1:
                        retval[nx,ny,...] = var[nx,ny,...]
                    else:
                        retval[nx,ny,...] = 0.5*(var[nx,ny,...] +
                                                 var[int(self.rightix[nx,ny]),
                                                     int(self.rightiy[nx,ny]),...])
        elif direction == 'y':
            for nx in range(self.nx):
                for ny in range(self.ny):
                    if ny == 0:
                        retval[nx,ny,...] = var[int(self.topix[nx,ny]),
                                                int(self.topiy[nx,ny]),...]
                    elif ny == self.ny-1:
                        retval[nx,ny,...] = var[nx,ny,...]
                    else:
                        retval[nx,ny,...] = 0.5 * (var[nx,ny,...] +
                                                   var[int(self.topix[nx,ny]),
                                                       int(self.topiy[nx,ny]),...])
        else:
            self.log.error('Invalid direction: %s',direction)
            return None

        return retval

    #ATTENTION: Improve docstring
    def gradient(self,var):
        """ Returns the gradient of 'var' for all cells. The last dimension 
        of the output distinguishes the gradients in x-, y- and in parallel
        direction.

        Parameters
        ----------
            var : np.array(nx,ny,...)

        Return Value
        ------------
            gradient : np.array(nx,ny,...,3)

        Notes
        -----
            gradient[...,0] is the gradient in x direction (poloidal)
            gradient[...,1] is the gradient in y direction (radial)
            gradient[...,2] is the gradient in parallel direction (to B)
        """

        if var.shape[0:2] != (self.nx,self.ny):
            self.log.error('Invalid shape of variable array: %s',var.shape)
            return

        # Shape + (3,) for x/y/parallel direction:
        grad = np.zeros(var.shape+(3,))

        # Use the 'corrected' cell width as used in b2plot
        # for the calculation of the y-cell distances:
        hy1 = self.hy*self.qz[:,:,1]
        hx  = self.hx
        hp  = self.hx*np.abs(self.bb[...,3]/self.bb[...,0])

        # Calculate gradients:
        for y in range(self.ny):
            for x in range(self.nx):
                xl = max(int(self.leftix  [x,y]),0)         # (Lower  x-value)
                yl = max(int(self.bottomiy[x,y]),0)         # (Lower  y-value)
                xh = min(int(self.rightix [x,y]),self.nx-1) # (Higher x-value)
                yh = min(int(self.topiy   [x,y]),self.ny-1) # (Higher y-value)
                # x Distance:
                dx = hx[xl,y]/2 + hx[xh,y]/2
                if x != xl and x != xh: dx += hx[x,y]
                # y Distance:
                dy = hy1[x,yl]/2 + hy1[x,yh]/2
                if y != yl and y != yh: dy += hy1[x,y]
                # Parallel Distance:
                dp = hp[xl,y]/2 + hp[xh,y]/2
                if x != xl and x != xh: dp += hp[x,y]
                # Gradients in x, y and parallel direction:
                grad[x,y,...,0] = (var[xh,y,...] - var[xl,y,...]) / dx
                grad[x,y,...,1] = (var[x,yh,...] - var[x,yl,...]) / dy
                grad[x,y,...,2] = (var[xh,y,...] - var[xl,y,...]) / dp

        return grad



    #ATTENTION: Not obvious. Poor naming
    #Should be called eirene_names and eirene_index?
    def atom_index(self,atom):
        """Returns the natm index for the specified atom."""
        if atom == 'D':
            atom = 'H'
        return self.atom_names.index(atom.title())

    #ATTENTION: Not obvious. Poor naming
    #Should be called b2_names and b2_index?
    def species_index(self,species):
        """Returns a list of species indizes calculated from the parameter
        'species'. This 'species' variable can be a list of species names
        (like ['N6+,N7+]') or a range of species, like 'N+-N5+' or a single
        species string (like 'N++')."""

        def sti(string): # string_to_index
            if type(string) == int: return string
            s = string.title()
            if s == 'D':  s = 'H'
            if s == 'D+': s = 'H+'
            if s in self.species_names: return self.species_names.index(s)
            s = s.replace('2+','++')
            if s in self.species_names: return self.species_names.index(s)
            # Else:
            self.log.error("Invalid species '%s'",string)

        if species == 'all':
            return range(self.ns)
        elif type(species) == int:
            return [species]
        elif type(species) == str:
            if '-' in species:
                sr = species.split('-')
                return range(sti(sr[0]),sti(sr[1])+1)
            else:
                return [sti(species)]
        elif type(species) == list:
            rlist = []
            for item in species:
                rlist.append(sti(item))
            return rlist


    #ATTENTION: Better name?
    @staticmethod
    def iy_closer_to(ds, dist=0):
        """ Calculates the closest radial index to a certain distance
        from the separatrix.

        Parameters
        ----------
        ds: np array, [ny]
            Values of the distance from the separatrix, ds
        dist : float
            Value of the desired distance from the separatrix.
            Default: 0

        Results
        -------
        ind: int
            Index of ds which contains the value which minimizes |ds-dist|

        """
        same_sign = (np.sign(ds) == np.sign(dist))
        min_dist = np.inf
        ind = None
        for i in range(ds.shape[0]):
            if same_sign[i]:
                if np.abs(ds[i]-dist)< min_dist:
                    min_dist = np.abs(ds[i]-dist)
                    ind = i
        return ind


    # ATTENTION: This should be replaced in the final version
    def momentum_balance(self, **kwargs):
        """ This works. Use this for now"""
        import solpspy.extensions.mockup_balance as blc
        blc.momentum_balance(self, **kwargs)








#                   ====== FUNCTIONS FOR INTERNAL USE ========
    def _get_basic_dimensions(self):
        """
        Retrieve basic dimensions: nx, ny, ns, natm, nmol, nion
        """
        self._read_signal('nx', 'nx')
        if self.nx is None:
            self.log.warning('nx could not be retrieved')
        else:
            self.nx = int(self.nx)

        self._read_signal('ny', 'ny')
        if self.ny is None:
            self.log.warning('ny could not be retrieved')
        else:
            self.ny = int(self.ny)

        self._read_signal('ns', 'ns')
        if self.ns is None:
            self.log.warning('ns could not be retrieved')
        else:
            self.ns = int(self.ns)

        no_eirene_dims=False
        self._read_signal('natm', 'natm')
        if self.natm is None:
            self.log.warning('natm could not be retrieved')
            no_eirene_dims=True
        else:
            self.natm = int(self.natm)

        self._read_signal('nmol', 'nmol')
        if self.nmol is None:
            self.log.warning('nmol could not be retrieved')
            no_eirene_dims=True
        else:
            self.nmol = int(self.nmol)

        self._read_signal('nion', 'nion')
        if self.nion is None:
            self.log.warning('nion could not be retrieved')
            no_eirene_dims=True
        else:
            self.nion = int(self.nion)

        if no_eirene_dims:
            self.b2standalone = True
            self.log.warning("Assuming b2 standalone run.")

        return


    def _read_signal(self, signal_name, var_name, attach_to = None):
        """ Retrieve data from the MDS+, given tree link and signal name.

        read_signal does not return any value. It automatically attach
        the signal as an attribute with a given name to the given object
        or self.

        Parameters
        ----------
        signal_name : str
            Storage name for the retrieve signal.
        var_name : str
            Variable name to retrieve signal. Compatible with Rundir names.
        attach_to : object, optional
            signal_name will be attached as attribute to object
            attach_to. Default to self.

        Returns
        -------
        None
        """


        if not self.conn_open:
            self.conn.openTree('solps', self.shot)
            self.conn_open=True

        if not attach_to:
            attach_to = self

        signal_link = self._MDSTREE(var_name)
        if signal_link:
            try:
                setattr(attach_to, signal_name, self.conn.get(signal_link).value.T)
            except:
                self.log.warning("error reading '{1}' for '{0}'".format(signal_name,
                                                                     signal_link))
                setattr(attach_to, signal_name, None)
        else:
            setattr(attach_to, signal_name, None)

        if self.conn_open:
            self.conn.closeTree('solps', self.shot)
            self.conn_open = False

    # ATTENTION: Read defaults from config file
    # ATTENTION: Improve docstring
    def _check_avail(self):
        """
        Check which files are avail in the for this MdsData.
        Include also a check for size, correction, etc.
        """
        #This is more like a tool than a method
        #And this does not work
        #self._find_file_module_path('.spectroscopy_data_shot_' +
        #                            str(self.shot) + '.pkl')

        tmp = self._find_nosolps_file(
                ['spectroscopy_data_shot_'+str(self.shot)+'.pkl',
                 '.spectroscopy_data_shot_'+str(self.shot)+'.pkl',
                 'spectroscopy_shot_'+str(self.shot)+'.pkl',
                 '.spectroscopy_shot_'+str(self.shot)+'.pkl',
                 'spectroscopy_'+str(self.shot)+'.pkl',
                 '.spectroscopy_'+str(self.shot)+'.pkl'],
                os.path.abspath('.'),
                os.path.join(os.path.abspath('.'), 'spectroscopy'),
                os.path.join(os.path.abspath('.'), '.spectroscopy'),
                os.path.join(os.path.abspath('.'), 'data/spectroscopy'),
                os.path.join(os.path.abspath('.'), 'data/.spectroscopy'),
                os.path.join(self.module_path, 'data/spectroscopy'),
                os.path.join(self.module_path, 'data/.spectroscopy'))
        if tmp:
            self.avail['spectroscopy_data'] = tmp


    #ATTENTION: Add docstrig
    def _find_nosolps_file(self,names, *paths):
        if isinstance(names, basestring):
            names = [names]
        else:
            names = list(names)
        for path in paths:
            for name in names:
                if os.path.isfile(os.path.join(path,name)):
                    return os.path.join(path,name)
        return None

    def _MDSTREE(self, var_name):
        """
        Contains dict name:signal of the MDS+ tree

        Note: IDENT contains many repeated keys.
        Names should be changed as to include them
        without conflict.

        SOLPS-ITER MDS+ tree includes some signals not present in
        SOLPS 5.0.
        Therefore, look for name in both, but send an warning message.
        """

        ident = '\IDENT::TOP:'
        identimp = '\IDENT::TOP.IMPSEP'
        identomp = '\IDENT::TOP.OMPSEP'

        snapshot_root = '\SOLPS::TOP.SNAPSHOT:'
        snaptopdims = '\SNAPSHOT::TOP.DIMENSIONS:'
        snaptopgrid = '\SNAPSHOT::TOP.GRID:'
        snaptop = '\SNAPSHOT::TOP:'

        timedept1 = '\TIMEDEP::TOP.TARGET1:'
        timedept2 = '\TIMEDEP::TOP.TARGET2:'
        timedept3 = '\TIMEDEP::TOP.TARGET3:'
        timedept4 = '\TIMEDEP::TOP.TARGET4:'

        timedepomp = '\TIMEDEP::TOP.OMP:'
        timedepimp = '\TIMEDEP::TOP.IMP:'

        MDSTREE_SOLPS50 ={
        #----------------------------- IDENT ----------------------------------
                'user':ident+'USER',
                'version':ident+'VERSION',
                'solpsversion':ident+'SOLPSVERSION',
                'directory':ident+'directory',
                'exp':ident+'EXP',


        #---------------------------- SNAPSHOT --------------------------------
                    # Dimensions
                'nx':snaptopdims+'NX',
                'ny':snaptopdims+'NY',
                'ns':snaptopdims+'NS',
                'natm':snaptopdims+'NATM',
                'nion':snaptopdims+'NION',
                'nmol':snaptopdims+'NMOL',

                    # Indices
                'imp':snaptopdims+'IMP',
                'omp':snaptopdims+'OMP',
                'sep':snaptopdims+'SEP',
                'targ1':snaptopdims+'TARG1',
                'targ2':snaptopdims+'TARG2',
                'targ3':snaptopdims+'TARG3',
                'targ4':snaptopdims+'TARG4',
                'regflx':snaptopgrid+'REGFLX',
                'regfly':snaptopgrid+'REGFLY',
                'regvol':snaptopgrid+'REGVOL',


                    # Neighbours
                'bottomix':snaptopgrid+'BOTTOMIX',
                'bottomiy':snaptopgrid+'BOTTOMIY',
                'leftix':snaptopgrid+'LEFTIX',
                'leftiy':snaptopgrid+'LEFTIY',
                'rightix':snaptopgrid+'RIGHTIX',
                'rightiy':snaptopgrid+'RIGHTIY',
                'topix':snaptopgrid+'TOPIX',
                'topiy':snaptopgrid+'TOPIY',

                    # Grid and vessel geometry
                'r':snaptopgrid+'R',
                'z':snaptopgrid+'Z',
                'cr':snaptopgrid+'CR',
                'cr_x':snaptopgrid+'CR_X',
                'cr_y':snaptopgrid+'CR_Y',
                'cz':snaptopgrid+'CZ',
                'cz_x':snaptopgrid+'CZ_X',
                'cz_y':snaptopgrid+'CZ_Y',
                'hx':snaptop+'HX',
                'hy':snaptop+'HY',
                'hy1':snaptop+'HY1',
                'dspar':snaptopgrid+'DSPAR',
                'dspol':snaptopgrid+'DSPOL',
                'dsrad':snaptopgrid+'DSRAD',
                'sx':snaptop+'SX',
                'sxperp':snaptop+'SXPERP',
                'sy':snaptop+'SY',
                'vessel':snaptopgrid+'VESSEL',

                    # Plasma characterization
                'am':snaptopgrid+'AM',
                'za':snaptopgrid+'ZA',
                'zn':snaptopgrid+'ZN',
                'bb':snaptop+'B',
                'po':snaptop+'PO',
                'pot':snaptopgrid+'POT',
                'poti':snaptopgrid+'POTI',
                'qz':snaptop+'QZ',
                'visc':snaptop+'VISC',
                'vol':snaptop+'VOL',

                    # Densities
                'ne':snaptop+'NE',
                'na':snaptop+'NA',
                'dab2':snaptop+'DAB2',
                'dmb2':snaptop+'DMB2',
                'dib2':snaptop+'DIB2',

                    # Temperatures
                'te':snaptop+'TE',
                'ti':snaptop+'TI',
                'tab2':snaptop+'TAB2',
                'tmb2':snaptop+'TMB2',
                'tib2':snaptop+'TIB2',

                    # Velocities
                'ua':snaptop+'UA',
                'vlax':snaptop+'VLAX',
                'vlay':snaptop+'VLAY',

                    # Fluxes
                'fchx':snaptop+'FCHX',
                'fchy':snaptop+'FCHY',
                'fhex':snaptop+'FHEX',
                'fhey':snaptop+'FHEY',
                'fhix':snaptop+'FHIX',
                'fhiy':snaptop+'FHIY',
                'fhjx':snaptop+'FHJX', # (not listed in the manual)
                'fhjy':snaptop+'FHJY', # (not listed in the manual)
                'fhmx':snaptop+'FHMX',
                'fhmy':snaptop+'FHMY',
                'fhpx':snaptop+'FHPX',
                'fhpy':snaptop+'FHPY',
                'fhtx':snaptop+'FHTX',
                'fhty':snaptop+'FHTY',
                'fmox':snaptop+'FMOX',
                'fmoy':snaptop+'FMOY',
                'fnax':snaptop+'FNAX',
                'fnax_32':snaptop+'FNAX_32',
                'fnax_52':snaptop+'FNAX_52',
                'fnay':snaptop+'FNAY',
                'fnay_32':snaptop+'FNAY_32',
                'fnay_52':snaptop+'FNAY_52',
                'pefa':snaptop+'PEFA',
                'pefm':snaptop+'PEFM',
                'pfla':snaptop+'PFLA',
                'pflm':snaptop+'PFLM',
                'refa':snaptop+'REFA',
                'refm':snaptop+'REFM',
                'rfla':snaptop+'RFLA',
                'rflm':snaptop+'RFLM',

                    #Coefficients
                'alf':snaptop+'ALF',
                'dna0':snaptop+'D',
                'dp':snaptop+'DP',
                'kye':snaptop+'KYE',
                'kyi':snaptop+'KYI',
                'kyi0':snaptop+'KYI0',
                'sig':snaptop+'SIG',
                'rpt':snaptop+'RPT', # (not listed in the manual)

                    # Rates, losses and sources
                'rcxhi':snaptop+'RCXHI',
                'rcxmo':snaptop+'RCXMO',
                'rcxna':snaptop+'RCXNA',
                'rqahe':snaptop+'RQAHE',
                'rqbrm':snaptop+'RQBRM',
                'rqrad':snaptop+'RQRAD',
                'rrahi':snaptop+'RRAHI',
                'rramo':snaptop+'RRAMO',
                'rrana':snaptop+'RRANA',
                'rsahi':snaptop+'RSAHI',
                'rsamo':snaptop+'RSAMO',
                'rsana':snaptop+'RSANA',

                'smo':snaptop+'SMO',
                'smq':snaptop+'SMQ',
                'b2npmo_smav':snaptop+'SMAV',
                'resmo':snaptop+'RESMO',

                    # Texts
                'textan':snaptop+'TEXTAN',
                'textcomp':snaptop+'TEXTCOMP',
                'textin':snaptop+'TEXTIN',
                'textmn':snaptop+'TEXTMN',
                'textpl':snaptop+'TEXTPL',


        #---------------------------- SNAPSHOT --------------------------------
                    #Targets
                'TARGET1_ds':timedept1+'DS',
                'TARGET1_ft':timedept1+'FT',
                'TARGET1_fe':timedept1+'FE',
                'TARGET1_fi':timedept1+'FI',
                'TARGET1_fc':timedept1+'FC',
                'TARGET1_te':timedept1+'TE',
                'TARGET1_ti':timedept1+'TI',
                'TARGET1_ne':timedept1+'NE',
                'TARGET1_po':timedept1+'PO',

                'TARGET2_ds':timedept2+'DS',
                'TARGET2_ft':timedept2+'FT',
                'TARGET2_fe':timedept2+'FE',
                'TARGET2_fi':timedept2+'FI',
                'TARGET2_fc':timedept2+'FC',
                'TARGET2_te':timedept2+'TE',
                'TARGET2_ti':timedept2+'TI',
                'TARGET2_ne':timedept2+'NE',
                'TARGET2_po':timedept2+'PO',

                'TARGET3_ds':timedept3+'DS',
                'TARGET3_ft':timedept3+'FT',
                'TARGET3_fe':timedept3+'FE',
                'TARGET3_fi':timedept3+'FI',
                'TARGET3_fc':timedept3+'FC',
                'TARGET3_te':timedept3+'TE',
                'TARGET3_ti':timedept3+'TI',
                'TARGET3_ne':timedept3+'NE',
                'TARGET3_po':timedept3+'PO',

                'TARGET4_ds':timedept4+'DS',
                'TARGET4_ft':timedept4+'FT',
                'TARGET4_fe':timedept4+'FE',
                'TARGET4_fi':timedept4+'FI',
                'TARGET4_fc':timedept4+'FC',
                'TARGET4_te':timedept4+'TE',
                'TARGET4_ti':timedept4+'TI',
                'TARGET4_ne':timedept4+'NE',
                'TARGET4_po':timedept4+'PO',

                    #Midplanes
                'OMP_ds':timedepomp+'DS',
                'OMP_te':timedepomp+'TE',
                'OMP_ti':timedepomp+'TI',
                'OMP_ne':timedepomp+'NE',

                'IMP_ds':timedepimp+'DS',
                'IMP_te':timedepimp+'TE',
                'IMP_ti':timedepimp+'TI',
                'IMP_ne':timedepimp+'NE',
                 }
        try:
            return MDSTREE_SOLPS50[var_name]
        except:
            self.log.warning(
                "'{0}' not found/implemented in MDSTREE dict".format(var_name))
            return None

#    def _sum_cs(self,var):
#        # Old version! (Could only handle vars of shape (nx,ny,ns))
#        """Returns the sum of var over all charge states per species.
#        (var [nx,ny,ns] ==> [nx,ny,natm])"""
#        sum = np.zeros((self.nx,self.ny,self.natm))
#        natm = 0
#        zn = self.zn[0]
#        for ns in range(self.ns):
#            if zn != self.zn[ns]:
#                zn = self.zn[ns]
#                natm += 1
#            sum[:,:,natm] += var[:,:,ns]
#        return sum

    def _sum_cs(self,var):
        """Returns the sum of var over all charge states per species.
        (var [...,ns,...] ==> [...,natm,...])"""
        species_index = np.where(np.array(var.shape)==self.ns)[0][-1]
        # WARNING: This might cause problems if one of the dimensions
        #          of the variable 'var' is of size self.ns!
        shape = list(var.shape)
        shape[species_index] = self.natm
        shape = tuple(shape)
        zn = list(OrderedDict.fromkeys(self.zn))
        sum = np.zeros(shape)
        for natm in range(self.natm):
            # How can I make this more general???
            if   species_index == 0:
                sum[      natm,...] = np.sum(var[      self.zn==zn[natm],...],
                                             species_index)
            elif species_index == 1:
                sum[    :,natm,...] = np.sum(var[    :,self.zn==zn[natm],...],
                                             species_index)
            elif species_index == 2:
                sum[  :,:,natm,...] = np.sum(var[  :,:,self.zn==zn[natm],...],
                                             species_index)
            elif species_index == 3:
                sum[:,:,:,natm,...] = np.sum(var[:,:,:,self.zn==zn[natm],...],
                                             species_index)
        return sum

    def _avg_cs(self,var):
        """Returns the average of var over all charge states per species.
        (var [nx,ny,ns] ==> [nx,ny,natm])"""
        avg = np.zeros((self.nx,self.ny,self.natm))
        natm = 0
        zn = self.zn[0]
        for ns in range(self.ns):
            if zn != self.zn[ns]:
                avg[:,:,natm] = avg[:,:,natm]/(zn+1)
                zn = self.zn[ns]
                natm += 1
            avg[:,:,natm] += var[:,:,ns]
        avg[:,:,natm] = avg[:,:,natm]/(zn+1)
        return avg

    def _rescale_neutrals(self,var):
        """Rescales the neutrals according to the switch 
        'b2mndr_rescale_neutrals_sources' in b2mn.dat"""
        factor = 1/float(self.read_switch('b2mndr_rescale_neutrals_sources'))
        rescale = np.ones(var.shape)
        species_index = np.where(np.array(var.shape)==self.ns)[0][-1]
        # WARNING: This might cause problems if one of the dimensions
        #          of the variable 'var' is of size self.ns!

        # How can I make this more general???
        if   species_index == 0:
            rescale[      self.za==0,...] = factor
        elif species_index == 1:
            rescale[    :,self.za==0,...] = factor
        elif species_index == 2:
            rescale[  :,:,self.za==0,...] = factor
        elif species_index == 3:
            rescale[:,:,:,self.za==0,...] = factor
        return var * rescale















#ATTENTION: Improve docstring in all of the class
#ATTENTION:
#Found an error about how masks are implemented. If outdiv_nolim is used
#before masks.outdiv, the result is different because the guarding cell
#is either accepted or not. That is wrong and has to be fixed.
class Masks(object):
    """Class containing different grid masks."""

    def __init__(self,solpspy_run):
        self._run = solpspy_run
        self.log = self._run.log

    #ATTENTION!
    @property
    @try_block
    def gdcl(self):
        """Guard cells."""
        self._gdcl = np.zeros((self._run.nx,self._run.ny))
        ## WRONG??
        #self._gdcl[0,:] = 1
        #self._gdcl[:,0] = 1
        #self._gdcl[self._run.nx-1,:] = 1
        #self._gdcl[:,self._run.ny-1] = 1

        self._gdcl[0] = 1
        self._gdcl[-1] = 1
        self._gdcl[:,0] = 1
        self._gdcl[:,-1] = 1

    @property
    def guard_cells(self):
        """Alias for self.gdcl"""
        return self.gdcl

    @property
    @try_block
    def rlcl(self):
        """Real cells."""
        #self._rlcl = np.ones((self._run.nx,self._run.ny)) - self.gdcl
        self._rlcl = np.logical_not(self.gdcl)

    @property
    def real_cells(self):
        """Alias for self.rlcl"""
        return self.rlcl

    @property
    @try_block
    def xpoint(self):
        """Grid mask for the grid cells surrounding the x-point."""
        self._xpoint = np.zeros((self._run.nx,self._run.ny))
        for nx in range(self._run.nx):
            for ny in range(self._run.ny):
                cell = self._run.grid[nx,ny] # [nvertex,R|z]
                for v in range(len(cell)):
                    if (cell[v][0] == self._run.xpoint[0] and
                        cell[v][1] == self._run.xpoint[1]):
                        self._xpoint[nx,ny] = 1

    #ATTENTION: Not valid for USN
    @property
    @try_block
    def innertarget(self):
        self._innertarget = np.zeros((self._run.nx,self._run.ny))
        self._innertarget[1,:] = 1 # nx=1 (rlcl)

    #ATTENTION: Not valid for USN
    @property
    @try_block
    def outertarget(self):
        self._outertarget = np.zeros((self._run.nx,self._run.ny))
        self._outertarget[self._run.nx-2,:] = 1

    @property
    @try_block
    def div(self):
        self._div = self.inndiv + self.outdiv

    @property
    def divertor(self):
        """Alias of self.div"""
        return self.div

    @solps_property
    def notdiv(self):
        self._notdiv = self.rlcl*(np.ones((self._run.nx,self._run.ny))-self.div)

    @property
    @try_block
    def outdiv(self):
        self._outdiv = self.generate(region = 'Outer divertor')

    @property
    @try_block
    def inndiv(self):
        self._inndiv = self.generate(region = 'Inner divertor')

    @property
    @try_block
    def pfr(self):
        self._pfr = (1-self.sol) * self.notcore * self.rlcl

    @property
    @try_block
    def core(self):
        self._core = ( 1 - self.notcore ) * self.rlcl

    @property
    @try_block
    def notcore(self):
        self._notcore = self.generate(region = 'Not core')

    @property
    @try_block
    def sol(self):
        self._sol = self.generate(region = 'SOL')

    @property
    @try_block
    def sol_without_div(self):
        self._sol_without_div = ( self.sol*(1-self.inndiv)*(1-self.outdiv)
                                  * self.rlcl )

    @property
    @try_block
    def sol_lfs(self):
        # Find highest vertex to separate hfs and lfs:
        zmax = -np.inf
        xmax = 0
        for nx in range(self._run.nx):
            vmax = max(self._run.grid[nx,self._run.ny-1,:,1])
            if vmax > zmax:
                xmax = nx
                zmax = vmax
        self._sol_lfs = self.sol_without_div.copy()*self.rlcl
        self._sol_lfs[0:xmax+1,:] = 0

    @property
    @try_block
    def sol_hfs(self):
        self._sol_hfs = self.sol_without_div - self.sol_lfs




    def generate(self, **kwargs):
        """ Generates mask with given description.

        Parameters
        ----------
        region : str or list(str)
            Name or list of names of predefined grid regions.
            Can be 'outer divertor', 'inner divertor', 'sol' or 'not core'
            or a list with any of them.
            Default to None
        ix : int or list(int)
            If region is None, then ix represent the selected poloidal indexes
            of the (unbound) total radial range.
            If both ix and iy have valid values, then the mask is the
            interection of both.
            Default to []
        iy : int or list(int)
            If region is None, then iy represent the selected radial indexes
            of the (unbound) total poloidal range.
            If both ix and iy have valid values, then the mask is the
            interection of both.
            Default to []
        extra_dims : int or iterable(int)
            Expands mask from (nx,ny) to (nx,ny,extra_dims).
            Default to None.

        Returns
        -------
        mask: numpy.array with dtype=numpy.bool
            Array with True in the cells that dwell inside the desired
            subdivision of the grid.
            It has dimensions (nx,ny,extra_dims)
        """

        region = priority('region', kwargs, None)
        ix = priority(['ix','xrange'], kwargs, [])
        iy = priority(['iy','yrange'], kwargs, [])
        extra_dims = priority(['extra_dims', 'extra_dim', 'dims', 'dim'],
            kwargs, None)

        nx = self._run.nx
        ny = self._run.ny
        ns = self._run.ns
        sep = self._run.sep
        oul = self._run.oul
        odl = self._run.odl
        iul = self._run.iul
        idl = self._run.idl

        if region:

            mask = np.zeros((nx,ny), dtype=np.bool)
            if type(region) is str:
                region=[region.lower()]
            elif type(region) is list:
                region = [reg.lower() for reg in region]

            for reg in region:
                if ('outer divertor' in reg or 'outdiv' in reg or
                    'out div' in reg or 'lfs div' in reg):
                    mask[oul:odl] = True

                elif ('inner divertor' in reg or 'indiv' in  reg or
                      'inndiv' in reg or 'hfs div' in reg):
                    mask[idl:iul+1] = True

                elif 'sol' in reg:
                    mask[:,sep:ny] = True

                elif 'not core' in reg or 'notcore' in reg:
                    mask[idl:iul+1] = True
                    mask[oul:odl] = True
                    mask[:,sep:ny] = True

        elif ix is not []  or iy is not []:
            if type(ix) is not list: ix = [ix]
            if type(iy) is not list: iy = [iy]
            mask1 = np.zeros((nx,ny), dtype=np.bool)
            mask2 = np.zeros((nx,ny), dtype=np.bool)
            for i in xrange(nx):
                for k in xrange(ny):
                    if (i in ix):
                        mask1[i,k] = True
                    if (k in iy):
                        mask2[i,k] = True
            if ix and iy:
                mask = np.logical_and(mask1, mask2)
            elif ix:
                mask = mask1
            elif iy:
                mask = mask2

        if extra_dims:
            if '__iter__' not in extra_dims.__class__.__dict__:
                extra_dims = [extra_dims]

            for dm in extra_dims:
                mask = np.repeat(mask.T[np.newaxis], dm, axis=0).T

        return mask
