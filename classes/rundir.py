#!/usr/bin/env python

"""
The way in which solps5.0 and solps-iter fort.* are structured is different.
In solps-iter, these files contain now headers indicating the variable names.

IT IS FUNDAMENTAL TO ALWAYS OPEN THE FILES WITH
OS.PATH.JOIN(SELF.RUNDIR,NAME_OF_FILE)
(otherwise, it only works when rundir = '.')

Is it important that self.rundir remains relative or can be put in absolute path?
What are the advantages/disadvantages?
"""

# Common modules:
from __future__ import division, print_function
import os
import re
import logging
import itertools
import numpy as np
from collections import namedtuple
from sets import Set
import subprocess

# Special modules:

# Solpspy modules:
from solpspy.classes.mds import MdsData
from solpspy.tools.tools import try_block, solps_property
from solpspy.tools.tools import SimpleNamespace, styles
from solpspy.tools.tools import open_file
#from solpspy.timetraces import rundirtt
import rundirtt

## Equations output extension
from solpspy.extensions.eq_output import EquationsOutput

# =============================================================================
import logging
rundirlog = logging.getLogger('RundirData')
if not rundirlog.handlers:
    formatter = logging.Formatter(
            '%(name)s(%(ident)s) -- %(levelname)s: %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    rundirlog.addHandler(ch)
    rundirlog.propagate = False


# =============================================================================
class RundirData(MdsData):
    """
    Run directory methods
    Because sometimes, mds+ is not enough

    Remarks:
        readlines() makes the code slower than simply for line in file.
        might be worth the effort to change it.
        ALSO: Learn to use mmap. Might be the best option.
    """
    def __init__(self,rundir=None, name=None, b2standalone=False, **kwargs):
        if (rundir is None) or (rundir =='.'):
            self.rundir = os.getcwd()
        else:
            self.rundir = rundir
        self._type = 'RundirData'
        if name is None:
            self.name = os.path.basename(os.path.normpath(self.rundir))
        else:
            self.name = name
        self.ident = self.name # Alias to ensure compatibility with older code
        self.log = logging.LoggerAdapter(rundirlog, {'ident':self.name})
        self.avail = {}
        self._check_avail()
        self.b2standalone = b2standalone
        self._get_basic_dimensions()
        try:
            with open(os.path.join(self.rundir,'shotnumber.history')) as f:
                self.shot = int(f.readlines()[-1])
        except:
            self.shot = None
        self.mp = 1.672621777e-27
        self.qe = 1.602176565e-19
        self.ev = self.qe # Souldn't it be 8.6173303e-5 # kb/qe?

        self.out = rundirtt.TargetData(self, 'target4')
        self.inn = rundirtt.TargetData(self, 'target1')
        self.outmp = rundirtt.MidplaneData(self, 'omp')
        self.innmp = rundirtt.MidplaneData(self, 'imp')


        #TESTING:
        self.movies = rundirtt.Movies(self)


    #ATTENTION: Needed? I think there are already too many directions
    #ATTENTION: What does it even mean?? Poor naming
    @property
    @try_block
    def basename(self):
        """Basename of self.rundir"""
        self._basename = self.rundir.rstrip('/').split('/')[-1]


# IDENT PROPERTIES ===========================================================
    @property
    def user(self):
        self.log.warning("'user' not implemented yet")
        return None

    #ATTENTION: Improve this logic
    @solps_property
    def solpsversion(self):
        #IMPROVE THIS LOGIC
        if 'b2fplasmf' in self.avail:
            fd = self.avail['b2fplasmf']
        elif 'b2fstate' in self.avail:
            fd = self.avail['b2fstate']
        elif 'b2fstati' in self.avail:
            fd = self.avail['b2fstati']
        elif 'b2fgmtry' in self.avail:
            fd = self.avail['b2fgmtry']
        with open(fd,'r') as b2p:
            title = b2p.readline()
            self._solpsversion = title[7:17]

    #ATTENTION: Improve this logic
    @solps_property
    def gitversion(self):
        #IMPROVE THIS LOGIC
        if 'b2fplasmf' in self.avail:
            fd = self.avail['b2fplasmf']
        elif 'b2fstate' in self.avail:
            fd = self.avail['b2fstate']
        elif 'b2fstati' in self.avail:
            fd = self.avail['b2fstati']
        elif 'b2fgmtry' in self.avail:
            fd = self.avail['b2fgmtry']
        with open(fd,'r') as b2p:
            title = b2p.readline()
            self._gitversion = title[18:].strip()

    @property
    def directory(self):
        """ Full path of self.rundir, equivalent to MdsData.directory"""
        return os.path.abspath(self.rundir)

    @solps_property
    def baserun(self):
        """Directory of the baserun folder"""
        dir = self.directory.rstrip('/')
        while not os.path.isdir(dir + '/baserun'):
            dir = dir.rsplit('/',1)[0]
            if dir == '':
                return None
        self._baserun = dir + '/baserun'

    @solps_property
    def exp(self):
        """str : exp id of machine on which the simulation is based."""
        # IS THERE A BETTER WAY TO GET THIS?
        b2md = os.path.join(self.rundir,'b2md.dat')
        if not os.path.isfile(b2md):
            b2md = os.path.join(self.baserun,'b2md.dat')
        if not os.path.isfile(b2md):
            return None
        with open(b2md,'r') as file:
            for line in file:
                if 'exp' in line:
                    self._exp = line.split("'")[1]

# SNAPSTHOP PROPERTIES =======================================================
    # -------------------- Dimensions -----------------------------------------
    # nx,ny,ns,natm,nmol and nion are read with self._get_basic_dimensions()


    # -------------------- Indices --------------------------------------------

    @solps_property
    def nnreg(self):
        self._read_signal('_nnreg','nnreg')
        self._nnreg = self._nnreg.astype(int)

    @solps_property
    def region(self):
        """(nx,ny,3):   Index of corresponding region
        0:  Volume region
        1:  x-region
        2:  y-region
        """
        self._read_signal('_region','region')
    @property
    def regvol(self):
        """ Alias for self.region[...,0]"""
        return self.region[...,0]
    @property
    def regflx(self):
        """ Alias for self.region[...,1]"""
        return self.region[...,1]
    @property
    def regfly(self):
        """ Alias for self.region[...,2]"""
        return self.region[...,2]



    #Read convert/process.F
    @solps_property
    def jxi(self):
        """Logic can be found in the definition of jxi in the documentation"""
        dummy = self.read_switch('b2mwti_jxi')
        if dummy:
            self._jxi = int(dummy) + 1
        else:
            if   self.nnreg[0] == 1:
                self._jxi = self.nx//4
            elif self.nnreg[0] == 4 or self.nnreg[0] == 5:
                self._jxi = (self.leftcut[0]+
                    (self.rightcut[0]-self.leftcut[0])//4 + 1)
            elif self.nnreg[0] == 8:
                self._jxi = (self.leftcut[0]+self.leftcut[1])/2
    @solps_property
    def jxa(self):
        """Logic can be found in the definition of jxa in the documentation"""
        dummy = self.read_switch('b2mwti_jxa')
        if dummy:
            self._jxa = int(dummy) + 1
        else:
            if   self.nnreg[0] == 1:
                self._jxa = 3*self.nx//4
            elif self.nnreg[0] == 4 or self.nnreg[0] == 5:
                self._jxa = (self.rightcut[0]-
                    (self.rightcut[0]-self.leftcut[0])//4 - 1)
            elif self.nnreg[0] == 8:
                self._jxa = (self.rightcut[0]+rightcut[1])/2

    @solps_property
    def sep(self):
        """Equals first flux surface in SOL"""
        if   self.nnreg[0] == 1:
            self._sep = self.ny//2
        elif self.nnreg[0] == 4 or self.nnreg[0] == 5:
            self._sep = self.topcut[0] + 1

    #ATTENTION: Implement for LSN in MDS
    @property
    def targ1(self):
        self.log.warning("'targ1' not implemented yet")
        return None
    #ATTENTION: Implement for LSN in MDS
    @property
    def targ2(self):
        self.log.warning("'targ2' not implemented yet")
        return None
    #ATTENTION: Implement for LSN in MDS
    @property
    def targ3(self):
        self.log.warning("'targ3' not implemented yet")
        return None
    #ATTENTION: Implement for LSN in MDS
    @property
    def targ4(self):
        self.log.warning("'targ4' not implemented yet")
        return None



    # -------------------- Neighbours and cuts --------------------------------
    @solps_property
    def leftix(self):
        """Index in base 0, Python-like"""
        self._read_signal('_leftix','leftix')
        self._leftix +=1
        self._leftix = self._leftix.astype(np.int)

    @solps_property
    def leftiy(self):
        """Index in base 0, Python-like"""
        self._read_signal('_leftiy','leftiy')
        self._leftiy +=1
        self._leftiy = self._leftiy.astype(np.int)

    @solps_property
    def rightix(self):
        """Index in base 0, Python-like"""
        self._read_signal('_rightix','rightix')
        self._rightix +=1
        self._rightix = self._rightix.astype(np.int)

    @solps_property
    def rightiy(self):
        """Index in base 0, Python-like"""
        self._read_signal('_rightiy','rightiy')
        self._rightiy +=1
        self._rightiy = self._rightiy.astype(np.int)

    @solps_property
    def bottomix(self):
        """Index in base 0, Python-like"""
        self._read_signal('_bottomix','bottomix')
        self._bottomix +=1
        self._bottomix = self._bottomix.astype(np.int)

    @solps_property
    def bottomiy(self):
        """Index in base 0, Python-like"""
        self._read_signal('_bottomiy','bottomiy')
        self._bottomiy +=1
        self._bottomiy = self._bottomiy.astype(np.int)

    @solps_property
    def topix(self):
        """Index in base 0, Python-like"""
        self._read_signal('_topix','topix')
        self._topix +=1
        self._topix = self._topix.astype(np.int)

    @solps_property
    def topiy(self):
        """Index in base 0, Python-like"""
        self._read_signal('_topiy','topiy')
        self._topiy +=1
        self._topiy = self._topiy.astype(np.int)

    @solps_property
    def leftcut(self):
        self._read_signal('_leftcut','leftcut')
        self._leftcut = self._leftcut.astype(int)

    @solps_property
    def rightcut(self):
        self._read_signal('_rightcut','rightcut')
        self._rightcut = self._rightcut.astype(int)

    @solps_property
    def topcut(self):
        self._read_signal('_topcut','topcut')
        self._topcut = self._topcut.astype(int)

    @solps_property
    def bottomcut(self):
        self._read_signal('_bottomcut','bottomcut')
        self._bottomcut = self._bottomcut.astype(int)


    # -------------------- Grid and vessel geometry ---------------------------
    #ATTENTION: In rundir it is read from input.dat most likely,
    #but in mds, it is read from mesh.extra.
    @solps_property
    def vessel(self):
        """(4,NLIMI) : Array containing the data for additional surfaces
                       defined in input.dat block 3b or in the mesh.extra
                       file."""
        if 'input.dat' in self.avail:
            try:
                self._read_structure_input_dat()
                return
            except:
                pass
        if 'mesh.extra' in self.avail:
            try:
                self._vessel = self.vessel_mesh
                return
            except:
                pass
    @solps_property
    def vessel_colors(self):
        """A dictionary containing the color indizes ILCOL as defined in the
        input.dat file for each additional surface. (Format {nsurface : ILCOL})"""
        if 'input.dat' in self.avail:
            self._read_structure_input_dat()
        else:
            self._vessel_colors = None

    #ATTENTION: Name should be self.mesh_extra??
    @solps_property
    def vessel_mesh(self):
        self._read_mesh_extra()

    #ATTENTION: Add docstrig
    #ATTENTION: Add input.dat as source of strata
    @solps_property
    def nstrata(self):
        try:
            with open(self.avail['b2.neutrals.parameters'], 'r') as b2f:
                    for line in b2f:
                        dummy = line.strip('\n,').replace("="," ").split()
                        if dummy[0].lower() == 'nstrai':
                            self._nstrata = int(dummy[1])
                            break
        except:
            self._nstrata = None

    #ATTENTION: Add docstrig
    @solps_property
    def strata(self):
        if 'b2.neutrals.parameters' not in self.avail:
            self._strata = None
            return
        with open(self.avail['b2.neutrals.parameters'], 'r') as b2f:
                names = []
                rcpos = []
                rcstart = []
                rcend = []
                for line in b2f:
                    dummy = line.strip().split('=')
                    if dummy[0].lower() == 'crcstra':
                        dummy = dummy[1].replace(","," ")
                        dummy = dummy.replace("'","").split()
                        for strata in dummy:
                            names.append(strata)
                    elif dummy[0].lower() == 'rcpos':
                        dummy = dummy[1].replace(","," ").split()
                        for strata in dummy:
                            rcpos.append(int(strata))
                    elif dummy[0].lower() == 'rcstart':
                        dummy = dummy[1].replace(","," ").split()
                        for strata in dummy:
                            rcstart.append(int(strata))
                    elif dummy[0].lower() == 'rcend':
                        dummy = dummy[1].replace(","," ").split()
                        for strata in dummy:
                            rcend.append(int(strata))
        Stratum = namedtuple('Stratum', ['crcstra','rcpos','rcstart','rcend'])
        self._strata = [Stratum(*s) for s in zip(names,rcpos,rcstart,rcend)]

    @solps_property
    def gs(self):
        """(nx,ny,3): Surfaces between neighbouring cells.
        [...,0]: poloidal surface (left face).
        [...,1]: radial surface (bottom face).
        [...,2]: toroidal surface."""
        self._read_signal('_gs','gs')
    @solps_property
    def sx(self):
        """ """
        self._sx = self.gs[...,0]
    @solps_property
    def sy(self):
        """ """
        self._sy = self.gs[...,1]

    #ATTENTION: Add docstrig
    @solps_property
    def cr(self):
        """(nx,ny) : R-coordinate of the cell centre [m]"""
        self._cr = 0.25*np.sum(self.crx, 2)

    @solps_property
    def cr_x(self):
        self._cr_x = (self.crx[1:,:,0]+self.crx[1:,:,2])/2.0

    @solps_property
    def cr_y(self):
        self._cr_y = (self.crx[:,1:,0]+self.crx[:,1:,1])/2.0

    @solps_property
    def cz(self):
        """(nx,ny) : Z-coordinate of the cell centre [m]"""
        self._cz = 0.25*np.sum(self.cry, 2)

    @solps_property
    def cz_x(self):
        self._cz_x = (self.cry[1:,:,0]+self.cry[1:,:,2])/2.0

    @solps_property
    def cz_y(self):
        self._cz_y = (self.cry[:,1:,0]+self.cry[:,1:,1])/2.0

    @solps_property
    def crx(self):
        self._read_signal('_crx','crx')
    @property
    def r(self):
        """Alias for crx"""
        return self.crx

    @solps_property
    def cry(self):
        self._read_signal('_cry','cry')
    @property
    def z(self):
        """Alias for cry"""
        return self.cry

    @property
    def dsrad(self):
        self.log.warning("'dsrad' not implemented yet")
        return None

    @property
    def dspar(self):
        self.log.warning("'dspar' not implemented yet")
        return None


    ## ----------- EIRENE triangular grid ---------------------
    @property
    def ntriang(self):
        return self.triang.shape[0]

    @solps_property
    def triang(self):
        self._read_triang_mesh()

    @solps_property
    def crx_triang(self):
        self._read_triang_mesh()

    @solps_property
    def cry_triang(self):
        self._read_triang_mesh()


    def _read_triang_mesh(self):
        """ Triangular grid of EIRENE.
        """
        with open(self.avail['fort.33'],'r') as fort33:
            napex = int(fort33.readline())
            ceiling = int(np.ceil(napex/4))
            R = []
            for i in xrange(ceiling):
                elements = fort33.readline().split()
                for ele in elements:
                    R.append(np.float64(ele))
            z = []
            for i in xrange(ceiling):
                elements = fort33.readline().split()
                for ele in elements:
                    z.append(np.float64(ele))

        with open(self.avail['fort.34'],'r') as fort34:
            ntriang = int(fort34.readline())
            i_apex1 = []
            i_apex2 = []
            i_apex3 = []
            for line in fort34:
                elements = line.split()
                i_apex1.append(int(elements[1]))
                i_apex2.append(int(elements[2]))
                i_apex3.append(int(elements[3]))


        self._crx_triang = np.full((ntriang,3), np.nan)
        self._cry_triang = np.full((ntriang,3), np.nan)
        for i in xrange(ntriang):
            self._crx_triang[i,0] = R[i_apex1[i]-1]
            self._crx_triang[i,1] = R[i_apex2[i]-1]
            self._crx_triang[i,2] = R[i_apex3[i]-1]

            self._cry_triang[i,0] = z[i_apex1[i]-1]
            self._cry_triang[i,1] = z[i_apex2[i]-1]
            self._cry_triang[i,2] = z[i_apex3[i]-1]

        self._crx_triang *= 0.01 #In meters
        self._cry_triang *= 0.01 #In meters

        self._triang = np.zeros((ntriang,3,2))
        self._triang[...,0] = self._crx_triang
        self._triang[...,1] = self._cry_triang

        return


    @solps_property
    def pdena(self):
        self._read_fort46('_pdena', 'pdena')

    @solps_property
    def pdenm(self):
        self._read_fort46('_pdenm', 'pdenm')


    def _read_fort46(self, signal_name, var_name, attach_to=None):
        """
        """
        if not attach_to:
            attach_to = self
        try:
            var = self._fort46_parser(var_name)
            setattr(attach_to, signal_name, var)
        except:
            self.log.warning("error reading '{0}''".format(var_name))
            setattr(attach_to, signal_name, None)


    def _fort46_parser(self, var):
        """
        """
        ftr  = (self.ntriang)
        ftra = (self.ntriang, self.natm)
        ftrm = (self.ntriang, self.nmol)
        ftri = (self.ntriang, self.nion)
        local_dims = {'pdena': ftra, 'pdenm':ftrm}
        try:
            fname = 'fort.46'
            dims = local_dims[var]
            fd = self.avail['fort.46']
            nentries = np.prod(dims)
            #factor = self._factors(var)
            factor = 1.0
        except:
            self.log.warning("'{0}' could not be parsed".format(var))
            return None

        data = []

        #Eirene output for SOLPS-ITER
        with open(fd,'r') as f:
            #Skip unnecesary lines
            for i in xrange(2+self.natm+self.nmol+self.nion):
                f.readline()
            is_body = False
            for i, line in enumerate(f):
                wh_header = (line.split()[0] ==  '*eirene' and
                             var in line.split() and
                             line.split()[-1] == str(nentries))
                if wh_header:
                    i_header = i
                    is_body = True
                    #data = []
                    continue
                if is_body:
                    pieces = line.split()
                    for piece in pieces:
                        data.append(np.float(piece))
                    if len(data) == nentries: break

        return np.array(data).reshape(dims, order = 'F')*factor





    # -------------------- Plasma Characterization ----------------------------
    @solps_property
    def zamin(self):
        self._read_signal('_zamin','zamin')

    @solps_property
    def zamax(self):
        self._read_signal('_zamax','zamax')

    @solps_property
    def za(self):
        self._za  = (self.zamin+self.zamax)/2.0


    # -------------------- Densities ------------------------------------------


    # -------------------- Temperatures ---------------------------------------


    # -------------------- Velocities -----------------------------------------
    @solps_property
    def uadia(self):
        self._read_signal('_uadia','uadia')


    # -------------------- Fluxes and energies --------------------------------
    #ATTENTION: FHT IS MISSING AND ITS DEFINITION DEPENDS ON THE SOLPS
    #VERSION (5.0, iter 305, 306, etc)


    @solps_property
    def fna(self):
        self._read_signal('_fna','fna')
    @solps_property
    def fna32(self):
        self._read_signal('_fna32','fna_32')
    @solps_property
    def fna52(self):
        self._read_signal('_fna52','fna_52')

    @solps_property
    def fnax(self):
        self._fnax = self.fna[:,:,0,:]
    @solps_property
    def fnax32(self):
        self._fnax32 = self.fna32[:,:,0,:]
    @solps_property
    def fnax52(self):
        self._fnax52 = self.fna52[:,:,0,:]

    @solps_property
    def fnay(self):
        self._fnay = self.fna[:,:,1,:]
    @solps_property
    def fnay32(self):
        self._fnay32 = self.fna32[:,:,1,:]
    @solps_property
    def fnay52(self):
        self._fnay52 = self.fna52[:,:,1,:]

    @solps_property
    def fne(self):
        self._read_signal('_fne','fne')

    @solps_property
    def fnex(self):
        self._fnex = self.fne[...,0]

    @solps_property
    def fney(self):
        self._fney = self.fne[...,1]


    @solps_property
    def fmo(self):
        self._read_signal('_fmo','fmo')
    @solps_property
    def fmox(self):
        self._fmox = self.fmo[:,:,0,:]
    @solps_property
    def fmoy(self):
        self._fmoy = self.fmo[:,:,1,:]


    @solps_property
    def fhe(self):
        self._read_signal('_fhe','fhe')
    @solps_property
    def fhex(self):
        self._fhex = self.fhe[:,:,0]
    @solps_property
    def fhey(self):
        self._fhey = self.fhe[:,:,1]


    @solps_property
    def fhi(self):
        self._read_signal('_fhi','fhi')
    @solps_property
    def fhix(self):
        self._fhix = self.fhi[:,:,0]
    @solps_property
    def fhiy(self):
        self._fhiy = self.fhi[:,:,1]


    @solps_property
    def fch(self):
        self._read_signal('_fch','fch')
    @solps_property
    def fchx(self):
        self._fchx = self.fch[:,:,0]
    @solps_property
    def fchy(self):
        self._fchy = self.fch[:,:,1]

    @solps_property
    def fch_32(self):
        self._read_signal('_fch_32','fch_32')

    @solps_property
    def fch_52(self):
        self._read_signal('_fch_52','fch_52')

    @solps_property
    def fch_p(self):
        self._read_signal('_fch_p','fch_p')

    @solps_property
    def fhj(self):
        fhj = np.zeros((self.nx,self.ny,2))
        for ny in range(self.ny):
            for nx in range(self.nx):
                if self.leftix[nx,ny] >= 0:
                    fhj[nx,ny,0] = (-0.5 * self.fch[nx,ny,0] *
                                     (self.po[nx,ny] +
                                      self.po[self.leftix[nx,ny],
                                              self.leftiy[nx,ny]]))
                else:
                    fhj[nx,ny,0] = 0

                if self.bottomiy[nx,ny] >= 0:
                    fhj[nx,ny,1] = (-0.5 * self.fch[nx,ny,1] *
                                     (self.po[nx,ny] +
                                      self.po[self.bottomix[nx,ny],
                                              self.bottomiy[nx,ny]]))
                else:
                    fhj[nx,ny,1] = 0
        self._fhj = fhj

    @solps_property
    def fhm(self):
        fhm = np.zeros((self.nx,self.ny,2,self.ns))
        for ns in range(self.ns):
            for ny in range(self.ny):
                for nx in range(self.nx):
                    if self.leftix[nx,ny] >= 0:
                        fhm[nx,ny,0,ns] = (0.5*self.fna[nx,ny,0,ns]*
                                           (self.kinrgy[int(self.leftix[nx,ny]),
                                                        int(self.leftiy[nx,ny]),ns]
                                            + self.kinrgy[nx,ny,ns]))
                    else:
                        fhm[nx,ny,0,ns] = 0
                    if self.bottomiy[nx,ny] >= 0:
                        fhm[nx,ny,1,ns] = (0.5*self.fna[nx,ny,1,ns]*
                                           (self.kinrgy[int(self.bottomix[nx,ny]),
                                                        int(self.bottomiy[nx,ny]),ns]
                                            + self.kinrgy[nx,ny,ns]))
                    else:
                        fhm[nx,ny,1,ns] = 0
        self._fhm = fhm



    @solps_property
    def fhp(self):
        fhp = np.zeros((self.nx,self.ny,2,self.ns))
        for ns in range(self.ns):
            for ny in range(self.ny):
                for nx in range(self.nx):
                    if self.leftix[nx,ny] >= 0:
                        fhp[nx,ny,0,ns] = (0.5*self.fna[nx,ny,0,ns]*
                                           (self.rpt[self.leftix[nx,ny],
                                                        self.leftiy[nx,ny],ns]
                                            + self.rpt[nx,ny,ns]))*self.qe
                    else:
                        fhp[nx,ny,0,ns] = 0
                    if self.bottomiy[nx,ny] >= 0:
                        fhp[nx,ny,1,ns] = (0.5*self.fna[nx,ny,1,ns]*
                                           (self.rpt[self.bottomix[nx,ny],
                                                        self.bottomiy[nx,ny],ns]
                                            + self.rpt[nx,ny,ns]))*self.qe
                    else:
                        fhp[nx,ny,1,ns] = 0
        self._fhp = fhp



    @solps_property
    def fht(self):
        """ Not exactly same as FT
        """
        if self.solps_code == 'solps-iter':
            self._fht = (self.fhe + self.fhi + self.fhj +
                    np.sum(self.fhp,3) + np.sum(self.fhm,3) + self.fnt)
        elif self.solps_code == 'solps5.0':
            self._fht = (self.fhe + self.fhi + self.fhj +
                    np.sum(self.fhp,3) + np.sum(self.fhm,3))


    @solps_property
    def fhjx(self):
        self._fhjx = self.fhj[:,:,0]

    @solps_property
    def fhjy(self):
        self._fhjy = self.fhj[:,:,1]

    @solps_property
    def fhmx(self):
        self._fhmx = self.fhm[:,:,0,:]

    @solps_property
    def fhmy(self):
        self._fhmy = self.fhm[:,:,1,:]

    @solps_property
    def fhpx(self):
        self._fhpx = self.fhp[:,:,0,:]

    @solps_property
    def fhpy(self):
        self._fhpy = self.fhp[:,:,1,:]

    @solps_property
    def fhtx(self):
        self._fhtx = self.fht[:,:,0]

    @solps_property
    def fhty(self):
        self._fhty = self.fht[:,:,1]


    @solps_property
    def kinrgy(self):
        self._read_signal('_kinrgy','kinrgy')


    @solps_property
    def fluxt(self):
        if 'run.log' in self.avail:
            logdir = self.avail['run.log']
        elif 'run.log.gz' in self.avail:
            logdir = self.avail['run.log.gz']
        elif 'run.log.last10' in self.avail:
            logdir = self.avail['run.log.last10']
        else:
            self.log.warning("No run.log type file in self.available")
            self._fluxt = None
            return




    ### Only in SOLPS-ITER
    @solps_property
    def fna_mdf(self):
        self._read_signal('_fna_mdf','fna_mdf')

    @solps_property
    def fhe_mdf(self):
        self._read_signal('_fhe_mdf','fhe_mdf')

    @solps_property
    def fhi_mdf(self):
        self._read_signal('_fhi_mdf','fhi_mdf')

    @solps_property
    def fna_fcor(self):
        self._read_signal('_fna_fcor','fna_fcor')

    @solps_property
    def fna_nodrift(self):
        self._read_signal('_fna_nodrift','fna_nodrift')

    @solps_property
    def fna_he(self):
        self._read_signal('_fna_he','fna_he')

    @solps_property
    def fnaPSch(self):
        self._read_signal('_fnaPSch','fnaPSch')

    @solps_property
    def fhePSch(self):
        self._read_signal('_fhePSch','fhePSch')

    @solps_property
    def fhiPSch(self):
        self._read_signal('_fhiPSch','fhiPSch')

    @solps_property
    def fna_eir(self):
        self._read_signal('_fna_eir','fna_eir')

    @solps_property
    def fne_eir(self):
        self._read_signal('_fne_eir','fne_eir')

    @solps_property
    def fhe_eir(self):
        self._read_signal('_fhe_eir','fhe_eir')

    @solps_property
    def fhi_eir(self):
        self._read_signal('_fhi_eir','fhi_eir')

    @solps_property
    def fna_32(self):
        self._read_signal('_fna_32','fna_32')

    @solps_property
    def fna_52(self):
        self._read_signal('_fna_52','fna_52')

    @solps_property
    def fni_32(self):
        self._read_signal('_fni_32','fni_32')

    @solps_property
    def fni_52(self):
        self._read_signal('_fni_52','fni_52')

    @solps_property
    def fne_32(self):
        self._read_signal('_fne_32','fne_32')

    @solps_property
    def fne_52(self):
        self._read_signal('_fne_52','fne_52')

    @solps_property
    def fchdia(self):
        self._read_signal('_fchdia','fchdia')

    @solps_property
    def fchin(self):
        self._read_signal('_fchin','fchin')

    @solps_property
    def fchvispar(self):
        self._read_signal('_fchvispar','fchvispar')

    @solps_property
    def fchvisper(self):
        self._read_signal('_fchvisper','fchvisper')

    @solps_property
    def fchvisq(self):
        self._read_signal('_fchvisq','fchvisq')

    @solps_property
    def fchinert(self):
        self._read_signal('_fchinert','fchinert')

    @solps_property
    def vaecrb(self):
        self._read_signal('_vaecrb','vaecrb')

    @solps_property
    def vadia(self):
        self._read_signal('_vadia','vadia')

    @solps_property
    def wadia(self):
        self._read_signal('_wadia','wadia')

    @solps_property
    def veecrb(self):
        self._read_signal('_veecrb','veecrb')

    @solps_property
    def vedia(self):
        self._read_signal('_vedia','vedia')

    @solps_property
    def floe_noc(self):
        self._read_signal('_floe_noc','floe_noc')

    @solps_property
    def floi_noc(self):
        self._read_signal('_floi_noc','floi_noc')















    # -------------------- Coefficients ---------------------------------------
    @solps_property
    def hce0(self):
        self._read_signal('_hce0', 'hce0')

    @solps_property
    def kyeperp(self):
        self._kyeperp = self.hce0/self.ne

    @solps_property
    def hcib(self):
        self._read_signal('_hcib', 'hcib')

    @solps_property
    def kyiperp(self):
        self._kyiperp = self.hcib/self.na



    # -------------------- Rates, losses/sources and residuals ----------------
    # For some reason, MdsData.resmo and RundirData.resmo are not the same.
    # However, it doesn't seem to be caused by the reading routines but
    # for a mismatch of the data itself.


    @solps_property
    def resco(self):
        """(nx,ny,ns): Residuals of the continuity equation.
        """
        self._read_signal('_resco', 'resco')
        if (self._resco is not None and
            self._resco.shape ==(self.nx,self.ny-1,self.ns)):
              self._resco = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                           self._resco), axis = 1)
    @solps_property
    def respo(self):
        """(nx,ny): Residuals of the potential equation.
        """
        self._read_signal('_respo', 'respo')
        if (self._respo is not None and
            self._respo.shape ==(self.nx,self.ny-1,self.ns)):
              self._respo = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                           self._respo), axis = 1)

    @solps_property
    def reshe(self):
        """(nx,ny): Residuals of the electron heat equation.
        """
        self._read_signal('_reshe', 'reshe')
        if (self._reshe is not None and
            self._reshe.shape ==(self.nx,self.ny-1,self.ns)):
              self._reshe = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                           self._reshe), axis = 1)
    @solps_property
    def reshi(self):
        """(nx,ny): Residuals of the ion heat equation.
        """
        self._read_signal('_reshi', 'reshi')
        if (self._reshi is not None and
            self._reshi.shape ==(self.nx,self.ny-1,self.ns)):
              self._reshi = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                           self._reshi), axis = 1)



    @solps_property
    def sna(self):
        self._read_signal('_sna', 'sna')

    @solps_property
    def particle_source(self):
        self._particle_source = (self.sna[...,0,:] +
                                 self.sna[...,1,:]*self.na)

    @solps_property
    def b2stbr_sna(self):
        """(nx,ny,ns) : Recycling particle source [particles/s]

        Notes
        -----
        This is rescaled via the 'self._rescale_neutrals' method using the
        scaling factor specified in the 'b2mndr_rescale_neutrals_sources'
        switch in b2mn.dat.
        """
        self._read_signal('_b2stbr_sna', 'b2stbr_sna')
        self._b2stbr_sna = self._rescale_neutrals(self._b2stbr_sna)

    @solps_property
    def resmt(self):
        """(nx,ny) : Total momentum equation residuals [kg*m/s]"""
        self._read_signal('_resmt','resmt')

    @solps_property
    def smaf(self):
        """(nx,ny,4,ns): Coefficients of the linear expression of the
        parallel momentum source of species is from friction [N].
        """
        self._read_signal('_smaf', 'b2npmo_smaf')

    @solps_property
    def smag(self):
        """(nx,ny,4,ns): Coefficients of the linear expression of the
        parallel momentum source of species is from pressure gradient [N].
        """
        self._read_signal('_smag', 'b2npmo_smag')

    @solps_property
    def b2stbr_shi(self):
        self._read_signal('_b2stbr_shi','b2stbr_shi')
    @solps_property
    def b2stbr_she(self):
        self._read_signal('_b2stbr_she','b2stbr_she')

    @solps_property
    def b2stbc_shi(self):
        self._read_signal('_b2stbc_shi','b2stbc_shi')
    @solps_property
    def b2stbc_she(self):
        self._read_signal('_b2stbc_she','b2stbc_she')



    @solps_property
    def rtzmin(self):
        self._read_signal('_rtzmin','rtzmin')

    @solps_property
    def rtzmax(self):
        self._read_signal('_rtzmax','rtzmax')

    @solps_property
    def rtzn(self):
        self._read_signal('_rtzn','rtzn')

    @solps_property
    def rtt(self):
        self._read_signal('_rtt','rtt')

    @solps_property
    def rtn(self):
        self._read_signal('_rtn','rtn')

    @solps_property
    def rtlt(self):
        self._read_signal('_rtlt','rtlt')

    @solps_property
    def rtln(self):
        self._read_signal('_rtln','rtln')

    #Attention: All these quantities can be modified by the
    # /atomic_physics_rescale/ namelist.
    @solps_property
    def rtlsa(self):
        self._read_signal('_rtlsa','rtlsa')

    @solps_property
    def rtlra(self):
        self._read_signal('_rtlra','rtlra')
        rescal = float(self.read_switch('b2mndr_rescale_neutrals_sources'))
        if rescal is not None:
            self._rtlra += np.log(rescal)

    @solps_property
    def rtlqa(self):
        self._read_signal('_rtlqa','rtlqa')

    @solps_property
    def rtlcx(self):
        self._read_signal('_rtlcx','rtlcx')

    @solps_property
    def rtlrd(self):
        self._read_signal('_rtlrd','rtlrd')

    @solps_property
    def rtlbr(self):
        self._read_signal('_rtlbr','rtlbr')

    @solps_property
    def rtlza(self):
        self._read_signal('_rtlza','rtlza')

    @solps_property
    def rtlz2(self):
        self._read_signal('_rtlz2','rtlz2')

    @solps_property
    def rtlpt(self):
        self._read_signal('_rtlpt','rtlpt')

    @solps_property
    def rtlpi(self):
        self._read_signal('_rtlpi','rtlpi')



    @solps_property
    def rlsa(self):
        """(nx,ny,2,ns):    Ionisation rate coefficients."""
        self._load_b2spel()

    @solps_property
    def rlra(self):
        """(nx,ny,2,ns):    Recombination rate coefficients."""
        self._load_b2spel()

    @solps_property
    def rlqa(self):
        """(nx,ny,2,ns):    Heat loss rate coefficients."""
        self._load_b2spel()

    @solps_property
    def rlrd(self):
        """(nx,ny,2,ns):    Line radiation rate coefficients."""
        self._load_b2spel()

    @solps_property
    def rlbr(self):
        """(nx,ny,2,ns):    Bremsstrahlung radiation rate coefficients."""
        self._load_b2spel()

    @solps_property
    def rlbr(self):
        """(nx,ny,2,ns):    Bremsstrahlung radiation rate coefficients."""
        self._load_b2spel()

    @solps_property
    def rlza(self):
        """(nx,ny,2,ns):    Effective charge state coefficients."""
        self._load_b2spel()

    @solps_property
    def rlz2(self):
        """(nx,ny,2,ns):    Effective square charge coefficients."""
        self._load_b2spel()

    @solps_property
    def rlpt(self):
        """(nx,ny,2,ns):    Cumulative ionisation potential coefficients."""
        self._load_b2spel()

    @solps_property
    def rlpi(self):
        """(nx,ny,2,ns):    Effective ionisation potential coefficients."""
        self._load_b2spel()




    @solps_property
    def rsa(self):
        """(nx,ny,2,ns):    Ionisation rate."""
        self._load_b2sqel()

    @solps_property
    def rra(self):
        """(nx,ny,2,ns):    Recombination rate."""
        self._load_b2sqel()

    @solps_property
    def rqa(self):
        """(nx,ny,2,ns):    Heat loss rate."""
        self._load_b2sqel()

    @solps_property
    def rrd(self):
        """(nx,ny,2,ns):    Line radiation rate."""
        self._load_b2sqel()

    @solps_property
    def rbr(self):
        """(nx,ny,2,ns):    Bremsstrahlung radiation rate."""
        self._load_b2sqel()

    @solps_property
    def rbr(self):
        """(nx,ny,2,ns):    Bremsstrahlung radiation rate."""
        self._load_b2sqel()

    @solps_property
    def rza(self):
        """(nx,ny,2,ns):    Effective charge state."""
        self._load_b2sqel()

    @solps_property
    def rz2(self):
        """(nx,ny,2,ns):    Effective square charge."""
        self._load_b2sqel()

    @solps_property
    def rpt(self):
        """(nx,ny,2,ns):    Cumulative ionisation potential."""
        self._load_b2sqel()

    @solps_property
    def rpi(self):
        """(nx,ny,2,ns):    Effective ionisation potential."""
        self._load_b2sqel()



    # -------------------- Texts ----------------------------------------------






#                   ============== EXTENSIONS ================
    @solps_property
    def eqout(self):
        """ Equations output with b2mwdt = 4"""
        self._eqout = EquationsOutput(self)














    ## TO BE EVALUATED, CORRECTED AND IMPLEMENTED
    # ========= Properties: ===================================================

    # ==================
    # In b2fplasmf:
    @property
    @try_block
    def smth(self):
        """Thermal force [N]"""
        self._smth = self.smpt + self.smpr
    @property
    def thermal_force(self):
        """Thermal force [N]. Alias of smth"""
        return self.smth

    @property
    @try_block
    def smpr(self):
        """Thermal force (proportional to exhp) [N]"""
        self._read_signal('_smpr', 'smpr')

    @property
    @try_block
    def smpt(self):
        """Thermal force (proportional to Ti and Te gradients) [N]"""
        self._read_signal('_smpt', 'smpt')

    @property
    @try_block
    def smfr(self):
        """Friction force [N]"""
        self._read_signal('_smfr', 'smfr')
    @property
    def friction_force(self):
        """Friction force [N]. Alias of smfr"""
        return self.smfr

    # ==================
    # In b2fstate:

    #What is this for??
    @property
    @try_block
    def time(self):
        self._read_signal('_time','time')

    # ==================
    #In b2fgmtry:
    @property
    @try_block
    def fpsi(self):
        self._read_signal('_fpsi','fpsi')
    @property
    @try_block
    def ffbz(self):
        self._read_signal('_ffbz','ffbz')
    @property
    @try_block
    def qc(self):
        self._read_signal('_qc','qc')

    @property
    @try_block
    def pbs(self):
        self._read_signal('_pbs','pbs')



    # ========================== FUNCTIONS ====================================

    #ATTENTION: Poor naming? Utility?
    def open_file(self,file):
        """Opens .png, .pdf, .(e)ps or text files in the run directory."""
        open_file(file=file,dir=self.rundir)

    def read_signal(self, signal_name, var_name, attach_to = None):
        """
        Similar API to the one of mds: Name then link

        MdsData         read_signal('_zn', 'blablabla:ZN')
        RundirData      read_signal('_zn', 'zn')

        Does it add something or should I simply rename parser to read_signal?
        """
        if not attach_to:
            attach_to = self
        try:
            var = self.parser(var_name)
            setattr(attach_to, signal_name, var)
        except:
            self.log.warning("error reading '{0}''".format(var_name))
            setattr(attach_to, signal_name, None)


    def parser(self, var):
        """
        Parses and decide which file to parse.
        It simply returns the array with the value or None
        For now, it only parses known variables and files
        Maybe it would be beter to have different parsers separately
        and let read_signal to decide
        """
        try:
            fname, dims = self.where(var)
            fd = self.avail[fname]
            nentries = np.prod(dims)
            factor = self._factors(var)
        except:
            self.log.warning("'{0}' could not be parsed".format(var))
            return None

        b2f_like_files = ['b2fplasmf','b2fstate','b2fstati','b2fgmtry',
                'b2fpardf', 'b2frates']
        fort_like_files = ['fort.44']
        time_trace_like_files = []

        data = []

        #B2f files
        if fname in b2f_like_files:
            with open(fd, 'r') as f:
                is_body = False
                for i, line in enumerate(f):
                    if (line.split()[0] == '*cf:' and
                        #line.split()[3] == var.lower() and
                        line.split()[3] == var and
                        line.split()[2] > 0):
                        i_header = i
                        nentry = int(line.split()[2])
                        is_body = True
                        #data = []
                        continue
                    if is_body:
                        pieces = line.split()
                        for piece in pieces:
                            data.append(np.float(piece))
                        if len(data) == nentry: break

        #Eirene output for SOLPS-ITER
        elif (fname in fort_like_files and self.solps_code == 'solps-iter'):
            with open(fd,'r') as f:
                #Skip unnecesary lines
                for i in xrange(2+self.natm+self.nmol+self.nion):
                    f.readline()
                is_body = False
                for i, line in enumerate(f):
                    wh_header = (line.split()[0] ==  '*eirene' and
                                 var in line.split() and
                                 line.split()[-1] == str(nentries))
                    if wh_header:
                        i_header = i
                        is_body = True
                        #data = []
                        continue
                    if is_body:
                        pieces = line.split()
                        for piece in pieces:
                            data.append(np.float(piece))
                        if len(data) == nentries: break

        #Eirene output for SOLPS5.0
        elif (fname in fort_like_files and self.solps_code == 'solps5.0'):
            order=['dab2', 'tab2', 'dmb2', 'tmb2', 'dib2', 'tib2']
            pos = order.index(var)
            skip_entries = 0
            for v in order[:pos]:
               skip_entries += np.prod(self.where(v)[1])
            with open(fd,'r') as f:
                #Skip all unnecessary lines and entries
                ## Hint: Many!
                for i in xrange(2+self.natm+self.nmol+self.nion):
                    f.readline()
                is_body = False
                counter = 0
                for i, line in enumerate(f):
                    if skip_entries == 0 and i == 0:
                        is_body = True
                        #data = []

                    if is_body:
                        pieces = line.split()
                        for piece in pieces:
                            data.append(np.float(piece))
                        if len(data) == nentries: break

                    if not is_body:
                        counter += len(line.split())
                        if counter >= skip_entries:
                            is_body = True
                            #data = []
                            continue

        #If not recognized, return None
        else:
            return None
        return np.array(data).reshape(dims, order = 'F')*factor



    #ATTENTION: What is this for??
    # Mark for deletion??
    def time_series(self, var, name=None, target=None):
        """ 
        Needs to check if pupynere is loaded and, if not, load it. Else this
        cannot be used!
        It doesn't work yet.
        """
        if target is None:
            target = self
        if name is None:
            name = var

        #Maybe use scipy.io better?
        try:
            import pupynere
        except:
            try:
                import solpspy.tools.pupynere
            except:
                setattr(target, name, None)
                return

        #CHANGE TO SELF.AVAIL FORM, MAYBE?
        exeb2tallies = os.path.join(self.rundir, 'b2mn.exe.dir/b2tallies.nc')
        b2tallies = os.path.join(self.rundir, 'b2tallies.nc')
        exeb2time = os.path.join(self.rundir, 'b2mn.exe.dir/b2time.nc')
        b2time = os.path.join(self.rundir, 'b2time.nc')

        if os.access(exeb2tallies, os.R_OK):
            ftallies=pupynere.netcdf_file(exeb2tallies,'r')
        elif os.access(b2tallies, os.R_OK):
            ftallies=pupynere.netcdf_file(b2tallies,'r')
        else:
            ftallies = ''
        if os.access(exeb2time, os.R_OK):
            ftime = pupynere.netcdf_file(exeb2time,'r')
        elif os.access(b2time, os.R_OK):
            ftime = pupynere.netcdf_file(b2time,'r')
        else:
            ftime = ''

        if var in ftallies:
            setattr(target, name, ftallies[var])
            return
        elif var in ftime:
            setattr(target, name, ftime[var])
            return
        else:
            setattr(target, name, None)
            return

    #ATTENTION: Does not work?? At least in slab model
    def read_switch(self, name, b2file = 'b2mn.dat'):
        """
          Used to read switchers of b2-stylefiles (b2*.dat)
          Maybe in the future, add dictionary with location according to name
          similar to self.where for the outputs.

        Parameters
        ----------
        name: (str)
            Name of the switch
        b2file: (str)
            Name of the b2-style file in which the switch is declared

        Outputs
        -------
        (str)
            Value of the switch
        """
        try:
            with open(self.avail[b2file], 'r') as f:
                stack = []
                for line in f:
                    dummy = line.replace("'","").split()
                    try:
                        if dummy[0].lower() == name.lower():
                            stack.append(dummy[1])
                    except:
                        pass
                try:
                    return stack[-1]
                except:
                    return None
        except:
            return None



    def where(self, var):
        """
          Locate the (best) file which contains a certain variable and
        provides the name of the file and the dimensions of the variable
        """
        signal = var.lower()

        s = (self.ns)
        xy = (self.nx, self.ny)
        reg = (3) # Should be shot.nnreg
        xys = (self.nx, self.ny, self.ns)
        xy2 = (self.nx, self.ny, 2)
        xy3 = (self.nx, self.ny, 3)
        xy4 = (self.nx, self.ny, 4)
        xy2s = (self.nx, self.ny, 2, self.ns)
        xy4s = (self.nx, self.ny, 4, self.ns)
        xyreg = (self.nx, self.ny, 3) #Should be shot.nnreg

        #Eirene stores them in matix without guarding cells
        fxy = (self.nx-2,self.ny-2)
        fxya = (self.nx-2,self.ny-2, self.natm)
        fxym = (self.nx-2,self.ny-2, self.nmol)
        fxyi = (self.nx-2,self.ny-2, self.nion)

        # For rates
        rtnt = self.rtnt
        rtnn = self.rtnn
        rtns = self.rtns
        rt = (self.rtnt, self.rtnn, self.rtns)

        in_b2fstate = {'zamin':s, 'zamax':s, 'zn':s, 'am':s,
           'na':xys, 'ne':xy, 'ua':xys, 'uadia':xy2s, 'te':xy, 'ti':xy,
           'po':xy, 'fna':xy2s, 'fhe':xy2, 'fhi':xy2, 'fch':xy2,
           'fch_32':xy2, 'fch_52':xy2, 'kinrgy':xys, 'time':1, 'fch_p':xy}

        ## Only for newer versions of SOLPS-ITER
        in_b2fstate_iter = {
           'fna_mdf':xy2s, 'fhe_mdf':xy2, 'fhi_mdf':xy2,'fna_fcor':xy2s,
           'fna_nodrift':xy2s,'fna_he':xy2s,'fnapsch':xy2s,
           'fhepsch':xy2, 'fhipsch':xy2, 'fna_eir':xy2s, 'fne_eir':xy2,
           'fhe_eir':xy2, 'fhi_eir':xy2, 'fna_32':xy2s, 'fna_52':xy2s,
           'fni_32':xy2, 'fni_52':xy2, 'fne_32':xy2,'fne_52':xy2,'fchdia':xy2,
           'fchin':xy2, 'fchvispar':xy2, 'fchvisper':xy2, 'fchvisq':xy2,
           'fchinert':xy2, 'vaecrb':xy2s, 'vadia':xy2s, 'wadia':xy2s,
           'veecrb':xy2, 'vedia':xy2, 'floe_noc':xy2, 'floi_noc':xy2}

        if self.solps_code == 'solps-iter':
            in_b2fstate.update(in_b2fstate_iter)


        in_b2fplasmf = {'bb':xy4, 'crx':xy4, 'cry':xy4, 'ffbz':xy4,
           'fpsi':xy4, 'gs':xy3, 'hx':xy, 'hy':xy, 'qz':xy2, 'qc':xy,
           'vol':xy, 'pbs':xy2, 'fch':xy2, 'fch0':None, 'fchp':None,
           'fhe':xy2, 'fhe0':None, 'fhep':None, 'fhet':None, 'fhi':xy2,
           'fhi0':None, 'fhip':None, 'fhit':None, 'fna':xy2s, 'fna0':None,
           'fnap':None, 'fne':xy2, 'fni':None, 'na':xys, 'na0':None, 'nap':None,
           'ne':xy, 'ne0':None, 'ne2':None, 'nep':None, 'ni':xy2, 'ni0':None,
           'pb':None, 'po':xy, 'po0':None, 'pop':None, 'te':xy, 'te0':None,
           'tep':None, 'ti':xy, 'ti0':None, 'tip':None, 'ua':xys, 'ua0':None,
           'uap':None, 'uadia':xy2s, 'fchdia':None, 'fmo':xy2s, 'fna_32':xy2s,
           'fna_52':xy2s, 'fni_32':None, 'fni_52':None, 'fne_32':None,
           'fne_52':None, 'wadia':None, 'vaecrb':None, 'facdrift':None,
           'fac_ExB':None, 'fchvispar':None, 'fchvisper':None, 'fchin':None,
           'fna_nodrift':xy2s, 'fac_vis':None, 'resco':xys, 'reshe':xy,
           'reshi':xy, 'resmo':xys, 'resmt':xy, 'respo':xy, 'sch':None,
           'she':None, 'shi':None, 'smo':xy4s, 'smq':xy4s, 'sna':xy2s, 'sne':None,
           'rsana':xys, 'rsahi':xys, 'rsamo':xys, 'rrana':xys, 'rrahi':xys,
           'rramo':xys, 'rqahe':xys, 'rqrad':xys, 'rqbrm':xys, 'rcxna':None,
           'rcxhi':None, 'rcxmo':xys, 'b2stbr_sna':xys, 'b2stbr_smo':None,
           'b2stbr_she':xy, 'b2stbr_shi':xy, 'b2stbr_sch':None,
           'b2stbr_sne':None, 'b2stbc_sna':None, 'b2stbc_smo':None,
           'b2stbc_she':xy, 'b2stbc_shi':xy,' b2stbc_sch':None,
           'b2stbc_sne':None, 'b2stbm_sna':None, 'b2stbm_smo':None,
           'b2stbm_she':None, 'b2stbm_shi':None, 'b2stbm_sch':None,
           'b2stbm_sne':None, 'b2sihs_divue':None, 'b2sihs_divua':None,
           'b2sihs_exbe':None, 'b2sihs_exba':None, 'b2sihs_visa':None,
           'b2sihs_joule':None, 'b2sihs_fraa':None, 'b2sihs_str':None,
           'b2npmo_smaf':xy4s, 'b2npmo_smag':xy4s, 'b2npmo_smav':xy4s,
           'smpr':xys, 'smpt':xys, 'smfr':xys, 'ext_sna':None, 'ext_smo':None,
           'ext_she':None, 'ext_shi':None, 'ext_sch':None, 'ext_sne':None,
           'calf':None, 'cdna':None, 'cdpa':None, 'ceqp':None, 'chce':None,
           'chci':None, 'chve':None, 'chvemx':None, 'chvi':None, 'chvimx':None,
           'csig':None, 'cvla':None, 'cvsa':None, 'cthe':None, 'cthi':None,
           'csigin':None,'cvsa_cl':None,'fllime':None, 'fllimi':None,
           'fllim0fna':None, 'fllim0fhi':None, 'fllimvisc':None, 'vsal':None,
           'sig0':None, 'hce0':xy, 'alf0':None, 'hci0':xy, 'hcib':xys,
           'dpa0':xys, 'dna0':xys, 'vsa0':None, 'vla0':None, 'csig_an':None,
           'calf_an':None, 'nstra':None, 'sclstra':None, 'sclrtio':None,
           'fhp':xy2s,'fhj':xy2,'fhm':xy2s,'fht':xy2,'b2npmo_smat':xy4s}
           # ^ The values in the last line only exist with the SOLPS
           # code from Felix Reimold!

        in_b2fgmtry = {'crx':xy4, 'cry':xy4, 'fpsi':xy4, 'ffbz':xy4,
           'bb':xy4, 'vol':xy, 'hx':xy, 'hy':xy, 'qz':xy2, 'qc':xy,
           'gs':xy3, 'nlreg':1, 'nlxlo':1, 'nlxhi':1, 'nlylo':1,
           'nlyhi':1, 'nlloc':None, 'nncut':1, 'leftcut':self.nncut,
           'rightcut':self.nncut, 'topcut':self.nncut, 'bottomcut':self.nncut,
           'leftix':xy, 'rightix':xy, 'topix':xy, 'bottomix':xy, 'leftiy':xy,
           'rightiy':xy, 'topiy':xy, 'bottomiy':xy, 'region':xyreg,
           'nnreg':reg, 'resignore':None, 'periodic_bc':None, 'pbs':xy2,
           'parg':None}

        in_b2fpardf = {'zamin':None, 'zamax':None, 'zn':None, 'am':None,
            'cbir':None, 'cbnr': None,'cbrbrk':None, 'cbsna':None, 'cbsmo':None,
           'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None,
           'cbmsb':None, 'cbsna':None, 'cbsmo':None, 'cbshi':None, 'cbshe':None,
           'cbsch':None, 'cbrec':None, 'cbmsa':None, 'cbmsb':None, 'cbsna':None,
           'cbsmo':None, 'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None,
           'cbmsa':None, 'cbmsb':None, 'cbsna':None, 'cbsmo':None, 'cbshi':None,
           'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None, 'cbmsb':None,
           'cbsna':None, 'cbsmo':None, 'cbshi':None, 'cbshe':None, 'cbsch':None,
           'cbrec':None, 'cbmsa':None, 'cbmsb':None, 'cbsna':None, 'cbsmo':None,
           'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None,
           'cbmsb':None, 'cfdf0':None, 'cfdna':None, 'cfdpa':None, 'cfvla':None,
           'cfvsa':None, 'cfhci':None, 'cfhce':None, 'cfsig':None, 'cfalf':None,
           'cflim':None}

        in_b2frates = {  'ppout':None, 'rtnt':None, 'rtnn':None, 'rtns':None,
           'rtzmin':rtns,'rtzmax':rtns,'rtzn':rtns, 'rtt':rtnt,  'rtn':rtnn,
           'rtlt':rtnt,  'rtln':rtnn,  'rtlsa':rt,'rtlra':rt,'rtlqa':rt,
           'rtlcx':rt, 'rtlrd':rt, 'rtlbr':rt,'rtlza':rt,'rtlz2':rt,
           'rtlpt':rt, 'rtlpi':rt}


        in_fort44 = {'dab2':fxya, 'tab2':fxya, 'dmb2':fxym, 'tmb2':fxym,
                     'dib2':fxyi, 'tib2':fxyi}

        if   signal in in_b2fplasmf and 'b2fplasmf' in self.avail:
            return ('b2fplasmf', in_b2fplasmf[signal])
        elif signal in in_b2fstate and 'b2fstate' in self.avail:
            return ('b2fstate', in_b2fstate[signal])
        elif signal in in_b2fgmtry and 'b2fgmtry' in self.avail:
            return ('b2fgmtry', in_b2fgmtry[signal])
        elif signal in in_b2fpardf and 'b2fpardf' in self.avail:
            return ('b2fpardf', in_b2fpardf[signal])
        elif signal in in_b2frates and 'b2frates' in self.avail:
            return ('b2frates', in_b2frates[signal])
        elif signal in in_fort44 and 'fort.44' in self.avail:
            return ('fort.44', in_fort44[signal])
        elif signal in in_b2fstate and 'b2fstati' in self.avail:
            self.log.warning("'{0}' signal read from b2fstati".format(signal))
            return ('b2fstati', in_b2fstate[signal])

        elif signal in in_b2fstate and 'baserun_b2fstate' in self.avail:
            self.log.warning("'{0}' signal read from baserun/b2fstate".format(signal))
            return ('baserun_b2fstate', in_b2fstate[signal])
        elif signal in in_b2fstate and 'baserun_b2fstati' in self.avail:
            self.log.warning("'{0}' signal read from baserun/b2fstati".format(signal))
            return ('baserun_b2fstati', in_b2fstate[signal])
        else:
            self.log.error(
                    "File(s) containing '{0}' not available".format(signal))
            raise Exception



    # ========= Private methods: ==============================================
    #ATTENTION: Add docstrig
    def _get_basic_dimensions(self):

        #Get nx, ny and ns from b2fstat*
        if 'b2fstate' in self.avail or 'b2fstati' in self.avail:
            try:
                gdir = self.avail['b2fstate']
            except:
                gdir = self.avail['b2fstati']
            with open(gdir) as fg:
                for i in range(2):
                    fg.readline()
                tmp = fg.readline().split()
                self.nx = int(tmp[0]) + 2
                self.ny = int(tmp[1]) + 2
                self.ns = int(tmp[2])
        #Or get nx, ny from b2fgmtry and ns form b2fpardf
        elif 'b2fgmtry' in self.avail or 'b2fpardf' in self.avail:
            if 'b2fgmtry' in self.avail:
                gdir = self.avail['b2fgmtry']
                with open(gdir) as fg:
                    for i in range(2):
                        fg.readline()
                    tmp = fg.readline().split()
                    self.nx = int(tmp[0]) + 2
                    self.ny = int(tmp[1]) + 2
            else:
                self.log.warning("nx, ny could not be retrieved.")
        else:
            self.log.error("nx, ny could not be retrieved.")

            if 'b2fgmtry' in self.avail:
                pdir = self.avail['b2fpardf']
                with open(pdir) as fp:
                    for i in range(2):
                        fp.readline()
                self.ns = int(fp.readline().split()[0])
            else:
                self.log.error("ns could not be retrieved")


        #Read EIRENE dims: natm, nmol and nion
        try:
            fdir = self.avail['fort.44']
            with open(fdir,'r') as fd:
                line = [int(x) for x in fd.readline().split()[:3]]
                if (line[0]+2 != self.nx) or (line[1]+2 != self.ny):
                    self.log.error("Faulty fort.44")
                    self.natm = None
                    self.nmol = None
                    self.nion = None
                    self.jvft44 = None
                    return
                self.jvft44 = line[2]
                line = [int(x) for x in fd.readline().split()]
                self.natm = line[0]
                self.nmol = line[1]
                self.nion = line[2]
                self.atm = []
                self.mol = []
                self.ion = []
                for i in xrange(self.natm):
                    self.atm.append(fd.readline().strip())
                for i in xrange(self.nmol):
                    self.mol.append(fd.readline().strip())
                for i in xrange(self.nion):
                    self.ion.append(fd.readline().strip())
        except:
            self.log.warning("natm, nmol and nion could not be retrieved.")
            self.natm = None
            self.nmol = None
            self.nion = None
            self.jvft44 = None
            self.log.warning("Assuming b2 standalone run.")
            self.b2standalone=True


        #Read Rates dims: rtnt, rtnn, rtns
        try:
            fdir = self.avail['b2frates']
            with open(fdir,'r') as fd:

                # Go to 'label':
                while ( True ):
                    line = fd.readline()
                    if line.split()[-1] == 'label':
                        break

                # Read out rates table, which is 2 words after the timestamp (containing ':'):
                try:
                    self.rates_table = fd.readline().split(':')[1].split()[2]
                except:
                    self.log.warning("Name of atomic rates table"+
                                     " could not be retrieved")
                    self.rates_table = None

                # Go to 'rtnt,rtnn,rtns':
                while ( True ):
                    line = fd.readline()
                    if line.split()[-1] == 'rtnt,rtnn,rtns':
                        break

                # Read 'rtnt,rtnn,rtns':
                line = [int(x) for x in fd.readline().split()]
                self.rtnt = line[0] + 1
                self.rtnn = line[1] + 1
                self.rtns = line[2]
        except:
            self.log.warning("rtnt, rtnn and rtns could not be retrieved")
            self.rtnt = None
            self.rtnn = None
            self.rtns = None



    def _read_signal(self, signal_name, var_name, attach_to = None):
        """
        Similar API to the one of mds: Name then link

        MdsData         read_signal('_zn', 'blablabla:ZN')
        RundirData      read_signal('_zn', 'zn')

        Does it add something or should I simply rename parser to read_signal?
        """
        if not attach_to:
            attach_to = self
        try:
            fname, dims = self.where(var_name)
            var = self.parser(var_name)
            if fname in ['fort.44']:
                var = self._extend(var)
            setattr(attach_to, signal_name, var)
        except Exception:
            #self.log.warning("error reading '{0}''".format(var_name))
            self.log.exception("Error reading '{0}''".format(var_name))
            setattr(attach_to, signal_name, None)



    #ATTENTION: Maybe find_file should return the direction and not add it directly
    #Attention: Mabe the files should be defined in config instead of here so people
    # can look for files with weird names and stuff
    def _check_avail(self):
        """
        Check which files are avail in the rundir.
        Include also a check for size, correction, etc.
        Check of size should not only check for non zero, but also for correct size

        Include  self._find_nosolps_file for spectroscopy, lines of sight, etc
        Or maybe move the finding of files wherever they are going to be
        used (i.e. to classes)
        """
        #B2 dat files
        self._find_file('b2mn.dat')
        self._find_file('b2ag.dat')
        self._find_file('b2ar.dat')
        self._find_file('b2ah.dat')

        #B2 input/parameters files
        self._find_file('b2.neutrals.parameters')
        self._find_file('b2.boundary.parameters')
        self._find_file('b2.transport.parameters')
        self._find_file('b2.transport.inputfile')

        #B2F files
        self._find_file('b2fplasmf')
        self._find_file('b2fplasma')
        self._find_file('b2fstate')
        self._find_file('b2fstati')
        self._find_file('b2fgmtry')
        self._find_file('b2fpardf')
        self._find_file('b2frates')

        #In baserun
        self._find_file('baserun_b2fstate')
        self._find_file('baserun_b2fstati')

        #Function should have the option for look up in extra locations
        self._find_file('mesh.extra')

        #Eirene files
        self._find_file('input.dat')
        self._find_file('fort.33')
        self._find_file('fort.34')
        self._find_file('fort.35')
        self._find_file('fort.44')
        self._find_file('fort.46')

        #log files
        self._find_file('run.log')
        self._find_file('run.log.gz')
        self._find_file('run.log.last10')
        self._find_file('eir.log')

        #ncfiles and friends
        self._find_file('b2time.nc')
        self._find_file('b2tallies.nc')
        self._find_file('particle_fluxes.nc')
        self._find_file('dsl')
        self._find_file('dsi')
        self._find_file('dsa')
        self._find_file('dsr')
        # da files
        self._find_file('dsR')
        self._find_file('dsL')
        self._find_file('dsTR')
        self._find_file('dsTL')

        #Extra files
        self._find_file('.spectroscopy_data.pkl')

        #Testing:
        self._find_file('b2movies.nc')
        self._find_file('b2fmovie')


    #ATTENTION: Maybe find_file should return the direction and not add it directly
    #Maybe should be reading from config file what is the hierarchy (local, ../baserun, etc)

    #ATTENTION: THE CONFLICT BETWEEN B2FSTAT[IE] AND BASERUN_B2FSTAT[IE] IS NOT YET SOLVED.
    #IT HAS TO BE SOMEHOW IMPLEMENTED IN FACTORY TOO
    def _find_file(self, file):
        """
        Find if file exists and is not zero in rundir, else tries in baserun,
        else print message
        """
        if   (os.path.isfile(os.path.join(self.rundir,file)) and
            os.stat(os.path.join(self.rundir,file)).st_size != 0):
                file_dir = os.path.abspath(os.path.join(self.rundir,file))
                self.avail[file] = file_dir

        elif ((file !='b2fstate' and file !='b2fstati') and
            os.path.isfile(os.path.join(self.rundir,'../baserun/',file)) and
            os.stat(os.path.join(self.rundir,'../baserun/',file)).st_size != 0):
                file_dir = os.path.abspath(os.path.join(self.rundir,
                    '../baserun/',file))
                self.avail[file] = file_dir

        elif ((file =='baserun_b2fstate' or file =='baserun_b2fstati') and
            os.path.isfile(os.path.join(self.rundir,'../baserun/',file[8:])) and
            os.stat(os.path.join(self.rundir,'../baserun/',file[8:])).st_size != 0):
                file_dir = os.path.abspath(os.path.join(self.rundir,
                    '../baserun/',file[8:]))
                self.avail[file] = file_dir

        else:
            self.log.info("{0} not found in either rundir or baserun!".format(file))




    def _factors(self, var):
        """
        Temperatures are stored in J and not in eV
        """
        temps = Set(['te','ti','tab2','tmb2','tib2'])
        if var.lower() in temps:
           return 6.2415093432601784e+18
        else:
            return 1.0

    def _read_mesh_extra(self, target=None):
        """
        Reads mesh.extra to extract the vessel structure inside
        """
        try:
            with open(self.avail['mesh.extra'], 'r') as f:
                lines = f.readlines()
                nsurf = len(lines)
                vessel = np.zeros((nsurf,4))
                for i,line in enumerate(lines):
                    line.split()
                    vessel[i,:] = np.array([np.float(line.split()[k])
                                            for k in range(4)])
                self._vessel_mesh = vessel.T
        except:
            self._vessel_mesh = None

    def _read_structure_input_dat(self):
        """
        Parses the input.dat file and produces the following variables:

        self._vessel (4,NLIMI) : Array containing the data (positions)
                                 for the additional surfaces defined in 
                                 input.dat block 3b.

                                 NLIMI is the total number of surfaces as
                                 defined in input.dat.

                                 x1 = self._vessel[0,:]
                                 y1 = self._vessel[1,:]
                                 x2 = self._vessel[2,:]
                                 y2 = self._vessel[3,:]

        self._vessel_colors    : A dictionary containing the color index
                                 ILCOL as defined in the input.dat file for
                                 each surface. (Format {nsurface : ILCOL})
        """

        # Read input.dat into buffer:
        with open(self.avail['input.dat'],'r') as file:
            lines = file.readlines()

        # Parse input.dat:
        i      = 0
        nlines = len(lines)

        # Go to Block 3B:
        while i < nlines and not lines[i].lower().startswith("*** 3b"):
            i += 1
        i += 1

        # Parse Block 3B:
        data  = [[],[],[],[]] # Format: x1,y1,x2,y2
        ILCOL = []            # color integer ILCOL as defined in input.dat
        while i+3 < nlines and not lines[i].startswith("***"):
            if lines[i].startswith("*"):
                data[0].append(float(lines[i+3][0 :12])/100.0) # (cm --> m)
                data[1].append(float(lines[i+3][12:24])/100.0)
                data[2].append(float(lines[i+3][36:48])/100.0)
                data[3].append(float(lines[i+3][48:60])/100.0)
                ILCOL.append(int(lines[i+2].split()[5]))
                i += 4
            else:
                i += 1

        if data == []:
            self.log.warning("Could not read any vessel data from 'input.dat'!")
            raise Exception

        self._vessel = np.array(data)
        self._vessel_colors = {nsurf:ilcol for nsurf,ilcol in enumerate(ILCOL)}


    #ATTENTION: Improve docstrig
    #ATTENTION: Review logic
    def _extend(self, field):
        """
        Similar to b2plot/extend.F but only for grid dimensions.
        Should be similar to, but not implemented (why waste time right now?)
        For now, just use it for the standard case of connected spaces
        """
        ndim = list(field.shape)
        ndim[0], ndim[1] = ndim[0]+2, ndim[1]+2
        ndim = tuple(ndim)
        new_field = np.zeros(ndim)
        new_field[1:-1,1:-1] = field
        new_field[0] = new_field[1]
        new_field[-1] = new_field[-2]
        new_field[:,0] = new_field[:,1]
        new_field[:,-1] = new_field[:,-2]
        return new_field

    #ATTENTION: Improve docstrig
    #ATTENTION: Review logic
    def _read_triangular_mesh(self):
        """
        This method will read fort.33 and fort.34 to create a dictionary
        containing the triangular mesh that will be then used in conjunction
        to fort.46 to make color plots.
        self._triang_nodes contains the indexes of the vertices of the triangle
        self._triang_rz contains r and z for a given vertix index
        """
        try:
            with open(self.avail['fort.33'], 'r') as f:
                nnod = int(f.readline())
                r_trl = []
                z_trl = []
                r_token = True
                for line in f:
                    if r_token:
                        r_trl =  list(itertools.chain(r_trl,
                            [float(i) for i in line.split()]))
                        if len(r_trl) == nnod:
                            r_token = False
                    else:
                        z_trl =  list(itertools.chain(z_trl,
                            [float(i) for i in line.split()]))

                self._triang_rz = np.zeros((nnod,2))
                self._triang_rz[:,0] = np.array(r_trl)
                self._triang_rz[:,1] = np.array(z_trl)

            with open(self.avail['fort.34'], 'r') as f:
                self._triang_nodes =[]
                ntr = int(f.readline())
                for line in f:
                    line = line.split()
                    #Python notation
                    self._triang_nodes.append((int(line[1])-1,
                        int(line[2])-1, int(line[3])-1))
        except:
            self._triang_nodes = None
            self._triang_rz = None



    def _load_b2spel(self):
        """ Run and unpack variables calculated by b2spel"""
        from solpspy.b25_src.sources.b2spel import b2spel

        allvars = b2spel(self)

        if not hasattr(self,'_rlsa'):
           self._rlsa = allvars[0]
        if not hasattr(self,'_rlra'):
           self._rlra = allvars[1]
        if not hasattr(self,'_rlqa'):
           self._rlqa = allvars[2]
        if not hasattr(self,'_rlrd'):
           self._rlrd = allvars[3]
        if not hasattr(self,'_rlbr'):
           self._rlbr = allvars[4]
        if not hasattr(self,'_rlza'):
           self._rlza = allvars[5]
        if not hasattr(self,'_rlz2'):
           self._rlz2 = allvars[6]
        if not hasattr(self,'_rlpt'):
           self._rlpt = allvars[7]
        if not hasattr(self,'_rlpi'):
           self._rlpi = allvars[8]


    def _load_b2sqel(self):
        """ Run and unpack variables calculated by b2sqel"""
        from solpspy.b25_src.sources.b2sqel import b2sqel

        allvars = b2sqel(self)

        if not hasattr(self,'_rsa'):
           setattr(self,'_rsa',allvars[0])
        if not hasattr(self,'_rra'):
           setattr(self,'_rra',allvars[1])
        if not hasattr(self,'_rqa'):
           setattr(self,'_rqa',allvars[2])
        if not hasattr(self,'_rrd'):
           setattr(self,'_rrd',allvars[3])
        if not hasattr(self,'_rbr'):
           setattr(self,'_rbr',allvars[4])
        if not hasattr(self,'_rza'):
           setattr(self,'_rza',allvars[5])
        if not hasattr(self,'_rz2'):
           setattr(self,'_rz2',allvars[6])
        if not hasattr(self,'_rpt'):
           setattr(self,'_rpt',allvars[7])
        if not hasattr(self,'_rpi'):
           setattr(self,'_rpi',allvars[8])


