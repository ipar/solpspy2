#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    Class containing the time traces stored e.g. in b2time.nc and others.
"""

from __future__ import division, print_function

# Common modules
import os
import logging
import numpy as np
import itertools
from sets import Set
try:
    from scipy.io import netcdf
except:
    try:
        import pupynere
    except:
        try:
            import solpspy.tools.pupynere
        except:
            raise ImportError('No netcdf handler could be imported')

# Special modules
import mdstt
from solpspy.tools.tools import solps_property


# =============================================================================
class BaseTimeTraces(mdstt.BaseTimeTraces):
    """
    """
    def __init__(self, mother, wh):
        """
        Parameters
        ----------
        mother : object
            SolpsData instance
        wh : str
            Special location out of which the data are being retrieved.
            Must be one of {'target1','target2','target3','target4','imp','omp'}
        """
        self._which = wh.lower()
        self.log = mother.log
        self.mag = mother.magnetic_geometry

        #Rundir
        self.rundir = mother.rundir
        self.avail = mother.avail





    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------
    # -------------------- Singleton quantities -------------------------------



    #    =============== Methods ==========================
    def _read_signal(self,var_name, signal_name, attach_to=None):
        """ Retrieve data from the MDS+, given tree link and signal name.

        read_signal does not return any value. It automatically attach
        the signal as an attribute with a given name to the given object
        or self.

        Parameters
        ----------
        var_name : str
            Storage name of the variable for the retrieve signal.
        signal_name : str
            Name to retrieve signal.
            Only the stem of signal_name is given.
        attach_to : object, optional
            signal_name will be attached as attribute to object
            attach_to. Default to self.

        Returns
        -------
        None
        """
        if not attach_to:
            attach_to = self
        try:
            #Needed implementation of selection in DN geometries
            #where two values are sometimes given back.
            #Only the correct should be attached, according to context
            var = self._parser(signal_name, var_name=var_name)
            #Meanwhile, just squeeze it
            var = np.squeeze(var)
            setattr(attach_to, var_name, var)
        except:
            self.log.warning("error reading '{0}''".format(var_name))
            setattr(attach_to, var_name, None)


    def _parser(self, signal_name, var_name=None):
        """
        signal_name only contains the stem. The complete signal name must be
        evaluated here with the correct mixture of (signal_name + self._psym)

        Previous _read_ncfile(self, var) of Ferdinand
        """
        #in_b2time = ['an3da','an3di','an3dl','an3dr','dn3da','dn3di',
        #             'dnsepm','dp3da','dp3di','dpsepm','fc3dl','fc3dr',
        #             'fchsap','fchsapp','fchsip','fchsipp','fchxap',
        #             'fchxip','fchyap','fchyip','fe3dl','fe3dr','feesap',
        #             'feesapp','feesip','feesipp','feexap','feexip',
        #             'feeyap','feeyip','feisap','feisapp','feisip',
        #             'feisipp','feixap','feixip','feiyap','feiyip',
        #             'fetsap','fetsapp','fetsip','fetsipp','fetxap',
        #             'fetxip','fetyap','fetyip','fi3dl','fi3dr','fl3dl',
        #             'fl3dr','fn3dl','fn3dr','fnisap','fnisapp','fnisip',
        #             'fnisipp','fnixap','fnixip','fniyap','fniyip','fo3dl',
        #             'fo3dr','ft3dl','ft3dr','ke3da','ke3di','kesepm',
        #             'ki3da','ki3di','kisepm','lh3da','lh3di','ln3da',
        #             'ln3di','mn3da','mn3di','mn3dl','mn3dr','ne3da','ne3di',
        #             'ne3dl','ne3dr','nemxap','nemxip','nesepa','nesepi',
        #             'nesepm','ntstep','po3da','po3di','po3dl','po3dr',
        #             'pomxap','pomxip','posepa','posepi','posepm','pwmxap',
        #             'pwmxip','te3da','te3di','te3dl','te3dr','temxap',
        #             'temxip','tesepa','tesepi','tesepm','ti3da','ti3di',
        #             'ti3dl','ti3dr','timesa','timxap','timxip','tisepa',
        #             'tisepi','tisepm','tmhacore','tmhadiv','tmhasol','tmne',
        #             'tmte','tmti','tp3dl','tp3dr','tpmxap','tpmxip',
        #             'tpsepa','tpsepi','vs3da','vs3di','vssepm','vx3da',
        #             'vx3di','vxsepm','vy3da','vy3di','vysepm']
        in_b2time = ['fi3dtr', 'tesepa', 'fchyip', 'tisepa_std', 'tesepi',
                     'tesepm', 'te3dl', 'tesepa_std', 'tesepi_std', 'fnisapp',
                     'fe3dtr', 'kisepm', 'lh3di', 'an3dl', 'ki3di', 'an3di',
                     'timxip_av', 'temxap', 'an3da', 'mn3dl', 'posepa', 'fnisap',
                     'temxip', 'pomxip_av', 'timxap', 'posepi', 'vxsepm',
                     'timxip', 'posepm', 'an3dr', 'vysepm', 'temxap_av', 'ne3di',
                     'ke3di', 'feeyap', 'feisipp', 'nemxap', 'ke3da', 'feeyip',
                     'nemxip', 'nesepa_std', 'fnisip', 'vx3da', 'feesapp',
                     'feixip', 'ln3di','tisepm_av', 'feisip', 'po3dtr',
                     'timxap_std', 'po3dtl', 'dnsepm', 'temxip_std', 'fe3dl',
                     'posepa_av', 'fchxip', 'timxip_std', 'tpmxip', 'mn3dtr',
                     'fetxip', 'tp3dtl', 'tp3dtr', 'po3dl', 'vx3di', 'nemxap_av',
                     'fetxap', 'fniyap', 'tisepi_std', 'temxip_av', 'pomxap_av',
                     'nesepa', 'temxap_std', 'po3di', 'nesepi', 'fnisipp',
                     'nesepm', 'nesepm_av', 'tmhacore', 'feesap', 'fniyip',
                     'tisepm_std', 'feisapp', 'feesip', 'ntstep', 'posepm_std',
                     'tisepa_av', 'po3da', 'fchsapp', 'tmhasol', 'fl3dtr',
                     'dn3di', 'fchsipp', 'kesepm', 'feiyip', 'te3dr', 'fo3dtl',
                     'fo3dtr', 'pomxap_std', 'feiyap', 'tesepa_av', 'nemxap_std',
                     'fchyap', 'ne3dtr', 'dn3da', 'vs3di', 'ki3da', 'vs3da',
                     'nesepa_av', 'tpmxap', 'fe3dtl', 'fnixap', 'tmne',
                     'posepi_av', 'fchsip', 'pomxap', 'nemxip_av', 'ft3dr',
                     'fchsap', 'pomxip', 'posepm_av', 'fl3dr', 'dpsepm', 'po3dr',
                     'fo3dl', 'posepi_std', 'fl3dl', 'ti3dl', 'fnixip', 'ft3dl',
                     'ne3dtl', 'mn3dr', 'feesipp', 'vy3da', 'mn3da', 'dp3da',
                     'mn3di', 'timesa', 'feisap', 'nesepi_std', 'dp3di', 'fn3dtr',
                     'nesepm_std', 'pomxip_std', 'te3dtr', 'tesepm_std', 'fetyap',
                     'ne3dr', 'tmhadiv', 'fetyip', 'fchxap', 'te3dtl', 'fn3dtl',
                     'ti3da', 'ln3da', 'feixap', 'ti3di', 'tesepi_av', 'tisepm',
                     'tisepi', 'ti3dr', 'pwmxip', 'tisepa', 'te3da', 'fo3dr',
                     'tesepm_av', 'pwmxap', 'lh3da', 'te3di', 'tp3dl', 'fetsip',
                     'fetsapp', 'fi3dl', 'fi3dr', 'ne3dl', 'tp3dr', 'fetsap',
                     'fetsipp', 'ntim_batch', 'tisepi_av', 'ne3da', 'an3dtr',
                     'nemxip_std', 'fn3dr', 'fc3dl', 'posepa_std', 'batchsa',
                     'ti3dtr', 'ft3dtl', 'feexap', 'fe3dr', 'fl3dtl', 'tmte',
                     'feexip', 'ti3dtl', 'ft3dtr', 'tmti', 'fn3dl', 'fc3dr',
                     'an3dtl', 'nesepi_av', 'fc3dtl', 'fc3dtr', 'vy3di',
                     'timxap_av', 'vssepm', 'tpsepa', 'nastep', 'fi3dtl',
                     'mn3dtl', 'tpsepi']

        in_dsfiles  = ['dsl','dsi','dsa','dsr','dstl','dstr']
        in_dafiles  = ['dsL','dsR','dsTL','dsTR']

        in_b2tallies = ['b2bremreg','b2divua','b2divue','b2exba','b2exbe',
                        'b2fraa','b2joule','b2qie','b2radreg','b2sext_sch_reg',
                        'b2sext_she_reg','b2sext_shi_reg','b2sext_smo_reg',
                        'b2sext_sna_reg','b2sext_sne_reg','b2she','b2she0',
                        'b2shi','b2shi0','b2stbc_sch_reg','b2stbc_she_reg',
                        'b2stbc_shi_reg','b2stbc_smo_reg','b2stbc_sna_reg',
                        'b2stbc_sne_reg','b2stbm_sch_reg','b2stbm_she_reg',
                        'b2stbm_shi_reg','b2stbm_smo_reg','b2stbm_sna_reg',
                        'b2stbm_sne_reg','b2stbr_sch_reg','b2stbr_she_reg',
                        'b2stbr_shi_reg','b2stbr_smo_reg','b2stbr_sna_reg',
                        'b2stbr_sne_reg','b2str','b2visa','b2wrong1',
                        'b2wrong2','b2wrong3','fchxreg','fchyreg','fhexreg',
                        'fheyreg','fhixreg','fhiyreg','fhjxreg','fhjyreg',
                        'fhmxreg','fhmyreg','fhpxreg','fhpyreg','fhtxreg',
                        'fhtyreg','fnaxreg','fnayreg','nareg','ne2reg',
                        'nereg','nireg','poreg','qconvexreg','qconveyreg',
                        'qconvixreg','qconviyreg','rcxhireg','rcxmoreg',
                        'rcxnareg','resco_reg','reshe_reg','reshi_reg',
                        'resmo_reg','resmt_reg','respo_reg','rqahereg',
                        'rqbrmreg','rqradreg','rrahireg','rramoreg',
                        'rranareg','rsahireg','rsamoreg','rsanareg',
                        'species','tereg','times','tireg','volreg']

        #ATTENTION: Improve names to make them more general? pump[time,natm] e.g.
        in_particle_fluxes = ['Ar_puff','Ar_pump','Ar_pump_core','D2_pump',
                              'D2_pump_core','D_puff','D_puff_core','D_pump',
                              'D_pump_core','N2_pump','N2_pump_core','N_puff',
                              'N_pump','N_pump_core','time']




        if (signal_name.find('3d') != -1) or (signal_name == 'ds{}'):
            if var_name == '_da':
                signal_name = signal_name.format(self._symbols('da'))
            else:
                signal_name = signal_name.format(self._symbols('profile'))
        elif (signal_name.find('sep{}') != -1) or (signal_name.find('x{}p') != -1):
            signal_name = signal_name.format(self._symbols('singleton'))

        ncfile = None
        dsfile = None
        dafile = None
        if signal_name in in_b2time:
            ncfile = 'b2time.nc'
        elif signal_name in in_b2tallies:
            ncfile = 'b2tallies.nc'
        elif signal_name in in_particle_fluxes:
            ncfile = 'particle_fluxes.nc'
        elif signal_name in in_dsfiles:
            dsfile = signal_name
        elif signal_name in in_dafiles:
            dafile = signal_name
        else:
            self.log.error("Invalid variable (Not found): '{0}'".format(signal_name))
            return None

        if ncfile and ncfile not in self.avail:
            self.log.error("Ncfile '{0}' not in self.avail list".format(ncfile))
        elif dsfile and dsfile not in self.avail:
            self.log.error(
                    "DS-type file '{0}' not in self.avail list".format(dsfile))
        elif dafile and dafile not in self.avail:
            self.log.error(
                    "DA-type file '{0}' not in self.avail list".format(dsfile))

        if ncfile:
            try:
                with netcdf.netcdf_file(self.avail[ncfile],'r') as fnc:
                    tmp = np.array(fnc.variables[signal_name].data)

                    #Some quantities have scale, which is applied when uploaded to MDS+
                    if 'scale' in fnc.variables[signal_name].__dict__:
                        tmp *= fnc.variables[signal_name].scale

                    #Profile fluxes are uploaded as flux densities,
                    # Maybe using 'f' as the first letter is a bit weak
                    # Decision based on units could be used too
                    if (signal_name.find('3d') != -1) and signal_name[0] == 'f':
                        tmp /= self.da

                    return tmp

            except:
                self.log.exception(
                    "'{0}' could not be read from ncfile '{1}'".format(
                        signal_name, ncfile))
                return None

        elif dsfile:
            try:
                with open(self.avail[dsfile],'r') as fds:
                    ds = []
                    for line in fds:
                        ds.append(float(line))
                    return np.array(ds)
            except:
                self.log.error(
                    "'{0}' could not be read from DS-type file '{1}'".format(
                        signal_name, dsfile))
                return None
        elif dafile:
            try:
                with open(self.avail[dafile],'r') as fda:
                    da = []
                    for line in fda:
                        da.append(float(line))
                    return np.array(da)
            except:
                self.log.error(
                    "'{0}' could not be read from DA-type file '{1}'".format(
                        signal_name, dafile))
                return None



    def _symbols(self, var_type):
        """ Returns character of the solps nomenclature that identify  the
        corresponding position.

        It uses self._which to determine

        Parameters
        ----------
        var_type: str
           Type of variable. It can be one of {'profile', 'singleton'}

        Returns
        -------
        str of single character
        """
        _profiles_symbols = {'target1':'l',
                             'imp':'i',
                             'omp':'a',
                             'target4':'r',
                             'target2':'tl',
                             'target3':'tr'}
        _singletons_symbols_LSN = {'omp':'m',
                                   'imp':'', #Not defined, apparently
                                   'target1':'i',
                                   'target2':'i',
                                   'target3':'a',
                                   'target4':'a'}
        _da_symbols = {'target1':'L',
                       'target4':'R',
                       'target2':'TL',
                       'target3':'TR'}

        if var_type == 'profile':
            return _profiles_symbols[self._which]
        elif var_type == 'da':
            return _da_symbols[self._which]
        elif var_type == 'singleton':
            if self.mag.lower() == 'lsn':
                return _singletons_symbols_LSN[self._which]
            else:
                self.log.warning("Other magnetic geometries not yet implemented")
                return None
        else:
            self.log.error("Type '{0}' is not valid".format(var_type))
            return None





# =============================================================================
class TargetData(BaseTimeTraces, mdstt.TargetData):

    @solps_property
    def da(self):
        self._read_signal('_da','ds{}')

# =============================================================================
class MidplaneData(BaseTimeTraces, mdstt.MidplaneData):
    pass






















# =============================================================================
class Movies(object):
    """
    """
    def __init__(self, mother):
        """
        Parameters
        ----------
        mother : object
            SolpsData instance
        """
        self.mother = mother
        self.log = mother.log
        self.mag = mother.magnetic_geometry

        #Rundir
        self.rundir = mother.rundir
        self.avail = mother.avail





    #    =============== Properties =======================
    # -------------------- Dimensions and indices --------------------------

    @solps_property
    def ntstep(self):
        self._read_signal('_ntstep','ntstep')

    @solps_property
    def times(self):
        self._read_signal('_times','times')
    @solps_property
    def deltimes(self):
        self._read_signal('_deltimes','tim,dtim')

    # -------------------- Grid quantities ---------------------------------
    @solps_property
    def na(self):
        self._read_signal('_na','na')

    @solps_property
    def ua(self):
        self._read_signal('_ua','ua')

    @solps_property
    def te(self):
        self._read_signal('_te','te')

    @solps_property
    def ti(self):
        self._read_signal('_ti','ti')

    @solps_property
    def rqahesum(self):
        self._read_signal('_rqahesum','rqahesum')

    @solps_property
    def rqradsum(self):
        self._read_signal('_rqradsum','rqradsum')

    # -------------------- Delta quantities ---------------------------------
    @solps_property
    def delna(self):
        self._read_signal('_delna','delna')

    @solps_property
    def delua(self):
        self._read_signal('_delna','delna')

    @solps_property
    def delte(self):
        self._read_signal('_delte','delte')

    @solps_property
    def delti(self):
        self._read_signal('_delti','delti')

    @solps_property
    def delpo(self):
        self._read_signal('_delpo','delpo')

    # -------------------- Singleton quantities -------------------------------



    #    =============== Methods ==========================
    def _read_signal(self,var_name, signal_name, attach_to=None):
        """ Retrieve data from the MDS+, given tree link and signal name.

        read_signal does not return any value. It automatically attach
        the signal as an attribute with a given name to the given object
        or self.

        Parameters
        ----------
        var_name : str
            Storage name of the variable for the retrieve signal.
        signal_name : str
            Name to retrieve signal.
            Only the stem of signal_name is given.
        attach_to : object, optional
            signal_name will be attached as attribute to object
            attach_to. Default to self.

        Returns
        -------
        None
        """
        if not attach_to:
            attach_to = self
        try:
            #Needed implementation of selection in DN geometries
            var = self._parser(signal_name, var_name=var_name)
            setattr(attach_to, var_name, var)

        except:
            self.log.exception("error reading '{0}''".format(var_name))
            setattr(attach_to, var_name, None)


#    def _parser(self, signal_name, var_name=None):
#        """
#        signal_name only contains the stem. The complete signal name must be
#        evaluated here with the correct mixture of (signal_name + self._psym)
#
#        """
#        in_b2movienc = ['na', 'ua', 'te', 'ti', 'rqahesum', 'rqradsum', 'ntstep', 'times']
#        in_b2movie ={'na':'delna', 'ua':'delua', 'te':'delte', 'ti':'delti',
#                'po':'delpo', 'times':'tim,dtim'}
#        #WRONG< THESE ARE TRULLY DELTAS
#
#
#        xy  = (self.mother.nx, self.mother.ny)
#        xys = (self.mother.nx, self.mother.ny, self.mother.ns)
#        all_dims ={'na':xys, 'ua':xys, 'te':xy, 'ti':xy,
#                'po':xy, 'times':1, 'rqahesum':xy, 'rqradsum':xy}
#
#
#
#        if signal_name in in_b2movienc:
#            try:
#                with netcdf.netcdf_file(self.avail['b2movies.nc'],'r') as fnc:
#                    tmp = np.array(fnc.variables[signal_name].data.T)
#
#                    #Some quantities have scale, which is applied when uploaded to MDS+
#                    if 'scale' in fnc.variables[signal_name].__dict__:
#                        tmp *= fnc.variables[signal_name].scale
#
#                    #Place time as first index, so that (time, dims of quantity).
#                    if tmp.ndim > 1:
#                        dims = list(itertools.chain([tmp.shape[-1]], tmp.shape[:-1]))
#                        var = np.zeros(dims)
#                        for i in xrange(tmp.shape[-1]):
#                            var[i] = tmp[...,i].copy()
#                    else:
#                        var = tmp.copy()
#
#                    return var
#            except:
#                self.log.exception(
#                    "'{0}' could not be read from 'b2movies.nc'".format(signal_name))
#                return None
#
#        elif signal_name in in_b2movie:
#            proxy_name = in_b2movie[signal_name]
#            #from IPython import embed; embed()
#            factor = self._factors(signal_name)
#            data = []
#            with open(self.avail['b2fmovie'], 'r') as fmov:
#                is_body = False
#                for i, line in enumerate(fmov):
#                    #Inner loop to construct each body
#                    if (line.split()[0] == '*cf:' and
#                        line.split()[3] == proxy_name.lower() and
#                        line.split()[2] > 0):
#                        i_header = i
#                        nentry = int(line.split()[2])
#                        is_body = True
#                        tmp = []
#                        continue
#                    if is_body:
#                        pieces = line.split()
#                        for piece in pieces:
#                            tmp.append(np.float(piece))
#                        if len(tmp) == nentry:
#                            tmp = np.array(tmp)
#                            #reshape
#                            tmp = tmp.reshape(all_dims[signal_name], order = 'F')*factor
#                            data.append(tmp)
#                            is_body=False
#                            continue
#            return np.array(data)



    def _parser(self, signal_name, var_name=None):
        """
        signal_name only contains the stem. The complete signal name must be
        evaluated here with the correct mixture of (signal_name + self._psym)

        """
        in_b2movienc = ['na', 'ua', 'te', 'ti', 'rqahesum', 'rqradsum', 'ntstep', 'times']
        in_b2movie =['delna', 'delua', 'delte', 'delti', 'delpo', 'tim,dtim']


        xy  = (self.mother.nx, self.mother.ny)
        xys = (self.mother.nx, self.mother.ny, self.mother.ns)
        all_dims ={'na':xys, 'ua':xys, 'te':xy, 'ti':xy,'po':xy, 'times':1,
                'delna':xys, 'delua':xys, 'delte':xy, 'delti':xy,'delpo':xy, 'tim,dtim':1,
                'rqahesum':xy, 'rqradsum':xy}



        if signal_name in in_b2movienc:
            try:
                with netcdf.netcdf_file(self.avail['b2movies.nc'],'r') as fnc:
                    tmp = np.array(fnc.variables[signal_name].data.T)

                    #Some quantities have scale, which is applied when uploaded to MDS+
                    if 'scale' in fnc.variables[signal_name].__dict__:
                        tmp *= fnc.variables[signal_name].scale

                    #Place time as first index, so that (time, dims of quantity).
                    if tmp.ndim > 1:
                        dims = list(itertools.chain([tmp.shape[-1]], tmp.shape[:-1]))
                        var = np.zeros(dims)
                        for i in xrange(tmp.shape[-1]):
                            var[i] = tmp[...,i].copy()
                    else:
                        var = tmp.copy()

                    return var
            except:
                self.log.exception(
                    "'{0}' could not be read from 'b2movies.nc'".format(signal_name))
                return None

        elif signal_name in in_b2movie:
            factor = self._factors(signal_name)
            data = []
            with open(self.avail['b2fmovie'], 'r') as fmov:
                is_body = False
                for i, line in enumerate(fmov):
                    #Inner loop to construct each body
                    if (line.split()[0] == '*cf:' and
                        line.split()[3] == signal_name.lower() and
                        line.split()[2] > 0):
                        i_header = i
                        nentry = int(line.split()[2])
                        is_body = True
                        tmp = []
                        continue
                    if is_body:
                        pieces = line.split()
                        for piece in pieces:
                            tmp.append(np.float(piece))
                        if len(tmp) == nentry:
                            #import pdb; pdb.set_trace()
                            tmp = np.array(tmp)
                            #reshape
                            tmp = tmp.reshape(all_dims[signal_name], order = 'F')*factor
                            data.append(tmp)
                            is_body=False
                            continue
            return np.array(data)





    @staticmethod
    def _factors(var):
        """
        Temperatures are stored in J and not in eV
        """
        temps = Set(['te','ti','tab2','tmb2','tib2'])
        if var.lower() in temps:
           return 6.2415093432601784e+18
        else:
            return 1.0
















