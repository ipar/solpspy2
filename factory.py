import os
import numpy as np

from .classes.mds import MdsData
from .classes.rundir import RundirData
from .classes.alt_mag_geom import MdsDataUSN, RundirDataUSN
from .classes.alt_mag_geom import MdsDataDDN, RundirDataDDN
from .classes.alt_mag_geom import MdsDataDDNU, RundirDataDDNU

import logging
log = logging.getLogger('SolpsData')
if not log.handlers: #Prevents from adding repeated streamhandlers
    formatter = logging.Formatter(
            '%(name)s -- %(levelname)s: %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    log.addHandler(ch)
    log.propagate = False #Otherwise, root logger also prints
#
#def SolpsData(id=None,**kwargs):
#
#    if id is None:
#        id='.'
#
#    if 'mag' in kwargs:
#        mag = kwargs['mag']
#    elif 'magnetic_geometry' in kwargs:
#        mag = kwargs['magnetic_geometry']
#    elif 'maggeom' in kwargs:
#        mag = kwargs['maggeom']
#    else:
#        mag = 'lsn'
#
#    #If MDS+
#    if isinstance(id, int) or isinstance(id, (int,long)):
#        if mag.lower() == 'lsn':
#            return MdsData(id)
#        else:
#            return MdsDataDDN(id)
#
#    #If Rundir
#    else:
#        if mag.lower() == 'lsn':
#            return RundirData(id)
#        else:
#            return RundirDataDDN(id)
#


def SolpsData(ident=None,**kwargs):
#def SolpsData(**kwargs):
    """ Factory-like class for solpspy to decide between MDS+ or rundir, as well
    as the corresponding magnetic geometry.

    Input
    -----
    ident: int or str
        ID must be an int (MDS+ shot number) or a str (path)

    mag: str, optional
        Magnetic geometry for the given ID.
        Default to None, which will trigger  inference.
        Aliases: magnetic_geometry, maggeom


    Note
    ----
        For any case to be treated purely MDS-like, the magnetic configuration
        has to be set up to 'lsn'. This will cause, e.g., that for USN, targ1
        will correspond to upper outer target.

        In the future, inference will be set the default in config files, with
        the possibility of setting default to 'lsn', etc for cases.
        For example, some people might always want to work with lsn-type.
    """

    # ID must be an int (mds number) or a str (path)
    if ident is None:
        ident='.'
    #if 'ident' in kwargs and kwargs['ident'] is None:
    #    ident='.'
    #else:
    #    ident = kwargs['ident']

    # Detect the type of solps class
    if isinstance(ident, int) or isinstance(ident, (int,long)):
        solps_type = 'mds'
    else:
        solps_type = 'rundir'

    # If magnetic geometry is given, accept it:
    if 'mag' in kwargs:
        mag = kwargs['mag']
        log.info("User set magnetic geometry: {0}".format(mag.upper()))
    elif 'magnetic_geometry' in kwargs:
        mag = kwargs['magnetic_geometry']
        log.info("User set magnetic geometry: {0}".format(mag.upper()))
    elif 'maggeom' in kwargs:
        mag = kwargs['maggeom']
        log.info("User set magnetic geometry: {0}".format(mag.upper()))

    # else, try to deduce it:
    elif solps_type == 'mds':
        mag = _mds_magnetic_geometry(ident)
    elif solps_type == 'rundir':
        mag = _rundir_magnetic_geometry(ident)

    # If it cannot be determined, then default to LSN.
    if mag is None:
        log.warning("Factory could not infer the magnetic geometry")
        log.warning("Defaulting to 'LSN'")
        mag = 'lsn'



    ## Return corresponding class
    # If MDS+
    if solps_type == 'mds':
        if mag.lower() == 'lsn':
            log.info("Factory converged into: MDS+ and LSN")
            return MdsData(ident, **kwargs)
        elif mag.lower() == 'usn':
            log.info("Factory converged into: MDS+ and USN")
            return MdsDataUSN(ident, **kwargs)
        elif mag.lower() == 'ddnu':
            log.info("Factory converged into: MDS+ and DDNU")
            return MdsDataDDNU(ident, **kwargs)

    # If Rundir
    elif solps_type == 'rundir':
        if mag.lower() == 'lsn':
            log.info("Factory converged into: Rundir and LSN")
            return RundirData(ident, **kwargs)
        elif mag.lower() == 'usn':
            log.info("Factory converged into: Rundir and USN")
            return RundirDataUSN(ident, **kwargs)
        elif mag.lower() == 'ddnu':
            log.info("Factory converged into: Rundir and DDNU")
            return RundirDataDDNU(ident, **kwargs)








#------------------------------------------------------------------------------
def _mds_magnetic_geometry(ident):
    """ Do enough logic to get magnetic configuration running.
    For that, one needs:
        cz or z
        For the future, maybe just B?

        This only works, for now, for LSN and USN.
        """
    log.info("Infering magnetic geometry of MDS+ ID: '{0}'".format(ident))
    # Load MDSplus module
    try:
        import MDSplus
    except:
        log.error("MDSplus package not found")
        return None

    # Open MDS+ tree signal
    conn = MDSplus.Connection('solps-mdsplus.aug.ipp.mpg.de:8001')
    conn.openTree('solps', ident)

    # Single null, double null or linear?
    try:
        nnreg = conn.get('\SNAPSHOT::TOP.GRID:REGVOL').max()
    except:
        log.error("Error reading '{0}''".format('nnreg'))
        return None
    if nnreg == 4:
        magconfig = 'sn'
        log.info("Inferred Single Null configuration.")
    elif nnreg == 8:
        magconfig = 'ddn'
        log.info("Inferred Disconected Double Null configuration.")
    else:
        log.error("Configuration with {0} volumetric".format(nnreg)+
                " regions not implemented.")
        return None


    # Upper or lower primary divertor?
    # For the future: Adapt to CDN.
    try:
        sep = int(conn.get('\SNAPSHOT::TOP.DIMENSIONS:SEP'))
        sep += 1
    except:
        log.error("Error reading '{0}''".format('sep'))
        return None

    try:
        cz = conn.get('\SNAPSHOT::TOP.GRID:CZ').value.T
        pos = cz[:,sep]
    except:
        log.error("Error reading '{0}''".format('CZ'))
        try:
            z = conn.get('\SNAPSHOT::TOP.GRID:Z').value.T
            pos = z[:,sep,0]
        except:
            log.error("Error reading '{0}''".format('Z'))
            return None


    #Return type:
    #WARNING: very wrong that ddn returns DDNU always.
    if (pos.argmin() == 0) or (pos.argmin() == len(pos)-1):
        if magconfig.lower() == 'sn':
            log.info("Inferred magnetic geometry: LSN")
            return 'lsn'
        elif magconfig.lower() == 'ddn':
            log.info("Inferred magnetic geometry: DDN")
            return 'ddnu' #FOR NOW, BUT VERY VERY WRONG
    else:
        if magconfig.lower() == 'sn':
            log.info("Inferred magnetic geometry: USN")
            return 'usn'
        elif magconfig.lower() == 'ddn':
            log.info("Inferred magnetic geometry: DDNU")
            return 'ddnu'






#------------------------------------------------------------------------------

def _rundir_magnetic_geometry(ident):
    """ Do enough logic to get magnetic configuration running.
    -  Get self.avail just for cz or z

    """
    log.info("Infering magnetic geometry of rundir path: '{0}'".format(ident))


    # FIND FILES. To be substituted by read_config
    #No longer valid as only b2fgmtry contains regions
    #if _find_file(id, 'b2fplasmf'):
    #    b2file = _find_file(id,'b2fplasmf')
    #elif _find_file(id, 'b2fgmtry'):
    #    b2file = _find_file(id,'b2fgmtry')
    #else:
    #     log.error("Neither b2fplasmf nor b2fgmtry could be found!")
    #     return None

    if _find_file(ident, 'b2fgmtry'):
        b2file = _find_file(ident,'b2fgmtry')
    else:
         log.error("b2fgmtry could be found!")
         return None
    try:
        nx, ny = _get_basic_dimensions(ident)
    except:
        log.error("Error reading '{0}''".format('(nx,ny)'))
        return None


    # Single null, double null or linear?
    try:
        nnreg = _parser(b2file,'nnreg',3)
    except:
        log.error("Error reading '{0}''".format('nnreg'))
        return None
    if nnreg[0] == 4:
        magconfig = 'sn'
        log.info("Inferred Single Null configuration.")
    elif nnreg[0] == 8:
        magconfig = 'ddn'
        log.info("Inferred Disconected Double Null configuration.")
    else:
        log.error("Configuration with {0} volumetric".format(nnreg)+
                " regions not implemented.")
        return None



    # Upper or lower primary divertor?
    # For the future: Adapt to CDN.
    try:
        xy4 = (nx, ny, 4)
        z = _parser(b2file,'cry', xy4)
        sep = z.shape[1]//2+1
        pos = z[:,sep,0]
    except:
        log.error("Error reading '{0}''".format('Z'))
        return None


    ## The problem with this logic is if neither target is the lowest point.
    if (pos.argmin() == 0) or (pos.argmin() == len(pos)-1):
        if magconfig.lower() == 'sn':
            log.info("Inferred magnetic geometry: LSN")
            return 'lsn'
        elif magconfig.lower() == 'ddn':
            log.info("Inferred magnetic geometry: DDN")
            return 'ddnu' #FOR NOW, BUT VERY VERY WRONG
    else:
        if magconfig.lower() == 'sn':
            log.info("Inferred magnetic geometry: USN")
            return 'usn'
        elif magconfig.lower() == 'ddn':
            log.info("Inferred magnetic geometry: DDNU")
            return 'ddnu'






def _find_file(rundir_path, b2file):
    """
    """
    if   (os.path.isfile(os.path.join(rundir_path,b2file)) and
        os.stat(os.path.join(rundir_path,b2file)).st_size != 0):
            return os.path.abspath(os.path.join(rundir_path,b2file))

    elif ((file != 'b2fstate' and file != 'b2fstati') and
        os.path.isfile(os.path.join(rundir_path,'../baserun/',b2file)) and
        os.stat(os.path.join(rundir_path,'../baserun/',b2file)).st_size != 0):
            return os.path.abspath(os.path.join(rundir_path,'../baserun/',b2file))

    elif ((file =='baserun_b2fstate' or file =='baserun_b2fstati') and
        os.path.isfile(os.path.join(rundir_path,'../baserun/',b2file[8:])) and
        os.stat(os.path.join(rundir_path,'../baserun/',b2file[8:])).st_size != 0):
            return os.path.abspath(os.path.join(rundir_path,'../baserun/',b2file[8:]))

    else:
        log.info("{0} not found in either rundir or baserun!".format(b2file))
        return None






def _parser(b2file, var, dims):
    data = []
    with open(b2file, 'r') as f:
        is_body = False
        for i, line in enumerate(f):
            if (line.split()[0] == '*cf:' and
                line.split()[3] == var.lower() and
                line.split()[2] > 0):
                i_header = i
                nentry = int(line.split()[2])
                is_body = True
                continue
            if is_body:
                pieces = line.split()
                for piece in pieces:
                    data.append(np.float(piece))
                if len(data) == nentry: break
    return np.array(data).reshape(dims, order = 'F')





def _get_basic_dimensions(rundir_path):
        #Get nx, ny and ns from b2fstat*
        if _find_file(rundir_path,'b2fstate') or _find_file(rundir_path,'b2fstati'):
            if _find_file(rundir_path,'b2fstate'):
                gdir = _find_file(rundir_path,'b2fstate')
            elif _find_file(rundir_path, 'b2fstati'):
                gdir = _find_file(rundir_path,'b2fstati')

            with open(gdir) as fg:
                for i in range(2):
                    fg.readline()
                tmp = fg.readline().split()
                nx = int(tmp[0]) + 2
                ny = int(tmp[1]) + 2
            return nx,ny

        #Or get nx, ny from b2fgmtry and ns form b2fpardf
        elif _find_file(rundir_path, 'b2fgmtry') or _find_file(rundir_path,'b2fpardf'):
            try:
                gdir = _find_file(rundir_path, 'b2fgmtry')
                with open(gdir) as fg:
                    for i in range(2):
                        fg.readline()
                    tmp = fg.readline().split()
                    nx = int(tmp[0]) + 2
                    ny = int(tmp[1]) + 2
            except:
                self.log.warning("nx, ny could not be retrieved")
                return None

            return nx,ny

        #Or get nx, ny from baserun b2state or b2fstati
        elif _find_file(rundir_path,'baserun_b2fstate') or _find_file(rundir_path,'baserun_b2fstati'):
            if _find_file(rundir_path,'baserun_b2fstate'):
                gdir = _find_file(rundir_path,'baserun_b2fstate')
            elif _find_file(rundir_path, 'baserun_b2fstati'):
                gdir = _find_file(rundir_path,'baserun_b2fstati')

            with open(gdir) as fg:
                for i in range(2):
                    fg.readline()
                tmp = fg.readline().split()
                nx = int(tmp[0]) + 2
                ny = int(tmp[1]) + 2
            return nx,ny


# IN order to know if it is ddn or sn
#def _get_nreg(rundir_path):
