from __future__ import division, print_function
import os
import re
import sys
import gzip
import itertools
import subprocess
import numpy as np
from functools import wraps
import collections
from collections import OrderedDict, deque
import logging
import copy

slog = logging.getLogger('solpspy')

try:
    import yaml
except:
    from solpspy.external_modules import yaml
    slog.info("Using local version of pyYAML package.")

class SimpleNamespace(object):
    """
    Rough equivalent to types.SimpleNamespace class for Python 3.3+.
    One could also use the empty class:
                 def SimpleNamespace:
                     pass
    but this one provides some perks, and looks cooler.
    __repr__ provides what is see when it the object is called without purpose
    and in the "official" case, it shows all the keys and their types
        def __repr__(self):
            keys = sorted(self.__dict__)
            items = ("{}={!r}".format(k, self.__dict__[k]) for k in keys)
            return "{}({})".format(type(self).__name__, ", ".join(items))
    However, I found it a bit annoying, so mine just gives back the keys.
    When moving the code to Python 3, this will not be necessary anymore.
    """
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        keys = sorted(self.__dict__)
        items = ("{}".format(k) for k in keys)
        return "SimpleNamespace({})".format(" ".join(items))

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

class snv(SimpleNamespace):
    """
    Equal to SimpleNamespace but in verbose mode
    """
    def __repr__(self):
        keys = sorted(self.__dict__)
        items = ("{} = {!r}".format(k, self.__dict__[k]) for k in keys)
        return "snv({})".format("   ".join(items))

def try_block(func):
    """
    Tries self._ATTR and, if it is not there, then computes FUNC,
    which should create self._ATTR and returns it.
    Else, it returns self._ATTR.

    Additionally, after it has successfully implemented ._ATTR, if it is
    a grid attribute, it will be flipped in the USN case.
    LSN should remain unchanged.
    """
    @wraps(func)
    def wrapped(self, *args, **kwargs):
        try:
            return getattr(self, '_'+func.__name__)
        except:
            try:
                func(self) #Func should create self._variable
                return getattr(self, '_'+func.__name__)
            except Exception:
                #self.log.exception("{} could not be set".format(func.__name__))
                setattr(self, '_'+func.__name__, None)
                return getattr(self, '_'+func.__name__)
    return wrapped


def solps_property(func):
    """ Sums up both try_block and property in a single decorator"""
    return property(try_block(func))

def priority(names, *args):
    """
    Inputs:
    ------
        Names:  List of possible names of the property. List order implies
                priority order. I.e. if two or more names are present, the
                value of the first name will be accepted.

        Args:   List of all the possible dicts in which the property can
                be found.
                The last provided argument is the default.

    Outputs:
    -------
        Return: Value of the property, named with priority according to names order,
                of the highest priority source.
    """
    #If names is just a string, list create a list of characters
    if isinstance(names, basestring):
        names = [names]
    else:
        names = list(names)
    for a in args[:-1]:
        for b in names:
            if b in a:
                return a[b]
    return args[-1]

def parse_dataframe(df,criterion):
    """Parses a pandas DataFrame for entries following certain criterion.

    For computational reasons, it divides the criterions in those
    affecting numbers and those affecting python objects, but the final
    result gets put together again.

    Parameters
    ----------
    df: pandas.DataFrame type
        The data frame to be parsed

    criterion: dict
        Dictionary which keys correspond to labels and which values correspond
        to a single criterion for each value.
        Dictionaries with multiple values per key are not allowed.


    Results
    -------
    pandas DataFrame object, which entries correspond to df entries
    that follow criterion.
    """
    import pandas as pd
    cnames = list(criterion)
    cseries = pd.Series(criterion)
    tmp = df[cnames]
    tmp_num = tmp.select_dtypes(exclude=['object'])
    tmp_obj = tmp.select_dtypes(include=['object'])
    cnames_num = list(tmp_num)
    cseries_num = pd.to_numeric(cseries[cnames_num])
    cnames_obj = list(tmp_obj)
    cseries_obj = cseries[cnames_obj]
    try:
        mask_num = np.isclose(tmp_num,cseries_num)
        tmp_num = tmp_num.where(mask_num)
    except:
        pass
    try:
        mask_obj = (tmp_obj == cseries_obj)
        tmp_obj = tmp_obj.where(mask_obj)
    except:
        pass
    tmp2 = pd.concat([tmp_num,tmp_obj], axis=1)
    tmp2 = tmp2.dropna()
    return df.iloc[tmp2.index]

def read_config():
    """ Reads default and local configuration files and merge them together
    with appropiate priority order.
    Improved so now it modifies a deep dict just fine.
    """

    config = {}
    local  = {}

    # Read 'default.yaml' file:
    yaml_file = os.path.join(module_path(),'config/default.yaml')
    try:
        config = yaml_loader(yaml_file)
    except IOError:
        slog.exception("Error opening 'default.yaml' file '%s'",yaml_file)
    except:
        slog.exception("Yaml file contains unparseable statement '%s'",yaml_file)

    # Read 'local.yaml' file:
    yaml_file = os.path.join(module_path(),'config/local.yaml')
    try:
        local = yaml_loader(yaml_file)
    except IOError as e:
        slog.info("No local Yaml file '%s' is provided",yaml_file)
    except Exception as e:
        slog.exception("Yaml file contains unparseable statement '%s'",yaml_file)

    return update(config, local)

def update(d, u):
    """ Returns updated version of D overwritten by U, without modification
    of D and U.
    """
    tmp = copy.deepcopy(d)
    for k, v in u.iteritems():
        if isinstance(v, collections.Mapping):
            tmp[k] = update(tmp.get(k, {}), v)
        else:
            tmp[k] = v
    return tmp

def yaml_loader(name):
    """
    Loads yaml file with 'name' safely and takes into account correctly
    the scientific notation (no point before e/E and/or sign afterwards needed)
    """
    
    loader = yaml.SafeLoader
    loader.add_implicit_resolver(
        u'tag:yaml.org,2002:float',
        re.compile(u'''^(?:
         [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
        |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
        |\\.[0-9_]+(?:[eE][-+][0-9]+)?
        |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
        |[-+]?\\.(?:inf|Inf|INF)
        |\\.(?:nan|NaN|NAN))$''', re.X),
        list(u'-+0123456789.'))

    with open(name, 'r') as fr:
        data = yaml.load(fr, Loader=loader)
    if data is None:
        data = {}
    return data

def exp_cross_references(exp):
    """
    Substitutes arguments values beginning with $ with the corresponding
    reference.
    E.g.:
        id: $nshot
        nshot: 34314

        will result in
        id: 34314
        nshot: 34314

    Edit 2021: One can use & to add a tag before a value and then reference it
    by *. Eg: nshot: &nshot 34311 and then id: *nshot.
    """
    try:
        news = {}
        for k,v in exp['defaults'].iteritems():
            if isinstance(v, str) and v.startswith('$'):
                ref = v[1:]
                news[k] = exp[ref]
        exp['defaults'].update(news)

        news = {}
        for case in exp['cases']:
            for k,v in case.iteritems():
                if isinstance(v, str) and v.startswith('$'):
                    ref = v[1:]
                    news[k] = case[ref]
            case.update(news)

    except IOError as e:
        slog.exception("Error opening db file")
    except KeyError as e:
        slog.exception("Reference to non existing key field '{}'.".format(ref))
    except Exception as e:
        slog.exception("Yaml file contains unparseable statement.")

    return exp

def yaml_xrefs(yamldict):
    """
    Substitutes arguments values beginning with $ with the corresponding
    reference.
    E.g.:
        id: $nshot
        nshot: 34314

        will result in
        id: 34314
        nshot: 34314

    Notes
    -----
    They only work for same level references.
    """
    try:
        news = {}
        for k,v in yamldict.iteritems():
            if isinstance(v, str) and v.startswith('$'):
                ref = v[1:]
                news[k] = yamldict[ref]
        yamldict.update(news)
    except IOError as e:
        slog.exception("Error opening db file.")
    except KeyError as e:
        slog.exception("Reference to non existing key field '{}'.".format(ref))
    except Exception as e:
        slog.exception("Yaml file contains unparseable statement.")

    return yamldict

def yaml_runrefs(yamldict):
    """
    Allow syntax like:
        nesepm: *ne[omp,sep]
        so that after shot has been loaded, and
        >> shot.ne[shot.omp,shot.sep]
        >> 2.0e19
        then
        nesemp:2.0e19

        This will only serve for ordering if ordering is done
        after loading, which in principle shouldn't be a problem,
        as reordering of loaded cases wouldn't mean a reload,
        just a change of indexes.

        It would also require to match * with True when filtering
        through the database and, loading, reapplying the filtering.
    """

#def db_iterator(db, indices):

def module_path():
    dirpath = os.path.abspath(sys.modules['solpspy'].__file__)
    return dirpath.rstrip(os.path.basename(dirpath))

def nested_depth(d):
    """ Returns the deepest point of the given dict"""
    if not isinstance(d, dict):
        return 0
    if not d:
        return 1
    return 1 + max(nested_depth(v) for v in d.values())

def cellface(c1, c2):
    """
    """
    interface = np.zeros((2,2))
    k=0
    for i in xrange(c2.shape[0]):
        try:
            ind = np.where((np.isclose(c1, c2[i]).all(axis=1)))[0][0]
            interface[k] = c1[ind]
            k+=1
        except:
            pass
    return interface

def format_e(n, lower_case=True):
    if lower_case:
        a = '%e' % n
        return a.split('e')[0].rstrip('0').rstrip('.') + 'e' + a.split('e')[1]
    else:
        a = '%E' % n
        return a.split('E')[0].rstrip('0').rstrip('.') + 'E' + a.split('E')[1]

def styles(name=None):
    """
    """
    #Old version, when tools was on root folder
    #styles_path = os.path.join(os.path.dirname(__file__), 'mplstyles')

    dirpath = os.path.abspath(sys.modules['solpspy'].__file__)
    dirpath = dirpath.rstrip(os.path.basename(dirpath))

    styles_path = os.path.join(dirpath, 'mplstyles')
    if not name:
        return [f[:-9] for f in os.listdir(styles_path)
                if os.path.isfile(os.path.join(styles_path, f))]
    else:
        try:
            if name[-9:] == '.mplstyle':
                return os.path.join(styles_path,name)
            else:
                return os.path.join(styles_path,name+'.mplstyle')
        except:
            return None




elements = {1:'h',2:'he',3:'li',4:'be',5:'b',6:'c',7:'n',8:'o',9:'f',
            10:'ne',11:'na',12:'mg',13:'al',14:'si',15:'p',16:'s',
            17:'cl',18:'ar',19:'k',20:'ca',21:'sc',22:'ti',23:'v',
            24:'cr',25:'mn',26:'fe',27:'co',28:'ni',29:'cu',30:'zn',
            31:'ga',32:'ge',33:'as',34:'se',35:'br',36:'kr',37:'rb',
            38:'sr',39:'y',40:'zr',41:'nb',42:'mo',43:'tc',44:'ru',
            45:'rh',46:'pd',47:'ag',48:'cd',49:'in',50:'sn',51:'sb',
            52:'te',53:'i',54:'xe',55:'cs',56:'ba',57:'la',58:'ce',
            59:'pr',60:'nd',61:'pm',62:'sm',63:'eu',64:'gd',65:'tb',
            66:'dy',67:'ho',68:'er',69:'tm',70:'yb',71:'lu',72:'hf',
            73:'ta',74:'w',75:'re',76:'os',77:'ir',78:'pt',79:'au',
            80:'hg'}

#ATTENTION: Not clear what is the purpose
def sort_lists(lists,reverse=False):
    """Takes a list of lists as argument, and sorts all lists with respect
    to the values in the first list."""

    size = len(lists[0])
    for l in lists:
        if size != len(l):
            #print("Error in sort_lists(): inconsistent size of lists.")
            slog.error("In sort_lists(): inconsistent size of lists.")
            return

    dict = {}
    for i in range(size):
        key = lists[0][i]
        if key not in dict:
            dict[key] = [[]]
        else:
            dict[key].append([])
        for l in lists:
            dict[key][-1].append(l[i])

    retval = ()
    for l in range(len(lists)):
        retval += ([],)
        for key in sorted(dict,reverse=reverse):
            for sublist in dict[key]:
                retval[-1].append(sublist[l])

    return retval

def convert_to_latex_unit(input):

    # Create latex output for units in plots.
    #
    # Example:
    #  input: '10^19 m^3 eV s^-1 m^-2 * photons*MW eV^-1'
    #  returns:
    #  -> '[$10^{19}\mathrm{m}\mathrm{s}^{-1}\mathrm{photons}\mathrm{MW}$]'

    if input == '' or input == '[]': return ''

    inputs = input.replace('*',' ').replace('[','').replace(']','').split()
    outputs = []
    for i in inputs:
        outputs.append(i.split('^'))
        if '^' not in i:
            outputs[-1].append(1)

    output = OrderedDict()
    for o in outputs:
        output[o[0]] = output.get(o[0],0) + int(o[1])

    unit = r'[$'
    for o in output:
        if output[o] == 0: continue
        try:
            int(o)
            unit += o
        except:
            unit += r'\mathrm{' + o + r'}'
        if output[o] != 1:
            unit += r'^{' + str(output[o]) + r'}'
        if o != output.keys()[-1]:
            unit += r'\,'
    unit += r'$]'
    unit  = unit.replace('\mathrm{%}','\%')

    return unit

def open_file(file,dir='.'):
    """Opens .png, .pdf, .(e)ps or text files in the run directory."""
    file = os.path.join(dir,file)
    if not os.path.isfile(file):
        #print('File does not exist:',file)
        slog.error("File does not exist: {}".format(file))
        return

    if file.endswith('.pdf'):
       #prog = 'acroread'
        prog = 'gv'
    elif file.endswith('.ps') or file.endswith('.eps'):
        prog = 'gv'
    elif file.endswith('.png'):
        prog = 'display'
    else:
        prog = 'emacs'

    FNULL = open(os.devnull, 'w')
    subprocess.Popen([prog,file],stdout=FNULL,stderr=subprocess.STDOUT)
    FNULL.close()

class LinAlg2d(object):
    """A simple class to handle lines and intersections of lines in 2D.
    I am almost sure, there must be something like this already, but I
    couldn't find anything. Maybe this can be replaced (and just stay
    as an alias or so)."""

    def __init__(self):
        pass

    class Line(object):
        """A line created from two points."""
        def __init__(self,start,end):
            self.start = np.array(start)
            self.end   = np.array(end)
            self.dir   = self.end - self.start

    class LineDir(Line):
        """A line created from one point and a direction."""
        def __init__(self,start,dir):
            self.start = np.array(start)
            self.dir   = np.array(dir)
            self.end   = self.start + self.dir

    @staticmethod
    def find_intersection(line1,line2):
        """Returns the intersection point of two lines line1 and line2"""
        Ax = line1.start[0]
        Ay = line1.start[1]
        ax = line1.dir[0]
        ay = line1.dir[1]
        Bx = line2.start[0]
        By = line2.start[1]
        bx = line2.dir[0]
        by = line2.dir[1]
        if ax*by - ay*bx == 0: # Parallel lines, no intersection!
            return None
        t  = ( (Bx-Ax)*ay - (By-Ay)*ax ) / ( ax*by - ay*bx )
        return np.array([Bx+t*bx,By+t*by])

    @staticmethod
    def calculate_distance(point1,point2):
        p1 = np.array(point1)
        p2 = np.array(point2)
        return np.linalg.norm(p2-p1)

    @staticmethod
    def points_on_same_side_of_line(point1,point2,line):
        """Returns True if point1 and point2 are on the same side of line"""
        # Make a line out of point1 and point2:
        pline = LinAlg2d.Line(point1,point2)
        # Find the intersection:
        # (or how much pline needs to be extended to the intersection)
        Ax = line.start[0]
        Ay = line.start[1]
        ax = line.dir[0]
        ay = line.dir[1]
        Bx = pline.start[0]
        By = pline.start[1]
        bx = pline.dir[0]
        by = pline.dir[1]
        t  = ( (Bx-Ax)*ay - (By-Ay)*ax ) / ( ax*by - ay*bx )

        if 0.000001 < t < 0.999999: # (If the point is on the line, numerical
            return False          # imprecision might result in the wrong
        else:                     # outcome when using 0 < t < 1 ...)
            return True

    @staticmethod
    def point_inside_cell(cell,point):
        """Returns True if the point is inside of the cell"""
        # cell must be a run.grid[nx,ny] array

        # Cell Center:
        x = np.average(cell[:,0])
        y = np.average(cell[:,1])
        cc = np.array([x,y])

        # Cell Faces:
        cf = []
        for i in range(cell.shape[0]):
            if i == 0:
                face = LinAlg2d.Line(cell[-1],cell[i])
            else:
                face = LinAlg2d.Line(cell[i-1],cell[i])
            cf.append(face)
        for face in cf:
            if not LinAlg2d.points_on_same_side_of_line(cc,point,face):
                return False
        return True

    @staticmethod
    def distance_cylindrical_coordinates(start,end):
        """Calculate the distance between two
        points given in cylindrical coordinates."""
        # Convert to cartesian coordinates:
        x = (end  [0]*np.cos(end  [2]*np.pi/180.0)-
             start[0]*np.cos(start[2]*np.pi/180.0))
        y = (end  [0]*np.sin(end  [2]*np.pi/180.0)-
             start[0]*np.sin(start[2]*np.pi/180.0))
        z =  end  [1]   -    start[1]
        return np.linalg.norm(np.array([x,y,z]))

#@profile
def grep(regex=None, file=None, flags=None):
    """
        Search for regex in file and return a list tuples with first the first
        index indicating the line incident and the second index providing a list
        of lines that the context flags required

        TOO SLOW for parsing of run.log.gz
    """

    if file.endswith('.gz'):
        rlog = gzip.open(file,'r')
    else:
        rlog = open(file,'r')

    if flags is None:
        flags = {}

    mylen1=1
    mylen2=0
    flaga=False
    try:
        mylen1 = flags['c'] + 1
        mylen2 = flags['c']
    except:
        if 'b' in flags:
            mylen1 = flags['b'] + 1
        if 'a' in flags:
            mylen2 = flags['a']

    blocks = []
    stackb = deque(maxlen = mylen1)
    stacka = deque(maxlen = mylen2)
    stackc = deque(maxlen = mylen1+mylen2)
    #stackc = []

    regex = re.compile(regex)
    #import pdb; pdb.set_trace()
    while True:
        last_pos = rlog.tell()
        line = rlog.readline()

        if line == '':
            break
        stackb.append(line)

        if re.findall(regex,line):
            for k in xrange(mylen2):
                ln = rlog.readline()
                if ln == '':
                    break
                stacka.append(ln)

            stackc.extend(stackb)
            stackc.extend(stacka)
            blocks.append(stackc.__copy__())

            stackc.clear()
            stacka.clear()
            rlog.seek(last_pos)
            if rlog.readline() == '':
                break

    return blocks


        
# NOT WORKING AT ALL
def grep_3(pattern, file_path):
    import io
    import mmap
    pattern = re.compile(pattern)
    #with io.open(file_path, "r", encoding="utf-8") as f:
    with gzip.open(file_path, "r") as f:
        return re.findall(pattern, mmap.mmap(f.fileno(),0, access=mmap.ACCESS_READ))


# ----------------------- DEVELOPMENT ----------------------------------

#ATTENTION: It only works in IPP if kk_abock is available.
# DO NOT TRUST. IT IS NOT RELIABLE FOR ALL PEOPLE
def get_rhopol(run, discharge, t):
    import kk_abock as kk_a
    tmp = kk_a.kk(discharge, diagnostic='EQH')
    rhopol = np.zeros_like(run.cr)
    for i in xrange(run.nx):
        rhopol[i] = tmp.Rz_to_rhopol(t, run.cr[i], run.cz[i])
    return rhopol


##ATTENTION: It only works in IPP if kk_abock is available.
## DO NOT TRUST. IT IS NOT RELIABLE FOR ALL PEOPLE
#def get_Romp_from_rhopol(run, discharge, t, rhopol):
#    import kk_abock as kk_a
#    tmp = kk_a.kk(discharge)
#    Romp = np.zeros_like(run.cr)
#    for i in xrange(run.nx):
#        Romp[i] = tmp.rhopol_to_Rz(t, rhopol, 0.0) #For my grid is 0.08, not 0.0
#    return Romp
#


# ----------------------- DEVELOPMENT ----------------------------------
if __name__ == "__main__":
    rlog = '../tests/misc/grep_file.dat'
    rlog = '../tests/misc/mock_run.log.gz'
    results = grep('IPANU', rlog, flags={'c':2})



