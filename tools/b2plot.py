#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function   # Import python 3.3 print function

import os
import subprocess
import numpy as np
import matplotlib.pyplot as plt

def call_b2plot(cmd, delete_cmd_file = False):
    """
    Calls b2plot with the specified b2plot command or uses
    the specified cmd-file if the argument ends with '.cmd'.
    """

    if cmd.endswith('.cmd'):
        command = ["echo" , "'" + cmd + "' cmd"]
    else:
        command = ["echo" , cmd]

    FNULL = open(os.devnull, 'w')
    p1 = subprocess.Popen(command, stdout=subprocess.PIPE)
    p2 = subprocess.check_output(('b2plot'), stdin=p1.stdout, stderr=FNULL)
    p1.wait()
    FNULL.close()

    if cmd.endswith('.cmd') and delete_cmd_file:
        try: os.remove(cmd)
        except: pass

    return p2

def get_b2plot_variable(solpspy_run,b2plotcmd,singlevalue=False):
    """Returns the variable calculated via b2plot with 'b2plotcmd'
    as numpy array."""

    cmd_file = os.path.join(solpspy_run.rundir,'get_b2plot_variable.tmp.cmd')

    # Create the blplot cmd file:
    with open(cmd_file,'w') as f:
	f.write('comp write\n\n')
	f.write(b2plotcmd)
	f.write('\n\n')
	if not singlevalue:
	    for ny in range(-1,solpspy_run.ny-1):
		f.write(str(ny) + ' f.x\n')

    # Call b2plot:
    # (must be in the rundir!)
    cwd_tmp = os.getcwd()
    os.chdir(solpspy_run.rundir)
    call_b2plot(cmd_file,delete_cmd_file=True)
    os.chdir(cwd_tmp)

    # Check if a file was written:
    out_file = os.path.join(solpspy_run.rundir,'b2pl.exe.dir/b2plot.write')
    if not os.path.isfile(out_file):
	print('No b2plot output file found. Invalid b2plot command?')
	return

    # Readout data:
    data = []
    blocks = 0
    with open(out_file,'r') as f:
	for l in f:
	    if l.startswith('#'):
		blocks += 1
		continue
	    data.append(float(l.split()[-1]))

    if len(data) == 0:
	print('No data written. Invalid b2plot command?')
	return

    # Guess variable shape:
    if blocks == 1 and len(data) == 1:
	return data[0]
    elif blocks == solpspy_run.ny:
	shape = (solpspy_run.nx,solpspy_run.ny,1)
    elif blocks == solpspy_run.ny*solpspy_run.ns:
	shape = (solpspy_run.nx,solpspy_run.ny,solpspy_run.ns)
    elif blocks%solpspy_run.ny == 0:
	shape = (solpspy_run.nx,solpspy_run.ny,int(blocks/solpspy_run.ny))
    else:
	print('Could not deduce variable shape. (Blocks: ' + str(blocks)
	      + ', elements: ' + str(len(data)) + ')')
	return

    # Sort data:
    b2var = np.zeros(shape)
    i = 0
    for ny in range(shape[1]):
	for ns in range(shape[2]):
	    for nx in range(shape[0]):
		b2var[nx,ny,ns] = data[i]
		i+=1
    if b2var.shape[2] == 1:
	b2var = b2var[:,:,0]

    return b2var

def compare_parameters(solpspy_run,var1,var2,
                       verbose=2,rlclonly=False,plot=False,where=False):
    """Compares the parameters 'var1' and 'var2'. These arguments can
    also be b2plot commands, in which case the b2plot value will be
    calculated and used for the comparison."""

    if plot and rlclonly:
	print("Option 'plot' deactivates 'rlclonly', i.e., also the "
	      + "guard cells are considered for the comparison.")
	rlclonly = False

    if type(var1) == str:
	var1 = get_b2plot_variable(solpspy_run,var1)
    if type(var2) == str:
	var2 = get_b2plot_variable(solpspy_run,var2)

    if var1.shape != var2.shape:
	print('Incompatible variable shapes:',var1.shape,'vs.',var2.shape)
	return

    if rlclonly:
	var1 = var1[1:solpspy_run.nx-1,1:solpspy_run.ny-1,...]
	var2 = var2[1:solpspy_run.nx-1,1:solpspy_run.ny-1,...]

    allclose = np.allclose(var1,var2)

    if verbose == 0:
        return allclose

    if verbose == 1:
        if allclose:
            print('Equal')
        else:
            print('Inequal')
        return

    difftot = var2 - var1

    prange  = min( var1.max()-var1.min() , var2.max()-var2.min() )
    diffrel = difftot / prange
    # This 'relative' deviation is not really the relative deviation.
    # But if e.g. some flux has a zero crossing and is ~1W in var1
    # instead of ~2W in var2 it does not really matter if the fluxes
    # are in the order or ~MW otherwise. So 'prange' is just supposed
    # to give some idea of the magnitude of the parameter range and
    # 'diffrel' gives the difference of the values compared to that
    # general magnitude.

    min1 = '{:.4e}'.format(var1.min())
    min2 = '{:.4e}'.format(var2.min())
    max1 = '{:.4e}'.format(var1.max())
    max2 = '{:.4e}'.format(var2.max())
    avr1 = '{:.4e}'.format(np.average(var1))
    avr2 = '{:.4e}'.format(np.average(var2))

    dtmax = '{:.4e}'.format(np.nanmax(np.abs(difftot)))
    dtavr = '{:.4e}'.format(np.average(np.abs(difftot)))

    drmax = str(round(100*np.nanmax(np.abs(diffrel)),2))
    dravr = str(round(100*np.average(np.abs(diffrel)),2))

    print('')
    print('		{:<12}  {:<12}  {:<12}'.format('Min:','Max:',
						     'Average:'))
    print('Variable 1:  {:>12}  {:>12}  {:>12}'.format(min1,max1,avr1))
    print('Variable 2:  {:>12}  {:>12}  {:>12}'.format(min2,max2,avr2))
    print('')
    print("Maximum difference:",dtmax,'('+drmax,'%)')
    print("Average difference:",dtavr,'('+dravr,'%)')
    print("")
    if allclose:
        print(' ==> Equal')
    else:
        print(' ==> Inequal')

    if where:
        if type(where) == int:
            N = where
        else:
            N = 10

        values = {}
        while len(diffrel.shape) < 4:
            diffrel = np.expand_dims(diffrel,3)
        for i0 in range(diffrel.shape[0]):
            for i1 in range(diffrel.shape[1]):
                for i2 in range(diffrel.shape[2]):
                    for i3 in range(diffrel.shape[3]):
                        cell = [i0,i1,i2,i3]
                        if rlclonly: 
                            cell = [i0+1,i1+1,i2,i3]
                        value = np.abs(diffrel[i0,i1,i2,i3])
                        values.setdefault(value,[]).append(cell)
                        # values[np.abs(diffrel[i0,i1,i2,i3])] = cell

        print('')
        print('{:>12}'.format('Diff'),
              '{:>4}'.format('nx'),
              '{:>4}'.format('ny'),
              '{:>4}'.format('ns'))
        counter = 0
        for value in sorted(values.keys(),reverse=True):
            if value > 1  :
                vstr = str(int(0.5+100*value))
            else:
                vstr = str(round(100*value,2))
            for i in range(len(values[value])): 
                nx,ny,ns = values[value][i][0],values[value][i][1],values[value][i][2]
                gc = ''
                if (nx == 0 or nx == solpspy_run.nx-1 or 
                    ny == 0 or ny == solpspy_run.ny-1):
                    gc = '  (guard cell)'
                print('{:>12}'.format(vstr+' %'),
                      '{:>4}'.format(nx),
                      '{:>4}'.format(ny),
                      '{:>4}'.format(ns),gc)
                counter += 1
                if counter > N: break

    if plot:
	if var1.shape == solpspy_run.grid.shape[0:2]:
	    solpspy_run.plot.grid_data(var=100*diffrel,
                                       color_bar=True,
                                       cmap=plt.cm.seismic,
                                       clim=[-10,10],
                                       clabel='Deviation [%]')
	    plt.show()
	elif var1.shape == (solpspy_run.nx,solpspy_run.ny,solpspy_run.ns):
	    for ns in range(solpspy_run.ns):
		solpspy_run.plot.grid_data(var=100*diffrel[:,:,ns],
                                           color_bar=True,
                                           cmap=plt.cm.seismic,
                                           clim=[-10,10],
                                           clabel=('Deviation (' +
                                                   solpspy_run.species_names[ns]
                                                   + ') [%]'))
		plt.show()





















