
# THIS FILE DOES NOT SEEM TO BE USED! THE SAME METHODS ARE ALSO IN 'tools/tools.py'

# """
# This file contains:
#     - try_block
#     - solps_property
#     - priority
#     - module_path
#     - read_config
#     - styles
#     - yaml_loader
#     - format_e
# """
# from __future__ import division, print_function
# 
# import os
# import sys
# from functools import wraps
# 
# 
# 
# #ATTENTION: Improve docstring
# def try_block(func):
#     """
#     Tries self._ATTR and, if it is not there, then computes FUNC,
#     which should create self._ATTR and returns it.
#     Else, it returns self._ATTR.
# 
#     Additionally, after it has successfully implemented ._ATTR, if it is
#     a grid attribute, it will be flipped in the USN case.
#     LSN should remain unchanged.
#     """
#     @wraps(func)
#     def wrapped(self, *args, **kwargs):
#         try:
#             return getattr(self, '_'+func.__name__)
#         except:
#             try:
#                 func(self) #Func should create self._variable
#                 return getattr(self, '_'+func.__name__)
#             except:
#                 setattr(self, '_'+func.__name__, None)
#                 return getattr(self, '_'+func.__name__)
# 
#     return wrapped
# 
# 
# 
# #ATTENTION: Improve docstring
# def solps_property(func):
#     """ Sums up both try_block and property in a single decorator"""
#     return property(try_block(func))
# 
# 
# 
# def priority(names, *args):
#     """
#     Inputs:
#     ------
#         Names:  List of possible names of the property. List order implies
#                 priority order. I.e. if two or more names are present, the
#                 value of the first name will be accepted.
# 
#         Args:   List of all the possible dicts in which the property can
#                 be found.
#                 The last provided argument is the default.
# 
#     Outputs:
#     -------
#         Return: Value of the property, named with priority according to names order,
#                 of the highest priority source.
#     """
#     #If names is just a string, list create a list of characters
#     if isinstance(names, basestring):
#         names = [names]
#     else:
#         names = list(names)
#     for a in args[:-1]:
#         for b in names:
#             if b in a:
#                 return a[b]
#     return args[-1]
# 
# 
# 
# #ATTENTION: Improve docstring
# def read_config():
#     """ Reads default and local configuration files and merge them together
#     with appropiate priority order.
#     """
#     config = yaml_loader(os.path.join(module_path(),'config/default.yaml'))
#     local = yaml_loader(os.path.join(module_path(),'config/local.yaml'))
#     if not local:
#         local = {}
#     for section in config.iterkeys():
#         if section in local:
#             config[section].update(local[section])
#     return config
# 
# 
# 
# #ATTENTION: Improve docstring
# def styles(name=None):
#     """
#     """
#     #Old version, when tools was on root folder
#     #styles_path = os.path.join(os.path.dirname(__file__), 'mplstyles')
# 
#     dirpath = os.path.abspath(sys.modules['solpspy'].__file__)
#     dirpath = dirpath.rstrip(os.path.basename(dirpath))
# 
#     styles_path = os.path.join(dirpath, 'mplstyles')
#     if not name:
#         return [f[:-9] for f in os.listdir(styles_path)
#                 if os.path.isfile(os.path.join(styles_path, f))]
#     else:
#         try:
#             if name[-9:] == '.mplstyle':
#                 return os.path.join(styles_path,name)
#             else:
#                 return os.path.join(styles_path,name+'.mplstyle')
#         except:
#             return None
# 
# 
# 
# #ATTENTION: Improve docstring
# def module_path():
#     dirpath = os.path.abspath(sys.modules['solpspy'].__file__)
#     return dirpath.rstrip(os.path.basename(dirpath))
# 
# 
# 
# #ATTENTION: Improve docstring
# def yaml_loader(name):
#     """
#     Loads yaml file with 'name' safely and takes into account correctly
#     the scientific notation (no point before e/E and/or sign afterwards needed)
#     """
#     import yaml
#     import re
#     loader = yaml.SafeLoader
#     loader.add_implicit_resolver(
#         u'tag:yaml.org,2002:float',
#         re.compile(u'''^(?:
#          [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
#         |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
#         |\\.[0-9_]+(?:[eE][-+][0-9]+)?
#         |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
#         |[-+]?\\.(?:inf|Inf|INF)
#         |\\.(?:nan|NaN|NAN))$''', re.X),
#         list(u'-+0123456789.'))
#     with open(name, 'r') as fr:
#         data = yaml.load(fr, Loader=loader)
#     return data
# 
# 
# 
# #ATTENTION: Improve docstring
# def format_e(n, lower_case=True):
#     if lower_case:
#         a = '%e' % n
#         return a.split('e')[0].rstrip('0').rstrip('.') + 'e' + a.split('e')[1]
#     else:
#         a = '%E' % n
#         return a.split('E')[0].rstrip('0').rstrip('.') + 'E' + a.split('E')[1]
# 
# 
