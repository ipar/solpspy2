#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Routine to extract the AMJUEL/METHANE/HYDHEL fit parameters and
calculate cross sections of different reactions for different
parameters (like Te, ne, ...). The code is adapted from Mirko
Wensings Matlab Routine.

ATTENTION:
    One should be aware of the version of the used datset files. E.g., the
    reaction 'AMJUEL H.1 3.1.8M' from the old SOLPS 5.0 datasets from 2011
    appears to be renamed to 'AMJUEL H.1 3.1.8SD' in the newer SOLPS-ITER
    version (e.g., from 2018). There searching for 'AMJUEL H.1 3.1.8M' will
    result in an error.
"""

# THIS IS STILL INCOMPLETE! THE STATUS OF THE IMPLEMENTATION FOR THE 
# DIFFERENT REACTION TYPES (i.e. H.0-H.12) IS THE FOLLOWING:

# AMJUEL  H.0  Partially implemented (Reading coefficients is fine, evaluation not)
# AMJUEL  H.1  Finished.
# AMJUEL  H.2  Finished.
# AMJUEL  H.3  Finished.
# AMJUEL  H.4  Finished.
# AMJUEL  H.5  NOT YET IMPLEMENTED!!!
# AMJUEL  H.6  NOT YET IMPLEMENTED!!!
# AMJUEL  H.7  NOT YET IMPLEMENTED!!!
# AMJUEL  H.8  Partially implemented (Reading coefficients is fine, evaluation not)
# AMJUEL  H.9  NOT YET IMPLEMENTED!!!
# AMJUEL  H.10 Partially implemented (Reading coefficients is fine, evaluation not)
# AMJUEL  H.11 NOT YET IMPLEMENTED!!!
# AMJUEL  H.12 NOT YET IMPLEMENTED!!!

# METHANE H.1  NOT YET IMPLEMENTED!!!
# METHANE H.2  NOT YET IMPLEMENTED!!!
# METHANE H.3  Finished.

# HYDHEL  H.1  NOT YET IMPLEMENTED!!!
# HYDHEL  H.2  NOT YET IMPLEMENTED!!!
# HYDHEL  H.3  NOT YET IMPLEMENTED!!!

# =============================================================================

# Common modules
from __future__ import print_function
import os
import numpy as np

# ======== Definitions of the H.XX reaction classes: ==========================

class Hxx(object):
    def __init__(self,data=None):
        print('Warning: Unknown H-type, coefficients not read!')
        self.type = self._COEFF = self.reaction = self.physical_reaction = 'Unknown'
        if data is not None:
            self._init(data)
    def _init(self,data):
        self._data = data[:]
        self._read_reaction()
        self._read_coefficients()
    def info(self,all=True):
        print("")
        print("================================")
        print('Type:         ',self.type)
        print('Reaction:     ',self.reaction)
        print('              ',self.physical_reaction)
        if all: self._print_coefficients()
        print("================================")
        print("")
    def _print_coefficients(self):
        if self._COEFF == 'Unknown':
            print('Coefficients: Unknown')
            return
        print('Coefficients: ','{:<4}'.format(self._COEFF[0]),
              '{:>12.3e}'.format(self.__getattribute__(self._COEFF[0])))
        for coeff in self._COEFF[1:]:
            print('              ','{:<4}'.format(coeff),
                  '{:>12.3e}'.format(self.__getattribute__(coeff)))
    def _read_reaction(self):
        self.reaction = self._data[0].split('$')[0].replace('Reaction ','')
        if len(self._data[0].split('$')) > 3:
            print('Warning: cannot extract reaction from line:',self._data[0])
            self.physical_reaction = 'Unknown'
        else:
            self.physical_reaction = '$ '+self._data[0].split('$')[1]+' $'
    def _read_coefficients(self):
        pass
    def _set_coefficient(self,name,value):
        self.__setattr__(name,float(value.replace('D','e')))
    def _read_arg(self,arg,args):
        value = args[arg]
        if type(value) is not list and type(value) is not np.ndarray:
            value = [value]
        return np.array(value)
    def evaluate(self,**args):
        pass

class A0(Hxx):
    # Reaction type with 9 coefficients
    def __init__(self,data):
        self.type   = 'AMJUEL H.0'
        self._COEFF = ['p0','p1','p2','p3','p4','p5','p6','p7','p8']
        self._init(data)
    def _read_coefficients(self):
        d = ' '.join(self._data).split()
        for coeff in self._COEFF:
            self._set_coefficient(coeff,(d[d.index(coeff)+1]))

class A1(Hxx):
    # Reaction type with 9-15 coefficients (apparently the additional
    # alx/arx parameters are used when the limits given by elabmin and
    # elabmax are exceeded, if these are specified ...)
    def __init__(self,data):
        self.type   = 'AMJUEL H.1'
        self._COEFF = ['a0','a1','a2','a3','a4','a5','a6','a7','a8']
        #              'al0','al1','al2','ar0','ar1','ar2'] # <= not always!
        self._init(data)
    def _read_coefficients(self):
        self.elabmin = None
        self.elabmax = None
        for line in self._data:
            if 'elabmin' in line.lower():
                self._COEFF.extend(['al0','al1','al2'])
                elabmin = line
                while not elabmin[0].isdigit():
                    elabmin = elabmin[1:]
                while not elabmin[-1].isdigit():
                    elabmin = elabmin[:-1]
                self.elabmin = float(elabmin.replace(' ','+').replace('D','e'))
            if 'elabmax' in line.lower():
                self._COEFF.extend(['ar0','ar1','ar2'])
                elabmax = line
                while not elabmax[0].isdigit():
                    elabmax = elabmax[1:]
                while not elabmax[-1].isdigit():
                    elabmax = elabmax[:-1]
                self.elabmax = float(elabmax.replace(' ','+').replace('D','e'))
        d = ' '.join(self._data).split()
        for coeff in self._COEFF:
            self._set_coefficient(coeff,(d[d.index(coeff)+1]))
    def evaluate(self,**args):
        """Evaluates the reaction fit function at the value(s) e
        (which can be a single value, or a numpy array)."""
        e = self._read_arg('e',args)
        lnResult = np.zeros(e.shape)
        for n,coeff in enumerate(self._COEFF):
            for i,ex in enumerate(e):
                # Lower range (limited by elabmin):
                if self.elabmin is not None and ex < self.elabmin:
                    if 'l' not in coeff: continue
                    n = int(coeff[2])
                    lnResult[i] += self.__getattribute__(coeff)*((np.log(ex))**n)
                # Upper range (limited by elabmax):
                elif self.elabmax is not None and ex > self.elabmax:
                    if 'r' not in coeff: continue
                    n = int(coeff[2])
                    lnResult[i] += self.__getattribute__(coeff)*((np.log(ex))**n)
                # Normal range (elabmin < e < elabmax):
                else:
                    if 'r' in coeff or 'l' in coeff: continue
                    lnResult[i] += self.__getattribute__(coeff)*((np.log(ex))**n)
        return 1e-4*np.exp(lnResult) # Convert cm^2 to m^2

class A2(A0):
    # Reaction type with 9 coefficients => use A0
    def __init__(self,data):
        self.type   = 'AMJUEL H.2'
        self._COEFF = ['b0','b1','b2','b3','b4','b5','b6','b7','b8']
        self._init(data)
    def evaluate(self,**args):
        """Evaluates the reaction fit function at the value(s) te
        (which can be a single value, or a numpy array)."""
        te = self._read_arg('te',args)
        lnResult = np.zeros(te.shape)
        for n,coeff in enumerate(self._COEFF):
            lnResult += self.__getattribute__(coeff)*((np.log(te))**n)
        return 1e-6*np.exp(lnResult) # Convert cm^3/s to m^3/s

class A3(Hxx):
    # Reaction type with 9*9 coefficients
    def __init__(self,data):
        self.type   = 'AMJUEL H.3'
        self._init(data)
    def _read_coefficients(self):
        self._COEFF = ['e0t0','e1t0','e2t0','e0t1','e1t1','e2t1',
                       'e0t2','e1t2','e2t2','e0t3','e1t3','e2t3',
                       'e0t4','e1t4','e2t4','e0t5','e1t5','e2t5',
                       'e0t6','e1t6','e2t6','e0t7','e1t7','e2t7',
                       'e0t8','e1t8','e2t8','e3t0','e4t0','e5t0',
                       'e3t1','e4t1','e5t1','e3t2','e4t2','e5t2',
                       'e3t3','e4t3','e5t3','e3t4','e4t4','e5t4',
                       'e3t5','e4t5','e5t5','e3t6','e4t6','e5t6',
                       'e3t7','e4t7','e5t7','e3t8','e4t8','e5t8',
                       'e6t0','e7t0','e8t0','e6t1','e7t1','e8t1',
                       'e6t2','e7t2','e8t2','e6t3','e7t3','e8t3',
                       'e6t4','e7t4','e8t4','e6t5','e7t5','e8t5',
                       'e6t6','e7t6','e8t6','e6t7','e7t7','e8t7',
                       'e6t8','e7t8','e8t8']
        for l,line in enumerate(self._data):
            if 't-index' in line.lower() or 't index' in line.lower():
                break
        dtmp = ' '.join(self._data[l:]).split()
        data = []
        for d in dtmp:
            if '.' in d:
                data.append(d)
        for i,coeff in enumerate(self._COEFF):
            self._set_coefficient(coeff,data[i])
    def evaluate(self,**args):
        """Evaluates the reaction fit function at the value(s) e, ne
        (which can be single values, or numpy arrays)."""
        te = self._read_arg('te',args)
        e  = self._read_arg('e' ,args)
        lnResult = np.zeros(te.shape+e.shape)
        for coeff in self._COEFF:
            n    = int(coeff[3])
            m    = int(coeff[1])
            anm  = self.__getattribute__(coeff)
            lnTn = np.expand_dims(np.log(te)**n,1)
            lnEm = np.expand_dims(np.log(e) **m,0)
            lnResult += anm*lnEm*lnTn
        return 1e-6*np.exp(lnResult) # Convert cm^3/s to m^3/s

class A4(A3):
    # Reaction type with 9*9 coefficients => use A3
    def __init__(self,data):
        self.type   = 'AMJUEL H.4'
        self._init(data)
    def evaluate(self,**args):
        """Evaluates the reaction fit function at the value(s) te, ne
        (which can be single values, or numpy arrays)."""
        te = self._read_arg('te',args)
        ne = self._read_arg('ne',args)/1e6 # Density needs to be in cm^-3!
        if 1e8 >= ne.min() or ne.max() > 1e16:
            print("Warning: density exceeds recommended parameter range!")
            print("         recommended range: 1e8-1e16 cm^-3)")
            print("         min(ne) = "+'{:.2e}'.format(ne.min())+' cm^-3')
            print("         max(ne) = "+'{:.2e}'.format(ne.max())+' cm^-3')
        lnResult = np.zeros(te.shape+ne.shape)
        for coeff in self._COEFF:
            n    = int(coeff[3])
            m    = int(coeff[1])
            anm  = self.__getattribute__(coeff)
            lnTn = np.log(te)**n
            # Handle different density regimes:
            for i,nex in enumerate(ne):
                if nex <= 1e8: # i.e. lnnm <= 0
                    # The fits have been set up for a parameter range from
                    # ne = 1e8-1e6 cm^-3. For ne <= 1e8 the 2-parametric fit
                    # collapses to a one parametric fit vs. temperature only:
                    if m != 0:
                        continue
                    lnnm = 1
                else:
                    lnnm = np.log(nex/1e8)**m # (Density is rescaled by 1e8 in the fit)
                lnResult[:,i] += anm*lnnm*lnTn
        return 1e-6*np.exp(lnResult) # Convert cm^3/s to m^3/s

class A8(A2):
    # Reaction type with 9 coefficients => use A2
    def __init__(self,data):
        self.type   = 'AMJUEL H.8'
        self._COEFF = ['h0','h1','h2','h3','h4','h5','h6','h7','h8']
        self._init(data)

class A10(A4):
    # Reaction type with 9*9 coefficients => use A4
    def __init__(self,data):
        self.type   = 'AMJUEL H.10'
        self._init(data)

class M3(A3):
    # Reaction type with 9*9 coefficients => use A3
    def __init__(self,data):
        self.type   = 'METHANE H.3'
        self._init(data)
    def evaluate(self,**args):
        """Evaluates the reaction fit function at the value(s) e, ti
        (which can be single values, or numpy arrays)."""
        ti = self._read_arg('ti',args)
        e  = self._read_arg('e' ,args)
        lnResult = np.zeros(ti.shape+e.shape)
        for coeff in self._COEFF:
            n    = int(coeff[3])
            m    = int(coeff[1])
            anm  = self.__getattribute__(coeff)
            lnTn = np.expand_dims(np.log(ti)**n,1)
            lnEm = np.expand_dims(np.log(e) **m,0)
            lnResult += anm*lnEm*lnTn
        return 1e-6*np.exp(lnResult) # Convert cm^3/s to m^3/s

# =============================================================================

def read_reaction_rates(reaction,datafile='.'):
    """
    Returns an object containing the 'AMJUEL'/'HYDHEL'/'METHANE'
    reaction rates (i.e., the fit coefficients from the particular
    dataset) and a method to evaluate the reaction rates / cross
    sections for certain plasma parameters for the reaction
    specified as 'DATASET H.xx name'.
    ----------------
    Input parameters
        reaction : String containing the dataset, reaction type and
                   reaction name in the format 'DATASET H.xx name',
                   e.g., 'AMJUEL H.10 2.3.13a' (i.e., as e.g., also
                   specified in the SOLPS input.dat file)
        datafile : Path to the desired dataset file or to a directory
                   in which the database file(s) can be found as
                   'AMJUEL', 'HYDHEL' or 'METHANE' or as 'amjuel.tex',
                   'hydhel.tex' or 'methane.tex'
    ------------
    Output value
        An Hxx object containing the corresponding reaction coefficients
        and a method to evaluate the reaction fit function at certain
        plasma parameters via Hxx.evaluate(ne=X,te=X,e=X)
    """

    dset,htype,reac = _extract_reaction(reaction)
    file            = _get_database_file(datafile,dset)
    datablock       = _get_datablock(file,dset,htype,reac)

    # Create Hxx object:
    if dset == 'AMJUEL':
        if   htype == 'H.0' : return A0 (datablock)
        elif htype == 'H.1' : return A1 (datablock)
        elif htype == 'H.2' : return A2 (datablock)
        elif htype == 'H.3' : return A3 (datablock)
        elif htype == 'H.4' : return A4 (datablock)
        elif htype == 'H.8' : return A8 (datablock)
        elif htype == 'H.10': return A10(datablock)
    elif dset == 'METHAN' or dset == 'METHANE':
        if   htype == 'H.3' : return M3 (datablock)

    # Nothing from the above worked:
    raise Exception("Reaction type '" + htype + "' not yet implemented for '"
                    + reaction + "'!")

def _extract_reaction(reaction):
    """
    Isolates the requested dataset (i.e., 'AMJUEL', 'HYDHEL' or
    'METHANE'), the reaction type (i.e., the H.xx-value) and the
    reaction name.
    ----------------
    Input parameters
        reaction : String containing the dataset, reaction type and
                   reaction name in the format 'DATASET H.xx name',
                   e.g., 'AMJUEL H.10 2.3.13a'
    -------------
    Return values
        (dset,htype,reac) : Tuple containing the separated dataset,
                            H-type and reaction name strings.
    """
    r = reaction
    dset = r.split()[0]
    r = r.replace(dset,'').lstrip(' ')
    if ' ' not in r:
        if   r[4] == '.':
            r = r[:3]+' '+r[3:]
        elif r[5] == '.':
            r = r[:4]+' '+r[4:]
    htype = r.split()[0]
    try:
        reac  = r.split()[1]
    except:
        raise Exception("Could not extract reaction name: '" + reaction + "'")
    if dset not in ['AMJUEL','HYDHEL','METHANE','METHAN']:    
        raise Exception("Invalid reaction dataset or dataset (i.e., " +
                        "'AMJUEL', 'HYDHEL'  or 'METHANE') not specified: '" + 
                        reaction + "'")
    if htype[:2] != 'H.' or reac == '':    
        raise Exception("Invalid reaction '" + reaction + "'")
    return dset,htype,reac

def _get_database_file(datafile,dset):
    """Returns the requested database file for the specified dataset."""
    file = None
    datafile = os.path.abspath(datafile).rstrip('/')
    if os.path.isfile(datafile):
        file = datafile
    elif os.path.isdir(datafile):
        if dset.startswith('AMJ'):
            if   os.path.isfile(datafile+'/AMJUEL'):
                file = datafile+'/AMJUEL'
            elif os.path.isfile(datafile+'/amjuel.tex'):
                file = datafile+'/amjuel.tex'
        elif dset.startswith('HYD'):
            if   os.path.isfile(datafile+'/HYDHEL'):
                file = datafile+'/HYDHEL'
            elif os.path.isfile(datafile+'/hydhel.tex'):
                file = datafile+'/hydhel.tex'
        elif dset.startswith('MET'):
            if   os.path.isfile(datafile+'/METHANE'):
                file = datafile+'/METHANE'
            elif os.path.isfile(datafile+'/methane.tex'):
                file = datafile+'/methane.tex'
    if file is None or not os.path.isfile(file):
        raise Exception("No database file found for dataset '"
                        + dset + "' in '" + datafile + "'")
    file = os.path.realpath(file)
#   print("Found database file '"+file+"'")
    return file

def _get_datablock(file,dset,htype,reac):
    """Parses the database file and returns the block containing the
    data for the requested reaction."""
    # Open & read file:
    with open(file,'r') as f:
        lines = f.readlines()
    nl = len(lines)
    # Go to start of data sections:
    l = 0
    while l<nl and not '##BEGIN DATA HERE##' in lines[l]:
        l+=1
    if l == nl:
        raise Exception("Invalid database file - 'BEGIN DATA HERE' string not found!")
    # Go to correct section:
    while l<nl and not lines[l].lstrip(' ').startswith('\section{'+htype):
        l+=1
    if l == nl:
        raise Exception("Invalid database file - H-type section for '"+htype+"' not found!")
    # Go to correct reaction:
    while l<nl:
        if lines[l].lstrip(' ').startswith('Reaction '+reac+' '): break
        l+=1
    if l == nl:
        raise Exception("Reaction '"+dset+' '+htype+' '+reac+"' not found!")
    l_start = l
    l +=1
    # Go to next (sub)section:
    while l<nl:
        if (lines[l].lstrip(' ').startswith('\subsubsection{') or
            lines[l].lstrip(' ').startswith('\subsection{') or
            lines[l].lstrip(' ').startswith('\section{')): break
        l+=1
    l_end = l-1
    datablock = lines[l_start:l_end]
    return datablock
