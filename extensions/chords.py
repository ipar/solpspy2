#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    This module defines a class Chords() which can be used to do plots
    and calculations on different (e.g. spectroscopic) lines of sight.

Examples for plotting:
    Chords(run)['RIN-2'].plot(var      = run.ne,
                              title    = "Electron Density",
                              ylabel   = "$n_e$ [$10^{19}\mathrm{m}^{-3}$]",
                              factor   = 1e19,
                              save_as  = 'RIN-2_ne.pdf')

    Chords(run)['ROV-14'].plot(var      = [run.na[:,:,32],run.na[:,:,33]],
                               legend   = [run.species_names[32],
                                           run.species_names[33]],
                               title    = "Nitrogen density",
                               ylabel   = "$n$ [$10^{19}\mathrm{m}^{-3}$]",
                               factor   = 1e19)
"""

# Common modules
from __future__ import print_function
import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.patches import Polygon,Circle
from matplotlib.collections import PatchCollection
from collections import OrderedDict

# Solpspy modules
from solpspy.tools.tools import try_block, solps_property, priority, LinAlg2d

# =============================================================================

class Chords(object):
    """
    A class containing several Chord() objects for all lines
    of sight that are contained in the diaggeom_coords_file.
    """

    def __init__(self,solpspy_run,**kwargs):

        self._run = solpspy_run
        self.log = self._run.log

        if self._run.exp.startswith('AUG'):
            self.diaggeom_coords_file = priority(['diaggeom_coords_file',
                                                  'coords_file','chords',
                                                  'chords_file'], kwargs,
                                                 os.path.join(self._run.module_path,
                                                              'data/LOS/AUG/' +
                                                              '34209-LOS.coords'))
        elif self._run.exp == 'DEMO':
            self.diaggeom_coords_file = priority(['diaggeom_coords_file',
                                                  'coords_file','chords',
                                                  'chords_file'], kwargs,
                                                 os.path.join(self._run.module_path,
                                                              'data/LOS/DEMO/' +
                                                              'div_los.dat'))

        self.chords = OrderedDict()
        self._read_coords()

    def __getitem__(self,key): # Allows acessing elements via Chords()[key]
        return self.chords[key]

    def __iter__(self):        # Allows iterating through Chords()
        return iter(self.chords)

    def __repr__(self):        # Default print output
        return ( 'Chords '
                 + str(self.chords.keys()).replace('[','{').replace(']','}') )

    def _read_coords(self):
        if not os.path.isfile(self.diaggeom_coords_file):
            self.log.error('Diaggeom coords file not found: %s',
                            self.diaggeom_coords_file)
            return

        with open(self.diaggeom_coords_file,'r') as f:
            for l in f:
                # Skip empty lines:
                try:    l.split()[0]
                except: continue

                if not 'From R=' in l and not 'To   R=' in l:
                    loshead = l.split()[0]
                else:
                    r = float(l.split('R=')[1].split('m,')[0])
                    z = float(l.split('z=')[1].split('m,')[0])
                    p = float(l.split('phi=')[1])
                    if 'From R=' in l:
                        losnumber = l.split()[0].replace('.','')
                        if losnumber == 'From': losnumber = ''
                        f = np.array([r,z,p])
                    elif 'To   R=' in l:
                        t = np.array([r,z,p])
                        if losnumber:
                            losname = loshead+'-'+losnumber
                        else:
                            # (Old diaggeom .coords output format)
                            losname = loshead.split('0')
                            if losname[-1] == '': # Ends with 0 (e.g. RIN010)
                                losname = losname[0]+'-'+losname[-2]+'0'
                            else:
                                losname = losname[0]+'-'+losname[-1]

                        if losname in self.chords:
                            self.log.warning("Duplicate appearance of line "
                                              "of sight '%s'",losname)
                        self.chords[losname] = Chord(self._run,losname,f,t)

    def _add_chord(self,losname,start,end):
        self.chords[losname] = Chord(self._run,losname,start,end)

    def plot(self,var='ne',**kwargs):
        """A method to plot several lines of sight as color coded lines
        in the grid visualizing their line averaged values.

        The main arguments:
         - chords : A list of chords-names to be plotted. (Default: all)
         - var    : The value to be plotted (as line average for each los).
                    Can be a nx*ny matrix, a string like 'ne' (default),
                    or a list of values (e.g. from the experiment) with
                    len(var)==len(chords).
        """

        chords      = kwargs.pop('chords'     , self.chords.keys())
        log         = kwargs.pop('log'        , False)
        title       = kwargs.pop('title'      , '')
        label       = kwargs.pop('label'      , '')
        xlim        = kwargs.pop('xlim'       , None)
        ylim        = kwargs.pop('ylim'       , None)
        zlim        = kwargs.pop('zlim'       , None)
        cmap        = kwargs.pop('cmap'       , plt.cm.jet)
        fontsize    = kwargs.pop('fontsize'   , None)
        avr_or_int  = kwargs.pop('avr_or_int' , 'avr')
        showplot    = kwargs.pop('showplot'   , True)
        plotgrid    = kwargs.pop('plotgrid'   , True)
        save_as     = kwargs.pop('save_as'    , None)
        plot_vessel = kwargs.pop('plot_vessel'    , True)

        # =============
        # Preparations:

        if fontsize:
            matplotlib.rcParams.update({'font.size': fontsize})

        if var is 'ne' and label == '':
            label = 'Electron density [$\mathrm{m}^{-3}$]'

        # Get los values:
        if type(var) == list:
            # User defined list of values:
            if len(var) != len(chords):
                self.log.warning("Incompatible sizes of 'chords' and 'var': "
                                  +" %s vs. %s", len(chords),len(var))
                return
            values = var
        else:
            # Values from the simulation:
            if type(var) == str:
                try:
                    val = self._run.__getattribute__(var)
                except:
                    self.log.warning("Invalid variable: '%s'",var)
                    return
            elif type(var) == np.ndarray:
                val = var

            if val.shape != (self._run.nx,self._run.ny):
                self.log.warning("Invalid variable shape of 'var': %s",
                                  val.shape)
                return

            values = []
            for los in chords:
                if avr_or_int == 'int':
                    values.append(self.chords[los].integrate(val))
                else:
                    values.append(self.chords[los].average(val))

        # ==============
        # Create figure:
        fig = plt.figure(figsize=(9,11))
        canvas = fig.add_subplot(1,1,1)

        # Plot vessel:
        if plot_vessel:
            for i in xrange(self._run.vessel.shape[1]):
                R = np.array([self._run.vessel[0,i],self._run.vessel[2,i]])
                z = np.array([self._run.vessel[1,i],self._run.vessel[3,i]])
                canvas.plot(R,z,'k',linewidth = 1)

        # Plot grid:
        if plotgrid:
            for nx in range(self._run.nx):
                for ny in range(self._run.ny):
                    canvas.add_patch(Polygon(self._run.grid[nx,ny], True,
                                             linewidth=0.5, edgecolor='gray',
                                             fill=False))
        # Plot line of sight (LOS):
        patch_los = []
        patch_val = []
        for i in range(len(chords)):
            los = self.chords[chords[i]]
            val = values[i]
            if val is None: continue
            for ls in los.line_segments:
                patch_los.append(Polygon(np.array([ls.start[:2],ls.end[:2]])))
                patch_val.append(val)

        patch_val = np.array(patch_val)
        p = PatchCollection(patch_los)
        if zlim is not None:
            p.set_clim(zlim)
        if log:
            if zlim is not None:
                p.set_norm(colors.LogNorm(vmin = zlim[0], vmax = zlim[1]))
            else:
                p.set_norm(colors.LogNorm())
        p.set(array=patch_val,cmap=cmap)
        p.set_edgecolor('face')
        canvas.add_collection(p)
        cbar = plt.colorbar(p)
        if label: cbar.set_label(label, labelpad=10)

        # Plot options:
        canvas.axis('equal')
        xmin=self._run.grid[:,:,:,0].min()
        xmax=self._run.grid[:,:,:,0].max()
        ymin=self._run.grid[:,:,:,1].min()
        ymax=self._run.grid[:,:,:,1].max()
        dy = (ymax - ymin)*0.05
        dx = (xmax - xmin)*0.05
        xmin=xmin-dx
        xmax=xmax+dx
        ymin=ymin-dy
        ymax=ymax+dy
        if xlim is not None:
            xmin=xlim[0]
            xmax=xlim[1]
        if ylim is not None:
            ymin=ylim[0]
            ymax=ylim[1]
        canvas.axis([xmin, xmax, ymin, ymax])
        canvas.set_title(title)
        canvas.set_xlabel('R [m]')
        canvas.set_ylabel('z [m]')

        # Show the plot:
        fig.tight_layout()
        self._run.plot.pdfpages.save()
        if showplot: plt.show()
        if save_as is not None:
            plt.savefig(save_as)
        plt.close()

# =============================================================================

class Chord(object):
    """
    An object containing the information for one line of sight (LOS), and
    methods for plotting and for the calculation of line integrated and
    line averaged values.
    """

    def __init__(self,solpspy_run,name,start,end):
        """
        Parameters:
        -----------
        solpspy_run : RundirData or MdsData object
            The solpspy run which is to be considered.

        name : str
            The name of the line of sight

        start : (r,z,phi) np.array or list
            LOS start point

        end : (r,z,phi) np.array or list
            LOS end point

        los : numpy.ndarray
            The line of sight, specified as an numpy.ndarray with 
            shape (2,3) defining the start and endpoints in (r,z,rho)
            coordinates.
        """

        self._run  = solpspy_run     # SOLPSpy run
        self.log   = self._run.log

        self.name  = name            # Name of the line of sight
        self.start = np.array(start) # LOS start point: (r,z,phi) vector
        self.end   = np.array(end)   # LOS end point:   (r,z,phi) vector
        self.los   = np.vstack([self.start,self.end])

    class _Intersection(object):
        """Class which contains the position and some other
        properties of an intersection of a LOS with the grid"""
        def __init__(self,r,z,p,l,cells):
            if type(cells[0]) != list:
                cells = [cells]      # Should be a list of [nx,ny] lists of
            self.cells = cells       # all associated / neighboring cells
            self.r     = r
            self.z     = z
            self.p     = p
            self.l     = round(l,12) # Distance along the line of sight
                                     # (l==0 at LOS start, l==1 at LOS end)
            # (Rounding is necessary to be able to compare
            #  and sort the intersections later.)

        def combine(self,intersection):
            self.cells.extend(intersection.cells)

    class _LineSegment(object):
        def __init__(self,start,end,nx,ny):
            self.start  = np.array(start)
            self.end    = np.array(end)
            self.nx     = nx
            self.ny     = ny
            self.seg    = np.vstack([self.start,self.end])
            self.length = LinAlg2d.distance_cylindrical_coordinates(self.start,
                                                                    self.end)

    @solps_property
    def line_segments(self):
        """A list of _LineSegment objects."""
        self._line_segments = self._get_line_segments()

    @property
    @try_block
    def length(self):
        """Total length of the line of sight (including
        parts outside the computational domain)."""
        self._length = LinAlg2d.distance_cylindrical_coordinates(self.start,
                                                                 self.end)

    @property
    @try_block
    def cdlength(self):
        """Length of the line of sight in the computational domain."""
        self._cdlength = self.integrate()

    def integrate(self,var = 'rlcl'):
        """Calculate the line integral of the given value for the LOS."""
        if var is 'rlcl': var = self._run.masks.rlcl
        if not self._check_var(var): return
        int = 0
        for ls in self.line_segments:
            # Calculate the integral excluding guard cells:
            if (ls.nx == -1 or ls.ny == -1 or
                np.isnan(var[ls.nx,ls.ny]) or 
                np.isinf(var[ls.nx,ls.ny])):
                continue
            int += ls.length*var[ls.nx,ls.ny]*self._run.masks.rlcl[ls.nx,ls.ny]
        return int

    def average(self,var = 'rlcl'):
        """Calculate the line average of the given value for the LOS."""
        if self.cdlength == 0: return 0
        else: return self.integrate(var) / self.cdlength

    def minimum(self,var):
        """Returns the minimum value of 'var' along the LOS."""
        if not self._check_var(var): return
        minimum = np.inf
        for ls in self.line_segments:
            # Search the minimum excluding guard cells:
            if (ls.nx == -1 or ls.ny == -1 or
                var[ls.nx,ls.ny] == np.nan or var[ls.nx,ls.ny] == np.inf):
                continue
            minimum = min(minimum,
                          var[ls.nx,ls.ny]*self._run.masks.rlcl[ls.nx,ls.ny])
        return minimum

    def maximum(self,var):
        """Returns the maximum value of 'var' along the LOS."""
        if not self._check_var(var): return
        maximum = -np.inf
        for ls in self.line_segments:
            # Search the maximum excluding guard cells:
            if (ls.nx == -1 or ls.ny == -1 or
                var[ls.nx,ls.ny] == np.nan or var[ls.nx,ls.ny] == np.inf):
                continue
            maximum = max(maximum,
                          var[ls.nx,ls.ny]*self._run.masks.rlcl[ls.nx,ls.ny])
        return maximum

    def end_value(self,var):
        """Return the value of var at the end of the LOS."""
        v = 0
        for ls in self.line_segments:
            # Skip line segments outside of the grid:
            if ls.nx == -1 or ls.ny == -1: continue
            # Skip guard cells:
            if self._run.masks.gdcl[ls.nx,ls.ny]: continue
            v = var[ls.nx,ls.ny]
        return v

    def values_along_chord(self,var):
        x = [0]
        y = [0]
        i = 0
        for ls in self.line_segments:
            if ls.nx == -1 or ls.ny == -1: val = 0
            else: val = var[ls.nx,ls.ny]
            x.append(x[i])
            y.append(val)
            x.append(x[i] + ls.length)
            y.append(val)
            i += 2
        x.append(x[-1])
        y.append(0)
        return x,y

    def plot(self,var=None,**kwargs):
        """Plots the value of var along the chord (line of sight),
        if var=None only plots the chord in the 2d computational domain.

        Arguments:
          - var :    The value which will be plotted (np.array[nx,ny(,ns)])
          - title :  Title of the value plot

          - var      :  The value which will be plotted (np.array[nx,ny(,ns)]).
                        Can also be a list of values which will all be plotted
                        in one plot (so they should be of the same type, eg.
                        densities for different ionization stages).
          - legend   :  Legend for the plot of multiple vars (must be a list)
          - title    :  Title of the value plot
          - ylabel   :  y-label of the value plot
          - factor   :  y-factor as indicated on the y-label (e.g. kW => 1e3)
          - save_as  :  Filename under which the plot will be saved
          - showplot :  Toggles if the plot should be shown
          - nolos    :  If nolos == True only the value plot will be shown
          - notext   :  Suppress the integral/average text in the value plot
          - log      :  Logarithmic scale

        Example:
        Chords(run)['RIN-2'].plot(var      = run.ne,
                                  title    = "Electron Density",
                                  ylabel   = "$n_e$ [$10^{19}\mathrm{m}^{-3}$]",
                                  factor   = 1e19,
                                  save_as  = 'RIN-2_ne.pdf',
                                  showplot = True )
        """

        # Possible options and defaults:
        title       = kwargs.pop('title'    , '')
        ylabel      = kwargs.pop('ylabel'   , '')
        legend      = kwargs.pop('legend'   , None)
        factor      = kwargs.pop('factor'   , 1)
        save_as     = kwargs.pop('save_as'  , None)
        showplot    = kwargs.pop('showplot' , True)
        printval    = kwargs.pop('printval' , False)
        plotgrid    = kwargs.pop('plotgrid' , True)
        nolos       = kwargs.pop('nolos'    , False)
        notext      = kwargs.pop('notext'   , False)
        log         = kwargs.pop('log'      , False)
        cmap        = kwargs.pop('cmap'     , None)
        plot_vessel = kwargs.pop('plot_vessel'    , True)
        for kw in kwargs:
            self.log.error("Invalid option: %s",kw)
            return

        # Preparation:
        if var is None: var = []
        if type(var) != list: var = [var]
        if type(legend) != list: legend = len(var) * ['']
        while len(legend) < len(var):
            legend.append('')
        for v in var:
            if not self._check_var(v): return
        if save_as is not None and not save_as.endswith('.pdf.'): 
            save_as += '.pdf'
        if type(cmap) == str:
            try:
                cmap = plt.cm.get_cmap(cmap)
            except:
                self.log.error("Invalid color map: %s",cmap)
                return

        # 3 Basic options:
        #  - Plot only the line of sight in the grid
        #  - Plot only the values of 'var' along the los
        #  - Plot both next to each other
        if var is []:  plotlos = True  ; plotval = False
        elif nolos:    plotlos = False ; plotval = True
        else:          plotlos = True  ; plotval = True

        # Create figure:
        if plotlos and plotval:
            fig, (c_los, c_val) = plt.subplots(1, 2, figsize=(12,6),
                                      gridspec_kw = {'width_ratios':[1, 2]})
        elif plotlos:
            fig = plt.figure(figsize=(8,11))
            c_los = fig.add_subplot(1,1,1)
        elif plotval:
            fig = plt.figure(figsize=(11,8))
            c_val = fig.add_subplot(1,1,1)

        # ======================
        # Create the line of sight plot:
        if plotlos:

            # Plot vessel:
            if plot_vessel:
                for i in xrange(self._run.vessel.shape[1]):
                    R = np.array([self._run.vessel[0,i],self._run.vessel[2,i]])
                    z = np.array([self._run.vessel[1,i],self._run.vessel[3,i]])
                    c_los.plot(R,z,'k',linewidth = 1)

            # Plot grid:
            if plotgrid:
                for nx in range(self._run.nx):
                    for ny in range(self._run.ny):
                        c_los.add_patch(Polygon(self._run.grid[nx,ny], True,
                                                linewidth=0.5, edgecolor='gray',
                                                fill=False))
            # Plot line of sight (LOS):
            for ls in self.line_segments:
                # Highlight cells:
                if plotgrid and ls.nx != -1 and ls.ny != -1:
                    c_los.add_patch(Polygon(self._run.grid[ls.nx,ls.ny],
                                            alpha=0.5))
                # Draw the line of sight (each segment):
                c_los.add_patch(Polygon(ls.seg[:,:2],color='k'))
                # Highlight the intersection points:
                c_los.add_patch(Circle(ls.seg[0,:2],0.00003,color='k'))
                c_los.add_patch(Circle(ls.seg[1,:2],0.00003,color='k'))
            # Highlight the starting point:
            c_los.add_patch(Circle(self.start[:2],0.0035,color='k'))
            #c_los.add_patch(Circle(self.line_segments[0].start[:2],
            #                       0.0035,color='k'))

            # Plot options:
            c_los.axis('equal')
            xmin=self._run.grid[:,:,:,0].min()
            xmax=self._run.grid[:,:,:,0].max()
            ymin=self._run.grid[:,:,:,1].min()
            ymax=self._run.grid[:,:,:,1].max()
            xmin=min(xmin,self.start[0])
            xmax=max(xmax,self.start[0])
            ymin=min(ymin,self.start[1])
            ymax=max(ymax,self.start[1])
            dy = (ymax - ymin)*0.05
            dx = (xmax - xmin)*0.05
            c_los.axis([xmin-dx, xmax+dx, ymin-dy, ymax+dy])
            c_los.set_title(self.name)
            c_los.set_xlabel('R [m]')
            c_los.set_ylabel('z [m]')

            if not plotval:
                # Show the plot:
                fig.tight_layout()
                self._run.plot.pdfpages.save()
                if showplot: plt.show()
                if save_as is not None:
                    plt.savefig(save_as)
                plt.close(fig)
                return

        # ======================
        # Create the value plot:
        xmin = np.inf
        xmax = -np.inf
        for vi,v in enumerate(var):
            x = [0]
            y = [0]
            i = 0
            if printval:
                print('')
                print(55*'=')
                print('Line of sight: ',self.name)
                desc = legend[vi]
                if desc == '': desc = title
                if desc != '':
                    print('Values for the',desc,'plot:')
                desc = ylabel.replace('\mathrm','').replace('$','')
                desc = desc.replace('{','').replace('}','').replace('\\','')
                print('{:<25}'.format('Distance along LOS [m]'),desc)
                print('')
            for ls in self.line_segments:
                if ls.nx == -1 or ls.ny == -1: val = 0
                else: val = v[ls.nx,ls.ny]
                val = val / factor
                x.append(x[i])
                y.append(val)
                x.append(x[i] + ls.length)
                y.append(val)
                if printval:
                    print('{:<25}'.format(np.average([x[-1],x[-2]])),val)
                i += 2
            x.append(x[-1])
            y.append(0)
            if cmap is not None:
                color = cmap(float(vi)/(len(var)-1))
                c_val.plot(x,y,label=legend[vi],color=color)
            else:
                c_val.plot(x,y,label=legend[vi])

            # Set the plot range: (hide zero-values at the edges)
            i = 0
            while i < len(y)-1:
                if y[i] != 0: break
                i += 1
            xmin = min(xmin,x[i])
            i = len(y)-1
            while i > 0:
                if y[i] != 0: break
                i -= 1
            xmax = max(xmax,x[i])
        dx = (xmax - xmin)*0.05
        c_val.set_xlim([xmin-dx, xmax+dx])
        if legend[0] != '': c_val.legend(loc=0)

        # Plot options:
        c_val.set_xlabel("Distance along the line of sight ('"
                      + self.name + "') [m]")
        c_val.set_ylabel(ylabel)
        c_val.set_title(title)
        if log: c_val.set_yscale('log')

        if len(var) == 1 and not notext:
            text  = 'Line integral: ' + str(self.integrate(var[0])) + '\n'
            text += 'Line average: ' + str(self.average(var[0]))
            c_val.text( 0.05, 0.9, text, transform=c_val.transAxes )

        # Show the plot:
        fig.tight_layout(w_pad=3.2)
        self._run.plot.pdfpages.save()
        if showplot: plt.show()
        if save_as is not None:
            fig.savefig(save_as)
        plt.close(fig)

    def _get_line_segments(self):
        """Returns a list of line segments which are calculated
        from the intersections of the LOS with the grid."""
        # Find intersections with the grid:
        self.intersections = self._get_intersections()
        # --> Unsorted list of all intersections containing duplicates
        self.intersections = self._sort_intersections()
        # --> Sorted and unique list of all intersections

        # Print some warnings:
        if len(self.intersections) == 2: # (Start + end point only)
            self.log.warning("Line of sight '" + self.name + "' has no "
                             "intersection with the computational domain.")
            return []
        if self.intersections[0].l < 0:
            self.log.warning("Line of sight '" + self.name +
                             "': startpoint inside of the grid!") # - " +
#                            "automatically extended to the grid outline.")
        if self.intersections[-1].l > 1:
            self.log.warning("Line of sight '" + self.name +
                             "': endpoint inside of the grid!") # - " +
#                            "automatically extended to the grid outline.")

        # Create line segments:
        line_segments = []
        for i in range(len(self.intersections)-1):
            start = np.array([self.intersections[i].r,
                              self.intersections[i].z,
                              self.intersections[i].p])
            end   = np.array([self.intersections[i+1].r,
                              self.intersections[i+1].z,
                              self.intersections[i+1].p])
            lcenter = np.average([self.intersections[i].l,
                                  self.intersections[i+1].l])
            center = self._get_point_along_line(lcenter)[:2] # (r,z)

            nx = ny = -1
            for x,y in (self.intersections[i].cells +
                        self.intersections[i+1].cells):
                if x == -1 or y == -1:
                    continue
                if LinAlg2d.point_inside_cell(self._run.grid[x,y],center):
                    nx = x
                    ny = y
            line_segments.append(self._LineSegment(start,end,nx,ny))

#           # Debugging:
#           print("From intersections r:",self.intersections[i].r)
#           print("                   z:",self.intersections[i].z)
#           print("                   p:",self.intersections[i].p)
#           print("                   l:",self.intersections[i].l)
#           print("               cells:",self.intersections[i].cells)
#
#           print("               and r:",self.intersections[i+1].r)
#           print("                   z:",self.intersections[i+1].z)
#           print("                   p:",self.intersections[i+1].p)
#           print("                   l:",self.intersections[i+1].l)
#           print("               cells:",self.intersections[i+1].cells)
#
#           print("Line segment start:",line_segments[-1].start)
#           print("               end:",line_segments[-1].end)
#           print("             nx ny:",line_segments[-1].nx,
#                 line_segments[-1].ny,"was created.")
#           print("")

        # Check result:
        for ls in line_segments[1:-1]:
            if ls.nx == -1 or ls.ny == -1:
                self.log.warning("Line of sight '" + self.name + "' "
                                  + "not fully covered by the "
                                  + "computational domain.")
                break
        return line_segments

    def _sort_intersections(self):
        # Add start point:
        self.intersections.append(self._Intersection(self.start[0],
                                                     self.start[1],
                                                     self.start[2],
                                                     0.0,[-1,-1]))
        intersdict = {}
        for inter in self.intersections:
            if inter.l not in intersdict:
                intersdict[inter.l] = inter
            else:
                intersdict[inter.l].combine(inter)
        # --> unique

        intersections = []
        for l in sorted(intersdict.keys()):
            # TODO: What behavior do I want? Is this good?
            if l > 1 and [-1,-1] in intersdict[l].cells:
                continue
            if l > 1.1 or l < -0.1:
                continue
            intersections.append(intersdict[l])
        # --> sorted & filtered

        # Add end point only if no other intersection was found:
        if len(intersections) == 1:
            intersections.append(self._Intersection(self.end[0],self.end[1],
                                                    self.end[2],1.0,[-1,-1]))
        return intersections

    def _get_intersections(self):
        """Returns all intersections of self.los with the grid."""
        # Contains duplicate intersections, since the same intersection
        # can occur in different grid cells (if they share a cell face)!
        intersections = []
        for nx in range(self._run.nx): # Iterate through all cells
            for ny in range(self._run.ny):
                intersections.extend(self._find_grid_cell_intersections(nx,ny))
        return intersections

    def _find_grid_cell_intersections(self,nx,ny):
        """Returns all intersections of self.los with the grid cell [nx,ny]."""
        # Can contain more than two intersections, since a toriodal LOS
        # can pass through a grid cell twice!
        intersections = []
        for i in range(self._run.grid.shape[2]): # Iterate through cell-faces
            ni = i+1 if i != self._run.grid.shape[2]-1 else 0
            cf = np.vstack([self._run.grid[nx,ny,i],self._run.grid[nx,ny,ni]])
            intersections.extend(self._find_cell_face_intersections(cf,nx,ny))
        return intersections

    def _find_cell_face_intersections(self,cellface,nx,ny):
        """Returns the intersection points of self.los with a
        grid cell-face specified by its starting and endpoint
        as an numpy.ndarray of shape (2,2)"""
        # Can contain two intersections, since a toriodal LOS
        # can pass through a cell-face twice!
        intersections = []

        # The calculation is based on the following system of equations:
        #
        # [ ra + G * (rb - ra) ] * cos(T) = rc * cos(pc) + L [ rd * cos(pd) - rc * cos(pc) ]
        # [ ra + G * (rb - ra) ] * sin(T) = rc * sin(pc) + L [ rd * sin(pd) - rc * sin(pc) ]
        #   za + G * (zb - za)            = zc           + L [ zd - zc ]
        #
        # With:
        #  - ra,rb,za,zb: The start / end positions of the cell face (r,z)
        #  - rc,rd,zc,zd,pd,pd: The start / end positions of the LOS (r,z,rho)
        #  - T: The toroidal angle Theta
        #  - G: The position along the cell face (G=0 at the starting point and G=1 at the end point)
        #  - L: The position along the LOS       (L=0 at the starting point and L=1 at the end point)

        # The points defining the cell face:
        ra = cellface[0,0] # R
        za = cellface[0,1] # Z
        rb = cellface[1,0] # R
        zb = cellface[1,1] # Z

        # The points defining the LOS:
        rc = self.los[0,0] # R
        zc = self.los[0,1] # Z
        pc = self.los[0,2] # Phi
        rd = self.los[1,0] # R
        zd = self.los[1,1] # Z
        pd = self.los[1,2] # Phi

        # L can be calculated via the quadratic formula after having solved
        # the above system of equations (in which Theta can be eliminated by
        # solving for cos(T) and sin(T) and using cos^2 + sin^2 = 1):
        #
        # L1/2 = [ -b +/- sqrt(b^2 - 4ac) ] / 2a

        # For this we calculate a, b and c (using a few auxiliary parameters):
        cc  = np.cos(pc*np.pi/180.0)
        sc  = np.sin(pc*np.pi/180.0)
        cd  = np.cos(pd*np.pi/180.0)
        sd  = np.sin(pd*np.pi/180.0)
        cdc = (rd*cd-rc*cc)
        sdc = (rd*sd-rc*sc)

        # ==================================================================
        # Handle special cases (to avoid later zero divisions, etc.):
        if rc == rd and zc == zd and pc == pd:
            # Invalid LOS
            # self.log.warning("Line of sight '" + self.name + "': "
            #                  + "Invalid LOS Parameters!")
            return []
        if za == zb == zc == zd:
            # LOS coincides with (horizontal) cell face
            # => Find intersections with rings at r == ra and r == rb:
            for r0 in [ra,rb]:
                a = cdc**2 + sdc**2
                b = 2*rc*(sc*sdc + cc*cdc)
                c = rc**2 - r0**2
                bac = b**2 - 4*a*c
                if bac <= 0:
                    # No intersection exists:
                    continue
                L1 = (-b + np.sqrt(bac)) / (2*a)
                L2 = (-b - np.sqrt(bac)) / (2*a)
                for L in [L1,L2]:
                    cT = (rc*cc + L*cdc) / r0
                    T  = np.arccos(cT) * 180.0/np.pi # --> in degrees
                    # Rounding errors can lead to cT being slightly
                    # bigger than one, which leads to T = nan:
                    if np.isnan(T):
                        if   cT >  0.99 and cT <  1.01:
                            T = 0.0
                        elif cT < -0.99 and cT > -1.01:
                            T = 180.0
                        else:
                            self.log.warning("Line of sight '"+self.name+"': "
                                             +"Theta = nan, could not determine"
                                             +" arccos(" + str(cT) + ")")
                    intersections.append(self._Intersection(r0,za,T,L,[nx,ny]))
                    # print("Special Case A:",L,nx,ny)
            return intersections
        elif za == zb and zc == zd:
            # No intersection exists:
            # print("Special Case B")
            return []
        elif za == zb:
            # Search for intersection of LOS with a plane at z == za == zb:
            L    = (za-zc)/(zd-zc)
            Pint = self._get_point_along_line(L)
            # print("Special Case C:",L,nx,ny)
            return [self._Intersection(Pint[0],Pint[1],Pint[2],L,[nx,ny])]
        if ( pc == pd and
             # Cell face and LOS-r-z-projection parallel:
             (rd-rc)/(rb-ra) == (zd-zc)/(zb-za) and
             # Cell face and LOS-r-z-projection coinciding:
             (ra-rc)/(rd-rc) == (za-zc)/(zd-zc) ):
            La = (ra-rc)/(rd-rc)
            Lb = (rb-rc)/(rd-rc)
            intersections.append(self._Intersection(ra,za,pc,La,[nx,ny]))
            intersections.append(self._Intersection(rb,zb,pc,Lb,[nx,ny]))
            # print("Special Case C:",Lz,Lb,nx,ny)
            return intersections
        # ==================================================================
        # Back to the standard case ...

        zdc = (rb-ra)*(zd-zc)/(zb-za) # ATTENTION: ZERO DIVISION?!
        zca = (rb-ra)*(zc-za)/(zb-za) # ATTENTION: ZERO DIVISION?!

        a = cdc**2 + sdc**2 - zdc**2
        b = 2*(rc*cc*cdc + rc*sc*sdc - zdc*(ra+zca))
        c = rc**2 - (ra + zca)**2

        # Check if a solution exists (bac := b^2 - 4ac):
        bac = b**2 - 4*a*c
        if bac <= 0:
            # No intersection exists:
            return []

        L1 = (-b + np.sqrt(bac)) / (2*a)
        L2 = (-b - np.sqrt(bac)) / (2*a)

        for L in [L1,L2]:
            G = (zc - za + L*(zd-zc)) / (zb-za)
            if G < 0 or G > 1:
                # Intersection outside of grid cell:
                continue

            cT = (rc*cc + L*cdc) / (ra + G*(rb-ra))
            T  = np.arccos(cT) * 180.0/np.pi # --> in degrees

            # Rounding errors can lead to cT being slightly
            # bigger than one, which leads to T = nan:
            if np.isnan(T):
                if   cT >  0.99 and cT <  1.01:
                    T = 0.0
                elif cT < -0.99 and cT > -1.01:
                    T = 180.0
                else:
                    self.log.warning("Line of sight '" + self.name + "': "
                                     + "Theta = nan, could not determine "
                                     + "arccos(" + str(cT) + ")")

            rint = ra + G*(rb-ra)
            zint = za + G*(zb-za)
            pint = T

            intersections.append(self._Intersection(rint,zint,pint,L,[nx,ny]))
        return intersections

    def _get_point_along_line(self,l,line=None):
        """Returns the (r,z,phi) coordinates of the point on the line 'line'
        at the position l along the line.

        Parameters
        ----------
        l : float
            Position along the line, l==0 at the starting point
            of the line and l==1 at the endpoint of the line.

        line : numpy.ndarray of shape (2,3) (default: self.los)
            An 2x3 array defining the start and endpoint in
            (r,z,phi) coordinates of the line to be considered.
            If line==None self.los is used.
        """

        if line is None:
            line = self.los

        # The points defining the line:
        rc = line[0,0] # R
        zc = line[0,1] # Z
        pc = line[0,2] # Phi
        rd = line[1,0] # R
        zd = line[1,1] # Z
        pd = line[1,2] # Phi

        cc  = np.cos(pc*np.pi/180.0)
        sc  = np.sin(pc*np.pi/180.0)
        cd  = np.cos(pd*np.pi/180.0)
        sd  = np.sin(pd*np.pi/180.0)

        # Cartesian coordinates:
        x = rc*cc + l*(rd*cd-rc*cc)
        y = rc*sc + l*(rd*sd-rc*sc)
        z = zc    + l*(zd-zc)

        # Zylindrical coordinates:
        r = np.sqrt(x**2+y**2)
        z = z
        p = np.arctan2(x,y) * 180.0/np.pi # --> in degrees

        return np.array([r,z,p])

    def _check_var(self,var):
        if var.shape != self._run.grid.shape[:2]:
            self.log.error("Varbiable shape incompatible with grid shape: "
                            + "%s vs. %s",var.shape,self._run.grid.shape[:2])
            return False
            # TODO: Exception ...
        return True
