from IPython import embed
import numpy as np

a = 2.2296455161984999e+19 #fun_inside[13,0]
b = 2.2296455162264302e+19 #[13,0]

print b/a

print type(a)
print type(b)
print np.finfo(type(a))
print np.finfo(type(b))


sa0 =  8.0694335468649296e+31
sa1 = -3619155371577.1699

sb0 =  8.0694335468649296e+31
sb1 = -3619155371577.1699


tmp1 = sa0 + a*sa1
tmp2 = sb0 + b*sb1
print np.isclose(tmp1,tmp2)

tmp3 = sa0 + b*sa1
tmp4 = sb0 + a*sb1
print np.isclose(tmp3,tmp4)

tmp5 = sb0 + b*sa1
tmp6 = sa0 + a*sb1
print np.isclose(tmp5,tmp6)

tmp7 = a*sa1
tmp8 = b*sb1
print np.isclose(tmp7,tmp8)



embed()
