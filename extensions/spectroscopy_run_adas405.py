import sys
sys.path.append('/afs/ipp/u/mcavedon/repository/python')
sys.path.append('/afs/ipp/u/mcavedon/repository/python/adas')
# from Debug.Ipsh import *

def run_adas405(uid=None, year=None, defyear=89, filter=None, elem=None,
                files=None, partial=False, mh=1.0, mz=1.0,
                te=[0.0], dens=[0.0], unit_te='ev', all=False,
                tion=[0.0], denh=[0.0], cx=False, logfile=None):

    """

      PURPOSE    :  Runs the adas405 equilibrium balance code as a python function

      frac, power = run_adas405(elem=elem, uid=uid, year=year, dens=dens, te=te, all=True, ....)

                    NAME       TYPE     DETAILS
      REQUIRED   :  uid        str      username of adf11 location.
                                        'adas' for central ADAS data.
                    year       int      year of adf11 data.
                    defyear    int      default year of adf11 data
                                        (defaults to 89 if not set).
                    filter     str      filter (if appropriate).
                    elem       str      element
                    te         real()   temperatures requested
                    dens       real()   densities requested
                    tion       real()   ion temperatures requested
                                        (if /cx is used).
                    denh       real()   ion densities requested
                                        (if /cx is used).
                    mh         real     Hydrogen isotope mass
                                        (if /cx is used).
                    mz         real     Element isotope mass
                                        (if /cx is used).
                    log        str      name of output text file.
                                        (defaults to no output).
                    files      struc    Optional dictionary with replacement
                                        names for individula files.
                                               {'acd'  : '',  $
                                                'scd'  : '',  $
                                                'ccd'  : '',  $
                                                'prb'  : '',  $
                                                'prc'  : '',  $
                                                'qcd'  : '',  $
                                                'xcd'  : '',  $
                                                'plt'  : ''   }
                                            Not all file names are required - just
                                            those for replacement.

      OPTIONAL   :  all        bool   return 2D coeff(te, dens): default is false

      RETURNS    :  frac       dict   { 'stage' :   list of ion/metastable descriptors
                                        'ion'   :   Equilibrium abundances
                    power      dict   { 'stage' :   list of ion/metastable descriptors
                                        'ion'   :   Stage contribution to power
                                        'plt'   :   Line power
                                        'prb'   :   Continuum power (recombination + bremsstrahlung)
                                        'prc'   :   CX power
                                        'total' :   total line power

                    Dimensions are (nte, nstage) where nte is number of Te/dens pairs
                                   (ndens, nte, nstage) if all=True

      MODIFIED   :
           1.1     Martin O'Mullane
                     - First version

      VERSION    :
            1.1    20-08-2012

    """

#------------------------------------------------------------------------------

    def _run405_filenames(year, defyear, elem, uid, filter, partial, files):
        """
           Construct filenames for adas405
        """

        import os

        # construct filenames

        log_name  = os.getenv("LOGNAME")
        adas_user = os.getenv("ADASUSER")
        adas_cent = os.getenv("ADASCENT")

        dnames   = ['acd', 'scd', 'ccd', 'prb', 'prc', 'qcd', 'xcd', 'plt']
        dirnames = dnames

        if uid != 'adas':

           i1 = adas_user.find(log_name)
           i2 = len(log_name)
           p1 = adas_user[:i1]
           p2 = "".join(uid.split())
           p3 = adas_user[i1+i2:]

           fileroot = p1 + p2 + p3 + '/adf11/'

        else:

           fileroot = adas_cent + '/adf11/'


        if partial == True:
           defdirnames = [s + defyear + 'r' for s in dirnames]
           dirnames    = [s + year + 'r' for s in dirnames]
        else:
           defdirnames = [s + defyear for s in dirnames]
           dirnames    = [s + year for s in dirnames]

        if filter == None:
           filenames = [fileroot + s + '/' + s + '_' + elem + '.dat' for s in dirnames]
           defnames  = [fileroot + s + '/' + s + '_' + elem + '.dat' for s in defdirnames]


        # Overwrite user filenames which are missing with default ones

        fileavailable = [0] * 8
        defavailable  = [0] * 8

        for j in range(8):

           f = defnames[j]
           if (os.path.isfile(f) and os.access(f, os.R_OK)) == True:
              defavailable[j] = 1

           f = filenames[j]
           if (os.path.isfile(f) and os.access(f, os.R_OK)) == True:
              fileavailable[j] = 1
           else:
              fileavailable[j] = 0
              filenames[j] = defnames[j]


        # Replace with filenames from files list

        if files:
           for f in files.keys():
              if f in dnames:
                i = dnames.index(f)
                filenames[i] = files[f]
                fileavailable[i] = 1

        # Retrun

        return filenames, fileavailable, defavailable

#------------------------------------------------------------------------------



    def _run405_calc(fortran_code, today, yr, defyr, esym,
                     filenames, fileavailable, defavailable, indices, is_partial,
                     te_int, dens_int, tion_int, dion_int, mz, mh,
                     logfile):
        """
           run fortran adas405 (within limits of Te/dens dimensions)
        """

        import subprocess

        # Launch fortran and interact with it

        pipe = subprocess.Popen(fortran_code,
               shell=True, bufsize=-1,
               stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)

        # First (input) panel

        pipe.stdin.write('%s\n' % today)
        pipe.stdin.flush()

        rep = 'NO'
        pipe.stdin.write('%s\n' % rep)
        pipe.stdin.flush()

        pipe.stdin.write('%s\n' % yr)
        pipe.stdin.flush()
        pipe.stdin.write('%s\n' % defyr)
        pipe.stdin.flush()
        pipe.stdin.write('%s\n' % esym)
        pipe.stdin.flush()

        scriptname = 'NULL'
        pipe.stdin.write('%s\n' % scriptname)
        pipe.stdin.flush()

        pipe.stdin.write('%s\n' % flt)
        pipe.stdin.flush()

        for f in filenames:
           pipe.stdin.write('%s\n' % f)
           pipe.stdin.flush()
        for i in fileavailable:
           pipe.stdin.write('%d\n' % i)
           pipe.stdin.flush()
        for i in defavailable:
           pipe.stdin.write('%d\n' % i)
           pipe.stdin.flush()
        for i in indices:
           pipe.stdin.write('%d\n' % i)
           pipe.stdin.flush()

        pipe.stdin.write('%i\n' % is_partial)
        pipe.stdin.flush()

        line = pipe.stdout.readline()


        # Second (processing) panel

        line = pipe.stdout.readline()
        ntdmax = int(line)
        line = pipe.stdout.readline()
        nline = int(line)

        if nline > 0:
           ciion  = [''] * nline
           cmpts  = [''] * nline
           ctitle = [''] * nline
           for j in range(nline):
              line = pipe.stdout.readline()
              ciion[j] = line
              line = pipe.stdout.readline()
              cmpts[j] = line
              line = pipe.stdout.readline()
              ctitle[j] = line

        pipe.stdin.write('%d\n' % 0)
        pipe.stdin.flush()

        pipe.stdin.write('%s\n' % 'Test')
        pipe.stdin.flush()
        pipe.stdin.write('%d\n' % 0)
        pipe.stdin.flush()
        pipe.stdin.write('%d\n' % 2)         # Te in eV
        pipe.stdin.flush()
        pipe.stdin.write('%d\n' % te_int.size)
        pipe.stdin.flush()
        pipe.stdin.write('%f\n' % mz)
        pipe.stdin.flush()
        pipe.stdin.write('%f\n' % mh)
        pipe.stdin.flush()

        for j in range(te_int.size):
           pipe.stdin.write('%f\n' % te_int[j])
           pipe.stdin.flush()
           pipe.stdin.write('%f\n' % dens_int[j])
           pipe.stdin.flush()
           pipe.stdin.write('%f\n' % tion_int[j])
           pipe.stdin.flush()
           pipe.stdin.write('%f\n' % dion_int[j])
           pipe.stdin.flush()

        pipe.stdin.write('%d\n' % 0)
        pipe.stdin.flush()



        # Final (output) panel

        pipe.stdin.write('%d\n' % 0)
        pipe.stdin.flush()

        pipe.stdin.write('%d\n' % 1)   # we want fractional abundance
        pipe.stdin.flush()
        pipe.stdin.write('%d\n' % 1)
        pipe.stdin.flush()

        if logfile == None:
           pipe.stdin.write('%d\n' % 0)
           pipe.stdin.flush()
        else:
           pipe.stdin.write('%d\n' % 1)
           pipe.stdin.flush()
           pipe.stdin.write('%s\n' % logfile)
           pipe.stdin.flush()

        if goftfile == None:
           pipe.stdin.write('%d\n' % 0)
           pipe.stdin.flush()
        else:
           pipe.stdin.write('%d\n' % 1)
           pipe.stdin.flush()
           pipe.stdin.write('%s\n' % goftfile)
           pipe.stdin.flush()

        if logfile != None:
           pipe.stdin.write('%s\n' % 'Made by run_adas405 ' + today)
           pipe.stdin.flush()

        pipe.stdin.write('%d\n' % 0)
        pipe.stdin.flush()


        line = pipe.stdout.readline()
        itmax = int(line)
        xvals = zeros(itmax)

        for j in range(itmax):
           line = pipe.stdout.readline()
           xvals[j] = float(line)

        line = pipe.stdout.readline()
        nmsum = int(line)
        fvals = zeros((itmax, nmsum), float)
        poptit = [''] * nmsum

        for i in range(itmax):
           for j in range(nmsum):
              line = pipe.stdout.readline()
              fvals[i,j] = float(line)

        for j in range(nmsum):
           line = pipe.stdout.readline()
           poptit[j] = line

        line = pipe.stdout.readline()


        pipe.stdin.write('%d\n' % 0)     # and the power
        pipe.stdin.flush()
        pipe.stdin.write('%d\n' % 0)
        pipe.stdin.flush()
        pipe.stdin.write('%d\n' % 1)
        pipe.stdin.flush()
        pipe.stdin.write('%d\n' % 2)
        pipe.stdin.flush()

        if logfile == None:
           pipe.stdin.write('%d\n' % 0)
           pipe.stdin.flush()
        else:
           pipe.stdin.write('%d\n' % 1)
           pipe.stdin.flush()
           pipe.stdin.write('%s\n' % logfile)
           pipe.stdin.flush()

        pipe.stdin.write('%d\n' % 0)
        pipe.stdin.flush()

        if logfile != None:
           pipe.stdin.write('%s\n' % 'Made by run_adas405 ' + today)
           pipe.stdin.flush()

        pipe.stdin.write('%d\n' % 0)
        pipe.stdin.flush()

        line = pipe.stdout.readline()
        itmax = int(line)
        xvals = zeros(itmax)
        tvals = zeros((itmax,4), float)

        for j in range(itmax):
           line = pipe.stdout.readline()
           xvals[j] = float(line)

        line = pipe.stdout.readline()
        nmsum = int(line)
        pvals = zeros((itmax, nmsum), float)
        poptit = [''] * nmsum

        for i in range(itmax):
           for j in range(nmsum):
              line = pipe.stdout.readline()
              pvals[i,j] = float(line)

        for j in range(nmsum):
           line = pipe.stdout.readline()
           poptit[j] = line

        line = pipe.stdout.readline()

        for j in range(4):
           for i in range(itmax):
              line = pipe.stdout.readline()
              tvals[i,j] = float(line)


        pipe.stdin.write('%d\n' % 1)
        pipe.stdin.flush()


        # Close pipe and return fractional abundances and powers as dictionaries

        line = pipe.stdout.readline()
        if line == 1:
           pipe.stdin.close()

        return poptit, fvals, pvals, tvals

#------------------------------------------------------------------------------



    ### MAIN ###

    # External routines required

    import os
    from datetime import datetime
    from numpy import asarray, zeros
    from adaslib import i4eiz0, numlines

    # Initialisation and setup

    goftfile = None

    d     = datetime.utcnow()
    today = d.strftime("%d/%m/%y")

    fortran_code =  '/afs/ipp/u/mcavedon/adas/adas405/adas405.out'


    # Examine inputs

    if not elem: raise Exception("An element must be supplied")

    if files == None:

        if uid == None: raise Exception("A year is required")

        if year == None:
           year = defyear
           print "The default year will be used"

    if filter == None:
        flt = ''
    else:
        flt = str(filter)
    flt = "".join(flt.split())


    # check and conversions

    nz = i4eiz0(elem)
    if nz == -1: raise Exception("Invalid element")

    yr    = '%02i' % year
    defyr = '%02i' % defyear

    esym  = elem.lower()
    esym  = "".join(esym.split())

    te_int   = asarray(te, float)
    dens_int = asarray(dens, float)

    if not cx:
       tion_int = zeros(te_int.size)
       dion_int = zeros(dens_int.size)
    else:
       tion_int = asarray(tion, float)
       dion_int = asarray(denh, float)

    if unit_te.lower() == "kelvin":
       te_int = te_int / 11605.0

    if partial == False:
       indices = [1, 1, 0, 1, 0, 0, 0, 1]
       is_partial = 0
    else:
       indices = [1, 1, 0, 1, 0, 1, 1, 1]
       is_partial = 1


    filenames, fileavailable, defavailable = \
       _run405_filenames(yr, defyr, esym, uid, filter, partial, files)


    # Call fortran in chunks of 30 Te/dens pairs - fortran dimension constraint

    len_te   = te_int.size
    len_dens = dens_int.size

    if len_te == 1:
        te_int   = asarray([te], float)
        tion_int = asarray([tion_int], float)

    if len_dens == 1:
        dens_int = asarray([dens], float)
        dion_int = asarray([dion_int], float)

    if all == True:

       itval = len_te * len_dens

       te_int   = asarray([te_int] * len_dens).ravel()
       dens_int = asarray([dens_int] * len_te).transpose().ravel()
       tion_int = asarray([tion_int] * len_dens).ravel()
       dion_int = asarray([dion_int] * len_te).transpose().ravel()

    else:

       itval = min(len_dens, len_te)
       te_int   = te_int[0:itval]
       dens_int = dens_int[0:itval]
       tion_int = tion_int[0:itval]
       dion_int = dion_int[0:itval]


    MAXVAL = 30
    n_call = numlines(itval, MAXVAL)

    plt_coeff  = zeros(itval)
    prb_coeff  = zeros(itval)
    prc_coeff  = zeros(itval)
    ptot_coeff = zeros(itval)


    for j in range(n_call):

       ist = j*MAXVAL
       ifn = min((j+1)*MAXVAL, itval)

       te_ca   = te_int[ist:ifn]
       dens_ca = dens_int[ist:ifn]
       tion_ca = tion_int[ist:ifn]
       dion_ca = dion_int[ist:ifn]

       poptit, fvals, pvals, tvals = _run405_calc(fortran_code, today, yr, defyr, esym,
                                                  filenames, fileavailable, defavailable,
                                                  indices, is_partial,
                                                  te_ca, dens_ca, tion_ca, dion_ca,
                                                  mz, mh, logfile)
       if j == 0:
           nx = len(poptit)
           frac_coeff = zeros((itval, len(poptit)), float)
           pion_coeff = zeros((itval, len(poptit)), float)

       frac_coeff[ist:ifn,:] = fvals
       pion_coeff[ist:ifn,:] = pvals
       plt_coeff[ist:ifn]    = tvals[:,0]
       prb_coeff[ist:ifn]    = tvals[:,1]
       prc_coeff[ist:ifn]    = tvals[:,2]
       ptot_coeff[ist:ifn]   = tvals[:,3]

    # swith to 2D (dens,Te,stage) shape if all is true

    if all == True:
       frac_coeff = frac_coeff.reshape(len_dens, len_te, nz+1)
       pion_coeff = pion_coeff.reshape(len_dens, len_te, nz+1)
       plt_coeff  = plt_coeff.reshape(len_dens, len_te)
       prb_coeff  = prb_coeff.reshape(len_dens, len_te)
       prc_coeff  = prc_coeff.reshape(len_dens, len_te)
       ptot_coeff = ptot_coeff.reshape(len_dens, len_te)


    # prepare output and return

    frac = { 'stage' : poptit,
             'ion'   : frac_coeff }

    power = { 'stage' : poptit,
              'ion'   : pion_coeff,
              'plt'   : plt_coeff,
              'prb'   : prb_coeff,
              'prc'   : prc_coeff,
              'total' : ptot_coeff
            }

    return frac, power
