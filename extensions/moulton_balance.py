import os
import numpy as np
import scipy.io as io
from IPython import embed
import pdb
import itertools


# =============================================================================
import logging
balancelog = logging.getLogger('Balance')
if not balancelog.handlers:
    formatter = logging.Formatter(
            #'%(name)s(%(ident)s) -- %(levelname)s: %(message)s')
            '%(name)s -- %(levelname)s: %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    balancelog.addHandler(ch)
    balancelog.propagate = False


# =============================================================================
test = True  #To be deleted.
test = False #To be deleted.

# =============================================================================
# =============================================================================
class BaseBalance(object):
    """
    """
    def __init__(self, ncfile):
        """ Most "if test" should be removed afterwards.
        """

        #if test:
        #    self.comuse = io.loadmat(path+'/comuse.mat')['comuse'][0]
        try:
            self.comuse = io.loadmat(path+'/comuse.mat')['comuse'][0] #TEMPORARY
        except:
            pass



        self.log = balancelog
        self.ncfile = os.path.abspath(ncfile)
        self._load_data()
        self._correct_indices()
        self.isplot = 1 #FOR NOW

        self.apll = self.dv/self.hx*np.abs(self.B[...,0]/self.B[...,3])
        self.apllx = self.map_face(self.apll, 'left', method='volume')
        #if test:
        #    assert np.allclose(self.apll,
        #            #io.loadmat(os.path.join(path,'balmom.mat'))['apll'])
        #            io.loadmat(os.path.join(path,'balpart.mat'))['apll'])
        #    assert np.allclose(self.apllx,
        #            #io.loadmat(os.path.join(path,'balmom.mat'))['apllx'])
        #            io.loadmat(os.path.join(path,'balpart.mat'))['apllx'])

        self.dspol = np.zeros((self.nx, self.ny))
        #self.dspar = np.zeros((self.nx, self.ny)) ??
        for iy in xrange(self.ny):
            for ix in xrange(1,self.nx):
                self.dspol[ix,iy] = self.dspol[ix-1,iy]+ (self.hx[ix-1,iy] + self.hx[ix,iy])/2.0
                #dspar = dspol/pitch??
            pass
        #if test:
        #    assert np.allclose(self.dspol, self.comuse['dspol'][0])


        # Calculate c[rz] and c[rz]_[xy]
        self.cr = np.mean(self.r, 2)
        self.cz = np.mean(self.z, 2)

        # As copied from solpspy/classes/rundir.py
        self.cr_x = (self.r[1:,:,0]+self.r[1:,:,2])/2.0
        self.cr_y = (self.r[:,1:,0]+self.r[:,1:,1])/2.0
        self.cz_x = (self.z[1:,:,0]+self.z[1:,:,2])/2.0
        self.cz_y = (self.z[:,1:,0]+self.z[:,1:,1])/2.0
        if test:
            assert np.allclose(self.cr, self.comuse['cr'][0])
            assert np.allclose(self.cz, self.comuse['cz'][0])
            assert np.allclose(self.cr_x, self.comuse['cr_x'][0])
            assert np.allclose(self.cr_y, self.comuse['cr_y'][0])
            assert np.allclose(self.cz_x, self.comuse['cz_x'][0])
            assert np.allclose(self.cz_y, self.comuse['cz_y'][0])

        ##ATTENTION: Create this ds.
        #self.ds = ...

        # Better format for species names
        self.unspecies = self.unspecies.T
        self.species = []
        for spc in self.unspecies:
            tmp = ''
            for piece in spc:
                tmp += piece
            self.species.append(tmp.strip())


        return

    @property
    def latex(self):
        return {'nx': '$n_{x}$'}

    @property
    def variables(self):
        """ Variables of common use.
        """
        return {'nx':'nx_plus2', 'ny':'ny_plus2', 'nstra':'nstra', 'ns':'ns',
                'leftix':'leftix',    'leftiy':'leftiy',
                'rightix':'rightix',  'rightiy':'rightiy',
                'topix':'topix',      'topiy':'topiy',
                'bottomix':'bottomix','bottomiy':'bottomiy',
                'r':'crx', 'z':'cry',
                'dv':'vol', 'hx':'hx', 'B':'bb', 'za':'za',
                'sep':'jsep', 'omp':'jxa',
                'unspecies':'species'}


    @property
    def eirene_variables(self):
        """ Eirene variables of common use. They must be:
        {'given name': ('bal name', (default,shape)), etc}
        """
        return {}


    @property
    def ds(self):
        """(nx,ny) : Radial distance from the separatrix [m]"""
        try:
            return self._ds
        except:
            self._ds = np.zeros((self.nx,self.ny))
            proxy = np.zeros((self.nx,self.ny))
            for i in range(self.nx):
                proxy[i,0] = 0
                for k in range(1,self.ny):
                    proxy[i,k] = (proxy[i,k-1] +
                                  np.sqrt((self.cr[i,k] - self.cr[i,k-1])**2 +
                                          (self.cz[i,k] - self.cz[i,k-1])**2))
                ds_offset = (proxy[i,self.sep-1] + proxy[i,self.sep])/2
                self._ds[i,:] = proxy[i,:] - ds_offset
            return self._ds



    def _correct_indices(self):
        """ Change indices from Fortran to Python.
        """
        indices = ['leftix',    'leftiy',
                   'rightix',   'rightiy',
                   'topix',     'topiy',
                   'bottomix',  'bottomiy',
                   'sep', 'omp']
        for var in indices:
            tmp = getattr(self,var)
            setattr(self, var, tmp+1)

        self.sep += 1 # First cell in the SOL.

        return



    def _load_data(self):
        """ Same variables are called every single time, why not save time?
        Loads variables and eirene_variables.
        """
        with io.netcdf_file(self.ncfile) as ncf:
            for name, var  in self.variables.iteritems():
                try:
                    tmp = ncf.variables[var].data.T.copy()
                    if tmp.shape[0] == 1:
                        tmp = tmp[0]

                    if var.lower().startswith(('left','right','bottom','top')):
                        tmp = tmp.astype(np.int)

                    setattr(self, name, tmp)

                except KeyError:
                    try:
                        setattr(self, name, ncf.dimensions[var])
                    except:
                        self.log.exception(
                            "'{}' could not be read as '{}'.".format(name,var))

                except AttributeError:
                    try:
                        setattr(self, name, ncf.variables[var].data.copy())
                    except:
                        self.log.exception(
                            "'{}' could not be read as '{}'.".format(name,var))

            #Eirene variables
            use_eirene = ncf.variables['b2mndr_eirene'][0]
            for name, (var,defdims) in self.eirene_variables.iteritems():
                if  use_eirene != 0:
                    try:
                        setattr(self, name, ncf.variables[var].data.T.copy())

                    except KeyError:
                        try:
                            setattr(self, name, ncf.dimensions[var])
                        except:
                            self.log.exception(
                                "'{}' could not be read as '{}'.".format(name,var))

                    except AttributeError:
                        try:
                            setattr(self, name, ncf.variables[var].data.copy())
                        except:
                            self.log.exception(
                                "'{}' could not be read as '{}'.".format(name,var))
                else:
                    setattr(self, name, np.zeros(defdims))

        return




    def map_face(self, var, to, method='volume'):
        """ Map cell centered quantity to any of the four faces of the cell.
        """
        tmp = np.zeros_like(var)
        dix = getattr(self, to+'ix')
        diy = getattr(self, to+'iy')
        for ix in xrange(self.nx):
            for iy in xrange(self.ny):
                if   to.lower() == 'left'   and dix[ix,iy]<0:
                    continue
                elif   to.lower() == 'bottom' and diy[ix,iy]<0:
                    continue

                #Review the indexing of these. matlab is 1, python 0, mds -1
                elif to.lower() == 'right'  and dix[ix,iy]>self.nx:
                    continue
                elif to.lower() == 'top'    and diy[ix,iy]>self.ny:
                    continue

                varc = var[ix,iy]
                varn = var[dix[ix,iy],diy[ix,iy]]

                if method.lower() == 'volume':
                    dvc = self.dv[ix,iy]
                    dvn = self.dv[dix[ix,iy],diy[ix,iy]]
                    tmp[ix,iy] = (varc*dvn + varn*dvc)/(dvn+dvc)
                else:
                    tmp[ix,iy] = (varc + varn)/2.0

        return tmp





#    def div(self, flux):
#        """
#        In fact, this seems to be more like the integral of the full partial
#        derivative
#        integral 1/g d/dx (g/hx A) g dx = Aright - Aleft
#        """
#        div_flux = np.zeros_like(flux)
#
#        for iy in xrange(self.ny):
#            for ix in xrange(self.nx):
#
#                # Radial divergence
#                if self.topiy[ix,iy]>self.ny-1:
#                    pass
#                else:
#                    tix = self.topix[ix,iy]
#                    tiy = self.topiy[ix,iy]
#                    div_flux[ix,iy,1] = flux[ix,iy,1] - flux[tix, tiy,1]
#
#                # Poloidal divergence
#                if self.rightix[ix,iy]>self.nx-1:
#                    pass
#                else:
#                    rix = self.rightix[ix,iy]
#                    riy = self.rightiy[ix,iy]
#                    div_flux[ix,iy,0] = flux[ix,iy,0] - flux[rix,riy,0]
#                    div_flux[ix,iy,0] = flux[ix,iy,0] - flux[rix,riy,0]
#
#
#        return div_flux




    def div(self, flux):
        """
        In fact, this seems to be more like the integral of the full partial
        derivative
        integral 1/g d/dx (g/hx A) g dx = Aright - Aleft
        """
        div_flux = np.zeros_like(flux)

        for iy in xrange(self.ny):
            for ix in xrange(self.nx):

                # Radial divergence
                if self.topiy[ix,iy]>self.ny-1:
                    pass
                else:
                    tix = self.topix[ix,iy]
                    tiy = self.topiy[ix,iy]
                    div_flux[ix,iy,1] = flux[ix,iy,1] - flux[tix, tiy,1]

                # Poloidal divergence
                if self.rightix[ix,iy]>self.nx-1:
                    pass
                else:
                    rix = self.rightix[ix,iy]
                    riy = self.rightiy[ix,iy]
                    div_flux[ix,iy,0] = flux[ix,iy,0] - flux[rix,riy,0]
                    div_flux[ix,iy,0] = flux[ix,iy,0] - flux[rix,riy,0]


        return div_flux








#==============================================================================
#==============================================================================
#==============================================================================
class ParticleBalance(BaseBalance):
    """ Particle Balance class.
    """
    def __init__(self, ncfile, gmask=None, plotting=True):
        """ run is to be removed after testing.
        """
        super(ParticleBalance, self).__init__(ncfile)

        # Calculate radial divergence
        self.raddiv_pinch    = self.div(self.fnb_pinch)[...,1,:]
        self.raddiv_pll      = self.div(self.fnb_pll)[...,1,:]
        self.raddiv_drift    = self.div(self.fnb_drift)[...,1,:]
        self.raddiv_nanom    = self.div(self.fnb_nanom)[...,1,:]
        self.raddiv_panom    = self.div(self.fnb_panom)[...,1,:]
        self.raddiv_ch       = self.div(self.fnb_ch)[...,1,:]
        self.raddiv_pschused = self.div(self.fnb_pschused)[...,1,:]

        ## Alternative how to previous
        #varias = ['fnb_pinch', 'fnb_pll', 'fnb_drift', 'fnb_nanom',
        #          'fnb_panom', 'fnb_ch', 'fnb_pschused']
        #for var in varias:
        #    tmp = self.div(getattr(self,var))[...,1,:]
        #    name = var.split('_')[1]
        #    setattr(self, 'raddiv_'+name, tmp)


        if test:

            raddiv_pinch = io.loadmat(path+'/balpart.mat')['raddiv_pinch']
            raddiv_pll   = io.loadmat(path+'/balpart.mat')['raddiv_pll']
            raddiv_drift = io.loadmat(path+'/balpart.mat')['raddiv_drift']
            raddiv_nanom = io.loadmat(path+'/balpart.mat')['raddiv_nanom']
            raddiv_panom = io.loadmat(path+'/balpart.mat')['raddiv_panom']
            raddiv_ch    = io.loadmat(path+'/balpart.mat')['raddiv_ch']
            raddiv_pschused = io.loadmat(
                    path+'/balpart.mat')['raddiv_pschused']

            assert np.allclose(self.raddiv_pinch[...,1], raddiv_pinch)
            assert np.allclose(self.raddiv_pll[...,1], raddiv_pll)
            assert np.allclose(self.raddiv_drift[...,1], raddiv_drift)
            assert np.allclose(self.raddiv_nanom[...,1], raddiv_nanom)
            assert np.allclose(self.raddiv_panom[...,1], raddiv_panom)
            assert np.allclose(self.raddiv_ch[...,1], raddiv_ch)
            assert np.allclose(self.raddiv_pschused[...,1], raddiv_pschused)


        if plotting:
            radial_balance_plot(
                [self.fnb_pll[...,0,:],
                 self.fnb_drift[...,0,:],
                 self.fnb_nanom[...,0,:],
                 self.fnb_panom[...,0,:],
                 self.fnb_pinch[...,0,:],
                 self.fnb_ch[...,0,:],
                 self.fnb_pschused[...,0,:]
                ],

                [self.raddiv_drift,
                 self.raddiv_pll,
                 self.raddiv_nanom,
                 self.raddiv_panom,
                 self.raddiv_pinch,
                 self.raddiv_ch,
                 self.raddiv_pschused,
                 np.sum(self.eirene_mc_papl_sna,3),
                 np.sum(self.eirene_mc_pmpl_sna,3),
                 np.sum(self.eirene_mc_pipl_sna,3),
                 np.sum(self.eirene_mc_pppl_sna,3),
                 self.eirene_mc_core_sna, #In Moulton's
                 #np.sum(self.eirene_mc_core_sna, 3), #In my cases
                 self.b2stel_sna_ion,
                 self.b2stel_sna_rec,
                 self.b2stbc_sna,
                 self.b2stbm_sna,
                 self.b2stcx_sna,
                 self.ext_sna,
                 self.b2srdt_sna,
                 self.b2srsm_sna,
                 self.b2srst_sna,
                 self.b2stbr_phys_sna,
                 self.b2stbr_bas_sna,
                 self.b2stbr_first_flight_sna,
                ],

                self.rescb,

                [r'$dA_{xu}\Gamma_{xu}/dA_{||d}$',
                 r'$\Gamma_{||d}$',
                 r'$(\int_d^uS_{part}^{tot}dV)/dA_{||d}$',
                 r'$(\int_d^ures.dV)/dA_{||d}$'],

                ['poloidal projection of nv_{||}',
                 'dia.+ExB drifts',
                 'anomalous density diffusion',
                 'anomalous pressure diffusion',
                 'anomalous pinch',
                 'fnbx\_ch',
                 'PS'],

                ['rad. drift diverg.',
                 'rad. diverg. nv_{||}',
                 'rad. density diffusion diverg.',
                 'rad. pressure diffusion diverg.',
                 'rad. pinch diverg.',
                 'rad. diverg. fnby\_ch',
                 'rad. P-S diverg.',
                 'eirene\_mc atm.-plasma',
                 'eirene\_mc mol.-plasma',
                 'eirene\_mc t.ion-plasma',
                 'eirene\_mc recomb.',
                 'eirene\_mc core',
                 'b2stel ion',
                 'b2stel rec',
                 'b2stbc','b2stbm','b2stcx',
                 'source\_input',
                 'b2srdt','b2srsm','b2srst',
                 'b2stbr\_phys','b2stbr\_bas','b2stbr\_first\_flight'],

                self.comuse,

                gmask,

                self.apllx,

                False,

                True,

                [0,1,2,3],

                'm^{-2}s^{-1}',

                self,

                1,

                )


    @property
    def variables(self):
        tmp = { # Fluxes
                'fnb_pinch':'fna_pinch',
                'fnb_pll':'fna_pll',
                'fnb_drift':'fna_drift',
                'fnb_ch':'fna_ch',
                'fnb_nanom':'fna_nanom',
                'fnb_panom':'fna_panom',
                'fnb_pschused':'fna_pschused',

                # Sources
                'b2stbr_phys_sna':'b2stbr_phys_sna_bal',
                'b2stbr_bas_sna':'b2stbr_bas_sna_bal',
                'b2stbr_first_flight_sna':'b2stbr_first_flight_sna_bal',
                'b2stbc_sna':'b2stbc_sna_bal',

                'b2stbm_sna':'b2stbm_sna_bal',
                'ext_sna':'ext_sna_bal',
                'b2stel_sna_ion':'b2stel_sna_ion_bal',
                'b2stel_sna_rec':'b2stel_sna_rec_bal',
                'b2stcx_sna':'b2stcx_sna_bal',
                'b2srsm_sna':'b2srsm_sna_bal',
                'b2srdt_sna':'b2srdt_sna_bal',
                'b2srst_sna':'b2srst_sna_bal',
                # Residual
                'rescb':'resco',

                }

        dummy = super(ParticleBalance,self).variables.copy()
        dummy.update(tmp)
        return dummy

    @property
    def eirene_variables(self):
        nx,ny,nstra = self.nx, self.ny, self.nstra
        tmp = {# Sources
               'eirene_mc_papl_sna':('eirene_mc_papl_sna_bal', (nx,ny,1,nstra)),
               'eirene_mc_pmpl_sna':('eirene_mc_pmpl_sna_bal', (nx,ny,1,nstra)),
               'eirene_mc_pipl_sna':('eirene_mc_pipl_sna_bal', (nx,ny,1,nstra)),
               'eirene_mc_pppl_sna':('eirene_mc_pppl_sna_bal', (nx,ny,1,nstra)),
               'eirene_mc_core_sna':('eirene_mc_core_sna_bal', (nx,ny,1,nstra))}

        dummy = super(ParticleBalance,self).eirene_variables.copy()
        dummy.update(tmp)
        return dummy















# =============================================================================
# =============================================================================
# =============================================================================
class MomentumBalance(BaseBalance):
    """ Momentum balance class. Will be latter abstracted for other classes.
        David did it this way.
        resmo = np.sum(self.resmo[...,self.isplot],2)
        fmox_flua = np.sum(self.fmo_flua[...,0,self.isplot],3)
        fmoy_flua = np.sum(self.fmo_flua[...,1,self.isplot],3)
        I think that I will keep the ns saved, so that it can be decided at
        in calling __call__, not init.
    """
    def __init__(self, ncfile, gmask=None, plotting=True):
        """ run is to be removed after testing.
        """
        super(MomentumBalance, self).__init__(ncfile)

        # Calculate divergences

        self.raddiv_flua = self.div(self.fmo_flua)[...,1,:]
        tmp = self.div(self.fmo_cvsa + self.fmo_b2nxfv)
        self.raddiv_vis = tmp[...,1,:]
        self.visc = tmp[...,0,:]
        if test:
            raddiv_flua = io.loadmat(path+'/raddiv_flua.mat')['raddiv_flua']
            raddiv_vis = io.loadmat(path+'/raddiv_vis.mat')['raddiv_vis']
            visc = io.loadmat(path+'/visc.mat')['visc']
            assert np.allclose(self.raddiv_flua[...,1], raddiv_flua)
            assert np.allclose(self.raddiv_vis[...,1], raddiv_vis)
            assert np.allclose(self.visc[...,1], visc)

        self.b2sifr_smo = self.b2sifr_smoch + self.b2sifr_smotf


        if plotting:
            radial_balance_plot(
                self.fmo_flua[...,0,:],

                [self.visc, self.raddiv_flua, self.raddiv_vis, self.b2stbc_smo,
                 np.sum(self.eirene_mc_mapl_smo,3),
                 np.sum(self.eirene_mc_mmpl_smo,3),
                 np.sum(self.eirene_mc_mipl_smo,3),
                 np.sum(self.eirene_mc_cppv_smo,3),
                 self.b2stbm_smo, self.ext_smo,
                 self.b2stel_smq_ion, self.b2stel_smq_rec,
                 self.b2stcx_smq, self.b2srsm_smo,
                 self.b2srdt_smo, self.b2srst_smo, self.b2sifr_smo,
                 self.b2siav_smovh, self.b2siav_smovv,
                 self.b2sicf_smo, self.b2sian_smo, self.b2nxdv_smo,
                 self.b2sigp_smogp, self.b2stbr_phys_smo,
                 self.b2stbr_bas_smo],

                self.resmo,

                ##Poloidal balance apparently
                #['dA_xnmu_{||}u_x/dA_{||d}',
                # 'S_{mom}^{tot}dV/dA_{||d}',
                # 'res.dV/dA_{||d}'],

                [r'$dA_{xu}n_umu_{||u}u_{xu}/dA_{||d}$',
                 r'$n_dmu_{||d}^2$',
                 r'$(\int_d^uS_{mom}^{tot}dV)/dA_{||d}$',
                 r'$(\int_d^ures.dV)/dA_{||d}$'],


                ['nmu_{||}^2'],

                ['parallel viscosity','rad. diverg. nmu_{||}u_y',
                 'rad. diverg. visc.','b2stbc','eirene\_mc atm.-plasma',
                 'eirene\_mc mol.-plasma','eirene\_mc t.ion-plasma',
                 'eirene\_mc recomb.','b2stbm','source\_input','b2stel\_ion',
                 'b2stel\_rec','b2stcx','b2srsm','b2srdt','b2srst','b2sifr',
                 'b2siav\_smovh','b2siav\_smovv','b2sicf','b2sian','b2nxdv\_smo',
                 'stat. press. gradient','b2stbr\_phys','b2stbr\_bas'],

                self.comuse,

                gmask,

                self.apllx,

                False,

                True,

                [0,1,2,3],

                'Nm^{-2}',

                self,

                1,

                )



    @property
    def variables(self):
        tmp = { # Fluxes
                'fmo_flua':'fmo_flua',
                'fmo_cvsa':'fmo_cvsa',
                'fmo_b2nxfv':'fmo_b2nxfv',

                # Sources
                'b2stbr_phys_smo':'b2stbr_phys_smo_bal',
                'b2stbr_bas_smo':'b2stbr_bas_smo_bal',
                'b2stbc_smo':'b2stbc_smo_bal',

                'b2stbm_smo':'b2stbm_smo_bal',
                'ext_smo':'ext_smo_bal',
                'b2stel_smq_ion':'b2stel_smq_ion_bal',
                'b2stel_smq_rec':'b2stel_smq_rec_bal',
                'b2stcx_smq':'b2stcx_smq_bal',
                'b2srsm_smo':'b2srsm_smo_bal',
                'b2srdt_smo':'b2srdt_smo_bal',
                'b2srst_smo':'b2srst_smo_bal',
                'b2sifr_smoch':'b2sifr_smoch_bal',
                'b2sifr_smotf':'b2sifr_smotf_bal',
                'b2siav_smovh':'b2siav_smovh_bal',
                'b2siav_smovv':'b2siav_smovv_bal',
                'b2sicf_smo':'b2sicf_smo_bal',
                'b2sian_smo':'b2sian_smo_bal',
                'b2nxdv_smo':'b2nxdv_smo_bal',
                'b2sigp_smogp':'b2sigp_smogp_bal',

                # Residual
                'resmo':'resmo'}

        dummy = super(MomentumBalance,self).variables.copy()
        dummy.update(tmp)
        return dummy

    @property
    def eirene_variables(self):
        nx,ny,nstra = self.nx, self.ny, self.nstra
        tmp = {# Sources
               'eirene_mc_mapl_smo':('eirene_mc_mapl_smo_bal', (nx,ny,1,nstra)),
               'eirene_mc_mmpl_smo':('eirene_mc_mmpl_smo_bal', (nx,ny,1,nstra)),
               'eirene_mc_mipl_smo':('eirene_mc_mipl_smo_bal', (nx,ny,1,nstra)),
               'eirene_mc_cppv_smo':('eirene_mc_cppv_smo_bal', (nx,ny,1,nstra))}

        dummy = super(MomentumBalance,self).eirene_variables.copy()
        dummy.update(tmp)
        return dummy














#==============================================================================
#==============================================================================
#==============================================================================
class HeatBalance(BaseBalance):
    """ Heat Balance class.
    btype can be 'total', 'electron' or 'ion'.
    Based on baltoten.
    """
    def __init__(self, ncfile, gmask=None, plotting=True, btype='total'):
        super(HeatBalance, self).__init__(ncfile)

        test = False
        if test:
            matf = io.loadmat(path+'/baltotht.mat')

        # Calculate radial divergence
        #import pdb; pdb.set_trace()
        fht = []
        for var in self.variables:
            if var.startswith('fh'):
                tmp = self.div(getattr(self,var))[:,:,1]
                name = 'raddiv'+var[2:]
                setattr(self, name, tmp)
                fht.append(tmp)

                if test:
                    if tmp.ndim == 4:
                        assert np.allclose(tmp[...,1], matf[name])
                    else:
                        assert np.allclose(tmp, matf[name])
        fht = np.sum(np.array(fht),0)
        #embed()









    @property
    def variables(self):
        tmp = { # Fluxes
                'fhe_32': 'fhe_32',
                'fhe_52': 'fhe_52',
                'fhe_thermj': 'fhe_thermj',
                'fhe_cond': 'fhe_cond',
                'fhe_dia': 'fhe_dia',
                'fhe_ecrb': 'fhe_ecrb',
                'fhe_strange': 'fhe_strange',
                'fhe_pschused': 'fhe_pschused',

                'fhi_32': 'fhi_32',
                'fhi_52': 'fhi_52',
                'fhi_cond': 'fhi_cond',
                'fhi_dia': 'fhi_dia',
                'fhi_ecrb': 'fhi_ecrb',
                'fhi_strange': 'fhi_strange',
                'fhi_pschused': 'fhi_pschused',
                'fhi_inert': 'fhi_inert',
                'fhi_vispar': 'fhi_vispar',
                'fhi_anml': 'fhi_anml',
                'fhi_kevis': 'fhi_kevis',

                # Sources

                # Residual

                }

        dummy = super(HeatBalance,self).variables.copy()
        dummy.update(tmp)
        return dummy

    @property
    def eirene_variables(self):
        nx,ny,nstra = self.nx, self.ny, self.nstra
        tmp = {# Sources
               'eirene_mc_papl_sna':('eirene_mc_papl_sna_bal', (nx,ny,1,nstra)),
              }

        dummy = super(HeatBalance,self).eirene_variables.copy()
        dummy.update(tmp)
        return dummy







class EnergyBalance(HeatBalance):
    """ Heat Balance class.
    btype can be 'total', 'electron' or 'ion'.
    Based on baltoten.
    """
    def __init__(self, ncfile, gmask=None, plotting=True, btype='total'):
        super(EnergyBalance, self).__init__(ncfile)

        self.fhe_32[:,:,0] += self.fne[:,:,0]*self.te
        fna = []
        for var in self.variables:
            if var.startswith('fnb_'):
                fna.append(getattr(self,var))
        self.fna = np.sum(np.array(fna),0)


        self.fhi_ke = np.zeros((self.nx, self.ny))
        for ns in xrange(self.ns):
            self.fhi_32[:,:,0] += self.fna[:,:,0, ns]*self.ti
            self.fhi_ke += self.fna[:,:,0,ns]*self.kinrgy[:,:,0]




        embed()





    @property
    def variables(self):
        tmp = { # Other
                'te':'te',
                'ti':'ti',
                'kinrgy':'kinrgy',

                # Fluxes
                'fne':'fne',
                'fnb_pinch':'fna_pinch',
                'fnb_pll':'fna_pll',
                'fnb_drift':'fna_drift',
                'fnb_ch':'fna_ch',
                'fnb_nanom':'fna_nanom',
                'fnb_panom':'fna_panom',
                'fnb_pschused':'fna_pschused',

                # Sources

                # Residual

                }

        dummy = super(EnergyBalance,self).variables.copy()
        dummy.update(tmp)
        return dummy

    @property
    def eirene_variables(self):
        nx,ny,nstra = self.nx, self.ny, self.nstra
        tmp = {# Sources
               'eirene_mc_papl_sna':('eirene_mc_papl_sna_bal', (nx,ny,1,nstra)),
              }

        dummy = super(EnergyBalance,self).eirene_variables.copy()
        dummy.update(tmp)
        return dummy
































#ATTENTION: Change name to poloidal_integration or something like it??
def integral(var,gridmask, dimension='x'):
    """
    Selection method for the region should be pretty similar to that
    of the average function.
    Change creation of mask as to allow creation of sums with
    more than one dimensions (nx,ns), (ny,ns), etc.
    Add an automatic system to detect in which side of the DDNU
    or if in USN, etc, to get the reverse way of increasing X
    """
    nx = var.shape[0]
    ny = var.shape[1]
    if gridmask is None:
        gridmask = np.ones((nx, ny), dtype=np.bool)
    gmask = gridmask.astype(np.int)
    if dimension == 'x':
        tmp = np.full(var.shape[1:], np.nan)
        msk = np.any(gridmask,0)
        for iy in xrange(ny):
            if msk[iy] == False:
                continue
            tmp[iy] = (np.sum((gmask[:,iy].T*var[:,iy].T).T,0))
        return tmp

    elif dimension == 'y':
        pass

    else:
        return









# =============================================================================
# =============================================================================
# =============================================================================
##ATTENTION: itop and ibot are NOT working.
def get_gmask_limits(gmask):
    """ It requires an (nx,ny) gmask.
    """
    nx, ny = gmask.shape

    ileft  = np.full(ny, np.nan, dtype=np.int)
    iright = np.full(ny, np.nan, dtype=np.int)
    itop   = np.full(nx, np.nan, dtype=np.int)
    ibot   = np.full(nx, np.nan, dtype=np.int)

    for iy in xrange(ny):
        tmp = np.where(gmask[:,iy])[0]
        if tmp != []:
            ileft[iy] =  int(tmp[0])
            iright[iy] = int(tmp[-1])

    for ix in xrange(nx):
        tmp = np.where(gmask[ix])[0]
        if tmp != []:
            ibot[iy] = int(tmp[0])
            itop[iy] = int(tmp[-1])

    return ileft, iright, itop, ibot


def get_fvar_limits(var, gmask):
    """ Face variables require irigth+1, to get the left face quantity.
    It requires an (nx,ny) gmask.
    """
    ileft, iright, itop, ibot = get_gmask_limits(gmask)
#    ileft = ileft.astype(np.int)
#    iright = iright.astype(np.int)
#    itop = itop.astype(np.int)
#    ibot = ibot.astype(np.int)

    nx, ny = var.shape[0], var.shape[1]
    vleft  = np.full(var.shape[1:], np.nan)
    vright = np.full(var.shape[1:], np.nan)
    ydims = list(itertools.chain.from_iterable(
               [[var.shape[0]], var.shape[2:]]))
    vbot = np.full(ydims, np.nan)
    vtop = np.full(ydims, np.nan)

    for iy in xrange(ny):
        #if not np.isnan(ileft[iy]):
        if ileft[iy] > -1:
            vleft[iy] =  var[int(ileft[iy]) ,iy]
        #if not np.isnan(iright[iy]):
        if iright[iy] > -1:
            if iright[iy]+1 == ny:
                vright[iy] = var[iright[iy],iy]
            else:
                vright[iy] = var[iright[iy]+1,iy]

    for ix in xrange(nx):
        #if not np.isnan(ibot[ix]):
        if ibot[ix] > -1:
            vbot[ix] = var[ix, ibot[ix]]
        #if not np.isnan(itop[ix]):
        if itop[ix] > -1:
            if itop[ix]+1 == nx:
                vtop[ix] = var[ix, itop[ix]]
            else:
                vtop[ix] = var[ix, itop[ix]+1]

    return vleft, vright, vtop, vbot



#After initialization
def radial_balance_plot(flux, src, res, totname, fluxname, srcname,
                        comuse, gmask, area, reverse, ismom, axbal, unitstr,
                        mother, indspec):
    ##For now. Should it be change forever??
    #gmask[:,0]   = False
    #gmask[:,-1]  = False

    #embed()



    if not reverse:
        reversefac=1
        momfac=1
    elif ismom:
        reversefac = -1
        momfac = -1
    else:
        reversefac = -1
        momfac = 1



    #if test:
    #    #matflux = io.loadmat(path+'/mombal_flux.mat')['flux']
    #    matflux = io.loadmat(path+'/radbal_balmom_i.mat')['flux']
    #    assert np.allclose(flux[...,indspec], matflux)

    #    #matsrc = io.loadmat(path+'/mombal_src.mat')['src']
    #    matsrc = io.loadmat(path+'/radbal_balmom_i.mat')['src']
    #    for i,tmp in enumerate(src):
    #        try:
    #            if tmp.ndim == 3:
    #                assert np.allclose(tmp[...,indspec], matsrc[...,i])
    #            elif tmp.ndim == 2:
    #                assert np.allclose(tmp, matsrc[...,i])
    #            else:
    #                pdb.set_trace()
    #        except:
    #            pdb.set_trace()

    #    #matres = io.loadmat(path+'/mombal_res.mat')['res']
    #    matres = io.loadmat(path+'/radbal_balmom_i.mat')['res']
    #    assert np.allclose(res[...,indspec], matres)

    #    matindrad = io.loadmat(path+'/radbal_balmom_i.mat')['indrad']
    #    assert np.allclose(matindrad, gmask)

    #    ##They were correct until I put the $ $ signs.
    #    #matfluxname = io.loadmat(
    #    #        #path+'/mombal_fluxname.mat')['fluxname'][0][0]
    #    #        path+'/radbal_balmom_i.mat')['fluxname'][0][0]
    #    #assert fluxname == matfluxname

    #    #mattotname = io.loadmat(
    #    #        #path+'/mombal_totname.mat')['totname'][0]
    #    #        path+'/radbal_balmom_i.mat')['totname'][0]
    #    #for i,name in enumerate(mattotname):
    #    #    assert totname[i] == mattotname[i][0]

    #    #matsrcname = io.loadmat(
    #    #        #path+'/mombal_srcname.mat')['srcname'][0]
    #    #        path+'/radbal_balmom_i.mat')['srcname'][0]
    #    #for i,name in enumerate(matsrcname):
    #    #    assert srcname[i] == matsrcname[i][0]

    if test:
        matflux = io.loadmat(path+'/radbal_balpar_i.mat')['flux']
        for i,flx in enumerate(flux):
            assert np.allclose(flx[...,indspec], matflux[...,i])

        matsrc = io.loadmat(path+'/radbal_balpar_i.mat')['src']
        for i,tmp in enumerate(src):
            try:
                if i==11: #For now it is required.
                    continue
                elif tmp.ndim == 3:
                    assert np.allclose(tmp[...,indspec], matsrc[...,i])
                elif tmp.ndim == 2:
                    assert np.allclose(tmp, matsrc[...,i])
                else:
                    pdb.set_trace()
            except:
                pdb.set_trace()

        matres = io.loadmat(path+'/radbal_balpar_i.mat')['res']
        assert np.allclose(res[...,indspec], matres)

        matindrad = io.loadmat(path+'/radbal_balpar_i.mat')['indrad']
        assert np.allclose(matindrad, gmask)

        ##
        #matfluxname = io.loadmat(
        #        path+'/radbal_balpar_i.mat')['fluxname'][0]
        #for i,flxn in enumerate(fluxname):
        #    assert flxn == matfluxname[i]

        #mattotname = io.loadmat(
        #        path+'/radbal_balpar_i.mat')['totname'][0]
        #for i,name in enumerate(mattotname):
        #    try: assert totname[i] == mattotname[i][0]
        #    except: pdb.set_trace()

        #matsrcname = io.loadmat(
        #        path+'/radbal_balpar_i.mat')['srcname'][0]
        #for i,name in enumerate(matsrcname):
        #    assert srcname[i] == matsrcname[i][0]


    srcint = []
    for source in src:
        srcint.append(integral(source,gmask,'x'))

    resint = integral(res,gmask,'x')


    poli = np.where(gmask)[0]
    lefti  = poli[:gmask.shape[1]]
    righti = poli[-gmask.shape[1]:]
    ileft, iright, _,_ = get_gmask_limits(gmask)


    #fluxleft, fluxright, _,_ = get_fvar_limits(flux, gmask)
    fluxleft =[]
    fluxright=[]
    for flx in flux:
        tmp =get_fvar_limits(flx, gmask)
        fluxleft.append(tmp[0]), fluxright.append(tmp[1])
    ## Alternative definition: flux = np.array([flx.T for flx in flux]).T

    arealeft, arearight, _,_ = get_fvar_limits(area, gmask)


    # Momentum balance
    #if test:
    #    #matsrcint = io.loadmat(path+'/srcint.mat')['srcint']
    #    matsrcint = io.loadmat(path+'/radbal_balmom_i.mat')['srcint']
    #    for i,srci in enumerate(srcint):
    #        assert np.allclose(matsrcint[...,i], srci[1:-1,1])

    #    #matresint = io.loadmat(path+'/resint.mat')['resint'][0]
    #    matresint = io.loadmat(path+'/radbal_balmom_i.mat')['resint'][0]
    #    assert np.allclose(matresint, resint[1:-1,1])


    #if test:
    #    matfluxleft = io.loadmat(path+'/radbal_mom_lr.mat')['fluxleft'][:,0]
    #    assert np.allclose(matfluxleft, fluxleft[1:-1,1])

    #    # It has to be right +1 to get the left faced flux of the correct cell
    #    matfluxright = io.loadmat(path+'/radbal_mom_lr.mat')['fluxright'][:,0]
    #    assert np.allclose(matfluxright, fluxright[1:-1,1])
    #    assert np.allclose(matfluxright, flux[-1, 1:-1,1])

    #    matarealeft = io.loadmat(path+'/radbal_mom_lr.mat')['arealeft'][0]
    #    assert np.allclose(matarealeft, arealeft[1:-1])

    #    matarearight= io.loadmat(path+'/radbal_mom_lr.mat')['arearight'][0]
    #    assert np.allclose(matarearight, arearight[1:-1])

    # Particle balance
    if test:
        matsrcint = io.loadmat(path+'/radbal_balpar_i.mat')['srcint']
        for i,srci in enumerate(srcint):
            if i ==11: continue
            assert np.allclose(matsrcint[...,i], srci[1:-1,1])

        matresint = io.loadmat(path+'/radbal_balpar_i.mat')['resint'][0]
        assert np.allclose(matresint, resint[1:-1,1])


    if test:
        matfluxleft = io.loadmat(path+'/radbal_par_lr.mat')['fluxleft']
        for i,flxl in enumerate(fluxleft):
            assert np.allclose(matfluxleft[...,i], flxl[1:-1,1])

        matfluxright= io.loadmat(path+'/radbal_par_lr.mat')['fluxright']
        #embed()
        for i,flxr in enumerate(fluxright):
            assert np.allclose(matfluxright[...,i], flxr[1:-1,1])


        #matarealeft = io.loadmat(path+'/radbal_par_lr.mat')['arealeft'][0]
        #assert np.allclose(matarealeft, arealeft[1:-1])

        #matarearight= io.loadmat(path+'/radbal_par_lr.mat')['arearight'][0]
        #assert np.allclose(matarearight, arearight[1:-1])






    ## PLOTS

    xmxsep = mother.ds[mother.omp]*100 #dS omp in cm.

    if not reverse:
        fluxup = fluxleft
        fluxdown = fluxright
        areadown = arearight
    else:
        fluxup = fluxright
        fluxdown = fluxleft
        areadown = arealeft



    ## Momentum balance
    #if test:
    #    matfluxup = io.loadmat(path+'/radbal_mom_lr.mat')['fluxup'][:,0]
    #    assert np.allclose(matfluxup, fluxup[1:-1, 1])

    #    matfluxdown = io.loadmat(path+'/radbal_mom_lr.mat')['fluxdown'][:,0]
    #    assert np.allclose(matfluxdown, fluxdown[1:-1,1])

    #    matareadown = io.loadmat(path+'/radbal_mom_lr.mat')['areadown'][0]
    #    assert np.allclose(matareadown, areadown[1:-1])


    ## Particle balance
    if test:
        matfluxup = io.loadmat(path+'/radbal_par_lr.mat')['fluxup']
        for i,flxl in enumerate(fluxup):
            assert np.allclose(matfluxup[...,i], flxl[1:-1,1])

        matfluxdown = io.loadmat(path+'/radbal_par_lr.mat')['fluxdown']
        for i,flxl in enumerate(fluxdown):
            assert np.allclose(matfluxdown[...,i], flxl[1:-1,1])

        matareadown = io.loadmat(path+'/radbal_par_lr.mat')['areadown'][0]
        assert np.allclose(matareadown, areadown[1:-1])





    ## TOTAL
    fig, ax = plt.subplots(1, figsize=(16,6))
    if isinstance(fluxup, list):
        ax.plot(xmxsep, momfac*reversefac*np.nansum(fluxup,0)[...,1]/areadown,
                    label=totname[0])
    else:
        ax.plot(xmxsep, momfac*reversefac*fluxup[...,1]/areadown,
                label=totname[0])

    if isinstance(fluxdown, list):
        ax.plot(xmxsep, momfac*reversefac*np.nansum(fluxdown,0)[...,1]/areadown,
                    label=totname[1])
    else:
        ax.plot(xmxsep, momfac*reversefac*fluxdown[...,1]/areadown,
                label=totname[1])

    if isinstance(srcint, list):
        ax.plot(xmxsep, momfac*np.nansum(srcint,0)[...,1]/areadown,
                    label=totname[2])
    else:
        ax.plot(xmxsep, momfac*srcint[...,1]/areadown,
                label=totname[2])


    #ax.plot(xmxsep, momfac*resint[...,1]/areadown, label=totname[3])
    #tmp = momfac*(reversefac*(fluxup-fluxdown+np.nansum(srcint,0))[...,1])
    #ax.plot(xmxsep, tmp, label='post. calc.')

    ax.axhline(0.0, color='k')
    ax.axvline(0.0, color='k', ls= '--')
    ax.legend(loc='best')
    plt.draw()



    ## Source decomposition
    fig, ax = plt.subplots(1, figsize=(16,6))
    for ssrc, txt in zip(srcint, srcname):
        if (ssrc[...,1] == 0).all():
            continue
        ax.plot(xmxsep, momfac*ssrc[...,1]/areadown, label=txt)

    ax.axhline(0.0, color='k')
    ax.axvline(0.0, color='k', ls= '--')
    ax.legend(loc='best')
    plt.draw()



#    ## Source decomposition
#    srcintnew = srcint[:4]
#    srcnamenew = srcname[:4]
#
#    srcintnew.append(np.nansum(srcint[4:8],0))
#    srcnamenew.append('Eirene')
#
#    fig, ax = plt.subplots(1, figsize =(16,6))
#    for ssrc, txt in zip(srcintnew, srcnamenew):
#        ax.plot(xmxsep, momfac*ssrc[...,1]/areadown, label=txt)
#
#    ax.axhline(0.0, color='k')
#    ax.axvline(0.0, color='k', ls= '--')
#    ax.legend(loc='best')
#    ax.set_xlim([-1,1])
#    plt.draw()
#
#
#
#    ## Source decomposition. Ions summed (mostly zeros except D+)
#    fig, ax = plt.subplots(1, figsize =(16,6))
#    for ssrc, txt in zip(srcinteir, srcnameeir):
#        ax.plot(xmxsep, momfac*np.nansum(ssrc,1)/areadown, label=txt)
#
#    ax.axhline(0.0, color='k')
#    ax.axvline(0.0, color='k', ls= '--')
#    ax.legend(loc='best')
#    ax.set_xlim([-1,1])
#    plt.draw()











    plt.show()
    embed()












    return








if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from solpspy import *

    my_v=True
    moulton_v =False


    if my_v:
        #path = '/ptmp1/scratch/ipar/solps-iter/runs/aug_ddnu_34308_b2standalone/to_test_balance/nesepm=0.70_tecore=550eV_ticore=450eV_coreflux=5.00e19'

        #path = '/ptmp1/scratch/ipar/solps-iter/runs/aug_ddnu_nepow_feedback_coreflux=1.00e20/alt_Bt=+2.5T_kaveeva=1e-2_ip=1.0MA_BCCON=22_BCENI=1_BCENE=1/test_nesepm=1.0_tecore=550eV_ticore=450eV_bsigp_style=1'


        #path = '/ptmp1/scratch/ipar/solps-iter/runs/aug_ddnu_nepow_feedback_coreflux=1.00e20/alt_Bt=+2.5T_kaveeva=1e-2_ip=1.0MA_BCCON=22_BCENI=1_BCENE=1/fhitz/SI_33988m_5MW.D=3e22.Ar=1e19.N=2e19.test_20190313_b2sigp_style=1'
        #Move to a new folder just like 'test balance' or something??


        ####------------------------------------------------------ NEW ONES ---
        path = '/toks/scratch/ipar/solps-iter/runs/aug_ddnu_nepow_feedback_coreflux=1.00e20/alt_Bt=+2.5T_kaveeva=1e-2_ip=1.0MA_BCCON=22_BCENI=1_BCENE=1/test_nesepm=1.0_tecore=550eV_ticore=450eV_bsigp_style=1'

#        path = '/toks/scratch/ipar/solps-iter/runs/aug_ddnu_nepow_feedback_coreflux=1.00e20/alt_Bt=+2.5T_kaveeva=1e-2_ip=1.0MA_BCCON=22_BCENI=1_BCENE=1/fhitz/SI_33988m_5MW.D=3e22.Ar=1e19.N=2e19.test_20190313_b2sigp_style=1'

        path = '/toks/scratch/ipar/solps-iter/runs/aug_ddnu_34308_b2standalone/to_test_balance/nesepm=0.70_tecore=550eV_ticore=450eV_coreflux=5.00e19_newermatlab'

        run = SolpsData(path)


        #print "Momentum balance"
        #mombal = MomentumBalance(os.path.join(path,'balance.nc'),
        #        gmask = run.masks.outdiv)


        #print "Particle balance"
        #parbal = ParticleBalance(os.path.join(path,'balance.nc'),
        #        gmask = run.masks.outdiv)
        #        #gmask = run.masks.inndiv)

        #print "Energy balance"
        #parbal = EnergyBalance(os.path.join(path,'balance.nc'),
        #        gmask = run.masks.outdiv)
        #        #gmask = run.masks.inndiv)


        #print "Heat balance"
        #parbal = HeatBalance(os.path.join(path,'balance.nc'),
        #        gmask = run.masks.outdiv, plotting=False)

        print "Energy balance"
        parbal = EnergyBalance(os.path.join(path,'balance.nc'),
                gmask = run.masks.outdiv, plotting=False)


########### MOULTON EXAMPLE
    if moulton_v:
        path = '/afs/ipp-garching.mpg.de/home/i/ipar/repository/python/solpspy/tests/balance_mat/moulton_2317'

        indrad_mask = io.loadmat(path+'/masks.mat')['indrad']


        #print "Particle balance"
        #parbal = ParticleBalance(os.path.join(path,'balance.nc'),
        #        gmask = indrad_mask, plotting=False)
        #embed()

        print "Heat balance"
        parbal = HeatBalance(os.path.join(path,'balance.nc'),
                gmask = indrad_mask, plotting=False)
