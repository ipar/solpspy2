from __future__ import division, print_function
import numpy as np
from scipy.special import erf
import logging

from solpspy.tools.tools import  priority
"""
"""


# Config logging here



class HeatFlux(object):
    """
    INPUTS:
    args:
      yi     -> Variable to fit, e.g, MdsData.out.ft
      xi     -> Spatial distance for the fit
    kwargs:
      method -> Objective function and algorithm, e.g,NonLinearLeastSquares
      limits -> Dict with lims for a,b,c,d,e, e.g,  limits['a']= [min,max]
                If not limit, then set to None
      p      -> Parameters. On __init__ call, it contains the initial guess
              for the parameters
    OUTPUTS:
      s      <- Heat flux expansion
      lmbd   <- Power decay lenght (why lmbd?: lambda is keyword in Python)
      p      <- Optimal parameters, if read after calling fit

    METHODS:
      __call__ == Evaluates Wagner function with p in provided x
      fit      == Can be called to refit with saved p (could receive them
                  upon call as **kwargs, thus if none, then use self.p)

    Suggestion:
      Maybe use np.inf and -np.inf intead of None for unconstrained indp. var.
    """
    def __init__(self, xi, yi, **kwargs):
        if (xi.ndim != 1) or (xi.shape != yi.shape):
            #Logging shouldn't be config here
            logging.basicConfig()
            log = logging.getLogger("HeatFlux")
            log.error("x and y must have exactly 1D and must be equal")
            return
        self.xi = xi
        self.yi = yi
        self.method = priority(['method','solver'], kwargs, 'LeastSquares')
        self.p = priority(['p','params','parameters'], kwargs,
                {'a':np.max(yi), 'b':50,'c':1, 'd':20, 'e':0.5})
        self.limits = priority(['limits','lims'], kwargs, {})
        for letter in ['a','b','c','d','e']:
            if letter not in self.limits:
                self.limits[letter] = [None,None]
        self.fit()

    def fit(self):
        """
        Fit data using _ffit.
        One can alsocall this method for re-fitting after re-assigning
        parameters, limits, data points, etc.
        curve_fit does not have bounds kwargs in scipy 0.15.1 and 0.17.0
        is not working.
        But it seems that it now works with ffit
        """
        if self.method.lower() == 'leastsquares' or self.method.lower() == 'ls':
            from scipy.optimize import curve_fit
            ffit = lambda x,a,b,c,d,e,self=self: self._ffit(x,a,b,c,d,e,
                                                 limits=self.limits)
            a,b,c,d,e = self.p['a'],self.p['b'],self.p['c'],self.p['d'],self.p['e']
            popt,pcov = curve_fit(ffit,self.xi,self.yi, p0 =np.array([a,b,c,d,e]))
            a,b,c,d,e = popt
        elif self.method.lower() == 'robust':
            pass
        elif self.method.lower() == 'mcmc' or self.method.lower() == 'montecarlo':
            pass
        self.p['a'],self.p['b'],self.p['c'],self.p['d'],self.p['e'] = a,b,c,d,e
        self.lambdaq = self.p['b']
        self.s = self.p['d']
        self.bkg = self.p['e']


    @staticmethod
    def _ffit(x,a,b,c,d,e,limits):
        """
        For fitting routines. It must work as a common function and not a class
        function, therefore the staticmethod decorator.
        Dict p might be changed for individual letters, to feed the fit rout.
        Maybe change limits from list to tuple and no limits from None to inf
        """
        p ={} #A dict makes the comparison with limits easier
        p['a'],p['b'],p['c'],p['d'],p['e'] = a,b,c,d,e
        for letter in ['a','b','c','d','e']:
            if limits[letter][0] is not None:
                if p[letter] < limits[letter][0]:
                    return np.full(x.shape, 1e100)
            if limits[letter][1] is not None:
                if limits[letter][1] < p[letter]:
                    return np.full(x.shape, 1e100)
        return (p['a']*np.exp(-(x-p['c'])/p['b'])*
            (1-erf(p['d']/(2*p['b'])-(x-p['c'])/p['d'])) + p['e'])

    def __call__(self, x):
        """
        Evaluates Wagner function in x,
        f = a * exp(-(x-c)/b) * (1 - erf(d/(2*b) - (x-c)/d)) + e
        """
        a,b,c,d,e = self.p['a'],self.p['b'],self.p['c'],self.p['d'],self.p['e']
        return a * np.exp(-(x-c)/b) * (1 - erf(d/(2*b) - (x-c)/d)) + e


#------------------------------------------------------------------------------
if __name__ == '__main__':
# =============================================================================
#THIS doesn't work anymore
    #from solpspy import MdsData
    #from solpspy import family

    #experiments = ['Old divIIb nesepm']
    #include = {'nesepm':['all'], 'pin': [0.70]}
    #eva = family(experiments)
    #eva.get_shotlist(include = include)
    #eva.order(criteria = ['nesepm'])
    #eva.read_mds_data()
    #for shot in eva.mds:
    #    shot.heat_flux_parameters()
    #lambdas = np.array([shot.out.lmbd for shot in eva.mds])
    #mlambdas = np.array([12.6674, 13.3818, 14.7229, 16.8437, 19.5712, 21.2790,
    #   22.7372, 26.2339, 30.7647, 36.3945, 38.0343, 36.6113, 34.7995, 33.6163])
    #error_lambda = np.isclose(lambdas,mlambdas, rtol = 0.01) #Error del 1%
    #ind_el = np.where(error_lambda == False)[0][0]

    #eses = np.array([shot.out.s for shot in eva.mds])
    #ms = np.array([2.8411, 3.2329, 4.0718, 5.1989, 6.4548, 8.0377, 9.6909,
    #   10.8326, 11.8436, 12.2520, 12.1216, 12.3097, 12.7831, 13.3049])
    #error_s = np.isclose(eses,ms, rtol = 0.01) #Error del 1%
    #ind_es = np.where(error_s == False)[0][0]

    #x = np.linspace(shot.out.ds[0],shot.out.ds[-1],1000)
    #for shot in eva.mds:
    #    plt.figure()
    #    plt.plot(x, shot.out.wagner(x),'r')
    #    plt.plot(shot.out.ds,shot.out.ft,'k.')
    #    plt.draw()

    #embed()
# =============================================================================


    from IPython import embed
    embed()

















