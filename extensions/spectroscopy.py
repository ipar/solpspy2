#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    Provides a class for the calculation and plotting of spectroscopic
    properties according to the ADAS databases. Uses Python scripts 
    provided by Marco Cavedon for the ADAS data readout.

    Since the calculation of the photon emission coefficients takes quite 
    long, the _pec variable is written to a '.spectroscopy_data.pkl' file
    from which the data is automatically restored at startup, if the file
    exists.

Remarks:
    In this class wavelengths are given in [nm] and densities in [m^-3]
    (and photon emissivity coefficients in [m^3 s^-1] and line emissions
    in [photons m^-3 s^-1] and so on).

    However, one has to be aware that the scripts for the ADAS readout 
    use wavelengths in angstrom and photon emissivity coefficients in 
    [cm^3 s^-1] and densities in [cm^-3.]
"""

# Common modules
from __future__ import print_function
import os
import sys
import glob
import pickle
import logging
import numpy as np
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from collections import OrderedDict

# Solpspy modules
from solpspy.tools.tools import try_block, elements, priority
from solpspy.tools.tools import sort_lists, convert_to_latex_unit
from solpspy.plots.plotting_tools import savefig

# Marco Cavedons ADAS Scripts:
from solpspy.extensions.spectroscopy_run_adas405 import run_adas405
from solpspy.extensions.spectroscopy_read_adf15 import read_adf15

# =============================================================================

class SpectralLines(object):
    """Only used internally."""
    def __init__(self,wavelength,excitation,recombination,name):
        self.wl   = wavelength           # <-- in nanometers
        self.exc  = excitation           # <-- in photons m^-3 s^-1
        self.rec  = recombination        # <-- in photons m^-3 s^-1
        self.tot  = self.exc + self.rec  # <-- in photons m^-3 s^-1
        self.name = name

class Spectroscopy(object):
    """The main spectroscopy class, containing several properties and
    methods. See descriptions in the particular definition in the code."""

    def __init__(self,solpspy_run):

        logging.basicConfig()
        self._run = solpspy_run
        self.log = self._run.log
        self.log.info('Loading Spectroscopy() - Please be aware that the ' +
                       'calculation of the photon emissivities can take long.')

        # Path to the ADAS pec data files:
        # (Should they be provided within the solpspy tool?!)
        self._pec_files_path = os.path.join(self._run.module_path,'data/adas/')

        # Restore the data from the backup file if it exists:
        self._get_backup_filename()
        self._restore_data()

    # ====== Properties: ======================================================

    @property
    @try_block
    def pec(self):
        """The photon emission coefficients. This variable is constructed
        as a list of length self._run.ns containing an OrderedDict() for each
        species which contains all spectral lines and their corresponding
        line emission coefficients in each grid cell according to the ADAS
        database.

        Format: pec[ns]{ Wavelength : [nx,ny,excitation|recombination] }
        Unit: m^3 s^-1

        The wavelength is given in nm.
        pec[ns][wl][:,:,0] are the excitation components
        pec[ns][wl][:,:,1] are the recombination components
        """
        self._pec = []
        for ns in range(self._run.ns):
            self._pec.append(self._calculate_adas_emissivity(ns))
        self._store_data()

    @property
    @try_block
    def emission(self):
        """Returns a species list of OrederdDict() objects containing 
        all spectral lines and their corresponding photon line emission 
        in each grid cell calculated from the ADAS photon emission 
        coefficients (pec): emission = pec*ne*na

        Format: emission[ns]{ Wavelength : [nx,ny,excitation|recombination] }
        Unit: m^-3 s^-1

        The wavelength is given in nm.
        emission[ns][wl][:,:,0] is the excitation contribution
        emission[ns][wl][:,:,1] is the recombination contribution
        """ 
        self._emission = []
        for ns in range(self._run.ns):
            emission = OrderedDict()

            pec = self.pec[ns]
            ne = self._run.ne
            na = self._run.na_eirene
            # If simulation is not kinetic take fluid species
            if na is None:
                na = self._run.na

            for wl in pec:
                emission[wl] = np.zeros((self._run.nx,self._run.ny,2))
                # Excitation:
                emission[wl][:,:,0] = pec[wl][:,:,0] * ne * na[:,:,ns]
                # Recombination:
                emission[wl][:,:,1] = pec[wl][:,:,1] * ne * na[:,:,ns+1]

            self._emission.append(emission)

    @property
    @try_block
    def power(self):
        """Returns a species list of OrederdDict() objects containing all 
        spectral lines and their corresponding radiation power in each grid 
        cell calculated via: p_rad = emission * hc/wl

        Format: power[ns]{ Wavelength : [nx,ny,excitation|recombination] }
        Unit: W m^-3

        The wavelength is given in nm.
        power[ns][wl][:,:,0] is the excitation contribution
        power[ns][wl][:,:,1] is the recombination contribution
        """ 
        # E = hc/wl (Energy per photon)
        c = 299792458 # m/s
        h = 6.626070040e-34 # Js
        self._power = []
        for ns in range(self._run.ns):
            self._power.append(OrderedDict())
            for wl in self.emission[ns]:
                wlm = 1e-9*wl # nm --> m
                self._power[-1][wl] = self.emission[ns][wl]*h*c/wlm
                # in [W m^-3]

    @property
    @try_block
    def totalemission(self):
        """Total emission of all lines summed up. [photons s^-1 m^-3]
        Format: (nx,ny,ns,exc|rec)"""
        self._totalemission = self._total(self.emission,-np.inf,np.inf)

    @property
    @try_block
    def totalpower(self):
        """Total radiation power of all lines summed up. [W m^-3]
        Format: (nx,ny,ns,exc|rec)"""
        self._totalpower = self._total(self.power,-np.inf,np.inf)

    def _total(self,var,wlrange,wlmax=None):
        """Returns the sum of 'var' summed over all spectral lines within
        the specified wavelength range. 'var' has to be of the format of,
        e.g., 'self.pec'. Output-format: (nx,ny,ns,exc|rec)"""
        # Can be called via:
        #   - total(var,1,2)
        #   - total(var,[1,2])
        if type(wlrange) != list and wlmax is not None:
            wlrange = [wlrange,wlmax]
        sum = np.zeros((self._run.nx,self._run.ny,self._run.ns,2))
        for ns in range(self._run.ns):
            v = var[ns]
            for wavelength in v:
                if wavelength < wlrange[0] or wavelength > wlrange[1]: continue
                sum[:,:,ns,0] += v[wavelength][:,:,0]
                sum[:,:,ns,1] += v[wavelength][:,:,1]
        return sum

    @property
    @try_block
    def lz_adas(self):
        """Expected L_z values for each cell according to their density and
        temperature values; calculated according to the the ADAS database."""

        self._lz_adas = np.zeros((self._run.nx,self._run.ny,self._run.natm))

        for natom,atom in enumerate(self._run.atom_names):
            for nx in range(self._run.nx):
                for ny in range(self._run.ny):

                    te = self._run.te[nx,ny]
                    ne = self._run.ne[nx,ny]

                    try:
                        lz = self._calculate_lz_adas(atom,te,ne)
                    except ValueError:
                        self.log.warning('Calculation of lz_adas failed for '
                                          + "atom '%s', "
                                          + 'cell (%s:%s) (ne: %s, te: %s); '
                                          +'set to NaN!',atom,nx,ny,ne,te)
                        lz = np.nan

                    self._lz_adas[nx,ny,natom] = lz
        self._store_data()

    # ====== Hidden Methods: ==================================================

    def _calculate_adas_emissivity(self,ns):
        """Calculates the excitation and recombination emissivity of 
        different spectral lines / wavelengths according to the ADAS 
        database for species 'ns'."""

        # Initiate:
        emissivity = OrderedDict()

        # No excitation/recombination for fully ionized atoms:
        if self._run.za[ns] == self._run.zn[ns]:
            return OrderedDict()

        # Get species names and corresponding pec files:
        specname = elements[self._run.zn[ns]] + str(int(self._run.za[ns]))
        specnice = self._run.species_names[ns]
        allpecfiles = glob.glob(os.path.join(self._pec_files_path,'pec*.dat'))
        files = []
        for apf in sorted(allpecfiles,reverse=True):
            if apf.endswith(specname+'.dat'): files.append(apf)

        # No pec file(s) found:
        if files == []:
            self.log.warning('No ADAS data found for species %s.',specname)
            return OrderedDict()

        # Do the calculation:
        for ifile,file in enumerate(files):
            file = file.replace('-garching.mpg.de','') # Path too long
            info = read_adf15(file=file,info=True)
            # Get a list of all wavelengths / lines in the pec file:
            wlsi = info['wavelength']/10.0 # angstrom --> nm
            wls = []
            for wl in wlsi:
                if wl in emissivity: continue # (dupl. lines in different files)
                wls.append(wl)
            if wls != []: print('Reading data from',file)
            ntotal = len(wls)*self._run.ny
            wls = sorted(set(wls)) # Sorted & unique
            nprogress = 0
            for wl in wls:
                blocks = np.where(info['wavelength']/10.0==wl)[0]
                #if wl in emissivity: # from another pec-file
                #    nprogress += self._run.ny*len(blocks)
                #    continue
                emissivity[wl] = np.zeros((self._run.nx,self._run.ny,2))

                outputtxta = ("Calculating emissivity for species " + str(ns) 
                              + " ('" + specnice + "') (file " + str(ifile+1) 
                              + "/" + str(len(files)) + "):")
                outputtxtb = '(' + str(wl) + ' nm)'

                for block in blocks:
                    for ny in range(self._run.ny):

                        # Some output:
                        nprogress += 1
                        progress = str(round(100.*nprogress/ntotal,1)) + '%'
                        progress = '{:>6}'.format(progress)
                        print (outputtxta,progress,outputtxtb)

                        te = self._run.te[:,ny]
                        ne = self._run.ne[:,ny]*1e-6 # (density in cm^-3 !!!)
                        pec,inf = read_adf15(file=file,block=block+1,
                                             te=te,dens=ne)
                        # ^ Photon emission coefficients in cm^3 s^-1
                        if block != blocks[-1]:
                            emissivity[wl][:,ny,0] += pec*1e-6
                        else:
                            emissivity[wl][:,ny,1] += pec*1e-6
                        # ^ Photon emission coefficients in m^3 s^-1

        # Sorting:
        sortedemiss = OrderedDict()
        rlcl = np.expand_dims(self._run.masks.rlcl,2)
        for wl in sorted(emissivity):
            sortedemiss[wl] = emissivity[wl]*rlcl
            # (Also multiplied by rlcl since very strange
            #  things happen in the guard cells ...)

        return sortedemiss

    def _get_backup_filename(self):
        # Path to the file where the calculated photon emission
        # coefficients are stored, so they don't have to be
        # recalculated on every call:
        if self._run._type == 'RundirData':
            bckp_file = os.path.join(self._run.rundir,'.spectroscopy_data.pkl')
        else:
            bckp_dir = os.path.join(self._run.module_path,
                                    'data/.spectroscopy_data/')
            if not os.path.exists(bckp_dir):
                os.makedirs(bckp_dir)
            bckp_file = os.path.join(bckp_dir,'.spectroscopy_data_shot_'
                                     + str(self._run.shot) + '.pkl')
        self._backup_file = bckp_file

    def _store_data(self):
        stored_data = {}
        for key in ['_pec','_lz_adas']:
            try:
                stored_data[key] = self.__getattribute__(key)
            except:
                pass
        with open(self._backup_file, 'wb') as output:
            pickle.dump(stored_data, output, pickle.HIGHEST_PROTOCOL)

    def _restore_data(self):

        # Check if the backup file exists:
        if not os.path.isfile(self._backup_file):
            return

        # Check date ; return if b2fstate is newer than the backup file:
        if self._run._type == 'RundirData':
            if (os.path.getmtime(os.path.join(self._run.rundir,'b2fstate'))
                > os.path.getmtime(self._backup_file)):
                return

        try:
            with open(self._backup_file, 'rb') as input:
                stored_data = pickle.load(input)

            # Handle old backup files:
            if type(stored_data) is list:
                stored_data = {'_pec':stored_data}

            for key in stored_data.keys():
                self.__setattr__(key,stored_data[key])

            self.log.info('Restored data from file: %s',self._backup_file)
        except EOFError:
            self.log.error('Error in reading spectroscopy backup file: %s',
                            self._backup_file)

    def _calculate_lz(self,atom,**kwargs):
        """Caclulates the effective Lz values as they actually appear in
        the simulation. They are reconstructed via Lz = P_rad/(n_e*n_a).
        The four return values are lz,te,ne,cm:
         - lz: the calculated lz values in all cells (as a list, sorted for te)
         - te: the corresponding temperatures in all cells (as a list)
         - ne: the corresponding densities in all cell (as a list)
         - cm: a reccomended plt colormap for the ne color coding
        (The values are returned as lists so they are ready for plotting
        of lz curves (lz vs. te).)"""

        grid_mask  = kwargs.pop('grid_mask'  , None)

        for kw in kwargs:
            self.log.error('Invalid argument: %s',kw)
            return

        lzsum = self._run.lz[:,:,self._run.atom_index(atom)]

        lz = [] # Lz values
        te = [] # Temperatures
        ne = [] # Densities (used for the color coding)
        cm = plt.cm.jet

        for nx in range(self._run.nx):
            for ny in range(self._run.ny):
                # Exclude guard cells:
                if self._run.masks.gdcl[nx,ny]: continue
                # Exclude values outside of the grid mask (if provided):
                if grid_mask is not None: 
                    if not grid_mask[nx,ny]: continue
                te.append(self._run.te[nx,ny])
                lz.append(lzsum[nx,ny])
                ne.append(self._run.ne[nx,ny])

        te,lz,ne = sort_lists([te,lz,ne])
        return lz,te,ne,cm

    def _calculate_lz_adas(self,atom,te,ne):
        """Calculates the L_z value in the coronal equilibrium according
        to the ADAS database for the provided atom, electron temperature
        and electron density."""

        # Convert densities to cm^3:
        ne = 1e-6 * ne
        try:
            frac, power = run_adas405(uid='adas',year=96,
                                      defyear=89,elem=atom,
                                      partial=False,te=te,
                                      dens=ne)
            # Convert Wcm^3 --> Wm^3:
            lz = 1e-6 * power['total'][0]
        except TypeError:
            self.log.error("Error in calling ADAS. Maybe you forgot to do "+
            "'source /afs/ipp/u/adas/setup/adas_setup.csh'")
            lz = np.nan
        except ValueError:
            self.log.warning('Calculation of run_adas405 failed for '
                              + "atom '%s', ne: %s, te: %s; returned NaN!",
                              atom,1e6*ne,te)
            lz = np.nan
            raise ValueError
        return lz

    # ====== Public methods: ==================================================

    def get_line(self,species,wavelength,maxdiff=0.1):
        """Returns the line emission of the line which is closest to 
        the specified wavelength for the specified species."""

        if type(species) == str:
            species = self._run.species_index(species)[0]

        # All lines for this species:
        allspeclines = self.emission[species]

        # Find the one closest to 'wavelength':
        diff = np.inf
        for wl in allspeclines.keys():
            dtmp = abs(wavelength-wl)
            if dtmp < diff:
                wlline = wl
                diff   = dtmp

        if diff > maxdiff:
            self.log.warning('The line closest to the desired value was '
                              +'found at %s nm - this differs from the '
                              +'target value by %s nm',wlline,diff)
        else:
            self.log.info('The line closest to the desired value was '
                           +'found at %s nm - this differs from the '
                           +'target value by %s nm',wlline,diff)

        return allspeclines[wlline]

    def highest_emission(self,**kwargs):
        """Print out the species with the highest photon emission."""
        los     = kwargs.pop('los'     , 'whole_volume')
        n       = kwargs.pop('n'       , self._run.ns)
        wlrange = kwargs.get('wlrange' , None)
        vartype = kwargs.pop('var'     , 'emission')

        # Preparation:
        if wlrange is not None:
            if vartype == 'emission': tot = self._total(self.emission,wlrange)
            elif vartype == 'power': tot = self._total(self.power,wlrange)
            elif vartype == 'b2power':
                self.log.warning("Cannot use a wavelength range "
                                  + "with the B2-radiation values.")
                tot = self._total(self.power,wlrange)
        else:
            if vartype == 'emission': tot = self.totalemission
            elif vartype == 'power': tot = self.totalpower
            elif vartype == 'b2power':
                tot = self._run.line_radiation / np.expand_dims(self._run.vol,2)

        d = {}
        for ns in range(self._run.ns):
            if len(tot.shape) == 4:
                val = np.sum(tot[:,:,ns,:],2)
            else:
                val = tot[:,:,ns]

            if los == 'whole_volume':
                eint = np.sum(val)
                eavr = eint / np.sum(self._run.vol)
            else:
                eint = self._run.chords[los].integrate(val)
                eavr = self._run.chords[los].average(val)

            d[eint] = d.get(eint,[]) + [ns,eint,eavr]

        # Sorting:
        ds = OrderedDict()
        counter = 0
        for e in sorted(d,reverse=True):
            i = 0
            while i < len(d[e]):
                ns   = d[e][i]
                eint = d[e][i+1]
                eavr = d[e][i+2]
                ds[ns] = [eint,eavr]
                i += 3
            counter += 1
            if counter >= n: break

        # Print output:
        print('')
        if vartype == 'power':
            print('Species with the highest radiation power contribution ',
                  end='')
        else: # if vartype == 'emission':
            print('Species with the highest photon emissions ',end='')

        if los == 'whole_volume': 
            print('(in the whole volume)')
        else: 
            print("(on the line of sight '" + los + "')")
        print('')

        if vartype == 'power':
            print('{:<8}  {:>15}  {:>17}'.format('Species:',
                                                 'Total Power:',
                                                 'Average Power:'))
        else: # if vartype == 'emission':
            print('{:<8}  {:>15}  {:>17}'.format('Species:',
                                                 'Total emission:',
                                                 'Average Emission:'))
        print(44*'-')

        for ns in ds:
            spec = self._run.species_names[ns]
            eint = ds[ns][0]
            eavr = ds[ns][1]
            print('{:<8}  {:>15}  {:>17}'.format(spec,
                                                 '{:0.3e}'.format(eint),
                                                 '{:0.3e}'.format(eavr)))
        print('')

    def plot_spectrum(self,**kwargs):
        # Possible options and defaults:
        vartype   = kwargs.pop('var'       , 'emission')
        species   = kwargs.pop('species'   , 'all')
        xlim      = kwargs.pop('xlim'      , [-np.inf,np.inf])
        ylim      = kwargs.pop('ylim'      , None)
        los       = kwargs.pop('los'       , None)
        taglines  = kwargs.pop('taglines'  , True)
        tagsize   = kwargs.pop('tagsize'   , 9)
        tagoffset = kwargs.pop('tagoffset' , 0)
        lines     = kwargs.pop('lines'     , 'both') # 'none', 'only' or 'both'
        save_as   = kwargs.pop('save_as'   , None)
        showplot  = kwargs.pop('showplot'  , True)
        printN    = kwargs.pop('printN'    , 15)
        figsize   = kwargs.pop('figsize'   , None)

        for kw in kwargs:
            self.log.error('plot_spectrum(): Invalid argument: %s',kw)
            return

        # Preparation:
        if vartype == 'emission':
            var    = self.emission
            ylabel = 'Photon emission'
            unit = 'm^-3 s^-1'
            title = 'Photon emission according to the ADAS database'
        elif vartype == 'pec':
            var    = self.pec
            ylabel = 'Photon emissivity coefficient'
            unit = 'm^3 s^-1'
            title = ('Photon emission coefficients according '
                     + 'to the ADAS database')
        elif vartype == 'power':
            var    = self.power
            ylabel = 'Radiation power'
            unit = 'W m^-3'
            title = 'Radiation power according to the ADAS database'
        else:
            self.log.error('plot_spectrum(): Invalid vartype: %s',vartype)
            return

        if los is None:  title += ' (whole Volume)'
        else:            title += " ('" + los + "')"

        # Create species list:
        species = self._run.species_index(species)

        # Create list with all spectral lines:
        xlim_fnd = [np.inf,-np.inf]
        spectrallines = []
        for ns in species:
            for wl in var[ns]:
                if wl < xlim[0] or wl > xlim[1]: continue
                xlim_fnd = [min(wl,xlim_fnd[0]),
                            max(wl,xlim_fnd[1])]
                exc = var[ns][wl][:,:,0]
                rec = var[ns][wl][:,:,1]
                if los is None:
                    # Average photon emission desnity for the whole 
                    # computational volume --> [photons m^-3 s^-1]
                    totvol = np.sum(self._run.vol)
                    exc = np.sum(exc*self._run.vol)/totvol
                    rec = np.sum(rec*self._run.vol)/totvol
                else:
                    # Average photon emission density along the line of sight
                    # --> [photons m^-3 s^-1]
                    exc = self._run.chords[los].average(exc)
                    rec = self._run.chords[los].average(rec)
                name = self._run.species_names[ns]
                spectrallines.append(SpectralLines(wl,exc,rec,name))

        if spectrallines == []: 
            info = ('(Species: ' + str(species) 
                    + ' ; Wavelength: ' + str(xlim) + ')')
            self.log.info("No plot data within the specified species- "
                           + "and wavelength range. %s",info)
            return

        # Only if xlim == default use auto-limits:
        if xlim[0] == -np.inf: xlim[0] = xlim_fnd[0]
        if xlim[1] ==  np.inf: xlim[1] = xlim_fnd[1]

        # Generate plot data:
        if figsize is not None:
            fig,ax = plt.subplots(figsize=figsize)
        else:
            fig,ax = plt.subplots()
        if lines == 'none' or lines == 'both':
            # Create spectrum:
            sigma = 0.03 # nm
            d = xlim[1] - xlim[0]
            resolution = min(5000,max(200,int(3*d/sigma)))
            # height = 1/((2*3.14159)*sigma)
            
            x = np.linspace(xlim[0]-5*sigma,
                            xlim[1]+5*sigma,
                            resolution)
            y_exc = np.zeros((resolution))
            y_rec = np.zeros((resolution))
            y_tot = np.zeros((resolution))

            for s in spectrallines:
                gaus = mlab.normpdf(x,s.wl,sigma)
                y_exc += s.exc * gaus
                y_rec += s.rec * gaus
                y_tot += s.tot * gaus

                #print(wl)
                #print(y_tot)
                
            plt.plot(x,y_exc,zorder=10,label='Excitation')
            plt.plot(x,y_rec,zorder=10,label='Recombination')
            plt.plot(x,y_tot,zorder= 9,label='Total',linewidth=3.0)
            leg = plt.legend(loc=4)
            leg.set_zorder(20)

        if lines == 'only' or lines == 'both':
            # Add the lines to the plot:
            x = [] ; y = []
            for s in spectrallines:
                x.append(s.wl)
                y.append(s.tot)
            color = 'k'
            if lines == 'both': color = '0.8'
            ax.stem(x,y,color,markerfmt=' ')

        plt.yscale('log')
        # Labels:
        plt.title(title)
        plt.xlabel('Wavelength [nm]')
        plt.ylabel(ylabel + ' ' + convert_to_latex_unit(unit))

        # Set plot limits:
        d=(xlim[1]-xlim[0])*0.01
        plt.xlim(xlim[0]-d,xlim[1]+d)
        if ylim is not None: plt.ylim(ylim[0],ylim[1])
        # Plot grid:
        plt.grid(b=True, which='major', color='0.6', linestyle='-')
        plt.gca().set_axisbelow(True)
        # Tag lines:
        if taglines:
            for s in spectrallines:
                xpos = s.wl + tagoffset
                if xpos < xlim[0] or xpos > xlim[1]: continue
                plt.text(xpos, s.tot, s.name, fontsize = tagsize, zorder=11)

        # Printout of the highest lines:
        if printN:
            wl   = []
            tot  = []
            name = []
            for s in spectrallines:
                if s.wl < xlim[0]-d or s.wl > xlim[1]+d: continue
                wl.append(s.wl)
                tot.append(s.tot)
                name.append(s.name)
            tot,wl,name = sort_lists([tot,wl,name],reverse=True)

            print('')
            print('The most dominant lines within the plot range ('
                  +str(xlim[0])+'-'+str(xlim[1])+'nm):')
            print('')
            print('{:<8}  {:>11}  {:>10}'.format('Species:',
                                                 'Wavelength:',
                                                 'Intensity:'))
            print(33*'-')
            for i in range(len(wl)):
                if i >= printN: break
                print('{:<8}  {:>11}  {:>10}'.format(name[i],
                                                     str(wl[i])+' nm',
                                                     '{:0.3e}'.format(tot[i])))
            print('')

        plt.tight_layout()
        self._run.plot.pdfpages.save()
        if showplot: plt.show()
        plt.close()

#    def frac_vs_te(self,terange=[1,50],ne=1e19,res=100):
#        """Returns an array of calculated fractional ion abundances for the
#        electron density 'ne' and temperatures within the temperature range 
#        'terange'. The number of temperature points can be set by 'res'."""
#
#        frac_vs_te = np.zeros((res,self._run.ns))
#
#        te = np.linspace(terange[0],terange[1],res)
#        ne = np.zeros((res)) + ne*1e-6 # in cm^3!!!
#        ns = 0
#        while ns < self._run.ns:
#            atom = self._run.elements[self._run.zn[ns]]
#            try:
#                frac, power = run_adas405(uid='adas',year=96,defyear=89,
#                                          elem=atom,partial=False,te=te,
#                                          dens=ne)
#                for i in range(len(frac['ion'][0])):
#                    frac_vs_te[:,ns+i] = frac['ion'][:,i]
#            except TypeError:
#                self.log.error("Error in calling ADAS. Maybe "
#                                + "you forgot to do 'source "
#                                + "/afs/ipp/u/adas/setup/adas_setup.csh'")
#                return None
#            ns += self._run.zn[ns]+1
#        return frac_vs_te

    def plot_lz(self,**kwargs):
        return lz_plot(self,**kwargs)

class lz_plot(object):

    def __init__(self,spectroscopy,**kwargs):

        self.spec        = spectroscopy
        self.run         = self.spec._run

        def_xlabel       = '$T_e$ [$\mathrm{eV}$]'
        #def_ylabel      = '$L_z$ [$\mathrm{Wm}^3$]'
        def_ylabel       = (r'$L_z\,= P_\mathrm{rad} / (n_e n_a)$ '
                            + '[$\mathrm{Wm}^3$]')

        def_lcolors      = {'tot' : 'k',
                            'plt' : 'b',
                            'prb' : 'r',
                            'prc' : 'g',
                            'ion' : '0.5'}
        def_lstyles      = {'tot' : '-',
                            'plt' : '-',
                            'prb' : '-',
                            'prc' : '-',
                            'ion' : '--'}
        def_lwidth       = {'tot' : 2,
                            'plt' : 1,
                            'prb' : 1,
                            'prc' : 1,
                            'ion' : 1}

        self.species     = priority('species'           , kwargs, 'auto')
        self.ne          = priority('ne'                , kwargs, 1e19)
        self.res         = priority('res'               , kwargs, 1000)
        self.which       = priority('which'             , kwargs, ['prc','ion'])

        self.title       = priority('title'             , kwargs, None)
        self.legend      = priority(['legend','label']  , kwargs, None)
        self.legtit      = priority(['legtit',
                                     'legend_title']    , kwargs, '')
        self.nlegcol     = priority('nlegcol'           , kwargs, 1)
        self.legloc      = priority('legloc'            , kwargs, 0)
        self.lcolors     = priority('lcolors'           , kwargs, def_lcolors)
        self.lstyles     = priority(['lstyles',
                                     'linestyles']      , kwargs, def_lstyles)
        self.lwidth      = priority(['lwidth',
                                     'linewidth']       , kwargs, def_lwidth)
        self.mcolor      = priority('mcolor'            , kwargs, 'k')
        self.mstyle      = priority(['mstyle',
                                     'markerstyle']     , kwargs, 'o')
        self.msize       = priority(['msize',
                                     'markersize']      , kwargs, 4)
        self.colorcoding = priority('colorcoding'       , kwargs, 'ne')
        self.xlabel      = priority('xlabel'            , kwargs, def_xlabel)
        self.ylabel      = priority('ylabel'            , kwargs, def_ylabel)

        self.xlim        = priority('xlim'              , kwargs, [1,1e3])
        self.ylim        = priority('ylim'              , kwargs, None)
        self.nbins       = priority('nbins'             , kwargs, ['auto',
                                                                   'auto'])
        self.fontsize    = priority('fontsize'          , kwargs, None)
        self.figsize     = priority('figsize'           , kwargs, (8,6))
        self.showplot    = priority(['showplot','show'] , kwargs, True)
        self.saveas      = priority(['saveas','save_as'], kwargs, None)

        # Create (log-spaced) te values:
        self.te = np.logspace(np.log10(self.xlim[0]),
                              np.log10(self.xlim[1]),
                              self.res)

        # Convert density to cm^3:
        self.ne = self.ne*1e-6

        self._guess_species()
        self._get_lz()
        self._plot()

    def _guess_species(self):
        """Returns the impurity species with the the highest
        particle content (ignoring the main ion species)."""
        if self.species != 'auto':
            return
        N = np.sum(self.run.na_atom*np.expand_dims(self.run.vol,2),(0,1))
        if len(N) < 2 :
            i = 0
        else:
            i = np.where(N == np.max(N[1:]))[0][0]
        self.species = self.run.atom_names[i]
        self.run.log.info("lz_plot(): Set species to '%s'",self.species)

    def _get_lz(self):
        pdata = [] # pdata[atom][density]
        try:
            ra_kwargs = {'uid'     : 'adas',
                         'year'    : 96,
                         'defyear' : 89,
                         'elem'    : self.species,
                         'partial' : False,
                         'te'      : self.te,
                         'dens'    : self.ne,
                         'all'     : True}
            self._ra_frac,self._ra_power = run_adas405(**ra_kwargs)
        except TypeError:
            print("Error in calling ADAS. Maybe you forgot to do")
            print("'source /afs/ipp/u/adas/setup/adas_setup.csh'")
            return

        # Convert Wcm^3 --> Wm^3:
        self.lz_adas = {'tot' : 1e-6*self._ra_power['total'][0],
                        'plt' : 1e-6*self._ra_power['plt'][0],
                        'prb' : 1e-6*self._ra_power['prb'][0],
                        'prc' : 1e-6*self._ra_power['prc'][0],
                        'ion' : 1e-6*self._ra_power['ion'][0]}
        # According to the manual these things are:
        # (http://www.adas.ac.uk/man/chap4-05.pdf)
        #  - plt: total line power function
        #  - prc: charge exchange recombination power function
        #  - prb: recombination + bremsstrahlung power function
        #  - ion: power function contributions evaluated using the 
        #         generalised collisional radiative coefficients

        # IMPORTANT REMARK: These terms are mixed up in the run_adas405
        #                   Python script (version from 2018-06-28)!
        #                   I.e. what is called 'plt' is originally 'prb',
        #                   what is called 'prc' is originally 'plt' and
        #                   what is called 'prb' is originally 'prc'!!!

        # Comment: 'prc'   is the sum of all 'ion' parts (in wrong nomenclature)
        #          'total' is the sum of 'prc' and 'plt' (in wrong nomenclature)
        #          'prb'   is zero (i.e. 1e-42) everywhere 
        #          ( ... because there is no 'prc' (original name) data?!)
        
    def _cett(self,val):
        # Move this to tools?
        """Converts an exponent to tex string"""
        exp = np.log10(val)
        if exp%1 != 0:
            return str(val)
        return '10^{' + str(int(exp)) + '}'

    def _plot(self):

        if self.fontsize:
            matplotlib.rcParams.update({'font.size': self.fontsize})

        fig = plt.figure(figsize=self.figsize)
        ax = fig.add_subplot(111)

        for w in self.which:
            if w == 'ion':
                for i in range(self.lz_adas[w].shape[1]):
                    ax.plot(self.te, self.lz_adas[w][:,i],
                            color     = self.lcolors[w],
                            linewidth = self.lwidth[w],
                            linestyle = self.lstyles[w])
            else:
                ax.plot(self.te, self.lz_adas[w],
                        color     = self.lcolors[w],
                        linewidth = self.lwidth[w],
                        linestyle = self.lstyles[w])

        # Include simulation data points:
        lz,te,ne,cm = self.spec._calculate_lz(atom=self.species)
        if self.colorcoding == 'ne':
            c = ne
        else:
            c = self.mcolor
        p = ax.scatter(te,lz,
                       c          = c,
                       cmap       = cm,
                       marker     = self.mstyle,
                       s          = self.msize,
                       edgecolors ='none',
                       norm       = matplotlib.colors.LogNorm())
        if self.colorcoding == 'ne':
            cb = plt.colorbar(p)
            cb.set_label('$n_e$ [$\mathrm{m}^{-3}$]')

        if self.title is None:
            self.title = ('$L_z$ power function for species '
                          + self.species + ' ($n_e=\,'
                          + self._cett(1e6*self.ne) + '\mathrm{m}^{-3}$)')

        ax.set_title(self.title)
        ax.set_xlabel(self.xlabel)
        ax.set_ylabel(self.ylabel)
        ax.set_xscale('log')
        ax.set_yscale('log')        
        ax.grid(b=True, color='0.5', linestyle='-')
        ax.set_xlim(self.xlim)
        if self.ylim is not None: 
            ax.set_ylim(self.ylim)
        elif self.which == []:
            ax.set_ylim([min(lz),max(lz)])
        if self.legend:
            ax.legend(loc=self.legloc)

        # Set number of ticks/bins:
        # DOESN'T WORK WITH LOG AXES ... WHICH I NEED HERE :/
        if self.nbins[0] != 'auto': 
            ax.locator_params(axis='x',nbins=self.nbins[0])
        if self.nbins[1] != 'auto': 
            ax.locator_params(axis='y',nbins=self.nbins[1])

        fig.tight_layout()

        if self.saveas is not None:
            if (not self.saveas.endswith('.png') and 
                not self.saveas.endswith('.pdf') and
                not self.saveas.endswith('.eps')):
                self.saveas += '.pdf'
            savefig(self.saveas)

        if self.showplot: 
            plt.show()

        plt.close()
