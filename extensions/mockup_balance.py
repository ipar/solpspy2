from __future__ import division, print_function

# Common modules:
import numpy as np
import matplotlib.pyplot as plt

# Special modules:

# Solpspy modules:
from solpspy.tools.tools import SimpleNamespace, try_block, styles, priority, cellface


import logging
mblog = logging.getLogger('Balance.Momentum')
if not mblog.handlers:
    ch = logging.StreamHandler()
    formatter = logging.Formatter(
            '%(name)s(%(name)s) -- %(levelname)s: %(message)s')
    ch.setFormatter(formatter)
    mblog.addHandler(ch)
    mblog.propagate = False


def momentum_balance(run, region='Outer divertor', check_region = False,
                     forward = True, printing = False, show=True,
                     saving =True, dictp = None):
    """
    Good for now
    Calculate momentum balance on the left/bottom faces of grid cells.
    INPUTS:
      Properly initialized solps_data shot

    OUTPUTS:
      - run.mom_balance -> SimpleNamespace containing the following:


    Definitions:


    
    Remarks:
     - There are some small differences with Moulton's MATLAB routines,
     because here the pitch angle which is used is Bperp/Btor instead of
     Bpol/Btotal.
    """
    run.mom_balance = SimpleNamespace()
    log = logging.LoggerAdapter(mblog,{'name':run.name})
    # Dispatch required data, to make it easier to read and work with
    qe = run.qe
    mp = run.mp
    nx = run.nx
    ny = run.ny
    ns = run.ns
    hx = run.hx
    hy = run.hy
    parallel_surface = run.parallel_surface_m #Moulton version, to test!
    leftix = run.leftix
    leftiy = run.leftiy
    rightix = run.rightix
    rightiy = run.rightiy
    bottomix = run.bottomix
    bottomiy = run.bottomiy
    topix = run.topix
    topiy = run.topiy

    smo = run.smo
    if smo is None:
        #run.log.info("smo is missing")
        smo = np.zeros((nx,ny,4,ns))
    elif (smo.shape != (nx,ny,4,ns)):
        #run.log.info("smo has wrong dimensions")
        mblog.warning("'smo' has wrong dimensions")
        smo = np.zeros((nx,ny,4,ns))

    smq = run.smq
    if smq is None:
        #run.log.info("smq is missing")
        smq = np.zeros((nx,ny,4,ns))
    elif (smq.shape != (nx,ny,4,ns)):
        #run.log.info("smq has wrong dimensions")
        mblog.warning("'smq' has wrong dimensions")
        smq = np.zeros((nx,ny,4,ns))

    smav = run.smav
    if smav is None:
        #run.log.info("smav is missing")
        smav = np.zeros((nx,ny,4,ns))
    elif (smav.shape != (nx,ny,4,ns)):
        #run.log.info("smav has wrong dimensions")
        mblog.warning("'smav' has wrong dimensions")
        smav = np.zeros((nx,ny,4,ns))

    fmox = run.fmox
    fmoy = run.fmoy

    ua = run.ua
    za = run.za
    dv = run.dv
    am = run.am
    b = run.b
    pitch = run.pitch
    invpitch = run.invpitch
    te = run.te
    ne = run.ne
    ti = run.ti
    na = run.na
    pstot = run.pstot

    fnax = run.fnax
    fnay = run.fnay

    resmo = run.resmo
    if resmo is None:
        #run.log.info("resmo is missing")
        resmo = np.zeros((nx,ny,ns))
        missing_resmo = True
    elif (resmo.shape != (nx,ny,ns)):
        #run.log.info("resmo has wrong dimensions")
        mblog.warning("'resmo' has wrong dimensions")
        resmo = np.zeros((nx,ny,ns))
        missing_resmo = True
    else:
        missing_resmo = False
    resmotot = np.sum(resmo[:,:,za>0],2)

    # Creates a nx x ny array with the mass of the species for each species
    mppc = mp*np.tile(am.reshape((1,1,ns)), (nx,ny,1))
    rob = mppc*na #mass density for every species

    ## Calculate the total the total sources from linearised components
    #smo: linear approximation to the parallel momentum sources for (is) [N]
    #  Solps manaual, page 195 and b2cdcv.F, line 866
    smo_rec = (smo[:,:,0,:] +
               smo[:,:,1,:]*ua +
               smo[:,:,2,:]*rob +
               smo[:,:,3,:]*rob*ua)
    #smq: total moentum sources [N]
    #  Solps manual, page 304
    smq_rec = (smq[:,:,0,:] +
               smq[:,:,1,:]*ua +
               smq[:,:,2,:]*rob +
               smq[:,:,3,:]*rob*ua)
    #smav: parallel momentum source from additional viscosity b2npmo_smav [N]
    #  Solps manual, page 304
    smav_rec = (smav[:,:,0,:] +
               smav[:,:,1,:]*ua +
               smav[:,:,2,:]*rob +
               smav[:,:,3,:]*rob*ua)
    smotot =  np.sum(smo_rec[:,:,za>0],2)
    smqtot =  np.sum(smq_rec[:,:,za>0],2)
    smavtot =  np.sum(smav_rec[:,:,za>0],2)

    # Calculate the static and total pressure forces...
    # Static pressure FORCE at cell centres
    psf = parallel_surface*pstot

    # Map to left cell face (Not exact!)
    psfx = np.zeros((nx,ny))
    spx = np.zeros((nx,ny))
    for iy in range(ny):
        for ix in range(nx):
            if leftix[ix,iy]<0:
                continue
            lft = (leftix[ix,iy],leftiy[ix,iy])
            fdv = dv[ix,iy]+dv[lft]

            f1 = psf[lft]*dv[ix,iy]
            f2 = psf[ix,iy]*dv[lft]
            psfx[ix,iy] = ((f1 + f2)/fdv)

            f1 = parallel_surface[lft]*dv[ix,iy]
            f2 = parallel_surface[ix,iy]*dv[lft]
            spx[ix,iy] = ((f1 + f2)/fdv)

    # Ion dynamic pressure FORCE at left faces (see b2urmo.F, line 104)
    flox = mppc*fnax # Mass poloidal flux
    fmodx = np.zeros((nx,ny,ns)) #Ion dynamic pressure force, mapped.
    for ins in range(ns):
        for iy in range(ny):
            for ix in range(nx):
                if leftix[ix,iy]<0:
                    continue
                fmodx[ix,iy,ins] = (flox[ix,iy,ins]*
                   (ua[ix,iy,ins] + ua[leftix[ix,iy],leftiy[ix,iy],ins])/2)
    # Viscous component at left faces
    fmovx = fmox - fmodx
    # Total pressure force at left cell faces
    ptotx = np.sum(fmodx[:,:,za>0],2) + psfx
    pvisx = np.sum(fmovx[:,:,za>0],2)

    # Ion dynamic pressure FORCE at bottom faces (see b2urmo.F, line 118)
    # Altered order wrt balmon.m
    floy = mppc*fnay # Mass poloidal flux
    fmody = np.zeros((nx,ny,ns))
    for ins in range(ns):
        for iy in range(ny):
            for ix in range(nx):
                if bottomix[ix,iy]<0:
                    continue
                fmody[ix,iy,ins] = (floy[ix,iy,ins]*(ua[ix,iy,ins] +
                          ua[bottomix[ix,iy],bottomiy[ix,iy],ins])/2)
    # Viscous component at bottom faces
    fmovy = fmoy - fmody
    # Total pressure FORCE at left cell faces. [ipar] Why are they different from the ones of poloidal?
    fmodytot = np.sum(fmody,2)
    fmovytot = np.sum(fmovy,2)

    raddivv = np.zeros((nx,ny)) # Total radial divergence (viscous)
    raddivd = np.zeros((nx,ny)) # Total radial divergence (dynamic)
    for iy in range(ny):
        for ix in range(nx):
            if topiy[ix,iy]>(ny-1):
                continue
            tix = topix[ix,iy]
            tiy = topiy[ix,iy]
            raddivv[ix,iy] = fmovytot[ix,iy] - fmovytot[tix,tiy]
            raddivd[ix,iy] = fmodytot[ix,iy] - fmodytot[tix,tiy]

    # Calculate the geometric term (Not exact, smag [ipar: smbg?] would be required, see b2sigp.F)
    pgm = np.zeros((nx,ny))
    for iy in range(ny):
        for ix in range(nx):
            if (leftix[ix,iy]<0) or (rightix[ix,iy]>(nx-1)):
                continue
            lix = leftix[ix,iy]
            liy = leftiy[ix,iy]
            rix = rightix[ix,iy]
            riy = rightiy[ix,iy]

            fl = (parallel_surface[ix,iy] - parallel_surface[lix,liy])
            fdvl = dv[ix,iy] + dv[lix,liy]
            fr = (parallel_surface[rix,riy] - parallel_surface[ix,iy])
            fdvr = dv[ix,iy] + dv[rix,riy]

            pgm[ix,iy] = pstot[ix,iy]*dv[ix,iy]*(fl/fdvl + fr/fdvr)

    #Select appropiate slices according to the provided mask
    # Usage of +-1 is better than right for single nulls
    if type(region) == str:
      if region.lower() == 'outer divertor' or region.lower() == 'outdiv':
        mask = run.masks.outdiv
      if region.lower() == 'inner divertor' or region.lower() == 'inndiv':
        mask = run.masks.inndiv

    if forward:
        #iup = np.where(mask[:,0])[0][0] #CORRECT
        iup = np.where(mask[:,run.sep])[0][0] +1 #FOR TESTING MOULTON
        #iup = np.where(mask[:,run.sep])[0][0] -1 #Corresponding to fmom calculations?
        idown = np.where(mask[:,run.sep])[0][-1] + 1 # To be used
        #idown = np.where(mask[:,run.sep])[0][-1]
        pind = range(iup,idown)
        source_factor = 1
    else:
        iup = np.where(mask[:,run.sep])[0][-1] +1
        idown = np.where(mask[:,run.sep])[0][0]
        pind = range(idown,iup)
        source_factor = -1
    rind = range(1,ny-1)

    spx_down = spx[idown,:]
    ptot_up = ptotx[iup,:]
    ptot_down = ptotx[idown,:]
    psf_up = psfx[iup,:]
    psf_down = psfx[idown,:]
    vis_up = pvisx[iup,:]
    vis_down = pvisx[idown,:]
    # Differences because one cell down the x-point
    # Radially displaced MATLAB(12) = Python[12] because size 36 vs 38
    raddivvint = np.sum(raddivv[pind],0)
    raddivvint[0],raddivvint[-1] = 0,0
    raddivdint = np.sum(raddivd[pind],0)
    raddivdint[0],raddivdint[-1] = 0,0
    geomint = np.sum(pgm[pind],0)
    geomint[0],geomint[-1] = 0,0
    smqsmavint = np.sum(smqtot[pind],0) + np.sum(smavtot[pind],0)
    smqsmavint[0],smqsmavint[-1] = 0,0
    neutint = np.sum(smotot[pind],0)
    neutint[0],neutint[-1] = 0,0
    resint = np.sum(resmotot[pind],0)
    resint[0],resint[-1] = 0,0

    othersource = vis_up-vis_down+raddivvint+raddivdint+geomint+smqsmavint
    error_lin = ptot_up - ptot_down + neutint + othersource

    # In cm.
    # omp-1 just for test, it is wrong in Moulton's!
    #rmrsep = (run.cr[run.omp,:] - run.cr_y[run.omp-1,run.sep])*100 
    rmrsep = (run.cr[run.omp-1,:] - run.cr_y[run.omp-1,run.sep-1])*100 

    


    # Patch all relevant variables for plots to run.balance.
    #Self only if saving is wanted?
    if saving:
        run.pind = pind
        run.rind = rind
        run.iup = iup
        run.idown = idown
        run.mom_balance.rmrsep = rmrsep
        run.mom_balance.spx = spx
        run.mom_balance.direction = 'down'
        run.mom_balance.ptotx = ptotx
        run.mom_balance.ptot_up = ptot_up/spx_down
        run.mom_balance.ptot_down = ptot_down/spx_down
        run.mom_balance.psfx = psfx
        run.mom_balance.ps_up = psf_up/spx_down
        run.mom_balance.ps_down = psf_down/spx_down
        run.mom_balance.neut_int = source_factor*neutint/spx_down
        run.mom_balance.others_source = source_factor*othersource/spx_down
        run.mom_balance.error = error_lin/spx_down
        run.mom_balance.residuals = source_factor*resint/spx_down
        run.mom_balance.parallel_viscosity = (vis_up-vis_down)/spx_down
        run.mom_balance.perpendicular_viscosity = raddivvint/spx_down
        run.mom_balance.divergence = raddivdint/spx_down
        run.mom_balance.geom_term = geomint/spx_down
        run.mom_balance.smqsmav = smqsmavint/spx_down

    if printing:
        xlim = np.max(rmrsep[rind])
        dict_print = {
                'subplots': True,
                'xlim_1':(-xlim, xlim),
                'ylim_1':(None, None),
                'xlim_2':(-xlim, xlim),
                'ylim_2':(None, None),
                'ylabel_1_extra':'',
                'ylabel_2_extra':'',
                'legend_font_size':12,
                'save':False,
                'save_path':'.',
                'save_name_1':'pressure_profiles.pdf',
                'save_name_2':'details_profiles.pdf'}
        if dict_print['subplots']:
               dict_print['figsize'] = (16,13.6)
        #else: 
        #       dict_print['figsize'] = (8,6)

        for key in dict_print: 
            try:
                dict_print[key] = dictp[key]
            except:
                pass

        if dict_print['subplots']:
            try:
                fig = plt.figure(figsize=dict_print['figsize'])
            except:
                fig = plt.figure()
            ax1 = fig.add_subplot(2,1,1)
            ax2 = fig.add_subplot(2,1,2)
        else:
            try:
                fig1 = plt.figure(figsize=dict_print['figsize'])
                fig2 = plt.figure(figsize=dict_print['figsize'])
            except:
                fig1 = plt.figure()
                fig2 = plt.figure()
            ax1 = fig1.add_subplot(1,1,1)
            ax2 = fig2.add_subplot(1,1,1)

     
        if dict_print['subplots']:
            #ax2.set_xlabel(r"dS$_{omp}$[cm]")
            ax2.set_xlabel("dS,omp [cm]")
            plt.setp(ax1.get_xticklabels(), visible=False)
        else:
            ax1.set_xlabel("dS,omp [cm]")
            ax2.set_xlabel("dS,omp [cm]")

        ax1.set_ylabel("[Pa]"+dict_print['ylabel_1_extra'])
        ax2.set_ylabel("[Pa]"+dict_print['ylabel_2_extra'])


        # Fig 1/ sub 1: Pressure profiles and main sources
        ax1.axvline(0, color = 'k', lw=1)
        ax1.axhline(0, color = 'k', lw=1)
        ax1.plot(rmrsep[rind], ptot_up[rind]/spx_down[rind],'k',
                 linewidth = 2, label='Total pressure upstream')
        ax1.plot(rmrsep[rind], psf_up[rind]/spx_down[rind],'k--',
                 linewidth = 2, label='Static pressure upstream')
        ax1.plot(rmrsep[rind], ptot_down[rind]/spx_down[rind],'r',
                 linewidth = 2, label='Total pressure downstream')
        ax1.plot(rmrsep[rind], psf_down[rind]/spx_down[rind],'r--',
                 linewidth = 2, label='Static pressure downstream')
        if not missing_resmo:
            ax1.plot(rmrsep[rind],error_lin[rind]/spx_down[rind],
                     color = 'DarkViolet', linewidth = 2, label='Error')
            ax1.plot(rmrsep[rind], neutint[rind]/spx_down[rind],
                 color='DarkGreen', linewidth = 2, label='Neutrals source')
        else:   
            ax1.plot(rmrsep[rind],np.zeros(len(rind)),
                     color = 'DarkViolet', linewidth = 2, 
                     label='Error*** (= 0.0)')
            ax1.plot(rmrsep[rind],-1.*error_lin[rind]/spx_down[rind], 
                 color='DarkGreen', linewidth = 2, 
                 label='Neutrals source*** (-Error)')
        ax1.plot(rmrsep[rind], othersource[rind]/spx_down[rind],
                 color='Blue', linewidth = 2, label='Other sources')
        try:
            ax1.legend(loc='best', 
                    prop={'size':dict_print['legend_font_size']})
        except:
            ax1.legend(loc='best')
        ax1.grid(True)

        ax1.set_xlim(dict_print['xlim_1'])
        ax1.set_ylim(dict_print['ylim_1'])

        if not dict_print['subplots'] and not dict_print['save']:
            fig1.tight_layout()            
        elif not dict_print['subplots'] and dict_print['save']:
            fig1.savefig(dict_print['save_path']+
                    '/'+dict_print['save_name_1'],
                        bbox_inches='tight')

        # Fig 2/ sub 2: Details of sources
        ax2.axvline(0, color = 'k', lw=1)
        ax2.axhline(0, color = 'k', lw=1)
        if not missing_resmo:
            ax2.plot(rmrsep[rind], neutint[rind]/spx_down[rind],
                 color='DarkGreen', linewidth = 2, label='Neutrals source')
        else:
            ax2.plot(rmrsep[rind],-1.*error_lin[rind]/spx_down[rind],
                 color='DarkGreen', linewidth = 2, 
                 label='Neutrals source*** (-Error)')

        ax2.plot(rmrsep[rind], (vis_up-vis_down)[rind]/spx_down[rind],'r',
                 linewidth = 2, label='Parallel viscosity')
        ax2.plot(rmrsep[rind], raddivvint[rind]/spx_down[rind],
                 color='b', linewidth = 2, label='Perpendicular viscosity')
        ax2.plot(rmrsep[rind], raddivdint[rind]/spx_down[rind],'k', 
                 linewidth = 2, label=r'div(mnu_{par}u_{perp})')
        ax2.plot(rmrsep[rind],geomint[rind]/spx_down[rind],
                 color = 'olive', linewidth = 2, label='Geom. term')
        ax2.plot(rmrsep[rind],smqsmavint[rind]/spx_down[rind],
                 color = 'DarkGray', linewidth = 2, label='smq + smav')
        if not missing_resmo:
            ax2.plot(rmrsep[rind],error_lin[rind]/spx_down[rind],
                     color = 'DarkViolet', linewidth = 2, label='Error')
        else:
            ax2.plot(rmrsep[rind], np.zeros(len(rind)),
                     color = 'DarkViolet', linewidth = 2, 
                     label='Error*** (= 0.0)')
        ax2.plot(rmrsep[rind],resint[rind]/spx_down[rind],
                 color = 'pink', linewidth = 2, label='Residuals')
        try:
            ax2.legend(loc='best',
                prop={'size':dict_print['legend_font_size']})
        except:
            ax2.legend(loc='best')
        ax2.grid(True)
        ax2.set_xlim(dict_print['xlim_2'])
        ax2.set_ylim(dict_print['ylim_2'])

        if not dict_print['subplots'] and not dict_print['save']:
            fig2.tight_layout()            
        elif dict_print['save']:
            fig2.savefig(dict_print['save_path']+
                    '/'+dict_print['save_name_2'],
                        bbox_inches='tight')
        else:
            plt.tight_layout()

        if show:
            plt.show()


  
    if saving == False and printing == False:
        mblog.warning("No task requested")
