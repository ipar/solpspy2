#Spaces in --omit are important. Omit takes a single line with comma separated patterns that can
# have * (anything) or ? (any single character).

import subprocess
subprocess.call(
    #'coverage report --include="/afs/ipp-garching.mpg.de/home/i/ipar/repository/python/solpspy/*"',
    'coverage report --include="/afs/ipp-garching.mpg.de/home/i/ipar/repository/python/solpspy/*" ' +
    '--omit="*__init__*,*test_*,*/test/*,*mockups.py,*generate_data_fixtures*,*main_pytest.py"',
    shell = True)
