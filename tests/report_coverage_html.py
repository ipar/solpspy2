import subprocess
subprocess.call(
    #'coverage html --include="/afs/ipp-garching.mpg.de/home/i/ipar/repository/python/solpspy/*"',
    'coverage html --include="/afs/ipp-garching.mpg.de/home/i/ipar/repository/python/solpspy/*" ' +
    '--omit="*__init__*,*test_*,*/test/*,*mockups.py,*generate_data_fixtures*,*main_pytest.py"',
    shell=True)
subprocess.call('firefox --new-window htmlcov/index.html', shell=True)
