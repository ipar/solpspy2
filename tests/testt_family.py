import os
import numpy as np
import pytest
from solpspy import *
from solpspy.tools.tools import module_path
import pandas as pd




scenarios = [
    {'name': '001',
     'include': [{'experiment': ['just testing', 'good testing']}],
     'exclude': [],
     'order':['experiment', 'ip', 'fhecore'],
     'ids': 'normal include'},

    {'name': '002',
     'include': [{'experiment': ['good testing', 'just testing']}],
     'exclude': [],
     'order':['experiment', 'ip', 'fhecore'],
     'ids': 'reversed include'},

    {'name': '003',
     'include': [{'experiment': ['good testing', 'just testing']}],
     'exclude': [],
     'order':['experiment', 'fhecore', 'ip'],
     'ids': 'reversed order'},
    ]

def ids_family(param):
    try:
        return param['ids']
    except:
        return repr(val)

@pytest.fixture(scope="module", params = scenarios, ids=ids_family)
def family(request):
    index = os.path.join(module_path(),'tests/mockup_family_files/databases/index.yaml')
    include = request.param['include']
    exclude = request.param['exclude']
    order = request.param['order']
    tmp = Family(include=include, order=order, index=index,
        default_index=False, autoload=False)
    tmp.scenario = request.param.copy()
    tmp.scenario['check'] = tmp.db['check'].copy()
    return tmp


def test_db(family):
    name = family.scenario['name']
    tmpdb = pd.read_pickle( 'mockup_family_files/{}/pickled_mocking_database'.format(name))
    assert family.db.equals(tmpdb)

def test_check(family):
    name =  family.scenario['name']
    check = family.scenario['check'].copy()
    tmpdb = pd.read_pickle( 'mockup_family_files/{}/pickled_mocking_database'.format(name))
    assert family.db['check'].equals(check)



property_vars = ['te', 'na']

target_vars = ['out.te']

@pytest.mark.parametrize('var', property_vars + target_vars)
def test_returned_samples(family, var):
    name = family.scenario['name']
    if len(var.split('.')) == 1:
        tmp1 = family.sample(lambda x: getattr(x,var))
        lpath = 'tests/mockup_family_files/{0}/{1}.npy'.format(name,var)
    else:
        raiz, var = var.split('.')
        tmp1 = family.sample( lambda x, var=var, raiz=raiz: getattr(getattr(x, raiz),var))
        lpath = 'tests/mockup_family_files/{0}/{1}.{2}.npy'.format(name,raiz, var)

    tmp2 = np.load(os.path.join(module_path(), lpath))

    for i in xrange(tmp1.shape[0]):
        assert np.allclose(tmp1[i], tmp2[i])




@pytest.mark.parametrize('var', property_vars + target_vars)
def test_named_samples(family, var):
    name = family.scenario['name']
    if len(var.split('.')) == 1:
        family.sample(lambda x: getattr(x,var), name='tmp1')
        lpath = 'tests/mockup_family_files/{0}/{1}.npy'.format(name,var)
    else:
        raiz, var = var.split('.')
        family.sample( lambda x, var=var, raiz=raiz: getattr(getattr(x, raiz),var), name='tmp1')
        lpath = 'tests/mockup_family_files/{0}/{1}.{2}.npy'.format(name,raiz, var)

    tmp2 = np.load(os.path.join(module_path(), lpath))

    for i in xrange(tmp2.shape[0]):
        assert np.allclose(family.samples.tmp1[i], tmp2[i])










#@pytest.mark.parametrize('scenario', scenarios, ids=ids_scenarios)
#def test_creation_(scenario):
#    tmp = Family(**scenario)
#    for scen in scenarios:
#        if scen.name == scenario.name:
#            comparison = scen
#            break
#    assert tmp.include == comparison['include']














###Just for manual testing
#def family():
#    index = os.path.join(module_path(),'tests/mockup_family_files/databases/index.yaml')
#    include = [{'experiment': ['just testing', 'good testing']}]
#    order = ['experiment', 'ip', 'fhecore']
#    return Family(include=include, order=order, index=index,
#        default_index=False, autoload=False)
#
##@profile
#def main():
#    #from IPython import embed
#    import pdb
#    import matplotlib.pyplot as plt
#    from IPython import embed
#    import pandas as pd
#
#    data = family()
#
#    data.load_runs(progress_bar=False)
#    data.sample(lambda x: np.average(x.out.te,0), name='tet')
#    data.sample(lambda x: x.out.ds, name='ds')
#    test = SolpsData(data.db.iloc[0].fullpath)
#
#
#    df  = data.db.copy()
#    df.to_pickle('mockup_family_files/pickled_mocking_database')
#    df2 = pd.read_pickle('mockup_family_files/pickled_mocking_database')
#    df.equals(df2)
#
#    embed()
#
#
#    return
#
#if __name__ == '__main__':
#    main()
#
#
#
