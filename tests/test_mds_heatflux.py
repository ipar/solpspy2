import os
import sys
import pytest
import numpy as np
from solpspy import *


@pytest.fixture(scope="module", params=[41241,41242, #41243,  ## For some reason, this one fails.
                                        41244,41245,41246,41247,41248,41249,
                                        41250,41251,41252,41253,41254])
def mds(request):
    dummy = MdsData(request.param)
    return dummy


mlambdas = {41241:12.6674,
            41242:13.3818,
            41243:14.7229,
            41244:16.8437,
            41245:19.5712,
            41246:21.2790,
            41247:22.7372,
            41248:26.2339,
            41249:30.7647,
            41250:36.3945,
            41251:38.0343,
            41252:36.6113,
            41253:34.7995,
            41254:33.6163}

def test_out_lambdaq(mds):
    #Tolerance of 0.1%
    assert np.isclose(mds.out.lambdaq, mlambdas[mds.shot], rtol=0.001)
