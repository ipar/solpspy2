import os
import sys
import numpy as np
import pytest
from solpspy.tools.tools import module_path

from mockups_bal import parbal, validity_bal


comuse_names = ['nx', 'ny','ns', 'natm','nmol',
    'leftix', 'leftiy', 'rightix', 'rightiy', 'bottomix', 'bottomiy',
    'topix', 'topiy']

parbal_names = ['raddiv_nanom']





#------------------------------------------------------------------------------
@pytest.mark.parametrize('name', geometry_names + parbal_names)
def test_parbal(parbal,name):
    assert validity_bal(parbalt,name)
