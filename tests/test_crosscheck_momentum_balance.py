import numpy as np
import pytest
from mockups import rundir, mds, validity_crosscheck, skip_ddn


def test_pind(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'pind')
def test_rind(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'rind')
    skip_ddn(mds)
def test_iup(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'iup')
def test_idown(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'idown')

def test_mom_rmrsep(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.rmrsep')
def test_mom_spx(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.spx')
def test_mom_direction(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.direction')
def test_mom_ptotx(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.ptotx')
def test_mom_ptot_up(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.ptot_up')
def test_mom_ptot_down(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.ptot_down')
def test_mom_psfx(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.psfx')
def test_mom_ps_up(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.ps_up')
def test_mom_ps_down(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.ps_down')
def test_mom_neut_int(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.neut_int')
def test_mom_others_source(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.others_source')
def test_mom_error(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.error')

@pytest.mark.skip(reason="Differences in resmo for MDS+ and rundir")
def test_mom_residuals(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.residuals')

def test_mom_parallel_viscosity(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.parallel_viscosity')
def test_mom_perpendicular_viscosity(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.perpendicular_viscosity')
def test_mom_divergence(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.divergence')
def test_mom_geom_term(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.geom_term')
def test_mom_smqsmav(mds,rundir):
    skip_ddn(mds)
    assert validity_crosscheck(mds,rundir,'mom_balance.smqsmav')

