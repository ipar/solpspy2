import numpy as np
import pytest
from mockups import mds, validity_mds, skip_ddn


def test_pind(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'pind')
def test_rind(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'rind')
def test_iup(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'iup')
def test_idown(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'idown')

def test_mom_rmrsep(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.rmrsep')
def test_mom_spx(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.spx')
def test_mom_direction(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.direction')
def test_mom_ptotx(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.ptotx')
def test_mom_ptot_up(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.ptot_up')
def test_mom_ptot_down(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.ptot_down')
def test_mom_psfx(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.psfx')
def test_mom_ps_up(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.ps_up')
def test_mom_ps_down(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.ps_down')
def test_mom_neut_int(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.neut_int')
def test_mom_others_source(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.others_source')
def test_mom_error(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.error')
def test_mom_residuals(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.residuals')
def test_mom_parallel_viscosity(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.parallel_viscosity')
def test_mom_perpendicular_viscosity(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.perpendicular_viscosity')
def test_mom_divergence(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.divergence')
def test_mom_geom_term(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.geom_term')
def test_mom_smqsmav(mds):
    skip_ddn(mds)
    assert validity_mds(mds,'mom_balance.smqsmav')
