import numpy as np
import pytest
from mockups import mds, rundir, skip_ddn


#def test_targ4(mds,rundir):
#    assert  mds.targ4 == rundir.targ4
#
#def test_targ1(mds,rundir):
#    assert mds.targ1 == rundir.targ1
#
def test_out_ds(mds,rundir):
    assert np.allclose(mds.out.ds, rundir.out.ds)

def test_out_da(mds,rundir):
    assert np.allclose(mds.out.da, rundir.out.da)

def test_out_ft(mds,rundir):
    assert np.allclose(mds.out.ft, rundir.out.ft)

def test_out_fi(mds,rundir):
    assert np.allclose(mds.out.fi, rundir.out.fi)

def test_out_te(mds,rundir):
    assert np.allclose(mds.out.te, rundir.out.te)

def test_out_ne(mds,rundir):
    assert np.allclose(mds.out.ne, rundir.out.ne)

def test_out_ti(mds,rundir):
    assert np.allclose(mds.out.ti, rundir.out.ti)

def test_out_po(mds,rundir):
    assert np.allclose(mds.out.po, rundir.out.po)

def test_out_fc(mds,rundir):
    assert np.allclose(mds.out.fc, rundir.out.fc)


def test_inn_ds(mds,rundir):
    assert np.allclose(mds.inn.ds, rundir.inn.ds)

def test_inn_da(mds,rundir):
    assert np.allclose(mds.inn.da, rundir.inn.da)

def test_inn_ft(mds,rundir):
    assert np.allclose(mds.inn.ft, rundir.inn.ft)

def test_inn_fi(mds,rundir):
    assert np.allclose(mds.inn.fi, rundir.inn.fi)

def test_inn_te(mds,rundir):
    assert np.allclose(mds.inn.te, rundir.inn.te)

def test_inn_ne(mds,rundir):
    assert np.allclose(mds.inn.ne, rundir.inn.ne)

def test_inn_ti(mds,rundir):
    assert np.allclose(mds.inn.ti, rundir.inn.ti)

def test_inn_po(mds,rundir):
    assert np.allclose(mds.inn.po, rundir.inn.po)

def test_inn_fc(mds,rundir):
    assert np.allclose(mds.inn.fc, rundir.inn.fc)


def test_outmp_ds(mds,rundir):
    assert np.allclose(mds.outmp.ds, rundir.outmp.ds)

def test_outmp_te(mds,rundir):
    assert np.allclose(mds.outmp.te, rundir.outmp.te)

def test_outmp_ne(mds,rundir):
    assert np.allclose(mds.outmp.ne, rundir.outmp.ne)

def test_outmp_ti(mds,rundir):
    assert np.allclose(mds.outmp.ti, rundir.outmp.ti)

def test_outmp_te(mds,rundir):
    assert np.allclose(mds.outmp.te, rundir.outmp.te)


def test_innmp_ds(mds,rundir):
    assert np.allclose(mds.innmp.ds, rundir.innmp.ds)

def test_innmp_te(mds,rundir):
    assert np.allclose(mds.innmp.te, rundir.innmp.te)

def test_innmp_ne(mds,rundir):
    assert np.allclose(mds.innmp.ne, rundir.innmp.ne)

def test_innmp_ti(mds,rundir):
    assert np.allclose(mds.innmp.ti, rundir.innmp.ti)
