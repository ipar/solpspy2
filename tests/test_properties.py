import os
import sys
import numpy as np
import pytest
from solpspy.tools.tools import module_path

from mockups import mds, validity_mds
from mockups import rundir, validity_crosscheck
from mockups import skip_by_species, skip_by_especies, skip_b2standalone
from mockups import skip_ddn, skip_sn, skip_no_mdsplus_module


ident_names = ['solpsversion', 'solps_code', 'exp']

geometry_names = ['vessel_mesh', 'grid', 'nx', 'ny','ns', 'natm','nmol',
    'nnreg', 'region', 'regflx', 'regfly', 'regvol', 'cr', 'cr_x', 'cr_y',
    'cz', 'cz_x', 'cz_y','r', 'z', 'imp','omp','sep', 'ixp', 'oxp', 'za','zn',
    'am', 'b','pitch','pitch_pt', 'pitch_tot','invpitch', 'dv', 'hx', 'hy',
    'parallel_surface_m', 'parallel_surface', 'ipardspar', 'domp', 'leftix',
    'leftiy', 'rightix', 'rightiy', 'bottomix', 'bottomiy', 'topix', 'topiy',
    'nncut', 'rightcut', 'leftcut', 'topcut', 'bottomcut',
    'xpoint', 'sy', 'sx', 'domp_parallel', 'mi', 'am_kg',]

snapshot_names = ['te', 'ti', 'ne', 'na', 'zeff', 'ua', 'po', 'pstot','pdynd',
    'fmox', 'fmoy', 'fna', 'fnax', 'fnay', 'fna32', 'fnax32', 'fnay32',
    'fna52', 'fnax52', 'fnay52', 'na_eirene', 'na_atom','fractional_abundances',
    'fhe', 'fhex', 'fhey','fhi', 'fhix', 'fhiy','fhj', 'fhjx', 'fhjy',
    'fhm', 'fhmx', 'fhmy',
    'fhp', 'fhpx', 'fhpy',
    #'fht', 'fhtx', 'fhty',
    'fch', 'fchx', 'fchy',
    'stored_energy', 'ni',
    #'pressure_gradient_force', #ASK FHITZ
    'cimp', 'pdyna','ptot',
    'dperp', 'kyiperp', 'kyeperp', 'rpt', 'rqahe', 'rqrad', 'line_radiation',
    'rrahi', 'rramo', 'rrana', 'rsahi', 'rsamo', 'electric_field',
    #'electric_force' #ASK FHITZ
    ]


snapshot_eirene_names = ['tab2', 'dab2', 'tmb2', 'dmb2', 'tib2','dib2' ]

residuals_names = ['smo','smq','smav']

#balance_index_names = ['oul','odl','iul','idl'] # Are necessary anymore?
balance_index_names = [] # Are necessary anymore?

#avg_names = ['na_avg']
avg_names = []
line_names = ['na_line']
#avg_eirene_names = ['dab2_avg', 'dmb2_avg']
avg_eirene_names = []
line_eirene_names = ['dab2_line', 'dmb2_line']

ddn_names = ['sep2', 'oxp2','ixp2']


#Most are just temporarily here
#gs is here because [...2] is not yet correctly implemented for crosschecking.
only_mds_names = ['user', 'vessel', 'dsrad', 'dspar', 'resmo', 'directory','gs']


# masks do not work if this is used! WHY?
problematic_names = ['outdiv_nolim', 'rsana']


masks_names =  ['outdiv', 'inndiv', 'notcore', 'sol']


#For comparison betwen rundir and rundir_npy, when implemented
only_rundir_names = ['vessel', 'gs']






#------------------------------------------------------------------------------
@pytest.mark.parametrize('name', ident_names + geometry_names +
    snapshot_names + snapshot_eirene_names + residuals_names +
    balance_index_names + ddn_names + avg_names + avg_eirene_names
    + only_mds_names)
def test_mds(mds,name):
    skip_no_mdsplus_module(mds)
    if name in snapshot_eirene_names:
        skip_b2standalone(mds)
    if name in  ddn_names:
        skip_sn(mds)
    assert validity_mds(mds,name)

@pytest.mark.skip(reason="Masks apparently not properly implemented yet")
@pytest.mark.parametrize('name', masks_names)
def test_mds_masks(mds,name):
    assert validity_mds(mds,'masks.'+name)


#@pytest.mark.parametrize('name', line_names + line_eirene_names)
#def test_mds_avg(mds,name):
#    tmp1 = getattr(mds,name)
#    mds_var_npy = os.path.join(module_path(),
#            'tests/mds_npy/'+str(mds.nshot)+'/'+name+'.npy')
#    tmp2 = np.load(mds_var_npy)
#    assert np.allclose(tmp1[1:-2], tmp2[1:-2])
#





#------------------------------------------------------------------------------
@pytest.mark.parametrize('name', ident_names + geometry_names +
    snapshot_names + snapshot_eirene_names + residuals_names +
    balance_index_names + ddn_names + avg_names + avg_eirene_names)
def test_crosscheck(mds,rundir,name):
    skip_no_mdsplus_module(mds)
    if name in snapshot_eirene_names:
        skip_b2standalone(mds)
    if name in  ddn_names:
        skip_sn(mds)
    assert validity_crosscheck(mds,rundir, name)

@pytest.mark.skip(reason="Masks apparently not properly implemented yet")
@pytest.mark.parametrize('name', masks_names)
def test_crosscheck_masks(mds,rundir,name):
    assert validity_crosscheck(mds,rundir, 'masks.'+name)








#------------------------------------------------------------------------------
@pytest.mark.skip(reason="rundir_npy not yet saved")
@pytest.mark.parametrize('name', only_rundir_names)
#def test_mds(rundir,name):
def test_rundir(rundir,name):
    if name in snapshot_eirene_names:
        skip_b2standalone(rundir)
    if name in  ddn_names:
        skip_sn(rundir)
    assert validity_rundir(rundir,name)










## The problem is to compare NaN and also the return of a list.
#------------------------------------------------------------------------------

#
#
##Previously in test_averages.py
##ATTENTION: Hard coded dependencies should be substituted by relative indexes.
#def test_dab2_line_11_0(mds):
#    skip_b2standalone(mds)
#    skip_ddn(mds)
#    n_at = (sum(mds.dab2[37:49,11,0]*mds.dv[37:49,11])/
#            sum(mds.dv[37:49,11]))
#    assert np.isclose(mds.dab2_line[11,0],n_at)
#
#def test_dab2_line_11_1(mds):
#    skip_b2standalone(mds)
#    skip_by_especies(mds, 2)
#    skip_ddn(mds)
#    n_at = (sum(mds.dab2[37:49,11,1]*mds.dv[37:49,11])/
#            sum(mds.dv[37:49,11]))
#    assert np.isclose(mds.dab2_line[11,1],n_at)
#
#def test_dab2_line_11_2(mds):
#    skip_b2standalone(mds)
#    skip_by_especies(mds, 3)
#    skip_ddn(mds)
#    n_at = (sum(mds.dab2[37:49,11,2]*mds.dv[37:49,11])/
#            sum(mds.dv[37:49,11]))
#    assert np.isclose(mds.dab2_line[11,2],n_at)
#
#def test_na_line_11_1(mds):
#    skip_ddn(mds)
#    n_at = (sum(mds.na[37:49,11,1]*mds.dv[37:49,11])/
#            sum(mds.dv[37:49,11]))
#    assert np.isclose(mds.na_line[11,1],n_at)
#
#def test_na_line_11_3(mds):
#    skip_ddn(mds)
#    skip_by_especies(mds, 4)
#    n_at = (sum(mds.na[37:49,11,3]*mds.dv[37:49,11])/
#            sum(mds.dv[37:49,11]))
#    assert np.isclose(mds.na_line[11,3],n_at)
