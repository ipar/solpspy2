"""
This might be the newst, more accurate version to generate data for
fixtures.
But I have already forgotten it.
"""

from __future__ import print_function
def main_func():
    import os
    from IPython import embed
    from solpspy import MdsData
    from solpspy import RundirData
    from solpspy import SolpsData
    import numpy as np
    import matplotlib.pyplot as plt
    import pdb

    # -------------------------------------------
    # TEST CASES:
    # -------------------------------------------
    shot = MdsData(94908) # Saved amds SOLPS5.0 for SOLPS-ITER run
    rundir = SolpsData('rundir_src/94908/run/')

    #shot = SolpsData(119603) # SOLPS-ITER ddnu magnetic geometry
    #rundir = SolpsData('rundir_src/119603/run/')
    # -------------------------------------------
    # -------------------------------------------

    if not os.path.exists('tmp_'+str(shot.shot)):
        os.makedirs('tmp_'+str(shot.shot))
    if not os.path.exists('tmp_'+str(shot.shot)+'_r'):
        os.makedirs('tmp_'+str(shot.shot)+'_r')

    for data, path in zip([shot, rundir],
            ['tmp_'+str(shot.shot), 'tmp_'+str(shot.shot)+'_r']):
        print("Creating data from {}".format(data._type))

        #Just for mds, for now
        if data._type.startswith('Mds'):
            np.save(path+'/user', data.user)
            np.save(path+'/dsrad', data.dsrad)
            np.save(path+'/dspar', data.dspar)
            np.save(path+'/resmo', data.resmo)

            np.save(path+'/targ4', data.targ4)
            np.save(path+'/targ1', data.targ1)

            np.save(path+'/directory', data.directory)

        #Just for mds, for now
        elif data._type.startswith('Rundir'):
            pass


        #For test_properties.py
        np.save(path+'/solpsversion', data.solpsversion)
        np.save(path+'/solps_version', data.solps_version)
        np.save(path+'/vessel', data.vessel)
        np.save(path+'/region', data.region)
        np.save(path+'/regflx', data.regflx)
        np.save(path+'/regfly', data.regfly)
        np.save(path+'/regvol', data.regvol)
        np.save(path+'/cr', data.cr)
        np.save(path+'/cr_y', data.cr_y)
        np.save(path+'/cz', data.cz)
        np.save(path+'/r', data.r)
        np.save(path+'/z', data.z)
        np.save(path+'/nx', data.nx)
        np.save(path+'/ny', data.ny)
        np.save(path+'/ns', data.ns)
        np.save(path+'/natm', data.natm)
        np.save(path+'/nmol', data.nmol)
        np.save(path+'/imp', data.imp)
        np.save(path+'/omp', data.omp)
        np.save(path+'/sep', data.sep)
        np.save(path+'/za', data.za)
        np.save(path+'/am', data.am)
        np.save(path+'/b', data.b)
        np.save(path+'/pitch', data.pitch)
        np.save(path+'/pitch_pt', data.pitch_pt)
        np.save(path+'/pitch_tot', data.pitch_tot)
        np.save(path+'/invpitch', data.invpitch)
        np.save(path+'/dv', data.dv)
        np.save(path+'/hx', data.hx)
        np.save(path+'/hy', data.hy)
        np.save(path+'/parallel_surface_m', data.parallel_surface_m)
        np.save(path+'/parallel_surface', data.parallel_surface)
        np.save(path+'/ipardspar', data.ipardspar)
        np.save(path+'/leftix', data.leftix)
        np.save(path+'/leftiy', data.leftiy)
        np.save(path+'/rightix', data.rightix)
        np.save(path+'/rightiy', data.rightiy)
        np.save(path+'/bottomix', data.bottomix)
        np.save(path+'/bottomiy', data.bottomiy)
        np.save(path+'/topix', data.topix)
        np.save(path+'/topiy', data.topiy)

        np.save(path+'/te', data.te)
        np.save(path+'/ti', data.ti)
        np.save(path+'/ne', data.ne)
        np.save(path+'/na', data.na)
        np.save(path+'/tab2', data.tab2)
        np.save(path+'/dab2', data.dab2)
        np.save(path+'/tmb2', data.tmb2)
        np.save(path+'/dmb2', data.dmb2)
        np.save(path+'/ua', data.ua)
        np.save(path+'/zeff', data.zeff)
        np.save(path+'/pstot', data.pstot)
        np.save(path+'/pdynd', data.pdynd)

        np.save(path+'/fna', data.fna)
        np.save(path+'/fna32', data.fna32)
        np.save(path+'/fna52', data.fna52)

        np.save(path+'/fnax32', data.fnax32)
        np.save(path+'/fnax52', data.fnax52)
        np.save(path+'/fnay32', data.fnay32)
        np.save(path+'/fnay52', data.fnay52)

        np.save(path+'/smo', data.smo)
        np.save(path+'/smq', data.smq)
        np.save(path+'/smav', data.smav)
        np.save(path+'/fmox', data.fmox)
        np.save(path+'/fmoy', data.fmoy)
        np.save(path+'/fnax', data.fnax)
        np.save(path+'/fnay', data.fnay)
        np.save(path+'/grid', data.grid)
        np.save(path+'/ds', data.ds)
        np.save(path+'/ixp', data.ixp)
        np.save(path+'/oxp', data.oxp)

        np.save(path+'/oul', data.oul)
        np.save(path+'/odl', data.odl)
        np.save(path+'/iul', data.iul)
        np.save(path+'/idl', data.idl)
        np.save(path+'/masks.outdiv', data.masks.outdiv)
        np.save(path+'/masks.inndiv', data.masks.inndiv)
        np.save(path+'/masks.notcore', data.masks.notcore)
        np.save(path+'/masks.sol', data.masks.sol)
        np.save(path+'/outdiv_nolim', data.outdiv_nolim)
        np.save(path+'/dab2_avg', data.dab2_avg)
        np.save(path+'/na_avg', data.na_avg)
        np.save(path+'/dmb2_avg', data.dmb2_avg)
        np.save(path+'/dab2_line', data.dab2_line)
        np.save(path+'/na_line', data.na_line)
        np.save(path+'/dmb2_line', data.dmb2_line)


        #test_time_traces

        np.save(path+'/out.ds', data.out.ds)
        np.save(path+'/out.ft', data.out.ft)
        np.save(path+'/out.fi', data.out.fi)
        np.save(path+'/out.te', data.out.te)
        np.save(path+'/out.ne', data.out.ne)
        np.save(path+'/out.ti', data.out.ti)
        np.save(path+'/out.po', data.out.po)
        np.save(path+'/out.fc', data.out.fc)

        np.save(path+'/inn.ds', data.inn.ds)
        np.save(path+'/inn.ft', data.inn.ft)
        np.save(path+'/inn.fi', data.inn.fi)
        np.save(path+'/inn.te', data.inn.te)
        np.save(path+'/inn.ne', data.inn.ne)
        np.save(path+'/inn.ti', data.inn.ti)
        np.save(path+'/inn.po', data.inn.po)
        np.save(path+'/inn.fc', data.inn.fc)



        np.save(path+'/outmp.ds', data.outmp.ds)
        np.save(path+'/outmp.te', data.outmp.te)
        np.save(path+'/outmp.ne', data.outmp.ne)
        np.save(path+'/outmp.ti', data.outmp.ti)

        np.save(path+'/innmp.ds', data.innmp.ds)
        np.save(path+'/innmp.te', data.innmp.te)
        np.save(path+'/innmp.ne', data.innmp.ne)
        np.save(path+'/innmp.ti', data.innmp.ti)


        if data._type == 'MdsDataDDNU':
            np.save(path+'/sep2', data.sep2)
            # Implement correctly after time traces of all the targets
            np.save(path+'/out2.ds', data.out2.ds)
            np.save(path+'/out2.ft', data.out2.ft)
            np.save(path+'/out2.fi', data.out2.fi)
            np.save(path+'/out2.te', data.out2.te)
            np.save(path+'/out2.ne', data.out2.ne)
            np.save(path+'/out2.ti', data.out2.ti)
            np.save(path+'/out2.po', data.out2.po)
            np.save(path+'/out2.fc', data.out2.fc)

            np.save(path+'/inn2.ds', data.inn2.ds)
            np.save(path+'/inn2.ft', data.inn2.ft)
            np.save(path+'/inn2.fi', data.inn2.fi)
            np.save(path+'/inn2.te', data.inn2.te)
            np.save(path+'/inn2.ne', data.inn2.ne)
            np.save(path+'/inn2.ti', data.inn2.ti)
            np.save(path+'/inn2.po', data.inn2.po)
            np.save(path+'/inn2.fc', data.inn2.fc)


        #Latest additions. Locate in better positions
        np.save(path+'/vessel_mesh', data.vessel_mesh)
        np.save(path+'/nnreg', data.nnreg)
        np.save(path+'/xpoint', data.xpoint)
        np.save(path+'/solps_code', data.solps_code)
        np.save(path+'/exp', data.exp)

        np.save(path+'/nncut', data.nncut)
        np.save(path+'/rightcut', data.rightcut)
        np.save(path+'/leftcut', data.leftcut)
        np.save(path+'/topcut', data.topcut)
        np.save(path+'/bottomcut', data.bottomcut)

        np.save(path+'/sy', data.sy)
        np.save(path+'/sx', data.sx)
        np.save(path+'/gs', data.gs)

        np.save(path+'/am_kg', data.am_kg)
        np.save(path+'/mi', data.mi)

        np.save(path+'/na_eirene', data.na_eirene)
        np.save(path+'/na_atom', data.na_atom)

        np.save(path+'/qz', data.qz)

        np.save(path+'/fractional_abundances', data.fractional_abundances)
        np.save(path+'/cimp', data.cimp)

        np.save(path+'/ni', data.ni)

        np.save(path+'/pdyna', data.pdyna)
        np.save(path+'/ptot', data.ptot)

        np.save(path+'/pressure_gradient_force', data.pressure_gradient_force)

        np.save(path+'/fhjx', data.fhjx)
        np.save(path+'/fhjy', data.fhjy)
        np.save(path+'/fhj', data.fhj)
        np.save(path+'/fhmx', data.fhmx)
        np.save(path+'/fhmy', data.fhmy)
        np.save(path+'/fhm', data.fhm)
        np.save(path+'/fhpx', data.fhpx)
        np.save(path+'/fhpy', data.fhpy)
        np.save(path+'/fhp', data.fhp)
        np.save(path+'/fhtx', data.fhtx)
        np.save(path+'/fhty', data.fhty)
        np.save(path+'/fht', data.fht)

        np.save(path+'/stored_energy', data.stored_energy)

        np.save(path+'/dperp', data.dperp)
        np.save(path+'/kyeperp', data.kyeperp)
        np.save(path+'/kyiperp', data.kyiperp)

        np.save(path+'/electric_field', data.electric_field)
        np.save(path+'/electric_force', data.electric_force)




        # Even later versions
        if data._type.startswith('Mds'):
            np.save(path+'/directory', data.directory)


        np.save(path+'/cr_x', data.cr_x)
        np.save(path+'/cz_x', data.cz_x)
        np.save(path+'/cz_y', data.cz_y)
        np.save(path+'/domp', data.domp)
        np.save(path+'/po', data.po)
        np.save(path+'/zn', data.zn)

        np.save(path+'/na_eirene', data.na_eirene)


        # Latest
        np.save(path+'/domp_parallel', data.domp_parallel)
        np.save(path+'/mi', data.mi)
        np.save(path+'/am_kg', data.am_kg)
        np.save(path+'/qz', data.qz)
        np.save(path+'/dib2', data.dib2)
        np.save(path+'/tib2', data.tib2)
        np.save(path+'/na_atom', data.na_atom)
        np.save(path+'/fractional_abundances', data.fractional_abundances)
        np.save(path+'/cimp', data.cimp)
        np.save(path+'/ni', data.ni)
        np.save(path+'/pdyna', data.pdyna)
        np.save(path+'/ptot', data.ptot)
        np.save(path+'/pressure_gradient_force', data.pressure_gradient_force)

        np.save(path+'/fhe', data.fhe)
        np.save(path+'/fhex', data.fhex)
        np.save(path+'/fhey', data.fhey)
        np.save(path+'/fhi', data.fhi)
        np.save(path+'/fhix', data.fhix)
        np.save(path+'/fhiy', data.fhiy)
        np.save(path+'/fhj', data.fhj)
        np.save(path+'/fhjx', data.fhjx)
        np.save(path+'/fhjy', data.fhjy)
        np.save(path+'/fhm', data.fhm)
        np.save(path+'/fhmx', data.fhmx)
        np.save(path+'/fhmy', data.fhmy)
        np.save(path+'/fhp', data.fhp)
        np.save(path+'/fhpx', data.fhpx)
        np.save(path+'/fhpy', data.fhpy)
        np.save(path+'/fht', data.fht)
        np.save(path+'/fhtx', data.fhtx)
        np.save(path+'/fhty', data.fhty)
        np.save(path+'/fch', data.fch)
        np.save(path+'/fchx', data.fchx)
        np.save(path+'/fchy', data.fchy)

        np.save(path+'/stored_energy', data.stored_energy)

        np.save(path+'/dperp', data.dperp)
        np.save(path+'/kyeperp', data.kyeperp)
        np.save(path+'/kyiperp', data.kyiperp)

        np.save(path+'/rpt', data.rpt)
        np.save(path+'/rqahe', data.rqahe)
        np.save(path+'/rqbrm', data.rqbrm)
        np.save(path+'/rqrad', data.rqrad)
        np.save(path+'/line_radiation', data.line_radiation)
        np.save(path+'/rrahi', data.rrahi)
        np.save(path+'/rramo', data.rramo)
        np.save(path+'/rrana', data.rrana)
        np.save(path+'/rsahi', data.rsahi)
        np.save(path+'/rsamo', data.rsamo)
        np.save(path+'/rsana', data.rsana)

        ##np.save(path+'/smaf', data.smaf)
        ##np.save(path+'/smag', data.smag)

        np.save(path+'/electric_field', data.electric_field)
        np.save(path+'/electric_force', data.electric_force)






    #embed()


if __name__ == '__main__':
     #Can be implemented as decorator, I think
     try:
        main_func()
     except:
        import pdb, traceback, sys
        type, value, tb = sys.exc_info()
        traceback.print_exc()
        pdb.post_mortem(tb)
