
@profile
def main():

    import MDSplus
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd

    import os
    import sys
    import re
    import sets
    import collections
    import logging
    import itertools
    import subprocess
    #from IPython import embed

    import solpspy as spy


if __name__ == '__main__':
    main()
