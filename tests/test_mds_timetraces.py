import os
import sys
import numpy as np
import pytest
from mockups import mds, skip_ddn, skip_sn, validity_mds

from solpspy.tools.tools import module_path


@pytest.mark.parametrize('name', ['targ1','targ4'])
def test_target_names(mds,name):
    assert validity_mds(mds, name)

@pytest.mark.parametrize('side', ['out','inn','out2','inn2'])
@pytest.mark.parametrize('name', ['ds','ft','fi','te','ne','ti','po','fc'])
def test_targets_timetraces(mds,side,name):
    if side == 'out2' or side == 'inn2':
        skip_sn(mds)
    assert validity_mds(mds, side+'.'+name)

@pytest.mark.parametrize('side', ['outmp','innmp'])
@pytest.mark.parametrize('name', ['ds','te','ne','ti'])
def test_midplanes_timetraces(mds,side,name):
    assert validity_mds(mds, side+'.'+name)


# Test derivated calculations
def test_ds_omp(mds):
    assert np.allclose(mds.outmp.ds, mds.ds[mds.omp])
def test_ds_imp(mds):
    assert np.allclose(mds.innmp.ds, mds.ds[mds.imp])
