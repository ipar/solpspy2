import os
import numpy as np
import pytest
from solpspy import *
from solpspy.tools.tools import module_path, yaml_loader

def runs_description():
    return yaml_loader(os.path.join(module_path(),'tests/runs_description.yaml'))


@pytest.fixture(scope="session", params=[94908, 119603])
#@pytest.fixture(scope="module", params=[94908, 119603])
def mds(request):
    valid_momentum_balance =['MdsData', 'RundirData']
    dummy = SolpsData(request.param)
    if dummy._type in valid_momentum_balance:
        dummy.momentum_balance(printing = False) #To be changed
    return dummy

@pytest.fixture(scope="session")
#@pytest.fixture(scope="module")
def rundir(mds):
    valid_momentum_balance =['MdsData', 'RundirData']
    dir = os.path.join(module_path(),
                  'tests/rundir_src/'+str(mds.nshot)+'/run')
    dummy = SolpsData(dir)
    if dummy._type in valid_momentum_balance:
        dummy.momentum_balance(printing = False) #To be changed
    return dummy






#Use global mds/rundir for now. If not possible, resort back to these and
#separated tests.

#@pytest.fixture(scope="module", params=[94908])
#def mds_sn(request):
#    dummy = MdsData(request.param)
#    dummy.momentum_balance(printing = False) #To be changed
#    return dummy
#
#@pytest.fixture(scope="module")
#def rundir_sn(mds_sn):
#    dir = os.path.join(module_path(),
#                  'tests/rundir_src/'+str(mds_sn.nshot)+'/run')
#    dummy = RundirData(dir)
#    dummy.momentum_balance(printing = False) #To be changed
#    return dummy
#
#
#
#
#
#@pytest.fixture(scope="module", params=[119603])
#def mds_ddn(request):
#    dummy = MdsDataDDNU(request.param)
#    return dummy
#
#@pytest.fixture(scope="module")
#def rundir_ddn(mds_ddn):
#    dir = os.path.join(module_path(),
#                  'tests/rundir_src/'+str(mds_ddn.nshot)+'/run')
#    dummy = RundirDataDDNU(request.param)
#    return dummy






#--------------------- Validation tools -----------------------------------
def validity_crosscheck(mds, rundir, var, var2=None):
    try:
        if var2 is None:
            var2 = var
        mdsvar = getattr(mds,var)
        runvar = getattr(rundir,var2)
    except:
        try:
            var = var.split('.')
            if var2 is None:
                var2 = var
            else:
                var2 = var2.split('.')
            mdsvar = getattr(getattr(mds,var[0]),var[1])
            runvar = getattr(getattr(rundir,var2[0]),var2[1])
        except:
            return False
    try:
        return np.allclose(mdsvar,runvar)
    except:
        try:
            return (mdsvar == runvar).all()
        except:
            try:
                return (mdsvar == runvar)
            except:
                return False


def validity_mds(mds,var):
    mds_var_npy = os.path.join(module_path(),
                  'tests/mds_npy/'+str(mds.nshot)+'/'+var+'.npy')
    dummy = np.load(mds_var_npy)
    try:
        numvar = getattr(mds,var)
    except:
        try:
            var = var.split('.')
            numvar = getattr(getattr(mds,var[0]),var[1])
        except:
            return False

    if numvar is None:
        try:
            return (dummy == np.array(None))
        except:
            return False
    else:
        try:
            return np.allclose(numvar,dummy)
        except:
            try:
                return (numvar == dummy).all()
            except:
                try:
                    return (numvar == dummy)
                except:
                    return False





#--------------------- Skipping   tools -----------------------------------
def skip_by_species(sim, ns):
    if sim.ns < ns:
        pytest.skip('Number of fluid species is just {}.'.format(str(sim.ns)))

def skip_by_especies(sim, natm):
    if sim.natm < natm:
        pytest.skip('Number of Eirene species is just {}.'.format(str(sim.natm)))

def skip_b2standalone(sim):
    if sim.natm is None and sim.nmol is None and sim.nion is None:
        pytest.skip('B2standalone simulation.')


def skip_ddn(sim):
    ddn_types =['MdsDataDDNU', 'RundirDataDDNU']
    if sim._type in ddn_types:
        pytest.skip('Test not for DDN simulations.')

def skip_sn(sim):
    ddn_types =['MdsDataDDNU', 'RundirDataDDNU']
    if sim._type not in ddn_types:
        pytest.skip('Test only for DDN features.')


def skip_no_mdsplus_module(sim):
    try:
        import MDSplus
        #sys.modules['MDSplus']
    except:
        pytest.skip('Test only for DDN features.')
