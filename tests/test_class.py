import numpy as np
import pytest
from mockups import rundir, mds
from mockups import runs_description



#def test_types(mds,rundir):
#    assert

def test_mds_type(mds):
    assert (mds._type.startswith('MdsData'))
def test_rundir_type(rundir):
    assert (rundir._type.startswith('RundirData'))

def test_magnetic_geometry_mds(mds):
    assert (runs_description()[mds.shot]['magnetic geometry'].lower() ==
            mds._magnetic_geometry.lower())

def test_magnetic_geometry_rundir(rundir):
    assert (runs_description()[rundir.shot]['magnetic geometry'].lower() ==
            rundir._magnetic_geometry.lower())
