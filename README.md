# SOLPSpy2

Python 2.7 version. This version is currently deprecated and may need some modifications to work again.

Python tools to deal effectively with SOLPS5.0 and SOLPS-ITER data stored in the MDS+ server and/or run directories.

This package is shared the aim to create a community based post-processing tool.
However, this is not an official SOLPS routine. The developers have tried to balance simplicity for the users with completness.
Do not use this routine with blind acceptance. Always keep an eye for posisble bugs and report them to the developer.

## Workflow glance

A general class SolpsData (recommended) can be used for both MDS+ shotnumbers and run directories.

```python
from solpspy import SolpsData

#Given an MDS+ shotnumber
shot = SolpsData(86514)

#Assuming I am in the directory that I want to analyze
rundir = SolpsData('.')
#Or given a path to the directory, relative or absolute
rundir2 = SolpsData(your_path)

#Examples of SolpsData variables
shot.te #Electron temperature numpy array (nx,ny)
rundir.ti #Ion temperature numpy array (nx,ny)
rundir2.na #Fluid B2.5 species density, (nx,ny,ns).
```

RundirData is intended to be a superset of MdsData. That means that all the variables and public functions defined for MdsData should also be availablefor RundirData, while the reverse is not true.
When an MDS+ data entry is formed, new quantities are created with post-processing routines of SOLPS. The developers have tried to reproduce some of those quantities, but not all of them, thus RundirData is not strictly a superset.

A list of quantities and functions is not available yet. Users should check the corresponding classes implementations
```
solpspy/classes/mds.py
solpspy/classes/rundir.py
```
or try to use 
```
shot.help("te") # Shows the docstring of the self.te property.
shot.find("temperature") # Parses all the available top level docstrings in search for the regular expression.
```
These methods are not yet recommended, since they only work for the high level docstrings yet. For example:
```
shot.help("out.te") # It won't go into the out namespace to fetch the docstring 
shot.find("temperature") # It won't show matches such as self.out.te.
```

A non-exhaustive list of features, assuming that shot is the SolpsData instance:

Indices:
 - shot.omp, shot.imp: Poloidal indices of the outer and inner midplanes.
 - shot.sep, (shot.sep2): Radial index of the FIRST flux tube in the SOL (and the secondary SOL, e.g., in a Double null configuration.)
 - shot.iout, shot.iinn, (shot.iout2, shot.iinn2): Poloidal indices of the outer, inner, (secondary outer and secondary inner) targets. NOT the guarding cells. E.g., in a DDNU, the secondary targets are the lower targets.

Namespaces:
 - shot.outmp, shot.innmp: Outer and inner midplanes times.nc and traces.nc data, usually of dimensions (nt, ny).
 - shot.out, shot.inn, (shot.out2, shot.inn2): Data of times.nc and traces.nc in the outer and inner targets (and secondary targets).
 - shot.eqout: Variables extracted from the internal operations of the SOLPS code, which are contained in the output folder of a run directory (Not available in MDS+).

Routines and functions:
 - shot.gradient(shot.te): It returns the poloidal, radial and parallel gradient of shot.te.
 - shot.plot.grid_data(shot.te, **kwargs): This will create the figure of shot.te in the corresponding physical grid. Use matplotlib.plt.show() to see it.
 - shot.chords : Will plot the quantity along the given line. I have to investigate this class more to fully understand its internal workings. It also seems to be working only for rundir cases and not MDS+ cases. As an example:
    ```python
         rundir.chords['RIN-2'].plot(var      = rundir.ne,
                               title    = "Electron Density",
                               ylabel   = "$n_e$ [$10^{19}\mathrm{m}^{-3}$]",
                               factor   = 1e19,
                               save_as  = 'RIN-2_ne.pdf')

    ```

### Non LSN cases:
SolpsData should be able to handle LSN, USN and DDNU SOLPS configurations. This means that the indices like that of the OMP or the outer divertor are correctly set. This is important since the order of indices is reversed in USN runs and the upper target indices are located in the middle of the grid for DDNU.


### Alternative usage of base classes
If, for some reason, there are problems with an specific instance or one needs to use the base classes:

An MDS+ case:

```python
from solpspy import MdsData

#Given an MDS+ shotnumber
shot = MdsData(86514)
shot.te #Electron temperature numpy array (nx,ny)

```

A SOLPS run directory:

```python
from solpspy import RundirData

#Assuming I am in the directory that I want to analyze
rundir = RundirData('.')

#Or given a path to the directory, relative or absolute
rundir2 = RundirData(your_path)
```



## Installation
### TOK cluster
Set path to the parent directory of the clone in the PYTHONPATH var.
That can be automatically done by adding something like:
```
setenv PYTHONPATH ${PYTHONPATH}:/afs/ipp-garching.mpg.de/home/i/ipar/repository/python
```
to your .public/user.cshrc.usr


IMPORTANT: In order to be able to use the MDSplus package, you have to:
```
module load mdsplus
source $MDSPLUS_DIR/setup.csh
```

The pytest package is necessary for testing, which is strongly recommended even for non-developers. In the TOK cluster, packages must be installed in the user space:
```
pip install --user pytest
```

### Other clusters
Talk with an expert user and try to find the equivalent of the required


## Requirements
### Standard python packages
 - numpy
 - scipy
 - matplotlib
### Specific or harder to find packages
 - MDSplus

### For testing
 - pytest




## Testing
Once pytest is available, the test can be run inside solpspy/ or solpspy/tests/ folder with:
```
pytest
```
The usual options of pytest are available:
```
pytest -x # Stops after the first problem is found
pytest -v # Verbose mode, in which names of files and tests are shown
pytest -xv # Combinations of flags are, of course, possible
```

In order to test the coverage of the test suite, one needs to be located inside solpspy/tests/ and run:
```
source coverage.sh # or ./coverage.sh
```
which will create the required coverage files. Then, in order to visualize the results,
two tools can be found in solpspy/tests/:
```
python report_coverage_console.py # Prints the results in the console. Basic output.

python report_coverage_html.py # Uses Firefox to show the same output of console
                               # but also displays the information in the source code.
                               # Advanced output.
```




## List of authors
 - Ivan Paradela Perez, original author and general developer
 - Ferdinand Hitzler, main author of chords and spectroscopy
