#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    Class for grid plots.
"""

# Common modules
from __future__ import print_function
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.patches import Polygon,Arrow
from matplotlib.collections import PatchCollection

# Special modules
from solpspy.tools.tools import priority, LinAlg2d
from solpspy.plots.plotting_tools import shifted_color_map,savefig,convert_figsize

# =============================================================================

class GridPlotBase(object):
    """
    Base class for grid plots ... Subclasses must have a few certain properties!
    """
    def _init0(self,**kwargs):

        self.title           = priority('title'               , kwargs, "")
        self.label           = priority(['label','clabel',
                                         'zlabel']            , kwargs, "")
        self.xlim            = priority('xlim'                , kwargs, None)
        self.ylim            = priority('ylim'                , kwargs, None)
        self.zoom            = priority('zoom'                , kwargs, None)
        self.xlabel          = priority('xlabel'              , kwargs, 'R [m]')
        self.ylabel          = priority('ylabel'              , kwargs, 'z [m]')
        self.clim            = priority(['zlim','clim']       , kwargs, None)
        self.cfactor         = priority(['zfactor','cfactor'] , kwargs, 1.0)
        self.cbar            = priority('cbar'                , kwargs, True)
        self.cbar_nbins      = priority('cbar_nbins'          , kwargs, None)
        self.log             = priority('log'                 , kwargs, False)
        self.cmap            = priority('cmap'                , kwargs, plt.cm.jet)
        self.cmap_center     = priority(['cmc','cmap_center'] , kwargs, None)
        self.sep             = priority('sep'                 , kwargs, True)
        self.sep_col         = priority('sep_col'             , kwargs, 'k')
        self.sep_lw          = priority('sep_lw'              , kwargs, 1.0)
        self.ves_colorcode   = priority(['ves_colorcode','vc'], kwargs, None)
        self.ves_linewidth   = priority(['ves_linewidth','vl'], kwargs, None)
        self.text            = priority('text'                , kwargs, [])
        self.annotate        = priority('annotate'            , kwargs, [])
        self.plot_grid       = priority(['grid','plot_grid']  , kwargs, False)
        self.cellsurface     = priority('cellsurface'         , kwargs, [])
        self.ax              = priority('ax'                  , kwargs, None)
        self.fontsize        = priority('fontsize'            , kwargs, None)
#       self.figsize         = priority('figsize'             , kwargs, (9,11))
        self.figsize         = priority('figsize'             , kwargs, (6.3,8))
        self.tb              = priority(['tb','transpbckg']   , kwargs, None)
        self.keep_axis_ratio = priority('keep_axis_ratio'     , kwargs, True)
        self.showplot        = priority(['showplot',
                                         'show_plot']         , kwargs, True)
        self.saveas          = priority(['saveas','save_as']  , kwargs, None)
        self.block           = priority('block'               , kwargs, False)

        self.var = self._prepVar()

    def _prepVar(self):
        if (self.var is None or 
            (type(self.var) is str and self.var.lower() == 'grid')):
            if self.plot_grid == False:
                self.plot_grid = True
            return np.ones((self.run.nx,self.run.ny))*np.nan
        elif type(self.var) is str:
            try:
                self.var = self.run.__getattribute__(self.var)
            except:
                raise Exception("Error: could not retrieve var: '" + self.var + "'")
        return self.var.copy()
        # Maybe also check if the dimensions are correct?!
        # No! Different shapes are possible for the VectorPlot!!!

    def _initialize_figure(self):
        if self.fontsize:
            matplotlib.rcParams.update({'font.size': self.fontsize})
        if self.ax is None:
            self.figsize = convert_figsize(self.figsize)
            self.fig     = plt.figure(figsize=self.figsize)
            self.ax      = self.fig.add_subplot(1,1,1)

    def _plot_vessel(self):

        # Defaults:
        vessel_color  = {i: 'k' for i in xrange(self.run.vessel.shape[1])}
        color_coding  = {2:'0.4', # 'r',
                         3:'0.4', # '0.8',
                         4:'0.4', # 'm',
                         5:'0.4', # 'b',
                         6:'0.4', # '0.5' # 6:'c'
                         }
        ves_linewidth = {i: 1 for i in xrange(self.run.vessel.shape[1])}

        # User input modifications:
        if self.ves_colorcode is not None and type(self.ves_colorcode) is dict:
            color_coding.update(self.ves_colorcode)
        if self.ves_linewidth is not None and type(self.ves_linewidth) is dict:
            ves_linewidth.update(self.ves_linewidth)

        try:
            vc     = self.run.vessel_colors
            colors = {i : color_coding.get(c,'k') for i,c in vc.items()}
            vessel_color.update(colors)
        except:
            pass

        for i in xrange(self.run.vessel.shape[1]):
            if vessel_color[i] is None or vessel_color[i] == 'transparent':
                continue
            R = np.array([self.run.vessel[0,i],self.run.vessel[2,i]])
            z = np.array([self.run.vessel[1,i],self.run.vessel[3,i]])
            self.ax.plot(R,z,vessel_color[i],linewidth=ves_linewidth[i])

    def _plot_grid(self):
        if not self.plot_grid:
            return
        grid_kwargs = {'linewidth' : 0.05,
                       'edgecolor' : '0.5',
                       'fill'      : False}
        if type(self.plot_grid) is dict:
            grid_kwargs.update(self.plot_grid)
        for nx in range(self.run.nx):
            for ny in range(self.run.ny):
                self.ax.add_patch(Polygon(self.run.grid[nx,ny],closed=True,
                                          **grid_kwargs))

    def _plot_separatrix(self):
        separatrix = np.zeros((self.run.nx,2))
        from solpspy.tools.tools import cellface
        for nx in range(self.run.nx):
            separatrix[nx] = cellface(self.run.grid[nx,self.run.sep],
                                      self.run.grid[nx,self.run.sep-1])[0]
        self.ax.plot(separatrix[:,0],separatrix[:,1],
                     color  = self.sep_col,
                     lw     = self.sep_lw,
                     zorder = 11)

    def _plot_data(self):
        if 'vector' in str(type(self)).lower():
            self.patch_collection = PatchCollection(self.patches,zorder=10)
        else:
            self.patch_collection = PatchCollection(self.patches)

        if type(self.clim)==str:
            try:
                gridmask = self.run.masks.__getattribute__(self.clim)
            except AttributeError as error:
                raise Exception("Error: invalid clim: '" + self.clim + "' ("+str(error)+")")
            i = -1
            v = []
            for nx,ny in self.nxny_pairs:
                i += 1
                if gridmask[nx,ny]:
                    v.append(self.pvalues[i])
            self.clim = [min(v),max(v)]

        if self.clim is not None:
            self.patch_collection.set_clim(self.clim)
        if self.log:
            if self.clim is not None:
                self.patch_collection.set_norm(colors.LogNorm(vmin = self.clim[0], 
                                                              vmax = self.clim[1]))
            else:
                self.patch_collection.set_norm(colors.LogNorm())

        # Adjust cmap if self.cmap_center:
        if self.cmap_center is not None:
            mn = np.min(self.pvalues/self.cfactor) if self.clim is None else self.clim[0]
            mx = np.max(self.pvalues/self.cfactor) if self.clim is None else self.clim[1]
            center_fraction = (self.cmap_center-mn)/(mx-mn)
            cmap = shifted_color_map(self.cmap, midpoint=center_fraction)
        else:
            cmap = self.cmap

        if 'vector' in str(type(self)).lower():
            self.patch_collection.set(array=self.pvalues/self.cfactor,cmap=cmap)
        else:
            self.patch_collection.set(array=self.pvalues/self.cfactor,
                                      cmap=cmap,linewidth=0.0)


        self.patch_collection.set_edgecolor('face')
        self.ax.add_collection(self.patch_collection)

    def _plot_colorbar(self):
        # Add colorbar:
        if not self.cbar:
            return
        cbkwargs = {}
        if self.clim:
            cbkwargs['extend'] = 'both'
        if type(self.cbar) is dict:
            cbkwargs.update(self.cbar)
        cbar = plt.colorbar(self.patch_collection,**cbkwargs)
        if self.label:
            cbar.set_label(self.label, labelpad=10)
        if self.cbar_nbins:
            from matplotlib import ticker
            tick_locator = ticker.MaxNLocator(nbins=self.cbar_nbins)
            cbar.locator = tick_locator
            cbar.update_ticks()

    def _set_plot_limits(self):
        if type(self.zoom) is str and self.zoom.lower().startswith('o'):
            self.xlim = [1.43,1.7]
            self.ylim = [-1.2,-0.85]
        elif type(self.zoom) is str and self.zoom.lower().startswith('i'):
            self.xlim = [ 1.149,1.45]
            self.ylim = [-1.116,-0.6]
        elif type(self.zoom) is str and self.zoom.lower().startswith('d'):
            self.xlim = [1.21,1.67]
            self.ylim = [-1.2,-0.45]

        # Get default plot limits:
        xmin=self.run.grid[:,:,:,0].min()
        xmax=self.run.grid[:,:,:,0].max()
        ymin=self.run.grid[:,:,:,1].min()
        ymax=self.run.grid[:,:,:,1].max()
        dy = (ymax - ymin)*0.05
        dx = (xmax - xmin)*0.05
        xmin=xmin-dx
        xmax=xmax+dx
        ymin=ymin-dy
        ymax=ymax+dy
        if self.xlim is not None:
            xmin=self.xlim[0]
            xmax=self.xlim[1]
        if self.ylim is not None:
            ymin=self.ylim[0]
            ymax=self.ylim[1]
        if self.keep_axis_ratio:
            self.ax.axis('equal')
            self.ax.axis([xmin, xmax, ymin, ymax])
        else:
            # Better control over [xy]lim, but figsize has to be adjusted
            self.ax.axes.set_aspect('equal')
            self.ax.set_ylim([ymin,ymax])
            self.ax.set_xlim([xmin,xmax])

    def _add_labels_and_title(self):
        # Title and x/y-labels:
        self.ax.set_title(self.title)
        self.ax.set_xlabel(self.xlabel)
        self.ax.set_ylabel(self.ylabel)
        if self.xlabel is '':
            self.ax.get_xaxis().set_ticks([])
        if self.ylabel is '':
            self.ax.get_yaxis().set_ticks([])

        # Insert text:
        if type(self.text) is not list:
            self.text = [self.text]
        for text in self.text:
            self.ax.text(**text)

        # Insert annotations:
        if type(self.annotate) is not list:
            self.annotate = [self.annotate]
        for annotate in self.annotate:
            self.ax.annotate(**annotate)

        # Highlight cell surfaces:
        if type(self.cellsurface) is not list:
            self.cellsurface = [self.cellsurface]
        for cs in self.cellsurface:
            cs.draw_cs()

    def _show_plot(self):
        # Show plot:
        plt.tight_layout()
        self.saveas = savefig(self.saveas,transp_bckg=self.tb)
	if self.showplot: plt.show(block=self.block)
        if self.block:    plt.close()

        # Reset self.ax, if it is supposed to be plotted again later:
        self.ax = None

# =============================================================================

class GridPlot(GridPlotBase):
    def __init__(self,solpspy_run,var,**kwargs):
        self.run = solpspy_run
        self.var = var
        # General Profile Initialization:
        self._init0(**kwargs)                     # (method in mother class)
        
        # Prepare data:
        self._prepare_data()

    def plot(self):
        # Create & show plot:
        self._plot()
        self._show_plot()            # (method in mother class)

    def _plot(self):
        # Create plot:
        self._initialize_figure()    # (method in mother class)
        self._plot_vessel()          # (method in mother class)
        self._plot_grid()            # (method in mother class)
        self._plot_data()            # (method in mother class)
        self._plot_colorbar()        # (method in mother class)
        self._plot_separatrix()      # (method in mother class)
        self._set_plot_limits()      # (method in mother class)
        self._add_labels_and_title() # (method in mother class)

    def _prepare_data(self):
        grid_cells = []
        grid_data  = []
        nxny_pairs = []
        for nx in range(self.run.nx):
            for ny in range(self.run.ny):
                if np.isnan(self.var[nx,ny]):
                    continue
                nxny_pairs.append((nx,ny))
                grid_cells.append(Polygon(self.run.grid[nx,ny]))
                grid_data.append(self.var[nx,ny])
        self.nxny_pairs = nxny_pairs
        self.patches    = grid_cells[:]       # Naming required in mother class
        self.pvalues    = np.array(grid_data) # Naming required in mother class

# =============================================================================

class CompPlot(GridPlotBase):
    def __init__(self,solpspy_run,var,**kwargs):
        self.run = solpspy_run
        self.var = var
        # General Profile Initialization:
        self._init0(**kwargs)                     # (method in mother class)
        # Computational domain specific defaults:
        self._axshift  = priority('_axshift' , kwargs, 0.5)
        self.xlabel    = priority('xlabel'   , kwargs, 'x cell index')
        self.ylabel    = priority('ylabel'   , kwargs, 'y cell index')
        self.xlim      = priority('xlim'     , kwargs, [0          -self._axshift,
                                                        self.run.nx-self._axshift])
        self.ylim      = priority('ylim'     , kwargs, [0          -self._axshift,
                                                        self.run.ny-self._axshift])
        self.plot_grid = priority('plot_grid', kwargs, True)
        self.figsize   = priority('figsize'  , kwargs, (8,6))

        # Prepare data:
        self._prepare_data()

    def plot(self):
        # Create & show plot:
        self._plot()
        self._show_plot()            # (method in mother class)

    def _plot(self):
        # Create plot:
        self._initialize_figure()    # (method in mother class)
        self._plot_data()            # (method in mother class)
        self._plot_grid()
        self._plot_regions()
        self._plot_colorbar()        # (method in mother class)
        self._set_plot_limits()
        self._add_labels_and_title() # (method in mother class)

    def _prepare_data(self):
        grid_cells = []
        grid_data  = []
        nxny_pairs = []
        for nx in range(self.run.nx):
            for ny in range(self.run.ny):
                if np.isnan(self.var[nx,ny]):
                    continue
                nxny_pairs.append((nx,ny))
                nxs = nx-self._axshift
                nys = ny-self._axshift
                grid_cells.append(Polygon([[nxs  ,nys  ],[nxs+1,nys  ],
                                           [nxs+1,nys+1],[nxs  ,nys+1]]))
                grid_data.append(self.var[nx,ny])
        self.nxny_pairs = nxny_pairs
        self.patches    = grid_cells[:]       # Naming required in mother class
        self.pvalues    = np.array(grid_data) # Naming required in mother class

    def _plot_grid(self):
        # Overwrites GridPlotBase._plot_grid()
        if not self.plot_grid:
            return
        for nx in range(self.run.nx):
            for ny in range(self.run.ny):
                nxs = nx-self._axshift
                nys = ny-self._axshift
                self.ax.add_patch(Polygon([[nxs  ,nys  ],[nxs+1,nys  ] ,
                                           [nxs+1,nys+1],[nxs  ,nys+1]],
                                          fill=False,linewidth=0.1,edgecolor='0.9'))
    def _plot_regions(self):
        # Separatrix:
        self.ax.plot([0           -self._axshift,self.run.ixp-self._axshift],
                     [self.run.sep-self._axshift,self.run.sep-self._axshift],
                     color='black',lw=2.0,zorder=11,ls=':')
        self.ax.plot([self.run.oxp+1-self._axshift,self.run.nx -self._axshift],
                     [self.run.sep  -self._axshift,self.run.sep-self._axshift],
                     color='black',lw=2.0,zorder=11,ls=':')
        self.ax.plot([self.run.ixp-self._axshift,self.run.oxp+1-self._axshift],
                     [self.run.sep-self._axshift,self.run.sep  -self._axshift],
                     color='black',lw=2.0,zorder=11,ls='--')
        # Inner Divertor Entrance:
        self.ax.plot([self.run.ixp-self._axshift,self.run.ixp-self._axshift],
                     [self.run.sep-self._axshift,self.run.ny -self._axshift],
                     color='black',lw=2.0,zorder=11,ls='--')
        self.ax.plot([self.run.ixp-self._axshift,self.run.ixp-self._axshift],
                     [0           -self._axshift,self.run.sep-self._axshift],
                     color='black',lw=2.0,zorder=11)
        # Outer Divertor Entrance:
        self.ax.plot([self.run.oxp+1-self._axshift,self.run.oxp+1-self._axshift],
                     [self.run.sep  -self._axshift,self.run.ny   -self._axshift],
                     color='black',lw=2.0,zorder=11,ls='--')
        self.ax.plot([self.run.oxp+1-self._axshift,self.run.oxp+1-self._axshift],
                     [0             -self._axshift,self.run.sep  -self._axshift],
                     color='black',lw=2.0,zorder=11)
        # Outer midplane:
        self.ax.plot([self.run.omp+0.5-self._axshift,self.run.omp+0.5-self._axshift],
                     [0               -self._axshift,self.run.ny     -self._axshift],
                     color='black',lw=1.0,zorder=11,ls=':')
        self.ax.plot([self.run.imp+0.5-self._axshift,self.run.imp+0.5-self._axshift],
                     [0               -self._axshift,self.run.ny     -self._axshift],
                     color='black',lw=1.0,zorder=11,ls=':')

    def _set_plot_limits(self):
        # Overwrites GridPlotBase._set_plot_limits()
        self.ax.set_xlim(self.xlim)
        self.ax.set_ylim(self.ylim)

# =============================================================================

class VectorPlot(GridPlotBase):
    """Creates a 2d plot of e.g. a vector field using colored arrows/vectors.
    
    Parameters
    ----------
        solpspy_run : solpspy run instance

        var : np.array(nx,ny) or np.array(nx,ny,2)
            The variable to be plotted. For the dirxy=='xy' option these
            have to be values where the vectors can be added, like, e.g.
            a force, or a flux density - but not a total flux!

        dirxy : string ('x' or 'y' or 'xy')
            The direction of the specified variable

        title : string
            Title written above the plot

        label : string
            The label of the color bar

        xlim : list (containing: two floats: [min,max])
        ylim : list (containing: two floats: [min,max])
        clim : list (containing: two floats: [min,max])
            The plot limits/ranges in x, y and c direction

        width : float
            Width of the vectors

        length : float
            The length of the vectors is automatically adjusted such,
            that they fit in the cell. With this parameter the length
            can additionally be adjusted.

        log : bool
            To switch the z axis to a logarithmic scale

        cmap : matplotlib.colors.LinearSegmentedColormap
            Color Map for the color coding of the vectors

        map_to_cell_center : bool
            Map values which are stored on the cell faces to the cell centers

        cfd : bool
            Calculate flux density! (Use this if var is a total flux.)

    """
    def __init__(self,solpspy_run,var,**kwargs):

        self.run = solpspy_run
        self.var = var

        # General Profile Initialization:
        self._init0(**kwargs)                     # (method in mother class)

        # Vector Plot Specific Initialization & Preparation:
        self._initVectorPlot0(**kwargs)

        # More Vector Plot Specific Preparations:
        self._initVectorPlot1()

        # Prepare data:
        self._calculate_vectors()

    def _initVectorPlot0(self,**kwargs):

        # Special default color map for the vector plots:
        cm = colors.LinearSegmentedColormap.from_list("", 
                                                      ["white",
                                                       "aliceblue",
                                                       "lightskyblue",
                                                       "skyblue",
                                                       "lime",
                                                       "yellow",
                                                       "red",
                                                       "darkred"])

        self.dirxy      = priority(['dir','dirxy','direction']   , kwargs, "xy")
        self.width      = priority('width'                       , kwargs, 0.02)
        self.length     = priority('length'                      , kwargs, 0.9)
        self.cmap       = priority('cmap'                        , kwargs, cm)
        self.mtcc       = priority(['mtcc' ,'map_to_cell_center'], kwargs, False)
        self.cfd        = priority('cfd'                         , kwargs, False)
        self.plot_grid  = priority(['grid','plot_grid']          , kwargs, True)
        self.sep_lw     = priority('sep_lw'                      , kwargs, 2.0)

    def _initVectorPlot1(self):

        # Prepare self.var for VectorPlot:
        if self.mtcc:
            self.var = self.run.map_cell_faces_to_center(self.var,
                                                         direction=self.dirxy)
        # Check shape of var:
        if (self.var.shape != (self.run.nx,self.run.ny) and
            self.var.shape != (self.run.nx,self.run.ny,2)):
            raise Exception("Error: invalid shape pf 'var' for vectorPlot()",
                            "with direction '" + self.dirxy + "': "+str(self.var.shape))
        if self.dirxy == 'xy' and self.var.shape != (self.run.nx,self.run.ny,2):
            raise Exception("Error: invalid shape pf 'var' for vectorPlot()",
                            "with direction '" + self.dirxy + "': "+str(self.var.shape))

        if self.dirxy == 'x':
            if self.var.shape == (self.run.nx,self.run.ny,2):
                self.var = self.var[:,:,0]
            self.varx = self.var
            self.vary = np.zeros(self.var.shape)
        elif self.dirxy == 'y':
            if self.var.shape == (self.run.nx,self.run.ny,2):
                self.var = self.var[:,:,1]
            self.varx = np.zeros(self.var.shape)
            self.vary = self.var
        elif self.dirxy == 'xy':
            self.varx = self.var[...,0]
            self.vary = self.var[...,1]

        if self.cfd:
            self.varx = self.varx / self.run.sx
            self.vary = self.vary / self.run.sy

    def plot(self):
        # Create & show plot:
        self._plot()
        self._show_plot()            # (method in mother class)

    def _plot(self):
        # Create plot:        
        self._initialize_figure()    # (method in mother class)
        self._plot_vessel()          # (method in mother class)
        self._plot_grid()            # (method in mother class)
        self._plot_data()            # (method in mother class)
        self._plot_colorbar()        # (method in mother class)
        self._plot_separatrix()      # (method in mother class)
        self._set_plot_limits()      # (method in mother class)
        self._add_labels_and_title() # (method in mother class)

    def _calculate_vectors(self):

        vectors    = []
        vecvals    = []
        nxny_pairs = []

        # Simple version:
        # (In each cell an arrow in the center of the cell
        #  with dimensions that is just fits into the cell)

        for nx in range(1,self.run.nx-1):     # (Skipping Guard Cells)
            for ny in range(1,self.run.ny-1): # (Skipping Guard Cells)

                # Calculate value of vector:
                val = np.sqrt(self.varx[nx,ny]**2 + self.vary[nx,ny]**2)
                if np.isnan(val) or val == 0:
                    continue

                # Cell center:
                cc = [self.run.cr[nx,ny],self.run.cz[nx,ny]]

                # Grid Vertex Indizes:
                #
                #   3       2          
                #  -|-------|-     y ^ 
                #   |       |        |
                #  -|-------|-       |---->
                #   0       1             x

                # Calculate normalized direction:
                # x direction inside of the cell:
                ex = (self.run.grid[nx,ny,2] + 
                      self.run.grid[nx,ny,1] - 
                      self.run.grid[nx,ny,3] - 
                      self.run.grid[nx,ny,0]) / 2
                # y direction inside of the cell:
                ey = (self.run.grid[nx,ny,2] + 
                      self.run.grid[nx,ny,3] - 
                      self.run.grid[nx,ny,0] - 
                      self.run.grid[nx,ny,1]) / 2
                # Vector direction:
                dirxy = self.varx[nx,ny]*ex + self.vary[nx,ny]*ey
                dirxy = dirxy/np.linalg.norm(dirxy)

                # Calculate vector length:
                # First define the boundary lines/cell faces:
                faces = [LinAlg2d.Line(self.run.grid[nx,ny,1],
                                       self.run.grid[nx,ny,2]),
                         LinAlg2d.Line(self.run.grid[nx,ny,0],
                                       self.run.grid[nx,ny,3]),
                         LinAlg2d.Line(self.run.grid[nx,ny,2],
                                       self.run.grid[nx,ny,3]),
                         LinAlg2d.Line(self.run.grid[nx,ny,0],
                                       self.run.grid[nx,ny,1])]
                # Find the intersections of the boundaries with the vector:
                vec = LinAlg2d.LineDir(cc,dirxy)
                ints = []
                for f in faces:
                    int = LinAlg2d.find_intersection(vec,f)
                    if int is None:
                        # Parallel lines, no intersection!
                        continue
                    # Only consider the two relevant
                    # intersections inside of the cell:
                    if LinAlg2d.point_inside_cell(self.run.grid[nx,ny],int):
                        ints.append(int)
                # Calculate the distance:
                d = LinAlg2d.calculate_distance(ints[0],ints[1])
                dirxy = d*dirxy*self.length

                # Shift vector to cell center:
                pos = cc-dirxy/2

                # Create Arrow:
                vectors.append(Arrow(pos  [0],pos  [1],
                                          dirxy[0],dirxy[1],
                                          self.width))
                vecvals.append(val)
                nxny_pairs.append((nx,ny))

        self.nxny_pairs = nxny_pairs
        self.patches    = vectors[:]        # Naming required in mother class
        self.pvalues    = np.array(vecvals) # Naming required in mother class

#        # TODO:
#        # Advanced version:
#        # (Combine small cells in such a way, that all vectors have
#        #  approximately the same dimensions, without loosing to
#        #  much detail.)
#        pass

# =============================================================================

class CompVectorPlot(CompPlot):

    def __init__(self,solpspy_run,var,**kwargs):

        self.run = solpspy_run
        self.var = var

        # General Profile Initialization:
        self._init0(**kwargs)                     # (method in mother class)

        # Vector Plot Specific Initialization & Preparation:
        self._initCompVectorPlot0(**kwargs)

        # More Vector Plot Specific Preparations:
        self._initCompVectorPlot1()

        # Prepare data:
        self._calculate_vectors()

    def _initCompVectorPlot0(self,**kwargs):

        # Special default color map for the vector plots:
        cm = colors.LinearSegmentedColormap.from_list("", 
                                                      ["white",
                                                       "aliceblue",
                                                       "lightskyblue",
                                                       "skyblue",
                                                       "lime",
                                                       "yellow",
                                                       "red",
                                                       "darkred"])

        # Defaults for Comp Plot:
        self._axshift  = priority('_axshift' , kwargs, 0.5)
        self.xlabel    = priority('xlabel'   , kwargs, 'x cell index')
        self.ylabel    = priority('ylabel'   , kwargs, 'y cell index')
        self.xlim      = priority('xlim'     , kwargs, [0          -self._axshift,
                                                        self.run.nx-self._axshift])
        self.ylim      = priority('ylim'     , kwargs, [0          -self._axshift,
                                                        self.run.ny-self._axshift])
        self.figsize   = priority('figsize'  , kwargs, (8,6))

        # Defaults for Vector Plot:
        self.dirxy      = priority(['dir','dirxy','direction']   , kwargs, "xy")
        self.width      = priority('width'                       , kwargs, 1.0)
        self.length     = priority('length'                      , kwargs, 0.9)
        self.cmap       = priority('cmap'                        , kwargs, cm)
        self.mtcc       = priority(['mtcc' ,'map_to_cell_center'], kwargs, False)
        self.cfd        = priority('cfd'                         , kwargs, False)
        self.plot_grid  = priority('plot_grid'                   , kwargs, True)

    def _initCompVectorPlot1(self):

        # Prepare self.var for CompVectorPlot:
        if self.mtcc:
            self.var = self.run.map_cell_faces_to_center(self.var,
                                                         direction=self.dirxy)
        # Check shape of var:
        if (self.var.shape != (self.run.nx,self.run.ny) and
            self.var.shape != (self.run.nx,self.run.ny,2)):
            raise Exception("Error: invalid shape pf 'var' for vectorPlot()",
                            "with direction '" + self.dirxy + "': "+str(self.var.shape))
        if self.dirxy == 'xy' and self.var.shape != (self.run.nx,self.run.ny,2):
            raise Exception("Error: invalid shape pf 'var' for vectorPlot()",
                            "with direction '" + self.dirxy + "': "+str(self.var.shape))

        if self.dirxy == 'x':
            if self.var.shape == (self.run.nx,self.run.ny,2):
                self.var = self.var[:,:,0]
            self.varx = self.var
            self.vary = np.zeros(self.var.shape)
        elif self.dirxy == 'y':
            if self.var.shape == (self.run.nx,self.run.ny,2):
                self.var = self.var[:,:,1]
            self.varx = np.zeros(self.var.shape)
            self.vary = self.var
        elif self.dirxy == 'xy':
            self.varx = self.var[...,0]
            self.vary = self.var[...,1]

        if self.cfd:
            self.varx = self.varx / self.run.sx
            self.vary = self.vary / self.run.sy

    def plot(self):
        # Create & show plot:
        self._plot()
        self._show_plot()            # (method in mother class)

    def _plot(self):
        # Create plot:        
        self._initialize_figure()    # (method in GridPlotBase class)
        self._plot_data()            # (method in GridPlotBase class)
        self._plot_grid()            # (method in CompPlot class)
        self._plot_regions()         # (method in CompPlot class)
        self._plot_colorbar()        # (method in GridPlotBase class)
        self._set_plot_limits()      # (method in CompPlot class)
        self._add_labels_and_title() # (method in GridPlotBase class)

    def _calculate_vectors(self):

        vectors    = []
        vecvals    = []
        nxny_pairs = []

        for nx in range(1,self.run.nx-1):     # (Skipping Guard Cells)
            for ny in range(1,self.run.ny-1): # (Skipping Guard Cells)

                # Calculate value of vector:
                val = np.sqrt(self.varx[nx,ny]**2 + self.vary[nx,ny]**2)
                if np.isnan(val) or val == 0:
                    continue

                # Calculate normalized direction:
                # x direction inside of the cell:
                ex = np.array([1,0])
                # y direction inside of the cell:
                ey = np.array([0,1])
                # Vector direction:
                dirxy = self.varx[nx,ny]*ex + self.vary[nx,ny]*ey
                # Normalize:
                dirxy = dirxy/np.linalg.norm(dirxy)
                dirxy = dirxy*self.length

                # Cell center:
                cc = [nx,ny]

                # Shift vector to cell center:
                pos = cc-dirxy/2

                # Create Arrow:
                vectors.append(Arrow(pos  [0],pos  [1],
                                          dirxy[0],dirxy[1],
                                          self.width))
                vecvals.append(val)
                nxny_pairs.append((nx,ny))

        self.nxny_pairs = nxny_pairs
        self.patches    = vectors[:]        # Naming required in mother class
        self.pvalues    = np.array(vecvals) # Naming required in mother class

# =============================================================================

class GridSubplots(object):
    def __init__(self,plots=None,**kwargs):

        if plots is None:
           self.plots = []
        elif type(plots) == list:
            self.plots = plots
        else:
            self.plots = [plots]

        self.suptitle    = priority('suptitle'          , kwargs, None)
        self.rows        = priority('rows'              , kwargs, 'auto')
        self.xlim        = priority('xlim'              , kwargs, None)
        self.ylim        = priority('ylim'              , kwargs, None)
        self.zoom        = priority('zoom'              , kwargs, None)
        self.onecolorbar = priority('onecolorbar'       , kwargs, False)
        self.clabel      = priority('clabel'            , kwargs, '')
        self.cfactor     = priority('cfactor'           , kwargs, 1.0)
        self.clim        = priority('clim'              , kwargs, None)
        self.cmap        = priority('cmap'              , kwargs, plt.cm.jet)
        self.cmap_center = priority(['cmc',
                                     'cmap_center']     , kwargs, None)
        self.log         = priority('log'               , kwargs, False)
        self.plot_grid   = priority('plot_grid'         , kwargs, False)
        self.shareax     = priority('shareax'           , kwargs, False) # Does not work anymore with the new python version :/
        self.spadjust    = priority('spadjust'          , kwargs, None)
        self.fontsize    = priority('fontsize'          , kwargs, None)
        self.figsize     = priority('figsize'           , kwargs, 'auto')
        self.showplot    = priority(['showplot','show'] , kwargs, True)
        self.block       = priority('block'             , kwargs, False)
        self.tb          = priority(['tb','transpbckg'] , kwargs, None)
        self.saveas      = priority(['saveas','save_as'], kwargs, None)

        # Some preparation:
        if self.fontsize:
            matplotlib.rcParams.update({'font.size': self.fontsize})

    def add_plot(self,plot):
        self.plots.append(plot)

    def plot(self):
        self._create_plot()
        self._show_plot()

    def _get_subplot_positions(self):
        nplots = len(self.plots)
        ncols  = ((len(self.plots)-1)//self.rows)+1
        nrows  = self.rows
        nrow   = 0
        ncol   = 0

        wr = ncols*[10]
        if self.onecolorbar:
            shape  = (nrows,ncols+1)
            wr.append(1)
        else:
            shape = (nrows,ncols)
            
        sp_pos = []
        for i in range(nplots):
            sp_pos.append((nrow,ncol))
            ncol += 1
            if ncol >= ncols:
                ncol  = 0
                nrow += 1
        return shape,sp_pos,wr

    def _get_clim(self):
        if self.clim is not None:
            return self.clim

        cmin =  np.inf
        cmax = -np.inf
        for plot in self.plots:
            cmin = min(np.nanmin(plot.pvalues/self.cfactor),cmin)
            cmax = max(np.nanmax(plot.pvalues/self.cfactor),cmax)
        if self.log:
            cmin = np.inf
            for plot in self.plots:
                for v in plot.pvalues/self.cfactor:
                    cmin = min(v,cmin) if v > 0 else cmin
        return [cmin,cmax]

    def _create_plot(self):
        # Prepare defaults:
        self.clim = self._get_clim()

        if self.rows is 'auto':
            self.rows = 1
            if len(self.plots) >  2: self.rows = 2
            if len(self.plots) >  8: self.rows = 3
            if len(self.plots) > 18: self.rows = 4

        if self.figsize is 'auto':
            height  = 12
            columns = ((len(self.plots)-1)//self.rows)+1
            width   = 9.0/11.0*height*columns/self.rows
            self.figsize = (width,height)
        self.figsize = convert_figsize(self.figsize)

        # Initialize figure:
	self.fig = plt.figure(figsize=self.figsize) 
#	self.fig.set_canvas(plt.gcf().canvas)
        self.axes = []
        shape,sp_pos,wr = self._get_subplot_positions()
        self.gs = matplotlib.gridspec.GridSpec(*shape,width_ratios=wr)

        # Create plots:
        for i,plot in enumerate(self.plots):
            pspkwargs = {}
            if i > 0 and self.shareax:
                pspkwargs['sharex'] = self.axes[0]
                pspkwargs['sharey'] = self.axes[0]
            self.axes.append(plt.subplot(self.gs[sp_pos[i]],**pspkwargs))
            plot.ax = self.axes[-1]
            plot.xlabel = ''
            plot.ylabel = ''
            if self.onecolorbar:
                plot.cbar        = False
                plot.cmap        = self.cmap
                plot.cmap_center = self.cmap_center
                plot.clim        = self.clim[:]
                plot.cfactor     = self.cfactor
                plot.log         = self.log
            if self.xlim:
                plot.xlim = self.xlim[:]
            if self.ylim:
                plot.ylim = self.ylim[:]
            if self.zoom:
                plot.zoom = self.zoom
            if self.plot_grid:
                plot.plot_grid = self.plot_grid
            
            plot._plot()

        # Draw color bar:
        if self.onecolorbar:
            self._draw_colorbar()

    def _draw_colorbar(self):
        self.axes.append(plt.subplot(self.gs[:,-1]))
        mpl   = matplotlib
        if self.log:
            norm = mpl.colors.LogNorm(vmin=self.clim[0],vmax=self.clim[1])
        else:
            norm = mpl.colors.Normalize(vmin=self.clim[0],vmax=self.clim[1])

        cbkwargs = {'ax':self.axes[-1],'cmap':self.cmap,'norm':norm}

        if self.clim:
            cbkwargs['extend'] = 'both'

        # Allow user modification of colorbar:
        ticklabels = None
        if type(self.onecolorbar) is dict:
            ticklabels = self.onecolorbar.pop('ticklabels',None)
            cbkwargs.update(self.onecolorbar)

        cbar  = mpl.colorbar.ColorbarBase(**cbkwargs)
        if ticklabels:
            cbar.ax.set_yticklabels(ticklabels)

        cbar.set_label(self.clabel)

    def _show_plot(self):

        if self.suptitle is not None:
            self.fig.suptitle(self.suptitle)

        try:
            self.fig.tight_layout()
        except:
            print("Warning: failed to set 'tight_layout'!")
        
        if self.suptitle is not None:
            self.fig.subplots_adjust(top=0.91)

        if self.spadjust:
            self.fig.subplots_adjust(**self.spadjust)

        self.saveas = savefig(self.saveas,transp_bckg=self.tb)
	if self.showplot: plt.show(block=self.block)
        if self.block:    plt.close()
