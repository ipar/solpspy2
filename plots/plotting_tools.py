#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    Provides some tools used in plotting routines.

    Also allows setting the dpi value and the transparency for '.png'
    images via the following environment variables:
     - PYTHON_PLOT_DPI                  (Default: 72)
     - PYTHON_PLOT_TRANSP_BCKG          (Default: 1)
    (Will be set when this module is loaded.)
"""

# Common modules
from __future__ import print_function
import os
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
# from mpl_toolkits.axes_grid1 import AxesGrid

# =============================================================================

# Read Environment variables:

PYTHON_PLOT_DPI         = os.environ.get('PYTHON_PLOT_DPI',300)
PYTHON_PLOT_TRANSP_BCKG = os.environ.get('PYTHON_PLOT_TRANSP_BCKG',True)
PYTHON_PLOT_TRANSP_BCKG = bool(int(PYTHON_PLOT_TRANSP_BCKG))

# Globally set '.png' dpi value:
matplotlib.rcParams['savefig.dpi'] = PYTHON_PLOT_DPI

# =============================================================================

def savefig(saveas,default='pdf',transp_bckg=None):
    if saveas is None:          return
    if saveas is False:         return
    if type(saveas) is not str: return
    if (not saveas.endswith('.png') and 
        not saveas.endswith('.pdf') and
        not saveas.endswith('.eps')):
        saveas += ('.' + default).replace('..','.')
    if transp_bckg is None:
        transp_bckg = PYTHON_PLOT_TRANSP_BCKG
    plt.savefig(saveas,transparent=transp_bckg)
    print("Created file: '" + saveas + "'")
    return saveas

def convert_figsize(figsize):
    """Converts the figure size to inches if it is given
    in mm or cm (which can be specified as a third index
    in the figzise tuple)."""
    if len(figsize) == 3:
        if figsize[2] == 'mm':
            f = 0.0393701
            figsize = (f*figsize[0],f*figsize[1])
        elif figsize[2] == 'cm':
            f = 0.393701
            figsize = (f*figsize[0],f*figsize[1])
        elif figsize[2] == 'inch' or figsize[2] == 'in':
            figsize = (figsize[0],figsize[1])
    return figsize

from solpspy.tools.tools import priority

def shifted_color_map(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    """
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.

    Notes
    -----
    From stackoverflow:
    https://stackoverflow.com/questions/7404116/
    defining-the-midpoint-of-a-colormap-in-matplotlib
    """
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False),
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap




def get_datalim(canvas, tight=False):
    """ Similar to the internal function of autoscale_view()
    It works for just ONE axis i.e ONE canvas and the lines in that canvas.
    axes/_base.py(1899)


    import matplotlib.transforms as mtransforms
    # X-axis
    dl = canvas.dataLim
    finite_dl = [d for d in dl if np.isfinite(d).all()]
    bb = mtransforms.BboxBase.union(dl)
    x0, x1 = bb.intervalx
    xlocator = canvas.xaxis.get_major_locator()
    try:
        x0,x1 = xlocator.nonsingular(x0,x1)
    except AttributeError:
        x0,x1 = mtransforms.nonsingular(x0,x1,increasing=False, expander=0.05)

    if not _tight:
        x0,x1 xlocator,view_limits(x0,x1) #Modified here
    self.set_xbound(x0,x1)

    self.set_xbound seems to add a little bit of padding around.
    """
    import matplotlib.transforms as mtransforms

    # X- axis
    xshared = canvas._shared_x_axes.get_siblings(canvas)
    dl = [ax.dataLim for ax in xshared]
    finite_dl = [d for d in dl if np.isfinite(d).all()]
    if len(finite_dl):
        dl = finite_dl
    bb = mtransforms.BboxBase.union(dl)
    x0, x1 = bb.intervalx

    # Y- axis
    yshared = canvas._shared_y_axes.get_siblings(canvas)
    dl = [ax.dataLim for ax in yshared]
    finite_dl = [d for d in dl if np.isfinite(d).all()]
    if len(finite_dl):
        dl = finite_dl
    bb = mtransforms.BboxBase.union(dl)
    y0, y1 = bb.intervaly

    return (x0,x1), (y0,y1)























def initialize_canvas(canvas=None, **kwargs):
    """ Type-agnostic creation and/or processing of canvas.

    Parameters
    ----------

    canvas: matplotlib canvas, optional.
        If given, then processing will be applied to it.
        If not given, then a new canvas will be created and processed.
        Default: None.

    figsize: tuple or list, optional.
        If a new canvas is to be created, size of the figure in which the
        canvas will be contained.
        Default: matplotlib default.

    title: str, optional
        Title of the canvas. Default: None.

    figtitle: str, optional
        Title of the figure of the canvas. Default: None.

    xlabel: str, optional
        Label of the x-axis. Default: None.

    ylabel: str, optional
        Label of the y-axis. Default: None.

    adjust: dict or list, optional
        Suplot adjustments to padding (left, top, right, bottom).
        Default: None.



    Returns
    -------
    fig: matplotlib figure.
    canvas: matplotlib canvas.


    Notes
    -----
    Add possibility of [xy]ticks and [xy]tickslabels.
    """

    if not canvas: #Then create the canvas
        fig, canvas = create_canvas(**kwargs)
    else: #If canvas is given, just get the associated figure
        fig = canvas.get_figure()

    kw = priority('kw', kwargs, {})
    no_processing = priority(['no_processing', 'untouch'],kwargs,kw,False)
    if no_procesing == False:
        fig, canvas = process_canvas(canvas=canvas,**kwargs)

    return fig, canvas



def create_canvas(**kwargs):
    """ Create a canvas after filtering the given kwargs.
    """
    kw = priority('kw', kwargs, {})
    figsize = priority(['figsize', 'fig_size'], kwargs, kw, None)

    creation_kw = {}
    if figsize:
        creation_kw['figsize'] = figsize

    return plt.subplots(**creation_kw)


def process_canvas(canvas, **kwargs):
    """ Process a canvas after filtering the given kwargs.
    """
    fig = canvas.get_figure()

    kw = priority('kw', kwargs, {})

    figsize = priority(['figsize', 'fig_size'], kwargs, kw, None)

    title = priority('title', kwargs, kw, None)
    fig_title = priority(['figtitle', 'fig_title'], kwargs, kw, None)

    xlabel = priority('xlabel', kwargs, kw, None)
    ylabel = priority('ylabel', kwargs, kw, None)

    xlim = priority('xlim', kwargs, kw, None)
    ylim = priority('ylim', kwargs, kw, None)

    aspect = priority('aspect', kwargs, kw, None)

    xticklabels = priority('xticklabels', kwargs, kw, 'default')
    yticklabels = priority('ylicklabels', kwargs, kw, 'default')

    adjust = priority('adjust', kwargs, kw, None)

    #Set titles
    if title:
        canvas.title(title)
    if fig_title:
        fig.title(fig_title)

    # Set labels
    if xlabel:
        canvas.set_xlabel(xlabel)
    if ylabel:
        canvas.set_ylabel(ylabel)

    # Set aspect ratio and limits
    if xlim:
        canvas.set_xlim(xlim)
    if ylim:
        canvas.set_ylim(ylim)

    # Set aspect ratio and limits
    if xlim is not None or ylim is not None:
        if aspect:
            canvas.axes.set_aspect(aspect) # Allow [xy]lim
        if ylim is not None:
            canvas.set_ylim(ylim)
        if xlim is not None:
            canvas.set_xlim(xlim)
    elif aspect:
        canvas.axis(aspect)

    if xticklabels is not None and xticklabels != 'default':
        canvas.axes.set_xticklabels(xticklabels)
    elif xticklabels is None:
        canvas.axes.set_xticklabels([])

    if yticklabels is not None and yticklabels != 'default':
        canvas.axes.set_yticklabels(yticklabels)
    elif yticklabels is None:
        canvas.axes.set_yticklabels([])

    # Adjusts subplots (mostly, white spacing around axis)
    if   adjust and isinstance(adjust, dict):
        fig.subplots_adjust(**adjust)
    elif adjust and isinstance(adjust, list):
        fig.subplots_adjust(*adjust)

    return fig, canvas











#def initialize_canvas(canvas=None, **kwargs):
#    """ Type-agnostic creation and/or processing of canvas.
#
#    Parameters
#    ----------
#
#    canvas: matplotlib canvas, optional.
#        If given, then processing will be applied to it.
#        If not given, then a new canvas will be created and processed.
#        Default: None.
#
#    figsize: tuple or list, optional.
#        If a new canvas is to be created, size of the figure in which the
#        canvas will be contained.
#        Default: matplotlib default.
#
#    title: str, optional
#        Title of the canvas. Default: None.
#
#    figtitle: str, optional
#        Title of the figure of the canvas. Default: None.
#
#    xlabel: str, optional
#        Label of the x-axis. Default: None.
#
#    ylabel: str, optional
#        Label of the y-axis. Default: None.
#
#    adjust: dict or list, optional
#        Suplot adjustments to padding (left, top, right, bottom).
#        Default: None.
#
#
#
#    Returns
#    -------
#    fig: matplotlib figure.
#    canvas: matplotlib canvas.
#
#
#    Notes
#    -----
#    Add possibility of [xy]ticks and [xy]tickslabels.
#    """
#
#
#    kw = priority('kw', kwargs, {})
#
#    figsize = priority(['figsize', 'fig_size'], kwargs, kw, None)
#
#    title = priority('title', kwargs, kw, None)
#    fig_title = priority(['figtitle', 'fig_title'], kwargs, kw, None)
#
#    xlabel = priority('xlabel', kwargs, kw, None)
#    ylabel = priority('ylabel', kwargs, kw, None)
#
#    xlim = priority('xlim', kwargs, kw, None)
#    ylim = priority('ylim', kwargs, kw, None)
#
#    aspect = priority('aspect', kwargs, kw, None)
#
#    xticklabels = priority('xticklabels', kwargs, kw, 'default')
#    yticklabels = priority('ylicklabels', kwargs, kw, 'default')
#
#    adjust = priority('adjust', kwargs, kw, None)
#
#
#    if not canvas: #Then create the canvas
#        creation_kw = {}
#        if figsize: #Just one of many that could be added
#            creation_kw['figsize'] = figsize
#        fig, canvas = plt.subplots(**creation_kw)
#    else: #If canvas is given, just get the associated figure
#        fig = canvas.get_figure()
#
#
#    #Set titles
#    if title:
#        canvas.title(title)
#    if fig_title:
#        fig.title(fig_title)
#
#    # Set labels
#    if xlabel:
#        canvas.set_xlabel(xlabel)
#    if ylabel:
#        canvas.set_ylabel(ylabel)
#
#    # Set aspect ratio and limits
#    if xlim:
#        canvas.set_xlim(xlim)
#    if ylim:
#        canvas.set_ylim(ylim)
#
#    # Set aspect ratio and limits
#    if xlim is not None or ylim is not None:
#        if aspect:
#            canvas.axes.set_aspect(aspect) # Allow [xy]lim
#        if ylim is not None:
#            canvas.set_ylim(ylim)
#        if xlim is not None:
#            canvas.set_xlim(xlim)
#    elif aspect:
#        canvas.axis(aspect)
#
#    if xticklabels is not None and xticklabels != 'default':
#        canvas.axes.set_xticklabels(xticklabels)
#    elif xticklabels is None:
#        canvas.axes.set_xticklabels([])
#
#    if yticklabels is not None and yticklabels != 'default':
#        canvas.axes.set_yticklabels(yticklabels)
#    elif yticklabels is None:
#        canvas.axes.set_yticklabels([])
#
#    # Adjusts subplots (mostly, white spacing around axis)
#    if   adjust and isinstance(adjust, dict):
#        fig.subplots_adjust(**adjust)
#    elif adjust and isinstance(adjust, list):
#        fig.subplots_adjust(*adjust)
#
#    return fig, canvas

















#def initialize_canvas(canvas=None,figsize=None, title=None, fig_title=None,
#        xlabel=None, ylabel=None, xlim=None, ylim=None, adjust=None,
#        creation_kw=None):
#    """
#    Receives canvas OR kwargs to create fig and canvas (e.g, figsize).
#    Then apply other kwargs to canvas and figure, such as such as limits,
#    titles, ticks, etc.
#
#    creation_kw or given as kwargs
#        figsize
#
#    title
#    fig_title
#
#    xlabel
#    ylabel
#
#    xlim
#    ylim
#
#    adjust
#
#    For the future, allow more than one subplot and thus make
#    that if canvas is a list, then  the parameters must be lists too.
#
#    """
#    if not canvas:
#        if not creation_kw:
#            creation_kw = {}
#        if figsize:
#            creation_kw['figsize'] = figsize
#        fig, canvas = plt.subplots(**creation_kw)
#    else:
#        fig = canvas.get_figure()
#
#
#    #Set titles
#    if title:
#        canvas.title(title)
#    if fig_title:
#        fig.title(fig_title)
#
#    # Set labels
#    if xlabel:
#        canvas.set_xlabel(xlabel)
#    if ylabel:
#        canvas.set_ylabel(ylabel)
#
#    # Set limits
#    if xlim:
#        canvas.set_xlim(xlim)
#    if ylim:
#        canvas.set_ylim(ylim)
#
#    # Adjusts subplots (mostly, white spacing around axis)
#    if   adjust and isinstance(adjust, dict):
#        fig.subplots_adjust(**adjust)
#    elif adjust and isinstance(adjust, list):
#        fig.subplots_adjust(*adjust)
#
#    return fig, canvas
