#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    Provides methods for plotting and visualization of data.
"""

# Common modules
from __future__ import print_function
import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

# Special modules
from solpspy.tools.tools import priority
from solpspy.fhitz_plot_tools import savefig_fhitz, convert_figsize, MarkerLine
from solpspy.fhitz_read_augped import AugpedData

# Debugging (remove later)
#import pdb; pdb.set_trace()
from IPython import embed

# =============================================================================

class xy(object):
    def __init__(self, x, y, **kwargs):

        #self.title   = kwargs.pop('title'   , '')
        self.label   = priority(['label','legend']       , kwargs, None)
        self.xlabel  = priority('xlabel'                 , kwargs, '')
        self.ylabel  = priority('ylabel'                 , kwargs, '')
        self.xfactor = priority('xfactor'                , kwargs, 1.0)
        self.xshift  = priority('xshift'                 , kwargs, 0.0)
        self.yfactor = priority('yfactor'                , kwargs, 1.0)
        self.yshift  = priority('yshift'                 , kwargs, 0.0)
        self.lwidth  = priority(['lwidth','linewidth']   , kwargs, 2)
        self.msize   = priority(['msize','markersize']   , kwargs, 6)
        self.lstyle  = priority(['lstyle','linestyle']   , kwargs, '-')
        self.mstyle  = priority(['mstyle','markerstyle'] , kwargs, 'o')
        self.color   = priority('color'                  , kwargs, 'k')
        self.legend  = self.label # Alias

        # List of data x-points:
        self.x = np.array([x/self.xfactor + self.xshift for x in x])
        # List of data y-points:
        self.y = np.array([y/self.yfactor + self.yshift for y in y])

class Profile(object):
    """Superclass for the PoloidalProfile and RadialProfile classes."""

    def _init0(self,**kwargs):
        # Read arguments and define defaults:
        self._log = self.run.log
        self.title    = priority('title'             , kwargs, '')
        self.legend   = priority(['legend','label']  , kwargs, None)
        self.legtit   = priority(['legtit',
                                  'legend_title']    , kwargs, '')
        self.nlegcol  = priority('nlegcol'           , kwargs, 1)
        self.legloc   = priority('legloc'            , kwargs, 0)
        self.legsize  = priority('legsize'           , kwargs, None)
        self.lstyles  = priority(['lstyles',
                                  'linestyles']      , kwargs, '-')
        self.mstyles  = priority(['mstyles',
                                  'markerstyles']    , kwargs, 'o')
        self.lwidth   = priority(['lwidth',
                                  'linewidth']       , kwargs, 2)
        self.msize    = priority(['msize',
                                  'markersize']      , kwargs, 4)
        self.colors   = priority('colors'            , kwargs, '')
        self.xlabel   = priority('xlabel'            , kwargs, 'auto')
        self.ylabel   = priority('ylabel'            , kwargs, '')
        self.xlim     = priority('xlim'              , kwargs, 'auto')
        self.ylim     = priority('ylim'              , kwargs, None)
        self.xfactor  = priority('xfactor'           , kwargs, 1.0)
        self.yfactor  = priority('yfactor'           , kwargs, 1.0)
        self.xshift   = priority('xshift'            , kwargs, 0.0)
        self.yshift   = priority('yshift'            , kwargs, 0.0)
        self.ynorm    = priority('ynorm'             , kwargs, False)
        self.nbins    = priority('nbins'             , kwargs, ['auto','auto'])
        self.log      = priority('log'               , kwargs, '')
        self.tags     = priority('tags'              , kwargs, True)
        self.tagpos   = priority('tagpos'            , kwargs, 'auto')
        # (^ y Position of the default tags)
        self.mlines   = priority('mlines'            , kwargs, [])
        # (^ A list of MarkerLine() objects)
        self.fontsize = priority('fontsize'          , kwargs, None)
        self.figsize  = priority('figsize'           , kwargs, (8,6))
        self.showplot = priority(['showplot','show'] , kwargs, True)
        self.saveas   = priority(['saveas','save_as'], kwargs, None)

    def _init1(self):
        # Preparations:
        self.figsize = convert_figsize(self.figsize)
        if self.fontsize:
            matplotlib.rcParams.update({'font.size': self.fontsize})

        # Ensure lists:
        if type(self.vars)    != list:  self.vars    = [self.vars]
        if type(self.pos)     != list:  self.pos     = [self.pos]
        self.nlines = len(self.vars)*len(self.pos)
        if type(self.legend) != list: self.legend  = self.nlines*[self.legend]
        if type(self.lwidth) != list: self.lwidth  = self.nlines*[self.lwidth]
        if type(self.msize)  != list: self.msize   = self.nlines*[self.msize]
        if type(self.lstyles)!= list: self.lstyles = self.nlines*[self.lstyles]
        if type(self.mstyles)!= list: self.mstyles = self.nlines*[self.mstyles]
        if type(self.mlines) != list: self.mlines  = [self.mlines]

        # Set default colors:
        if type(self.colors) == list:
            pass
        elif 'Colormap' in str(type(self.colors)):
            if self.nlines == 1:
                self.colors = [self.colors(0.5)]
            else:
                self.colors = [self.colors(1.0*i/(self.nlines-1))
                               for i in range(self.nlines)]
        elif type(self.colors) == str and self.colors != '':
            self.colors = self.nlines*[self.colors]
        else:
            self.colors = []
            while len(self.colors) < self.nlines:
                self.colors.extend(['k', 'b', 'r', 'g', 'c', 'm', 'y', 'w'])
            self.colors = self.colors[:self.nlines]

        if self.saveas is not None:
            self.saveas = os.path.join(self.run.rundir,self.saveas)

        # Error handling:
        if len(self.vars) > 1 and len(self.pos) > 1:
            print("Error: can't handle several variables and positions "+
                  "simultaneously. Use the add_subplot() method to add "+
                  "subplots.")
            return

    def plot(self,saveas = None,showplot = None,**kwargs):
        if saveas is not None:
            self.saveas = saveas
        if showplot is not None:
            self.showplot = showplot
        self._init_figure()
        self._plot(self.fig,self.ax,**kwargs)
        self._show_plot()

    def _auto_set_xlim(self):
        if type(self.xlim) != str:
            return

        if self.xlim == 'auto':             
            xmax = -np.inf
            xmin =  np.inf
            for xy in self.xy_pairs:
                xmax = max(xmax,np.max(xy.x))
                xmin = min(xmin,np.min(xy.x))
            diff = xmax - xmin
            xmin = xmin - 0.05*diff
            xmax = xmax + 0.05*diff
            self.xlim = [xmin,xmax]

    def _normalize_plot(self):
        """Rescales the data in such a way, that all graphs have the same
        maximum value within the current x-range (i.e. normalizing the data
        according to the current maximum value within the plot range)."""

        if not self.ynorm:
            return

        # Get maximum values:
        maxima = []
        # Iterate through xy-pairs (=graphs):
        for i,xy in enumerate(self.xy_pairs):
            maxima.append(-np.inf)
            
            # Iterate through data points:
            for ix in range(len(xy.x)):
                if xy.x[ix] >= self.xlim[0] and xy.x[ix] <= self.xlim[1]:
                    maxima[-1] = max(maxima[-1],xy.y[ix])

        # Normalize data:
        for i,xy in enumerate(self.xy_pairs):
            if self.ynorm == 'max':
                xy.y = xy.y * np.max(maxima) / maxima[i]
            else:
                xy.y = xy.y / maxima[i]

    def _init_figure(self):
	self.fig = plt.figure(1,figsize=self.figsize) 
	self.fig.set_canvas(plt.gcf().canvas)
        self.ax  = plt.subplot(1,1,1)

    def _plot(self,fig,ax,**kwargs):
        for xy in self.xy_pairs:
            plt.plot(xy.x,
                     xy.y,
                     label  = xy.legend,
                     lw     = xy.lwidth,
                     ms     = xy.msize,
                     ls     = xy.lstyle,
                     marker = xy.mstyle,
                     color  = xy.color)
        plt.title(self.title, y=1.02)
	plt.xlabel(self.xlabel)
        plt.ylabel(self.ylabel)
        if self.xlim is not None:
            plt.xlim(self.xlim)
        if self.ylim is not None:
            plt.ylim(self.ylim)

        if None not in self.legend and False not in self.legend:
            if self.legsize:
                plt.legend(loc=self.legloc,title=self.legtit,ncol=self.nlegcol,
                           prop={'size': self.legsize})
            else:
                plt.legend(loc=self.legloc,title=self.legtit,ncol=self.nlegcol)


        # Draw MarkerLines:
        for mline in self.mlines:
            if type(mline) != MarkerLine:
                mline = MarkerLine(mline)
            mline.draw_markerline(ax)

        plt.grid(b=True, which='major', color='0.5', linestyle='-')

        # Set number of ticks/bins:
        if self.nbins[0] != 'auto': 
            ax.locator_params(axis='x',nbins=self.nbins[0])
        if self.nbins[1] != 'auto': 
            ax.locator_params(axis='y',nbins=self.nbins[1])

    def _show_plot(self):
	plt.tight_layout()
        self.run.plot.pdfpages.save()
        self.saveas = savefig_fhitz(self.saveas)
	if self.showplot: plt.show()
        plt.close()

class ProfileSubplots(object):
    """Handles several profiles as a set of subplots."""

    def __init__(self,profiles=None,**kwargs):

        if profiles is None:
            self.profiles = []
        elif type(profiles) == list:
            self.profiles = profiles
        else:
            self.profiles = [profiles]

        self.suptitle = priority('suptitle'           , kwargs, None)
        self.xlabel   = priority('xlabel'             , kwargs, None)
        self.title    = priority('title'              , kwargs, None)
        self.columns  = priority('columns'            , kwargs, 1)
        self.colorbar = priority('colorbar'           , kwargs, False)
        self.figsize  = priority('figsize'            , kwargs, (8.27,11.69))
        self.showplot = priority(['showplot','show']  , kwargs, True)
        self.saveas   = priority(['saveas','save_as'] , kwargs, None)

        self.figsize = convert_figsize(self.figsize)

    def add_subplot(self,profile):
        self.profiles.append(profile)

    def plot(self):
        self._create_subplots()
        self._show_plot()

    def _get_subplot_positions(self):
        nplots = len(self.profiles)
        ncols  = self.columns
        nrows  = nplots // ncols + (nplots % ncols > 0)
        nrow   = 0
        ncol   = 0

        wr = ncols*[10]
        if self.colorbar:
            shape  = (nrows,ncols+1)
            wr.append(1)
        else:
            shape = (nrows,ncols)
            
        sp_pos = []
        for i in range(nplots):
            sp_pos.append((nrow,ncol))
            ncol += 1
            if ncol >= ncols:
                ncol  = 0
                nrow += 1
        return shape,sp_pos,wr

    def _create_subplots(self):

	self.fig = plt.figure(1,figsize=self.figsize) 
	self.fig.set_canvas(plt.gcf().canvas)
        self.axes = []
        shape,sp_pos,wr = self._get_subplot_positions()
        self.gs = matplotlib.gridspec.GridSpec(*shape,width_ratios=wr)

        for i,profile in enumerate(self.profiles):
            self.axes.append(plt.subplot(self.gs[sp_pos[i]]))
            profile._plot(self.fig,self.axes[-1])

        # Draw color bar:
        if self.colorbar:
            self._draw_colorbar()

        if self.title is not None:
            self.axes[0].set_title(self.title,y=1.06)
        if self.xlabel is not None:
            self.axes[-1].set_xlabel(self.xlabel)
            self.axes[-1].xaxis.labelpad = 12

    def _draw_colorbar(self):
        self.axes.append(plt.subplot(self.gs[:,-1]))
        vmin  = self.colorbar.get('min')
        vmax  = self.colorbar.get('max')
        cmap  = self.colorbar.get('cmap' , plt.cm.jet)
        label = self.colorbar.get('label', '')
        mpl   = matplotlib
        cbar  = mpl.colorbar.ColorbarBase(self.axes[-1], cmap=cmap,
                                          norm=mpl.colors.Normalize(vmin=vmin,
                                                                    vmax=vmax))
        cbar.set_label(label)

    def _show_plot(self):
        if self.suptitle == '__saveas__':
            if self.saveas is not None:
                self.suptitle = self.saveas.rstrip('/').split('/')[-1]
            else:
                self.suptitle = None
        if self.suptitle is not None:
            self.fig.suptitle(self.suptitle)

	self.fig.tight_layout()

        if self.suptitle is not None:
            self.fig.subplots_adjust(top=0.93)

        self.saveas = savefig_fhitz(self.saveas)
	if self.showplot: plt.show()
        plt.close()

class PoloidalProfile(Profile):
    """Creates a single poloidal profile plot of one or several grid
    variables. To add subplots use the 'ProfileSubplots' class.
    
    Parameters
    ----------
        solpspy_run : solpspy run instance

        vars : np.array(nx,ny) or list[np.array(nx,ny)]
            The grid array variable(s) to be plotted.

        pos : Integer or List[Integer]
            The position(s) (i.e. the ny index/indices) to be considered.
            Defaults to the separatrix position.

        title : String
            Plot title.

        legend : String or List[String]
            Legend text.

        legend_title : String
            Legend title.

        lwidth : List
            List of line widths.

        lstyles : List
            List of linestyles.

        mstyles : List
            List of markerstyles.

        colors : List
            List of colors.

        xlabel : String
        ylabel : String
            Axis labels.

        xlim : List[Float] ([min,max])
        ylim : List[Float] ([min,max])
            The plot limits/ranges in x and y direction.

        yfactor : Float
            Multiplier for the y axis (e.g. 1e6 for W --> MW)

        xcoord : String (defaults to 'pol')
            Representation of the x coordinate:
                'pol': Poloidal distance from the OMP along the flux tube
                'par': Parallel distance from the OMP along the flux tube
                'com': Computational domain (i.e. cell index numbers)

        log : String ('x', 'y' or 'xy')
            Switches the x-axis, the y-axis or both to a logarithmic scale.

        tags : Bool
            Switch on/off the MarkerLines() at the ixp, imp, omp and oxp
            positions.

        mlines : MarkerLine() or List[MarkerLine()]
            A list of user defined MarkerLines() to add vertical or horizontal
            lines to the plot.

        fontsize : Integer
           Sets the global fontsize for the plots to 'fontsize'.

        showplot : Bool
            If False the plot will not be shown. Usefull if several plots
            are created automatically and stored in a file.

        saveas : String
            Filename to save the plot.
    """

    def __init__(self,solpspy_run,vars,**kwargs):

        self.run  = solpspy_run
        self.vars = vars

        # General Profile Initialization:
        self._init0(**kwargs)                     # (method in mother class)

        # Poloidal Profile Specific Initialization & Preparation:
        self._initPol0(**kwargs)

        # General Initialization & Preparation:
        self._init1()                             # (method in mother class)

        # More Poloidal Profile Specific Preparations:
        self._initPol1()

    def _initPol0(self,**kwargs):
        # Poloidal Profile Specific Initialization & Preparation:
        self.pos    = priority(['pos','position'] , kwargs, self.run.sep)
        self.xcoord = priority('xcoord'           , kwargs, 'pol')
        self._auto_set_title()

    def _initPol1(self):
        # More Poloidal Profile Specific Preparations:
        if self.xlabel == 'auto':
            if self.xcoord == 'pol':
                self.xlabel = 'Poloidal Distance from the Outer Midplane [m]'
            elif self.xcoord == 'par':
                self.xlabel = 'Parallel Distance from the Outer Midplane [m]'
            elif self.xcoord == 'com':
                self.xlabel = 'Poloidal Cell Index'

        self._create_xy_pairs()
        self._auto_set_xlim()    # ... in mother class
        self._normalize_plot()   # ... in mother class
        self._auto_set_ylim()
        self._set_position_tags()

    def _auto_set_title(self):
        if type(self.title) == str and '_fluxtube_' in self.title:
            ypos = self.pos
            domp = self.run.ds[self.run.omp,ypos]
            if 1e3*domp > 10:
                domp = str(int(1e3*domp+0.5))
            else:
                domp = '{:.2}'.format(1e3*domp)
            t_fluxtube = ('$\Delta s_{\mathrm{omp}}=\ '
                          + domp + '\,\mathrm{mm}$')
            self.title = self.title.replace('_fluxtube_', t_fluxtube)

    def _create_xy_pairs(self):
        i = 0
        self.xy_pairs = []
        for v in self.vars:
            for p in self.pos:

                if self.xcoord == 'pol':
                    x = self.run.domp[:,p]
                elif self.xcoord == 'par':
                    x = self.run.domp_parallel[:,p]
                elif self.xcoord == 'com':
                    x = range(self.run.nx)

                y = v[:,p]
                xyargs = { 'xlabel'  : self.xlabel,
                           'ylabel'  : self.ylabel,
                           'xfactor' : self.xfactor,
                           'yfactor' : self.yfactor,
                           'xshift'  : self.xshift,
                           'yshift'  : self.yshift,
                           'legend'  : self.legend[i],
                           'lwidth'  : self.lwidth[i],
                           'msize'   : self.msize[i],
                           'lstyle'  : self.lstyles[i],
                           'mstyle'  : self.mstyles[i],
                           'color'   : self.colors[i]}
                self.xy_pairs.append(xy(x,y,**xyargs))
                i += 1

    def _auto_set_ylim(self):
        if type(self.ylim) != str:
            return

        if self.ylim == 'auto_sol':
            # Automatically adjust ylim, only considering data in the SOL
            xvals = range(self.run.ixp,self.run.oxp+1)
        elif self.ylim == 'auto_div':
            # Automatically adjust ylim, only considering data in the Divertor
            xvals = range(self.run.ixp)+range(self.run.oxp+1,self.run.nx)
        elif self.ylim == 'auto_inn':
            # Automatically adjust ylim, only considering data in the inner Div
            xvals = range(self.run.ixp)
        elif self.ylim == 'auto_out':
            # Automatically adjust ylim, only considering data in the outer Div
            xvals = range(self.run.oxp+1,self.run.nx)
        elif self.ylim == 'auto_xrange':
            # Automatically adjust ylim, only considering data in the xrange
            xvals = []
            x = self.xy_pairs[0].x
            for i in range(len(x)):
                if self.xlim[0] < x[i] and self.xlim[1] > x[i]:
                    xvals.append(i)

        ymin =  np.inf
        ymax = -np.inf
        for xy in self.xy_pairs:
            ymax = max(ymax,np.max(xy.y[xvals]))
            ymin = min(ymin,np.min(xy.y[xvals]))
        diff = ymax - ymin
        ymin = ymin - 0.04*diff
        ymax = ymax + 0.04*diff
        self.ylim = [ymin,ymax]

    def _set_position_tags(self):

        if self.tags == False or self.tags is None or self.tags == []:
            return

        # Calculate default values/positions:

        # Initialization:
        tags = {'it' : {},  # Inner Target
                'ix' : {},  # Inner X-Point
                'im' : {},  # Inner midplane
                'om' : {},  # Outer midplane
                'ox' : {},  # Outer X-Point
                'ot' : {}}  # Outer Target

        # Tags:
        tags['it']['tag'] = ' trgt'
        tags['ix']['tag'] =  ' ixp'
        tags['im']['tag'] =  ' imp'
        tags['om']['tag'] =  ' omp'
        tags['ox']['tag'] =  ' oxp'
        tags['ot']['tag'] = ' trgt'

        # x-Cell-Indizes:
        tags['it']['cell_index'] = 1
        tags['ix']['cell_index'] = np.where(self.run.masks.xpoint==1)[0][3]
        tags['im']['cell_index'] = self.run.imp
        tags['om']['cell_index'] = self.run.omp
        tags['ox']['cell_index'] = np.where(self.run.masks.xpoint==1)[0][4]
        tags['ot']['cell_index'] = self.run.nx-2

        # x-Positions:
        def get_x_pos(cell_index):
            if self.xcoord == 'pol':
                return self.run.domp[cell_index,self.pos[0]]
            elif self.xcoord == 'par':
                return self.run.domp_parallel[cell_index,self.pos[0]]
            elif self.xcoord == 'com':
                return cell_index
        for key in tags.keys():
            tags[key]['x_position'] = get_x_pos(tags[key]['cell_index'])

        # Tag-Offsets:
        for key in tags.keys():
            tags[key]['tag_offset'] = 0

        # Read user input:
        if self.tags == 'no_txt':
            for key in tags.keys():
                tags[key]['tag'] = ''
        if type(self.tags) == dict:
            for key in tags.keys():
                if key in self.tags.keys() and self.tags[key] == 'skip':
                    tags.pop(key)
                elif key in self.tags.keys():
                    tags[key].update(self.tags[key])
        if type(self.tags) == list:
            for key in tags.keys():
                if key not in self.tags:
                    tags.pop(key)

        # Get tag-y-position:
        if self.tagpos == 'auto':
            if self.ylim is None:
                ymin =  np.inf
                ymax = -np.inf
                for xy in self.xy_pairs:
                    ymin = min(ymin,np.min(xy.y))
                    ymax = max(ymax,np.max(xy.y))
            else:
                ymin = self.ylim[0]
                ymax = self.ylim[1]
            self.tagpos = ymin+(ymax-ymin)/25.0

        # Create MarkerLines:
        for key in tags.keys():
            self.mlines.append(MarkerLine(          tags[key]['x_position'],
                                          tag     = tags[key]['tag'],
                                          tagxpos = (tags[key]['x_position']+ 
                                                     tags[key]['tag_offset']),
                                          tagypos = self.tagpos))

class RadialProfile(Profile):

    """
    TODO DOCSTRING TODO
    """

    def __init__(self,solpspy_run,vars,**kwargs):

        self.run  = solpspy_run
        self.vars = vars

        # General Profile Initialization:
        self._init0(**kwargs)                     # (method in mother class)

        # Radial Profile Specific Initialization & Preparation:
        self._initRad0(**kwargs)

        # General Initialization & Preparation:
        self._init1()                             # (method in mother class)

        # More Radial Profile Specific Preparations:
        self._initRad1()

    def _initRad0(self,**kwargs):
        # Radial Profile Specific Initialization & Preparation:
        self.pos        = priority(['pos','position'] , kwargs, self.run.omp)
        self.xcoord     = priority('xcoord'           , kwargs, 'ds')
        self.experiment = priority('experiment'       , kwargs, None)

        if type(self.pos) == str:

            if   self.pos == 'omp':
                self.pos = self.run.omp
            elif self.pos == 'imp':
                self.pos = self.run.imp

            elif self.pos == 'oxp':
                self.pos = self.run.oxp
            elif self.pos == 'ixp':
                self.pos = self.run.ixp

            elif self.pos == 'inn':
                self.pos = 1              # First real cell (not guard cell)
            elif self.pos == 'out':
                self.pos = self.run.nx-2  # First real cell (not guard cell)

        if type(self.experiment) == str:
            self.experiment = [self.experiment]

    def _initRad1(self):
        # More Radial Profile Specific Preparations:
        if self.xlabel == 'auto':
            if self.xcoord == 'ds':
                self.xlabel = 'Distance from the Separatrix [m]'
            elif self.xcoord == 'com':
                self.xlabel = 'Radial Cell Index'

        self._create_xy_pairs()
        self._auto_set_xlim()    # ... in mother class
        self._add_experimental_data()
        self._normalize_plot()   # ... in mother class
        self._auto_set_ylim()

    def _create_xy_pairs(self):
        i = 0
        self.xy_pairs = []
        for v in self.vars:
            for p in self.pos:

                if self.xcoord == 'ds':
                    x = self.run.ds[p,:]
                elif self.xcoord == 'com':
                    x = range(self.run.ny)

                y = v[p,:]
                xyargs = { 'xlabel'  : self.xlabel,
                           'ylabel'  : self.ylabel,
                           'xfactor' : self.xfactor,
                           'yfactor' : self.yfactor,
                           'xshift'  : self.xshift,
                           'yshift'  : self.yshift,
                           'legend'  : self.legend[i],
                           'lwidth'  : self.lwidth[i],
                           'msize'   : self.msize[i],
                           'lstyle'  : self.lstyles[i],
                           'mstyle'  : self.mstyles[i],
                           'color'   : self.colors[i]}
                self.xy_pairs.append(xy(x,y,**xyargs))
                i += 1

    def _auto_set_ylim(self):
        if type(self.ylim) != str:
            return

        # Automatically adjust ylim, only considering data in the xrange
        if self.ylim == 'auto_xrange':
            ymin =  np.inf
            ymax = -np.inf
            for xy in self.xy_pairs:
                for i in range(len(xy.x)):
                    if self.xlim[0] < xy.x[i] and self.xlim[1] > xy.x[i]:
                        ymin = min(ymin,xy.y[i])
                        ymax = max(ymax,xy.y[i])
            diff = ymax - ymin
            ymin = ymin - 0.04*diff
            ymax = ymax + 0.04*diff
            self.ylim = [ymin,ymax]

    def _add_experimental_data(self):
        """
        Adds experimental data from AUGPED according to the
        self.experiment parameter, which is supposed to be a
        path of an AUGPED output file (or a list of paths).
        """
        if self.experiment is None:
            return

        if self.ylim is None:
            self.ylim = 'auto_xrange'

        for augped_file in self.experiment:
            # Define default plotting options for exp. data if not specified:
            def set_default(i,arg,default):
                try:    return arg[i]
                except: return default
            n = len(self.xy_pairs)
            legend = set_default(n,self.legend ,'Experiment: '+augped_file.rstrip('/').split('/')[-1])
            lwidth = set_default(n,self.lwidth ,0)
            msize  = set_default(n,self.msize  ,self.msize[-1])
            lstyle = set_default(n,self.lstyles,'')
            mstyle = set_default(n,self.mstyles,'+')
            color  = set_default(n,self.colors ,'0.5')

            augped = AugpedData(augped_file)
            x      = augped.data['ds']
            y      = augped.data['data']
           #e      = augped.data['errb']

            xyargs = { 'xlabel'  : self.xlabel,
                       'ylabel'  : self.ylabel,
                       'xfactor' : self.xfactor,
                       'yfactor' : self.yfactor,
                       'xshift'  : self.xshift,
                       'yshift'  : self.yshift,
                       'legend'  : legend,
                       'lwidth'  : lwidth,
                       'msize'   : msize,
                       'lstyle'  : lstyle,
                       'mstyle'  : mstyle,
                       'color'   : color}
            self.xy_pairs.insert(0,xy(x,y,**xyargs))
