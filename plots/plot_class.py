"""
TODO:
    -- Computational grid.
    -- Implement tests (create figures/canvas and test properties)
    -- Radial profiles from fhitz_*.
    -- Poloidal profiles from fhitz_*.
"""

from __future__ import print_function, division
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.animation as animation
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.backends.backend_pdf import PdfPages
from sets import Set


from solpspy.tools.tools import solps_property
from solpspy.tools.tools import priority, update
from solpspy.tools.tools import read_config
from solpspy.tools.tools import SimpleNamespace
from solpspy.plots.plotting_tools import shifted_color_map
from solpspy.plots.plotting_tools import get_datalim








# ===========================================
class BasePlot(object):
    """
    Logic:
    - Receive certain user parameters.
    - Process the parameters to separate them into those related to
      canvas initialization and process, artists and saving setups.
    - Read defaults from configuration files.
    - Overwrite the defaults by the user parameters.
    - Process canvas:
      + If given a canvas: Process the canvas.
      + If not given a canvas: Initialize and process the canvas.
    - Add artists or prepare for animation.
    - Plot or save, according to parameters.
    """

    def __init__(self, mother, canvas=None, **kwargs):
        #Allow other defaults instead of os.getcwd only
        self.storage_path = priority('storage_path', kwargs, os.getcwd())
        self._mother = mother #Solpspy case.

        #Process the given kwargs into various parameter groups
        self.params = self._process_arguments(**kwargs)

        # Associate self.canvas and self.fig with user canvas or new canvas.
        self._create_canvas(canvas)

        # Compose the plot.
        self.compose()

        # If some post treatment (show, save, animate, etc) is requested.
        if self.params['post']['show']:
            self.show(recompose=False)
        if self.params['post']['save']:
            self.save(recompose=False)



    #------------------------
    def compose(self):
        """ Helper function to create the figure. It is called by
        plot and save.
        In the future, for recompositions, try to modify as much as possible
        instead of redrawing.
        """
        self._mother.log.error(
            "Compose method not yet implemented for {}.".format(self.__class__))

    def plot(self, recompose=True):
        """Create the figure
        """
        if recompose:
            self.compose()
        plt.show()

#    def show(self):
#        #try:
#        #    self.fig.show()
#        #except:
#        #    self._mother.log.error(
#        #      "plt.show() used because a GUI backend is required for fig.show().")
#        #    plt.show()
#
#        plt.show()
#        return

    def save(self, name, recompose=True):
        """
        """
        self._mother.log.error(
            "Save method not yet implemented for {}.".format(self.__class__))

    def animate(self):
        """Use the other methods to create an animation, if possible.
        """
        self._mother.log.error(
            "Animate method not yet implemented for {}.".format(self.__class__))


    #------------------------

    def _create_canvas(self, canvas):
        """ Having it separated from __init__ allows for required adjustsments.
        """
        if canvas:
            self.canvas = canvas
            self.fig = canvas.get_figure()
        else:
            creation_kw = {}
            if self.params['canvas']['figsize']:
                creation_kw['figsize'] = self.params['canvas']['figsize']
            self.fig, self.canvas =  plt.subplots(**creation_kw)
        return


    def _process_canvas(self):
        """ In some plot classes, a different type of processing might
        be required.
        Any replacement of this method should modify both:
        self.fig and self.canvas
        """

        canvas = self.canvas
        try:
            fig = self.fig
        except:
            fig = canvas.get_figure()
        ckw = self.params['canvas']


        #Set titles
        if ckw['title']:
            canvas.title(ckw['title'])
        if ckw['figtitle']:
            fig.title(ckw['figtitle'])

        # Set labels
        if ckw['xlabel']:
            canvas.set_xlabel(ckw['xlabel'])
        if ckw['ylabel']:
            canvas.set_ylabel(ckw['ylabel'])


        # Set_aspect allows for easier management of [xy]lims but
        # modifies the canvas shape and the space it occupies.
        # When only xlim is set, the picture is quite weird, as it
        if ((ckw['xlim'] is not None or ckw['ylim'] is not None)
                and ckw['aspect'] and ckw['adapt_canvas']):
                canvas.axes.set_aspect(ckw['aspect'])
        elif ckw['aspect']:
            canvas.axis(ckw['aspect'])

        #Save data lims in case they are needed.
        _datalims = get_datalim(self.canvas)

        # Set [xy] limits of the axis.
        if ckw['xlim'] is not None:
            canvas.set_xlim(ckw['xlim'])
        elif ckw['use_datalim']:
            try:
                canvas.set_xlim(_datalims[0])
            except:
                self._mother.log.error(
                    "In {}, impossing xlim from dataLim failed.".format(
                        self.__class__.__name__))

        if ckw['ylim'] is not None:
            canvas.set_ylim(ckw['ylim'])
        elif ckw['use_datalim']:
            try:
                canvas.set_ylim(_datalims[1])
            except:
                self._mother.log.error(
                    "In {}, impossing ylim from dataLim failed.".format(
                        self.__class__.__name__))


        # Set the tick labels appropiately.
        if ckw['xticklabels'] is not None and ckw['xticklabels'] != 'default':
            canvas.axes.set_xticklabels(ckw['xticklabels'])
        elif ckw['xticklabels'] is None:
            canvas.axes.set_xticklabels([])

        if ckw['yticklabels'] is not None and ckw['yticklabels'] != 'default':
            canvas.axes.set_yticklabels(ckw['yticklabels'])
        elif ckw['yticklabels'] is None:
            canvas.axes.set_yticklabels([])


        # Adjusts subplots (mostly, white spacing around axis)
        if   ckw['adjust'] and isinstance(ckw['adjust'], dict):
            fig.subplots_adjust(**ckw['adjust'])
        elif ckw['adjust'] and isinstance(ckw['adjust'], list):
            fig.subplots_adjust(*ckw['adjust'])
        elif (ckw['adjust'] and isinstance(ckw['adjust'], str) and
            ckw['adjust'].lower() == 'tight'):
            fig.tight_layout()
        elif ckw['adjust'] is True:
            fig.tight_layout()
        else:
            self._mother.log.warning(
                "In {}, 'adjust' parameter is not valid.".format(
                    self.__class__.__name__))

        self.canvas = canvas
        self.fig = fig
        return


    #------------------------
    def _get_defaults(self):
        """
        Get defaults for parameters according to plotting stack. E.g:
                            Vessel
                              v
                            Base Figure

        Maybe could accept a list of names and just update in order. E.g:
        self._get_defaults(['base figure', 'vessel'])

        """
        fconf = read_config()['Plots']['base figure']

    #------------------------
    def _process_arguments(self, **kwargs):
        """ Returns dictionary which keys are'canvas', 'plot', 'post';
        either given by user or config defaults.
        """
        params = priority(['params','parameters'], kwargs,
                {'canvas':{}, 'artists':{}, 'post':{}})

        canvas_params = priority(['canvas_params', 'cp'], kwargs, {})
        artists_params = priority(['artists_params', 'plot_params','ap'],
                kwargs, {})
        post_params = priority(['post_params', 'pp'], kwargs, {})

        use_defaults = priority(['use_def','use_defaults', 'defaults'],
                kwargs, True)


        params['canvas'] = update(params['canvas'], canvas_params)
        params['artists']= update(params['artists'],artists_params)
        params['post']   = update(params['post'],   post_params)

        if not use_defaults:
            return params
        else:
            tmp = {}
            tmp['canvas']= self._canvas_parameters(**kwargs)
            tmp['artists']= self._artists_parameters(**kwargs)
            tmp['post']   = self._post_parameters(**kwargs)

            return update(tmp, params)


    #------------------------
    def _canvas_parameters(self, **kwargs):
        """ Separate the user given kwargs in different dicts for different
        purposes, as well as setting up the defaults.
        """
        defaults = self._get_defaults()
        tmp = {}

        # Figure
        tmp['figtitle'] = priority(['figtitle','fig_title', 'ftitle'],
             kwargs, defaults['figtitle'])
        tmp['figsize'] = priority(['figsize','fig_size','size_fig'],
             kwargs, defaults['figsize'])

        # Canvas
        tmp['title'] = priority(['title','canvas_title', 'ctitle'],
             kwargs, defaults['title'])
        tmp['aspect'] = priority(['aspect', 'aspect_ratio'], kwargs,
                defaults['aspect'])
        tmp['adjust'] = priority(['adjust'], kwargs, defaults['adjust'])
        tmp['xlabel'] = priority('xlabel', kwargs, defaults['xlabel'])
        tmp['ylabel'] = priority('ylabel', kwargs, defaults['ylabel'])

        tmp['xticklabels'] = priority('xticklabels',
                kwargs, defaults['xticklabels'])
        tmp['yticklabels'] = priority('yticklabels',
                kwargs, defaults['xticklabels'])

        tmp['xlim'] = priority('xlim', kwargs, defaults['xlim'])
        tmp['ylim'] = priority('ylim', kwargs, defaults['ylim'])

        tmp['use_datalim'] = priority(['use_datalim','datalim'], kwargs,
                defaults['use_datalim'])

        tmp['processing'] = priority(['processing'],
                kwargs, defaults['processing'])
        return tmp


    #------------------------
    def _artists_parameters(self, **kwargs):
        """ Separate the user given kwargs in different dicts for different
        purposes, as well as setting up the defaults.
        """
        self._mother.log.error(
            "Artists params not yet implemented for {}.".format(self.__class__))


    #------------------------
    def _post_parameters(self, **kwargs):
        """ Separate the user given kwargs in different dicts for different
        purposes, as well as setting up the defaults.
        """
        defaults = self._get_defaults()
        tmp = {}
        tmp['show'] = priority(['stand_alone', 'plot','show', 'print'],
                kwargs, defaults['show'])

        tmp['save'] = priority(['save'], kwargs, defaults['save'])
        return tmp



    #------------------------
    def _generate_artist(self,**kwargs):
        """
        """

# ===========================================























# ===========================================
class VesselPlot(BasePlot):
    """ Plots self.vessel

    Parameters
    ----------
    canvas: matplotlib axes. Optional.
        Object in which the vessel will be plotted.

    figsize: tuple, optional.
        If no canvas have been given, then this function will create
        figure of figsize and attach an axes.
        Default: (8,6)
        Alias: fig_size , size_fig.

    xlim: tuple or list, optional.
        Impose specific x limits on the figure. Both xlim and ylim
        must be compatible in the sense that either the aspect ratio
        is set to equal and thus either one of the limits or the figure
        size must adapt, as one of them must be not independent.
        Default: None

    ylim: tuple or list, optional.
        Impose specific y limits on the figure. Both xlim and ylim
        must be compatible in the sense that either the aspect ratio
        is set to equal and thus either one of the limits or the figure
        size must adapt, as one of them must be not independent.
        Default: None

    xticklabels: list, optional.
        Desired value to place the x tick labels.
        If 'default', then no further actions are taken.
        If None, then the ticks are removed.
        Default: 'default'

    yticklabels: list, optional.
        Desired value to place the y tick labels.
        If 'default', then no further actions are taken.
        If None, then the ticks are removed.
        Default: 'default'

    color: str, list(str) or dict(isurf:float), optional.
        If str, it must be a valid color argument of matplotlib, e.g. 'k'.
        Then that color is assigned to all plotted surfaces.
        If list(str), it must be of lenght equal to number of surfaces.
        It specifies the color of every surface.
        If dict, then keys must be the index of the surfaces.
        Default: 'k'
        Alias: colors, color_surfaces, colors_surfaces

    linewidth: float, list(float) or dict(isurf:float), optional.
        If float, then that line width is assigned to all plotted surfaces.
        If list(float), it must be of lenght equal to number of surfaces.
        It specifies the line width of every surface.
        If dict, then keys must be the index of the surfaces.
        Default: 1
        Alias: lw

    linestyle: float, list(float) or dict, optional.
        If float, then that line style is assigned to all plotted surfaces.
        If list(float), it must be of lenght equal to number of surfaces.
        It specifies the line width of every surface.
        If dict, then keys must be the index of the surfaces.
        Default: '-'
        Alias: ls

    stand_alone: bool, optional.
        If True, then use plt.show()
        Alias: plot, show, print.




    Returns
    -------

    fig: matplotlib figure
        If canvas was given, then it returns the fig of that canvas.
        Else, it returns the new fig created for the new canvas.

    canvas: matplotlib axes
        If canvas was given, the same canvas.
        Else, it returns the new canvas.




    Notes
    -----
    Add the option of line2D to pass it so it can be use elsewhere
    object.append(new lines in the for loop) then can be add with
    axis.add_line(line)


    For the future, separate clearly the process and kwargs related to the
    creation and process of the canvas from the creation and process
    of the vessel.
    """

    def __init__(self,mother,**kwargs):
        super(VesselPlot, self).__init__(mother, **kwargs)


#    #------------------------
#    def compose(self):
#        ## Reserve for self.plot or self.animate
#        ##Create and/or process to get the final canvas and figure.
#
#        vessel = self._generate_artist(self.params['artists'])
#
#        self.fig, self.canvas = self._initialize_canvas(
#                canvas=self.canvas, kw=self.params['canvas'])
#
#        for surf in vessel:
#            self.canvas.add_line(surf)
#
#
#        ### This apparently work
#        ##self.canvas.relim()
#        ##self.canvas.autoscale_view(True,True,True)
#
#        if self._adapt_canvas:
#            #Apply limits
#            self.canvas.set_aspect('equal', anchor='C')
#            self.canvas.set_xlim(1.0,2.0)
#            self.canvas.set_ylim([-1.2, -0.5])
#        else:
#            self.canvas.axis('equal')
#            self.canvas.set_xlim([1.0,  2.0])
#            #self.canvas.set_ylim([-1.2, -0.5])
#
#
#        return


    def compose(self):
        """ At this point, self.canvas and self.fig should exist
        already. They both are unprocessed yet.
        """

        vessel = self._generate_artist(self.params['artists'])
        for surf in vessel:
            self.canvas.add_line(surf)

        self._process_canvas()
        return




    #------------------------
    def _get_defaults(self):
        """ Plot stack:
        Vessel
          v
        Base Figure
        """
        fconf = read_config()['Plots']['base figure']
        vconf = read_config()['Plots']['vessel']
        return update(fconf, vconf)



    #------------------------
    def _canvas_parameters(self, **kwargs):
        defaults = self._get_defaults()
        tmp = super(VesselPlot, self)._canvas_parameters(**kwargs)

        tmp['adapt_canvas'] = priority(['adapt_canvas', 'reshape_canvas'],
                kwargs, defaults['adapt_canvas'])

        return tmp



    def _artists_parameters(self, **kwargs):
        """ Separate the user given kwargs in different dicts for different
        purposes, as well as setting up the defaults.

        For color, lw and ls:
        If it is a list, must be complete
        Elif a dict, use plain default color/ls/lw and update.
        Elif a strg/num, create a dict with that for each surface.
        """
        defaults = self._get_defaults()

        tmp = {}
        color = priority(['vessel_color','surface_color','color','colors',
            'color_surfaces', 'colors_surfaces'], kwargs, defaults['color'])
        lw = priority(['vessel_lw', 'vessel_linewidth','linewidth','lw'],
                kwargs, defaults['lw'])
        ls = priority(['vessel_ls', 'vessel_linestyle','linestyle','ls'],
                kwargs, defaults['ls'])

        #Get number of vessel surfaces.
        nsurf = self._mother.vessel.shape[1]

        ## Color
        if   isinstance(color, list):
            tmp['color'] = {i: color[i] for i in xrange(nsurf)}
        elif isinstance(color, dict):
            vessel_color = {i: defaults['color'] for i in xrange(nsurf)}
            tmp['color'] = update(vessel_color, color)
        elif isinstance(color,str):
            tmp['color'] = {i: color for i in xrange(nsurf)}
        else:
            self._mother.log.error(
              "In VesselPlot: 'color' set to an incompatible value.")

        ## Line width
        if   isinstance(lw, list):
            tmp['lw'] = {i: lw[i] for i in xrange(nsurf)}
        elif isinstance(lw, dict):
            vessel_lw = {i: defaults['lw'] for i in xrange(nsurf)}
            tmp['lw'] = update(vessel_lw, lw)
        elif isinstance(lw,int) or isinstance(lw,float):
            tmp['lw'] = {i: lw for i in xrange(nsurf)}
        else:
            self._mother.log.error(
              "In VesselPlot: 'lw' set to an incompatible value.")

        ## Line style
        if   isinstance(ls, list):
            tmp['ls'] = {i: ls[i] for i in xrange(nsurf)}
        elif isinstance(ls, dict):
            vessel_ls = {i: defaults['ls'] for i in xrange(nsurf)}
            tmp['ls'] = update(vessel_ls, ls)
        elif isinstance(ls,str):
            tmp['ls'] = {i: ls for i in xrange(nsurf)}
        else:
            self._mother.log.error(
              "In VesselPlot: 'ls' set to an incompatible value.")

        return tmp




    #------------------------
    def _generate_artist(self, akw):
        """
        """
        nsurf = self._mother.vessel.shape[1]
        tmp = []
        for i in xrange(nsurf):
            R = np.array([self._mother.vessel[0,i],self._mother.vessel[2,i]])
            z = np.array([self._mother.vessel[1,i],self._mother.vessel[3,i]])
            #tmp.append(mpl.lines.Line2D(R,z, color=akw['color'][i],
            #    linewidth=akw['lw'][i], ls=akw['ls'][i]))
            tmp.append(mpl.lines.Line2D(R,z, color=akw['color'][i],
                lw=akw['lw'][i], ls=akw['ls'][i]))
        return tmp

# ===========================================




class VesselComputationalPlot(VesselPlot):
    def __init__(self,mother,**kwargs):
        mother.vessel = self._generate_vessel(mother.nx,mother.ny)
        super(VesselComputationalPlot, self).__init__(mother, **kwargs)



    @staticmethod
    def _generate_vessel(nx,ny, xlen=1.0, ylen=1.0, offset=(-0.5,-0.5)):
        """
        Monkey patch to generate a vessel and attach it to mother before
        starting the init of VesselPlot.
        """
        vessel = np.zeros((4,4))
        xmx = xlen*(nx+1)
        ymx = ylen*(ny+1)

        vessel[0,0] = offset[0]
        vessel[1,0] = offset[1]
        vessel[2,0] = offset[0] + xmx
        vessel[3,0] = offset[1]

        vessel[0,1] = offset[0] + xmx
        vessel[1,1] = offset[1]
        vessel[2,1] = offset[0] + xmx
        vessel[3,1] = offset[1] + ymx

        vessel[0,2] = offset[0] + xmx
        vessel[1,2] = offset[1] + ymx
        vessel[2,2] = offset[0]
        vessel[3,2] = offset[1] + ymx

        vessel[0,3] = offset[0]
        vessel[1,3] = offset[1] + ymx
        vessel[2,3] = offset[0]
        vessel[3,3] = offset[1]

        return vessel


        for ix in xrange(nx):
            lbcx = offset[0] + xlen*(ix+1)
            for iy in xrange(ny):
                lbcy = offset[1] + ylen*(iy+1)
                grid[ix,iy,0] = (lbcx, lbcy)
                grid[ix,iy,1] = (lbcx+xlen, lbcy)
                grid[ix,iy,2] = (lbcx+xlen, lbcy+ylen)
                grid[ix,iy,3] = (lbcx, lbcy+ylen)















































































# ===========================================
class GdataPlot(BasePlot):
    """ Class for everything related to plot the grid and grid data.;w
    """
    def __init__(self,mother,var, **kwargs):
        # Computational is a special kwarg that has to be addressed separately.
        self._computational = priority('computational', kwargs,
                read_config()['Plots']['gdata']['computational'])
        if var is None:
            self.var = 'grid'
        else:
            self.var = var
        super(GdataPlot,self).__init__(mother, **kwargs)






    #------------------------
    def _get_defaults(self):
        """ Plot stack:
        Phys or Comp
          v
        Gdata
          v
        Vessel
          v
        Base Figure
        """
        fconf = read_config()['Plots']['base figure']
        vconf = read_config()['Plots']['vessel']
        gconf = read_config()['Plots']['gdata']
        if self._computational:
            econf = read_config()['Plots']['gdata comp']
        else:
            econf = read_config()['Plots']['gdata phys']
        return update(update(update(fconf, vconf), gconf),econf)


    #------------------------
    def _generate_computational_grid(self, xlen=1.0, ylen=1.0, offset=(-0.5,-0.5)):
        """
        It must be a variable with (nx,ny,4,2).
        The 4 is for each one of the corners points.
        The 2 is for (x,y) of each of those corner points.
        Maybe accept to put white lines over x-points and core.
        """
        nx = self._mother.nx
        ny = self._mother.ny
        grid = np.zeros((nx,ny,4,2))
        #for ix in xrange(nx):
        #    lbcx = offset[0]+0.5*xlen*ix
        #    for iy in xrange(ny):
        #        lbcy = offset[1]+0.5*ylen*iy
        #        grid[ix,iy,0] = (lbcx, lbcy)
        #        grid[ix,iy,1] = (lbcx+xlen*(ix+1), lbcy)
        #        grid[ix,iy,2] = (lbcx+xlen*(ix+1), lbcy+ylen*(iy+1))
        #        grid[ix,iy,3] = (lbcx, lbcy+ylen*(iy+1))
        for ix in xrange(nx):
            lbcx = offset[0] + xlen*(ix+1)
            for iy in xrange(ny):
                lbcy = offset[1] + ylen*(iy+1)
                grid[ix,iy,0] = (lbcx, lbcy)
                grid[ix,iy,1] = (lbcx+xlen, lbcy)
                grid[ix,iy,2] = (lbcx+xlen, lbcy+ylen)
                grid[ix,iy,3] = (lbcx, lbcy+ylen)

        return grid




    #------------------------
    #def _vessel_parameters(self, ckw):
    #    """ To be set from either vessel_kw dict or vessel_*
    #    or from the standard kwargs and defaults as much as possible.
    #    E.g:
    #    vessel_color should be given back as vessel_kw['color'] to match
    #    what vesselplot is expecting.
    #    """

    #------------------------
    def _canvas_parameters(self, **kwargs):
        defaults = self._get_defaults()
        tmp = super(GdataPlot, self)._canvas_parameters(**kwargs)

        vessel_color = priority(['vessel_color','vcolor'],
                kwargs, defaults, None)
        vessel_lw = priority(['vessel_lw','vessel_linewidth', 'vlw'],
                kwargs, defaults, None)
        vessel_ls = priority(['vessel_ls','vessel_linestyle', 'vls'],
                kwargs, defaults, None)

        if vessel_color:
            tmp['vessel_color'] = vessel_color
        if vessel_lw:
            tmp['vessel_lw'] = vessel_lw
        if vessel_ls:
            tmp['vessel_ls'] = vessel_ls

        return tmp


    #------------------------
    def _artists_parameters(self, **kwargs):
        """
        """
        defaults = self._get_defaults()
        nx = self._mother.nx
        ny = self._mother.ny

        tmp = {}

        tmp['computational'] = self._computational

        tmp['mask'] = priority('mask', kwargs, defaults['mask'])
        tmp['mask_color'] = priority(['mask_color', 'cmask', 'color_mask'],
                kwargs, defaults['mask_color'])

        tmp['norm'] = priority('norm', kwargs, defaults['norm'])
        tmp['cmap'] = priority(['cmap', 'color_map'], kwargs, defaults['cmap'])
        tmp['cmap_center'] = priority(['cmap_center', 'center'],
                kwargs, defaults['cmap_center'])
        tmp['cmap_shift'] = priority(['cmap_shift', 'shift'],
                kwargs, defaults['cmap_shift'])
        tmp['clim'] = priority(['clim', 'color_limits', 'limits_color'],
                kwargs, defaults['clim'])
        tmp['cbar'] = priority(['cbar', 'color_bar', 'colorbar'],
                kwargs, defaults['cbar'])
        tmp['clabel'] = priority(['clabel','colorbar_label','color_bar_label'],
                kwargs, defaults['clabel'])
        tmp['extend_cbar'] = priority(['extend_cbar'],
                kwargs, defaults['extend_cbar'])

        if not tmp['computational']:
            tmp['grid'] = self._mother.grid
        else:
            #Allow here for computational.
            #It must be a variable with (nx,ny,4,2).
            #The 4 is for each one of the corners points.
            #The 2 is for (x,y) of each of those corner points.
            #Maybe accept to put white lines over x-points and core.
            #akw['grid'] = self._generate_computational_grid(xlen,ylen)

            #tmp['grid'] = self._mother.grid
            tmp['grid'] = self._generate_computational_grid()

        tmp['use_grid'] = priority(['use_grid','grid_use'],
                kwargs, defaults['use_grid'])
        tmp['lw'] = priority(['grid_lw', 'lw'], kwargs, defaults['lw'])
        tmp['edgecolor'] = priority(['edgecolor', 'ec'],
                kwargs, defaults['edgecolor'])
        return tmp







        ### ADAPT ix AND iy FROM self.region()
        ##If mask only delimits the poloidal region, expand to grid
        #if (mask.ndim == 1) and (mask.shape == nx).all():
        #    bad = (np.array([[mask[i]]*self._mother.ny
        #           for i in range(self._mother.nx)]) == False)
        #elif (mask.ndim == 2) and (mask.shape == ( self._mother.nx,self._mother.ny)):
        #    bad = (mask == False)




    def _generate_artist(self, var, akw):
        """ Returns a patch collection with the correct colors and setup.
        It is suitable for animation, since each frame it can be passed
        var[i,:,:]. It would mean that limits get reapplied too.

        Parameters
        ----------
        var: numpy array (nx,ny).
            Must be a grid data array. If the desire quantity has different
            dimensions or different number of dimensions, that will be treated
            somewhere else.

        akw: dict.
            Contains the artists parameters.


        Returns
        ------
        p: PatchCollection
            Ready to be attached to the canvas. It has the corresponding
            shape, values, mask, cmap type, clims and value scale.
        """

        nx = self._mother.nx
        ny = self._mother.ny


        #Initialize patch collection with lists of polygons and values.
        gres = []
        vgres = []
        for i in xrange(nx):
            for k in xrange(ny):
                gres.append(Polygon(akw['grid'][i,k]))
                vgres.append(var[i,k])

        vgres = np.array(vgres)
        p = PatchCollection(gres)

        if type(akw['cmap']) == str:
            try:
                akw['cmap'] = plt.cm.__dict__[akw['cmap']]
            except:
                akw['cmap'] = plt.cm.hot

        #Apply mask, if given. Limits recalculated for masked array.
        if akw['mask'] is not None:
            bad = (akw['mask'] == False).reshape(nx*ny) # == not(mask)
            akw['cmap'].set_bad(color=akw['mask_color'])
            vgres = np.ma.masked_where(bad,vgres)
            p.set_clim((np.min(vgres),np.max(vgres))) #under/over needed?


        #Applye value limits, if given. They must be compatible with clim.
        if akw['clim'] is not None:
            p.set_clim(akw['clim'])
            aux_cmap = plt.cm.get_cmap(akw['cmap'])
            akw['cmap'].set_under(aux_cmap(0.0))
            akw['cmap'].set_over(aux_cmap(1.0))




        #ATTENTION: Does not work with clims yet
        #Limits can probably be added with start and stop key arguments
        # of shifted_color_map.

        #ATTENTION: Probably won't work with log scale

        #cmap_center gives the value of center of the color map, e.g., 0.0)
        # Important for divergent cmaps specially.
        if akw['cmap_center'] is not None:
            mx = np.max(vgres) if akw['clim'] is None else akw['clim'][1]
            mn = np.min(vgres) if akw['clim'] is None else akw['clim'][0]
            a = mx-mn
            b = mn
            center_fraction = (akw['cmap_center']-b)/a
            akw['cmap'] = shifted_color_map(akw['cmap'], name=akw['cmap'].name,
                    midpoint=center_fraction)
            #cmap = shifted_color_map(akw['cmap'], name=akw['cmap'].name,
            #        midpoint=center_fraction, start=mn, stop=mx)



        #Linear is the default, but if log, clim has to be set again inside
        # yscale('symlog') is symmetrical logarithm
        # Could be changed to apply here the limits directly also for linear
        if akw['norm'] == 'log':
          if akw['clim'] is not None:
            p.set_norm(colors.LogNorm(vmin=akw['clim'][0], vmax=akw['clim'][1]))
          else:
            p.set_norm(colors.LogNorm())


        #Display grid? If so, use lw.
        if akw['use_grid']:
            p.set(array=vgres, cmap =akw['cmap'], linewidth=akw['lw'])
        else:
            p.set(array=vgres, cmap =akw['cmap'], linewidth=0.75)

        # Equivalent to "set_gridcolor"
        p.set_edgecolor(akw['edgecolor'])

        return p



    def compose(self):
        p = self._generate_artist(self.var, self.params['artists'])

        # tmp here is needed because canvas an fig are not returned.
        if not self.params['artists']['computational']:
            tmp = VesselPlot(self._mother, canvas=self.canvas,
                    **self.params['canvas'])
        else:
            #tmp = VesselComputationalPlot(self._mother, canvas=self.canvas,
            #        **self.params['canvas'])

            fake_mother = SimpleNamespace(nx=self._mother.nx, ny=self._mother.ny, log=self._mother.log)
            tmp = VesselComputationalPlot(fake_mother, canvas=self.canvas,
                    **self.params['canvas'])

        self.canvas = tmp.canvas
        self.fig = tmp.fig

        self.canvas.add_collection(p)

        akw = self.params['artists']
        if akw['cbar']:
            if akw['clim'] is not None and akw['clabel']:
                self.fig.colorbar(p,ax=self.canvas,extend='both',
                        label=awk['clabel'])
            elif akw['clim'] is not None:
                self.fig.colorbar(p, ax=self.canvas, extend='both')
            elif akw['clabel']:
                self.fig.colorbar(p, ax=self.canvas, label=awk['clabel'])
            else:
                self.fig.colorbar(p, ax=self.canvas)
        return

# ===========================================











# ===========================================
##ATTENTION: Improve?? Maybe not everything in generate artists is necessary??
class TriangDataPlot(GdataPlot):
    """ Plots of the triangular grid of EIRENE
    """
    def _generate_artist(self, var, akw):
        """ Returns a patch collection with the correct colors and setup.
        It is suitable for animation, since each frame it can be passed
        var[i,:,:]. It would mean that limits get reapplied too.

        Parameters
        ----------
        var: numpy array (nx,ny).
            Must be a grid data array. If the desire quantity has different
            dimensions or different number of dimensions, that will be treated
            somewhere else.

        akw: dict.
            Contains the artists parameters.


        Returns
        ------
        p: PatchCollection
            Ready to be attached to the canvas. It has the corresponding
            shape, values, mask, cmap type, clims and value scale.
        """
        akw['grid'] = self._mother.triang

        ntr = self._mother.ntriang


        #Initialize patch collection with lists of polygons and values.
        gres = []
        vgres = []
        for i in xrange(ntr):
            gres.append(Polygon(akw['grid'][i]))
            vgres.append(var[i])

        vgres = np.array(vgres)
        p = PatchCollection(gres)

        if type(akw['cmap']) == str:
            try:
                akw['cmap'] = plt.cm.__dict__[akw['cmap']]
            except:
                akw['cmap'] = plt.cm.hot

        #Apply mask, if given. Limits recalculated for masked array.
        if akw['mask'] is not None:
            bad = (akw['mask'] == False).reshape(ntr) # == not(mask)
            akw['cmap'].set_bad(color=akw['mask_color'])
            vgres = np.ma.masked_where(bad,vgres)
            p.set_clim((np.min(vgres),np.max(vgres))) #under/over needed?


        #Applye value limits, if given. They must be compatible with clim.
        if akw['clim'] is not None:
            p.set_clim(akw['clim'])
            aux_cmap = plt.cm.get_cmap(akw['cmap'])
            akw['cmap'].set_under(aux_cmap(0.0))
            akw['cmap'].set_over(aux_cmap(1.0))




        #ATTENTION: Does not work with clims yet
        #Limits can probably be added with start and stop key arguments
        # of shifted_color_map.

        #ATTENTION: Probably won't work with log scale

        #cmap_center gives the value of center of the color map, e.g., 0.0)
        # Important for divergent cmaps specially.
        if akw['cmap_center'] is not None:
            mx = np.max(vgres) if akw['clim'] is None else akw['clim'][1]
            mn = np.min(vgres) if akw['clim'] is None else akw['clim'][0]
            a = mx-mn
            b = mn
            center_fraction = (akw['cmap_center']-b)/a
            akw['cmap'] = shifted_color_map(akw['cmap'], name=akw['cmap'].name,
                    midpoint=center_fraction)
            #cmap = shifted_color_map(akw['cmap'], name=akw['cmap'].name,
            #        midpoint=center_fraction, start=mn, stop=mx)



        #Linear is the default, but if log, clim has to be set again inside
        # yscale('symlog') is symmetrical logarithm
        # Could be changed to apply here the limits directly also for linear
        if akw['norm'] == 'log':
          if akw['clim'] is not None:
            p.set_norm(colors.LogNorm(vmin=akw['clim'][0], vmax=akw['clim'][1]))
          else:
            p.set_norm(colors.LogNorm())


        #Display grid? If so, use lw.
        if akw['use_grid']:
            p.set(array=vgres, cmap =akw['cmap'], linewidth=akw['lw'])
        else:
            p.set(array=vgres, cmap =akw['cmap'], linewidth=0.75)

        # Equivalent to "set_gridcolor"
        p.set_edgecolor(akw['edgecolor'])

        return p


























#    def old_gdata(self):
#
#        #General options
#        prettify = priority('prettify', kwargs, {})
#        computational = priority('computational', kwargs, prettify, False)
#        index = priority(['index','ind','dimension', 'dim','ns','natm','nmol','nion'],
#                kwargs, prettify, None)
#        title = priority('title', kwargs, '')
#
#        #Mask
#        mask = priority('mask', kwargs, prettify, None)
#        color_mask = priority(['color_mask','mask_color'], kwargs, prettify,
#                'lightgrey')
#
#        #Color map
#        cmap = priority(['cmap','color_map'], kwargs, prettify, plt.cm.hot)
#        if type(cmap) == str:
#            try:
#                cmap = plt.cm.__dict__[cmap]
#            except:
#                cmap = plt.cm.hot
#        clim = priority(['clim', 'color_lim','color_limits',
#            'colorbar_lim','colorbar_limits'],kwargs, prettify, None)
#        cbar = priority(['cbar','color_bar','colorbar'], kwargs, prettify, True)
#        clabel = priority('clabel', kwargs, prettify, '')
#        norm = priority('norm', kwargs, prettify, None)
#
#        #Add possibility so users can give exact value of center
#        cmap_center = priority(['cmap_center','color_map_center',
#            'center_cmap','center_color_map'], kwargs, prettify, None)
#        cmap_shift = priority(['cmap_shift','shift_cmap',
#            'color_map_shift','shift_color_map'], kwargs, prettify, None)
#        #cmap_center has priority over cmap_shift
#        if cmap_shift and not cmap_center:
#            cmap = shifted_color_map(cmap, midpoint=cmap_shift)
#
#        #Grid
#        lw  = priority(['lw','grid_lw', 'lw_grid'], kwargs, prettify, 0.00)
#        use_grid = priority('use_grid',kwargs, prettify, True)
#        edgecolor = priority('edgecolor', kwargs, prettify, 'face')
#
#        #Figure
#        fig_size = priority(['figsize','fig_size', 'size_fig'], kwargs, prettify, (8,6))
#        fig_labels = priority(['labels_fig','fig_labels'], kwargs, prettify, True)
#        separatrix = priority('separatrix', kwargs, prettify, False)
#        separatrix_color = priority('separatrix_color', kwargs, prettify, 'special')
#        separatrix_lw = priority('separatrix_lw', kwargs, prettify, 1.0)
#
#        plot_vessel = priority(['plot_vessel','vessel'], kwargs, prettify, True)
#
#        vessel_kwargs = priority(['vessel_kwargs','vessel_prettify',
#            'kwargs_vessel','prettify_vessel', 'kvessel'], kwargs, prettify, {})
#        if ('fig_size' not in vessel_kwargs and
#            'figsize'  not in vessel_kwargs and
#            'size_fig' not in vessel_kwargs):
#            vessel_kwargs['fig_size'] = fig_size
#        xlim = priority('xlim', kwargs, prettify, None)
#        ylim = priority('ylim', kwargs, prettify, None)
#        if xlim is not None:
#            vessel_kwargs['xlim'] = xlim
#        if ylim is not None:
#            vessel_kwargs['ylim'] = ylim
#
#
#
#        #Create a canvas in case none is provided
#        #Error: If canvas is provided, plot does NOT preserve proportionality
#        # if plot_vessel was not called before.
#        if canvas is None and not computational:
#            #This is reversed as how it should be
#            if plot_vessel:
#                self.vessel(canvas=canvas,**vessel_kwargs)
#            else:
#                fig = plt.figure(figsize=fig_size)
#                canvas = fig.add_subplot(1,1,1)
#            #ATTENTION:
#            #Change this to the new return from vessel and canvas.get_fig
#            fig= plt.gcf()
#            canvas = plt.gca()
#        elif canvas is None:
#            fig = plt.figure()
#            canvas = fig.add_subplot(1,1,1)
#        elif canvas and plot_vessel:
#            #self.plot_vessel(canvas = canvas, linewidth = 1)
#            self.vessel(canvas=canvas, **vessel_kwargs)
#
#        plt.title(title)
#
#        #If var is grid, plot simply the grid
#        #ATTENTION: Remove or substitute for region?
#        if type(var) == str and var == 'grid':
#            for i in range(self._mother.nx):
#                for k in range(self._mother.ny):
#                    canvas.add_patch(Polygon(self._mother.grid[i,k], True,
#                           linewidth = 0.5, edgecolor = 'gray', fill = False))
#
#        #If var is a string, try to get a real var (using index)
#        if type(var) == str and var != 'grid':
#            try:
#                if index is not None:
#                    try:
#                        var = getattr(self._mother, var)[:,:,index]
#                    except:
#                        self._mother.log.error("Index is not valid")
#                else:
#                    var = getattr(self._mother, var)
#            except:
#                self._mother.log.error("Variable unknown")
#
#        #ATTENTION: substitute this if for a creation or a true (nx,ny) or
#        # an exit if that cannot be achieved.
#        #If var is a [nx,ny] quantity
#        if (type(var) == np.ndarray and var.ndim == 2 and
#            var.shape[0]==self._mother.nx and var.shape[1]==self._mother.ny):
#
##            #Create a PatchCollection with all the cells in the grid
##            #ATTENTION: Separate this as a _function, to be called
##            # independently.
##            gres = []
##            vgres = []
##            if not computational:
##                for i in range(self._mother.nx):
##                  for k in range(self._mother.ny):
##                    gres.append(Polygon(self._mother.grid[i,k]))
##                    vgres.append(var[i,k])
##            else:
##                return #For now
##                #for i in range(self.nx):
##                #  for k in range(self.ny):
##                #    gres.append(Square())
##                #    vgres.append(var[i,k])
##
##            vgres = np.array(vgres)
##            p = PatchCollection(gres)
#
##            if mask is not None:
##                #If mask only delimits the poloidal region, expand to grid
##                if (mask.ndim == 1) and (mask.shape == self._mother.nx).all():
##                    bad = (np.array([[mask[i]]*self._mother.ny
##                           for i in range(self._mother.nx)]) == False)
##                elif (mask.ndim == 2) and (mask.shape == ( self._mother.nx,self._mother.ny)):
##                    bad = (mask == False)
##                cmap.set_bad(color=color_mask)
##                bad = bad.reshape(self._mother.nx*self._mother.ny)
##                vgres = np.ma.masked_where(bad,vgres)
##                p.set_clim((np.min(vgres),np.max(vgres)))
#
#
##            if clim is not None:
##                p.set_clim(clim)
##                aux_cmap = plt.cm.get_cmap(cmap)
##                cmap.set_under(aux_cmap(0.0))
##                cmap.set_over(aux_cmap(1.0))
#
#
##            #ATTENTION: Does not work with clims yet
##            if cmap_center is not None:
##                #Approximation between max and min of data to be used
##                mx = np.max(vgres) if clim is None else clim[1]
##                mn = np.min(vgres) if clim is None else clim[0]
##                a = mx-mn
##                b = mn
##                center_fraction = (cmap_center-b)/a
##                cmap = shifted_color_map(cmap, name=cmap.name, midpoint=center_fraction)
##
##
##            #Linear is the default, but if log, clim has to be set again inside
##            #ATTENTION:
##            #center_cmap won't probably work with this, but
##            #center_cmap is more important
##            #for divergent color maps, which are most likely negative
##            #(and thus, not suitable for log scale)
##            if norm == 'log':
##              if clim is not None:
##                p.set_norm(colors.LogNorm(vmin = clim[0], vmax = clim[1]))
##              else:
##                p.set_norm(colors.LogNorm())
##
##            if use_grid:
##                p.set(array=vgres, cmap =cmap, linewidth=lw)
##            else:
##                p.set(array=vgres, cmap =cmap, linewidth=0.75)
##            p.set_edgecolor(edgecolor)
##            canvas.add_collection(p)
#
#            #Aesthetics for the color bar
#            #Should be plt. or canvas.?
#            if cbar:
#                if clim is not None and clabel:
#                    plt.colorbar(p, extend = 'both', label=clabel)
#                elif clim is not None:
#                    plt.colorbar(p, extend = 'both')
#                elif clabel:
#                    plt.colorbar(p, label=clabel)
#                else:
#                    plt.colorbar(p)
#
#            #Print separatrix or not
#            if separatrix and not computational:
#                self.fieldline(canvas=canvas, color=separatrix_color,
#                                    lw=separatrix_lw)
#            elif separatrix and computational:
#                pass #Plot a straight line of the patches
#
#            #Show figure labels R[m] and z[m]
#            if fig_labels and not computational:
#                plt.xlabel('R [m]')
#                plt.ylabel('z [m]')
#
#        else:
#            self._mother.log.error("Data should be [nx,ny] or [nx,ny,index],"+
#                           " if index was specified")
#


































###################################################################################
#class GridPlot(BasePlot):
#    """ Class for everything related to plot the grid and grid data, including the vessel.
#    Previous self.plot.vessel will be just an alias for something like plot.GridPlot(var=None) and thus
#    only plotting the vessel.
#    """
#    def __init__(self,mother, **kwargs):
#        super(GridPlot,self).__init__(mother, **kwargs)
#
#        if var is None:
#            var = 'grid'
#
#    def plot(self, **kwargs):
#        kw_creation_canvas, kw_process_canvas, kw_creation
#        self.fig, self.canvas = self._process_canvas(**kwargs)
#        grid = self.generate_artist(**kwags)
#        grid = self.process_artist(**kwargs)
#
#
#    def _gdata_arguments(self, **kwargs):
#        """ Separate the user given kwargs in different dicts for different
#        purposes, as well as setting up the defaults.
#        """
#        var = priority(['var','quant'], kwargs, None)
#
#
#
#    def _process_canvas(self,canvas=None,**kwargs):
#        """
#        kwargs can be:
#            canvas
#            computational
#            vessel_kwargs
#            figsize
#
#        """
#        if canvas:
#            fig = canvas.get_figure()
#        else:
#            fig, canvas = self._processed_canvas(**kwargs)
#
#        # If physical and with vessel, plot it.
#        if not computational and plot_vessel == True:
#            fig, canvas = self.vessel(canvas=canvas, **vessel_params)
#
#        return fig, canvas
#
#    def generate_artist(self, **kwargs):
#        """
#        Create correct patch collection (either grid or computational)
#        with the correct colours, limits, norms, etc.
#        """
#
#    def process_artist(self, **kwargs):
#        """
#        Create correct patch collection (either grid or computational)
#        with the correct colours, limits, norms, etc.
#        """
#
#
#    def _vessel(self, canvas = None, **kwargs):
#        """ Plots self.vessel
#
#        Parameters
#        ----------
#        canvas: matplotlib axes. Optional.
#            Object in which the vessel will be plotted.
#
#        figsize: tuple, optional.
#            If no canvas have been given, then this function will create
#            figure of figsize and attach an axes.
#            Default: (8,6)
#            Alias: fig_size , size_fig.
#
#        xlim: tuple or list, optional.
#            Impose specific x limits on the figure. Both xlim and ylim
#            must be compatible in the sense that either the aspect ratio
#            is set to equal and thus either one of the limits or the figure
#            size must adapt, as one of them must be not independent.
#            Default: None
#
#        ylim: tuple or list, optional.
#            Impose specific y limits on the figure. Both xlim and ylim
#            must be compatible in the sense that either the aspect ratio
#            is set to equal and thus either one of the limits or the figure
#            size must adapt, as one of them must be not independent.
#            Default: None
#
#        xticklabels: list, optional.
#            Desired value to place the x tick labels.
#            If 'default', then no further actions are taken.
#            If None, then the ticks are removed.
#            Default: 'default'
#
#        yticklabels: list, optional.
#            Desired value to place the y tick labels.
#            If 'default', then no further actions are taken.
#            If None, then the ticks are removed.
#            Default: 'default'
#
#        color: str, list(str) or dict(isurf:float), optional.
#            If str, it must be a valid color argument of matplotlib, e.g. 'k'.
#            Then that color is assigned to all plotted surfaces.
#            If list(str), it must be of lenght equal to number of surfaces.
#            It specifies the color of every surface.
#            If dict, then keys must be the index of the surfaces.
#            Default: 'k'
#            Alias: colors, color_surfaces, colors_surfaces
#
#        linewidth: float, list(float) or dict(isurf:float), optional.
#            If float, then that line width is assigned to all plotted surfaces.
#            If list(float), it must be of lenght equal to number of surfaces.
#            It specifies the line width of every surface.
#            If dict, then keys must be the index of the surfaces.
#            Default: 1
#            Alias: lw
#
#        linestyle: float, list(float) or dict, optional.
#            If float, then that line style is assigned to all plotted surfaces.
#            If list(float), it must be of lenght equal to number of surfaces.
#            It specifies the line width of every surface.
#            If dict, then keys must be the index of the surfaces.
#            Default: '-'
#            Alias: ls
#
#        stand_alone: bool, optional.
#            If True, then use plt.show()
#            Alias: plot, show, print.
#
#
#
#
#        Returns
#        -------
#
#        fig: matplotlib figure
#            If canvas was given, then it returns the fig of that canvas.
#            Else, it returns the new fig created for the new canvas.
#
#        canvas: matplotlib axes
#            If canvas was given, the same canvas.
#            Else, it returns the new canvas.
#
#
#
#
#        Notes
#        -----
#        Add the option of line2D to pass it so it can be use elsewhere
#        object.append(new lines in the for loop) then can be add with
#        axis.add_line(line)
#
#
#        For the future, separate clearly the process and kwargs related to the
#        creation and process of the canvas from the creation and process
#        of the vessel.
#        """
#        vconf = read_config()['Plots']['vessel']
#        fconf = read_config()['Plots']['figure']
#
#        ## Creation of base figure and canvas.
#        # Read vessel and figure defaults: kwargs > vessel > figure.
#        fig_kw = {}
#        fig_kw['figsize'] = priority(['figsize','fig_size','size_fig'],
#                kwargs, vconf['figsize'], fconf['figsize'])
#
#        fig_kw['xlim'] = priority('xlim',
#                kwargs, vconf['figsize'], fconf['figsize'])
#        fig_kw['ylim'] = priority('ylim',
#                kwargs, vconf['figsize'], fconf['figsize'])
#
#        fig_kw['xticklabels'] = priority('xticklabels',
#                kwargs, vconf['xticklabels'], fconf['xticklabels'])
#        fig_kw['yticklabels'] = priority('yticklabels',
#                kwargs, vconf['yticklabels'], fconf['yticklabels'])
#
#        fig_kw['xlabel'] = priority('xabel',
#                kwargs, vconf['xlabel'], fconf['xlabel'])
#        fig_kw['ylabel'] = priority('yabel',
#                kwargs, vconf['ylabel'], fconf['ylabel'])
#
#        # Process the canvas if it is given, create and process if not.
#        if canvas:
#            fig, canvas = self._process_canvas(canvas=canvas, **fig_kw)
#        else:
#            fig, canvas = self._process_canvas(**fig_kw)
#
#
#
#
#
#
#
#
#
#
#
#        # Parameters of Vessel
#        color = priority(['color','colors','color_surfaces',
#            'colors_surfaces'], kwargs, 'k')
#        lw = priority(['linewidth','lw'], kwargs, 1)
#        ls = priority(['linestyle','ls'], kwargs, '-')
#
#        # Parameters of Print
#        stand_alone = priority(['stand_alone', 'plot','show', 'print'],
#                kwargs, False)
#
#
#        #Get number of surfaces creating the vessel.
#        nsurf = self._mother.vessel.shape[1]
#
#        # Create default dicts with given or default single arguments, str/num.
#        default_color = color if type(color) is str else 'k'
#        default_ls = ls if type(ls) is str else '-'
#        default_lw = lw if (type(lw) is int or type(lw) is float) else 1.0
#
#        vessel_color = {i: default_color for i in xrange(nsurf)}
#        vessel_ls = {i: default_ls for i in xrange(nsurf)}
#        vessel_lw = {i: default_lw for i in xrange(nsurf)}
#
#        # Transform inputs into dicts if they are lists.
#        if type(color) is list:
#            color = {i: color[i] for i in xrange(nsurf)}
#        if type(ls) is list:
#            ls = {i: ls[i] for i in xrange(nsurf)}
#        if type(lw) is list:
#            lw = {i: lw[i] for i in xrange(nsurf)}
#
#        # Update defaults  with user inputs for specific surfaces.
#        if type(color) is dict:
#            vessel_color.update(color)
#        if type(ls) is dict:
#            vessel_ls.update(ls)
#        if type(lw) is dict:
#            vessel_lw.update(lw)
#
#
#        # Create a canvas if necessary.
#        if canvas is None:
#            fig = plt.figure(figsize=fig_size)
#            canvas = fig.add_subplot(1,1,1)
#        else:
#            fig = canvas.get_figure()
#
#        # It works. Figure is what user must re-adjust then.
#        # Maybe do it automatically if xlim or ylim are detected
#        # and no canvas was given
#        if xlim is not None or ylim is not None:
#            canvas.axes.set_aspect('equal') # Allow [xy]lim
#            if ylim is not None:
#                canvas.set_ylim(ylim)
#            if xlim is not None:
#                canvas.set_xlim(xlim)
#        else:
#            canvas.axis('equal')
#
#        if xticklabels is not None and xticklabels != 'default':
#            canvas.axes.set_xticklabels(xticklabels)
#        elif xticklabels is None:
#            canvas.axes.set_xticklabels([])
#
#        if yticklabels is not None and yticklabels != 'default':
#            canvas.axes.set_yticklabels(yticklabels)
#        elif yticklabels is None:
#            canvas.axes.set_yticklabels([])
#
#        for i in xrange(nsurf):
#            R = np.array([self._mother.vessel[0,i],self._mother.vessel[2,i]])
#            z = np.array([self._mother.vessel[1,i],self._mother.vessel[3,i]])
#            canvas.plot(R,z, color=vessel_color[i], linewidth=vessel_lw[i],
#                    ls=vessel_ls[i])
#
#        if stand_alone:

#
#        return fig, canvas
#



if __name__ == '__main__':
    from solpspy import *
    from IPython import embed
    #shot = SolpsData(122360)

#    runpath = '/ptmp1/work/ipar/solps-iter-develop/runs/aug_ddnu_34623/necore_b2standalone_Bt=-2.47T_Ip=+0.6MA_ux7p_style=2_from_no_currents/necore=2.53e19_tecore=287eV_ticore=300eV_dt=1e-7_facvis=1.0_facdrift=1.0_facExB=1.0_BC_Lisa_no_currents_x3'
    #runpath = '/ptmp1/work/ipar/solps-iter-develop/runs/aug_ddnu_nepow_feedback/alt_Bt=-2.5T_kaveeva=1e-2_ip=0.8MA_inert=1.0_vispar=1.0_visper=1.0_i-n=1.0/nesepm=1.5_fhout=0.354MW_fhinn=0.354MW_BCCON=16_BCENI=16_BCENE=16_strg=0.1_testing_movies_alt'

    ## Fhitz iter N+Ar
    runpath = '/ptmp1/scratch/ipar/solps-iter/runs/aug_ddnu_nepow_feedback_coreflux=1.00e20/alt_Bt=+2.5T_kaveeva=1e-2_ip=1.0MA_BCCON=22_BCENI=1_BCENE=1/fhitz/SI_33988m_5MW.D=3e22.Ar=1e19.N=2e19.test_20190313'
    shot = SolpsData(runpath)


    testing1 = False
    if testing1:
        fig = plt.figure()
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)
        a = VesselPlot(shot, canvas = ax1, xlim=[1.0, 2.0], ylim=[-1.2, -0.5])
        b = VesselPlot(shot, canvas = ax2, adapt_canvas=False, xlim=[1.0, 2.0])
        #plt.show()

    testing2 = False
    if testing2:
        nsurf = shot.vessel.shape[1]
        clr = {i: 'k' for i in xrange(nsurf)}
        clr[200] = 'r'
        clr[210] = 'r'

        cls = {i: '-' for i in xrange(nsurf)}
        cls[200] = ':'
        cls[210] = ':'

        clw = {i: 1.0 for i in xrange(nsurf)}
        clw[200] = 10.0
        clw[210] = 10.0

        c1 = VesselPlot(shot)

        c = VesselPlot(shot, color=clr, ls=cls, lw=clw)

        d = VesselPlot(shot, color=clr, ls=cls, lw=clw, adjust='tight')

        f = VesselPlot(shot, color=clr, ls=cls, lw=clw, adjust='tight',
                xlim=[0.77,2.5])

        f2 = VesselPlot(shot, color=clr, ls=cls, lw=clw, adjust='tight',
                xlim=[0.97,1.7])

        f3 = VesselPlot(shot, color=clr, ls=cls, lw=clw, adjust='tight',
                ylim=[-1.3, -0.5])

        g = VesselPlot(shot, color=clr, ls=cls, lw=clw, adjust='tight',
                xlim=[0.77,2.5], ylim= [-1.5, 1.5])

        g = VesselPlot(shot, color=clr, ls=cls, lw=clw, adjust='tight',
                xlim=[0.77,2.5], ylim= [-1.3, -0.5])



    ####
    testing3 = False
    if testing3:
        te = shot.te
        aa = GdataPlot(shot, te)

        na = shot.na[...,1]
        bb = GdataPlot(shot, na, cmap='Greens', norm='log',
                clim=[1e18,1e19])
        cc = GdataPlot(shot, na, cmap='Greens', norm='log',
                clim=[1e18,None])

        ua = shot.ua[...,1]
        dd2= GdataPlot(shot, ua, cmap='PiYG')
        dd = GdataPlot(shot, ua, cmap='PiYG', cmap_center=0.0)

        fhex = shot.fhex
        dd2= GdataPlot(shot, fhex, cmap='PiYG')
        dd = GdataPlot(shot, fhex, cmap='PiYG', cmap_center=0.0, ylim = [0.73,1.2])



    testing4 = False
    #import pdb; pdb.set_trace()
    if testing4:
        fhex = shot.fhex

        dd = GdataPlot(shot, fhex, cmap='PiYG',
                cmap_center=0.0, ylim = [0.73,1.2])


        dd3 = GdataPlot(shot, fhex, cmap='PiYG',
                cmap_center=0.0, ylim = [0.73,1.2], xlim=[1.2,1.8],
                vessel_color='r')




    testing5 = True
    if testing5:
        a = TriangDataPlot(shot, shot.pdena[:,3])

































    plt.show()
    embed()

