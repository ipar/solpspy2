from __future__ import print_function, division

import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.animation as animation
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.backends.backend_pdf import PdfPages
from sets import Set

import numpy as np

from solpspy.tools.tools import solps_property, priority, cellface
from solpspy.plots.plotting_tools import shifted_color_map, processed_canvas


class NewPlots(object):
    def __init__(self, mother, **kwargs):
        #Set from config
        self.storage_path = priority('storage_path', kwargs, os.getcwd())
        self._mother = mother



    @staticmethod
    def _processed_canvas(**kwargs):
        return processed_canvas(**kwargs)



    def _get_correct_canvas(self,canvas=None,**kwargs):
        """
        kwargs can be:
            canvas
            computational
            vessel_kwargs
            figsize

        """
        if canvas:
            fig = canvas.get_figure()
        else:
            fig, canvas = self._processed_canvas(**kwargs)

        # If physical and with vessel, plot it.
        if not computational and plot_vessel == True:
            fig, canvas = self.vessel(canvas=canvas, **vessel_params)

        return fig, canvas






    def _generate_patch_collection(self, **kwargs):
        """
        Create correct patch collection (either grid or computational)
        with the correct colours, limits, norms, etc.
        """






# -----------------------------------------------------------------------------
    def refactor_vessel(self, canvas = None, **kwargs):
        """ Plots self.vessel

        Parameters
        ----------
        canvas: matplotlib axes. Optional.
            Object in which the vessel will be plotted.

        figsize: tuple, optional.
            If no canvas have been given, then this function will create
            figure of figsize and attach an axes.
            Default: (8,6)
            Alias: fig_size , size_fig.

        xlim: tuple or list, optional.
            Impose specific x limits on the figure. Both xlim and ylim
            must be compatible in the sense that either the aspect ratio
            is set to equal and thus either one of the limits or the figure
            size must adapt, as one of them must be not independent.
            Default: None

        ylim: tuple or list, optional.
            Impose specific y limits on the figure. Both xlim and ylim
            must be compatible in the sense that either the aspect ratio
            is set to equal and thus either one of the limits or the figure
            size must adapt, as one of them must be not independent.
            Default: None

        xticklabels: list, optional.
            Desired value to place the x tick labels.
            If 'default', then no further actions are taken.
            If None, then the ticks are removed.
            Default: 'default'

        yticklabels: list, optional.
            Desired value to place the y tick labels.
            If 'default', then no further actions are taken.
            If None, then the ticks are removed.
            Default: 'default'

        color: str, list(str) or dict(isurf:float), optional.
            If str, it must be a valid color argument of matplotlib, e.g. 'k'.
            Then that color is assigned to all plotted surfaces.
            If list(str), it must be of lenght equal to number of surfaces.
            It specifies the color of every surface.
            If dict, then keys must be the index of the surfaces.
            Default: 'k'
            Alias: colors, color_surfaces, colors_surfaces

        linewidth: float, list(float) or dict(isurf:float), optional.
            If float, then that line width is assigned to all plotted surfaces.
            If list(float), it must be of lenght equal to number of surfaces.
            It specifies the line width of every surface.
            If dict, then keys must be the index of the surfaces.
            Default: 1
            Alias: lw

        linestyle: float, list(float) or dict, optional.
            If float, then that line style is assigned to all plotted surfaces.
            If list(float), it must be of lenght equal to number of surfaces.
            It specifies the line width of every surface.
            If dict, then keys must be the index of the surfaces.
            Default: '-'
            Alias: ls

        stand_alone: bool, optional.
            If True, then use plt.show()
            Alias: plot, show, print.




        Returns
        -------

        fig: matplotlib figure
            If canvas was given, then it returns the fig of that canvas.
            Else, it returns the new fig created for the new canvas.

        canvas: matplotlib axes
            If canvas was given, the same canvas.
            Else, it returns the new canvas.




        Notes
        -----
        Add the option of line2D to pass it so it can be use elsewhere
        object.append(new lines in the for loop) then can be add with
        axis.add_line(line)


        For the future, separate clearly the process and kwargs related to the
        creation and process of the canvas from the creation and process
        of the vessel.
        """
        vconf = read_config()['Plots']['vessel']
        fconf = read_config()['Plots']['figure']

        ## Creation of base figure and canvas.
        # Read vessel and figure defaults: kwargs > vessel > figure.
        fig_kw = {}
        fig_kw['figsize'] = priority(['figsize','fig_size','size_fig'],
                kwargs, vconf['figsize'], fconf['figsize'])

        fig_kw['xlim'] = priority('xlim',
                kwargs, vconf['figsize'], fconf['figsize'])
        fig_kw['ylim'] = priority('ylim',
                kwargs, vconf['figsize'], fconf['figsize'])

        fig_kw['xticklabels'] = priority('xticklabels',
                kwargs, vconf['xticklabels'], fconf['xticklabels'])
        fig_kw['yticklabels'] = priority('yticklabels',
                kwargs, vconf['yticklabels'], fconf['yticklabels'])

        fig_kw['xlabel'] = priority('xabel',
                kwargs, vconf['xlabel'], fconf['xlabel'])
        fig_kw['ylabel'] = priority('yabel',
                kwargs, vconf['ylabel'], fconf['ylabel'])

        # Process the canvas if it is given, create and process if not.
        if canvas:
            fig, canvas = self._process_canvas(canvas=canvas, **fig_kw)
        else:
            fig, canvas = self._process_canvas(**fig_kw)











        # Parameters of Vessel
        color = priority(['color','colors','color_surfaces',
            'colors_surfaces'], kwargs, 'k')
        lw = priority(['linewidth','lw'], kwargs, 1)
        ls = priority(['linestyle','ls'], kwargs, '-')

        # Parameters of Print
        stand_alone = priority(['stand_alone', 'plot','show', 'print'],
                kwargs, False)


        #Get number of surfaces creating the vessel.
        nsurf = self._mother.vessel.shape[1]

        # Create default dicts with given or default single arguments, str/num.
        default_color = color if type(color) is str else 'k'
        default_ls = ls if type(ls) is str else '-'
        default_lw = lw if (type(lw) is int or type(lw) is float) else 1.0

        vessel_color = {i: default_color for i in xrange(nsurf)}
        vessel_ls = {i: default_ls for i in xrange(nsurf)}
        vessel_lw = {i: default_lw for i in xrange(nsurf)}

        # Transform inputs into dicts if they are lists.
        if type(color) is list:
            color = {i: color[i] for i in xrange(nsurf)}
        if type(ls) is list:
            ls = {i: ls[i] for i in xrange(nsurf)}
        if type(lw) is list:
            lw = {i: lw[i] for i in xrange(nsurf)}

        # Update defaults  with user inputs for specific surfaces.
        if type(color) is dict:
            vessel_color.update(color)
        if type(ls) is dict:
            vessel_ls.update(ls)
        if type(lw) is dict:
            vessel_lw.update(lw)


        # Create a canvas if necessary.
        if canvas is None:
            fig = plt.figure(figsize=fig_size)
            canvas = fig.add_subplot(1,1,1)
        else:
            fig = canvas.get_figure()

        # It works. Figure is what user must re-adjust then.
        # Maybe do it automatically if xlim or ylim are detected
        # and no canvas was given
        if xlim is not None or ylim is not None:
            canvas.axes.set_aspect('equal') # Allow [xy]lim
            if ylim is not None:
                canvas.set_ylim(ylim)
            if xlim is not None:
                canvas.set_xlim(xlim)
        else:
            canvas.axis('equal')

        if xticklabels is not None and xticklabels != 'default':
            canvas.axes.set_xticklabels(xticklabels)
        elif xticklabels is None:
            canvas.axes.set_xticklabels([])

        if yticklabels is not None and yticklabels != 'default':
            canvas.axes.set_yticklabels(yticklabels)
        elif yticklabels is None:
            canvas.axes.set_yticklabels([])

        for i in xrange(nsurf):
            R = np.array([self._mother.vessel[0,i],self._mother.vessel[2,i]])
            z = np.array([self._mother.vessel[1,i],self._mother.vessel[3,i]])
            canvas.plot(R,z, color=vessel_color[i], linewidth=vessel_lw[i],
                    ls=vessel_ls[i])

        if stand_alone:
            plt.show()

        return fig, canvas



    def vessel(self, canvas = None, **kwargs):
        """ Plots self.vessel

        Parameters
        ----------
        canvas: matplotlib axes. Optional.
            Object in which the vessel will be plotted.

        figsize: tuple, optional.
            If no canvas have been given, then this function will create
            figure of figsize and attach an axes.
            Default: (8,6)
            Alias: fig_size , size_fig.

        xlim: tuple or list, optional.
            Impose specific x limits on the figure. Both xlim and ylim
            must be compatible in the sense that either the aspect ratio
            is set to equal and thus either one of the limits or the figure
            size must adapt, as one of them must be not independent.
            Default: None

        ylim: tuple or list, optional.
            Impose specific y limits on the figure. Both xlim and ylim
            must be compatible in the sense that either the aspect ratio
            is set to equal and thus either one of the limits or the figure
            size must adapt, as one of them must be not independent.
            Default: None

        xticklabels: list, optional.
            Desired value to place the x tick labels.
            If 'default', then no further actions are taken.
            If None, then the ticks are removed.
            Default: 'default'

        yticklabels: list, optional.
            Desired value to place the y tick labels.
            If 'default', then no further actions are taken.
            If None, then the ticks are removed.
            Default: 'default'

        color: str, list(str) or dict(isurf:float), optional.
            If str, it must be a valid color argument of matplotlib, e.g. 'k'.
            Then that color is assigned to all plotted surfaces.
            If list(str), it must be of lenght equal to number of surfaces.
            It specifies the color of every surface.
            If dict, then keys must be the index of the surfaces.
            Default: 'k'
            Alias: colors, color_surfaces, colors_surfaces

        linewidth: float, list(float) or dict(isurf:float), optional.
            If float, then that line width is assigned to all plotted surfaces.
            If list(float), it must be of lenght equal to number of surfaces.
            It specifies the line width of every surface.
            If dict, then keys must be the index of the surfaces.
            Default: 1
            Alias: lw

        linestyle: float, list(float) or dict, optional.
            If float, then that line style is assigned to all plotted surfaces.
            If list(float), it must be of lenght equal to number of surfaces.
            It specifies the line width of every surface.
            If dict, then keys must be the index of the surfaces.
            Default: '-'
            Alias: ls

        stand_alone: bool, optional.
            If True, then use plt.show()
            Alias: plot, show, print.




        Returns
        -------

        fig: matplotlib figure
            If canvas was given, then it returns the fig of that canvas.
            Else, it returns the new fig created for the new canvas.

        canvas: matplotlib axes
            If canvas was given, the same canvas.
            Else, it returns the new canvas.




        Notes
        -----
        Add the option of line2D to pass it so it can be use elsewhere
        object.append(new lines in the for loop) then can be add with
        axis.add_line(line)


        For the future, separate clearly the process and kwargs related to the
        creation and process of the canvas from the creation and process
        of the vessel.
        """

        # Parameters of Figure
        fig_size = priority(['figsize','fig_size','size_fig'], kwargs, (8,6))
        xlim = priority('xlim', kwargs, None)
        ylim = priority('ylim', kwargs, None)
        xticklabels = priority('xticklabels', kwargs, 'default')
        yticklabels = priority('yticklabels', kwargs, 'default')

        # Parameters of Vessel
        color = priority(['color','colors','color_surfaces',
            'colors_surfaces'], kwargs, 'k')
        lw = priority(['linewidth','lw'], kwargs, 1)
        ls = priority(['linestyle','ls'], kwargs, '-')

        # Parameters of Print
        stand_alone = priority(['stand_alone', 'plot','show', 'print'],
                kwargs, False)


        #Get number of surfaces creating the vessel.
        nsurf = self._mother.vessel.shape[1]

        # Create default dicts with given or default single arguments, str/num.
        default_color = color if type(color) is str else 'k'
        default_ls = ls if type(ls) is str else '-'
        default_lw = lw if (type(lw) is int or type(lw) is float) else 1.0

        vessel_color = {i: default_color for i in xrange(nsurf)}
        vessel_ls = {i: default_ls for i in xrange(nsurf)}
        vessel_lw = {i: default_lw for i in xrange(nsurf)}

        # Transform inputs into dicts if they are lists.
        if type(color) is list:
            color = {i: color[i] for i in xrange(nsurf)}
        if type(ls) is list:
            ls = {i: ls[i] for i in xrange(nsurf)}
        if type(lw) is list:
            lw = {i: lw[i] for i in xrange(nsurf)}

        # Update defaults  with user inputs for specific surfaces.
        if type(color) is dict:
            vessel_color.update(color)
        if type(ls) is dict:
            vessel_ls.update(ls)
        if type(lw) is dict:
            vessel_lw.update(lw)


        # Create a canvas if necessary.
        if canvas is None:
            fig = plt.figure(figsize=fig_size)
            canvas = fig.add_subplot(1,1,1)
        else:
            fig = canvas.get_figure()

        # It works. Figure is what user must re-adjust then.
        # Maybe do it automatically if xlim or ylim are detected
        # and no canvas was given
        if xlim is not None or ylim is not None:
            canvas.axes.set_aspect('equal') # Allow [xy]lim
            if ylim is not None:
                canvas.set_ylim(ylim)
            if xlim is not None:
                canvas.set_xlim(xlim)
        else:
            canvas.axis('equal')

        if xticklabels is not None and xticklabels != 'default':
            canvas.axes.set_xticklabels(xticklabels)
        elif xticklabels is None:
            canvas.axes.set_xticklabels([])

        if yticklabels is not None and yticklabels != 'default':
            canvas.axes.set_yticklabels(yticklabels)
        elif yticklabels is None:
            canvas.axes.set_yticklabels([])

        for i in xrange(nsurf):
            R = np.array([self._mother.vessel[0,i],self._mother.vessel[2,i]])
            z = np.array([self._mother.vessel[1,i],self._mother.vessel[3,i]])
            canvas.plot(R,z, color=vessel_color[i], linewidth=vessel_lw[i],
                    ls=vessel_ls[i])

        if stand_alone:
            plt.show()

        return fig, canvas



                         # --------------------------
    def grid_data(self, var = 'grid', canvas = None, **kwargs):
        """ Plot a quantity in the b2 grid.

        Parameters
        ----------
        var: str or numpy array
            Quantity to be plotted. It can be passed as the name of an
            attribute of self or a numpy array.
            Defaults to 'grid' which will simply plot the grid.

        canvas: matplotlib canvas object (ax, fig, etc), optional
            The vessel and the grid will be plotted in the given canvas,
            which can be an axis of a figure, a whole figure, etc.
            Defaults to creating a new plt.figure object.

        prettify: dictionary, optional.
            Allow to pass all the other aesthetic variables in
            dictionary form. This helps to call this function more than once
            using the same dictionary instead of re-writting the kwargs.
            Locally defined kwargs will overwrite the ones on prettify.

        The possible options for kwargs and prettify are:
            computational: If True, show computational grid instead (Not yet ready)
            title: str
                Title of the plot.

            index: If var is a str naming a quantity with more than [nx,ny]
                (e.g. also ns), then the plotted variable is [:,:,index]
                There is a list of allowed aliases:
                ['index','ind','dimension', 'dim','ns','natm','nmol','nion']

            mask: A mask which will cover False labelled cells with light Grey.

            cmap: Preferred color map of the type given in matplotlib.cm or plt.cm.
                 Can be either the object or the name of matplotlib color map.

            cmap_shift: Moves the center of the color map from 0.5 to cmap_shift.
                 Then, 0 is bottom of color bar and 1 is top.
                 In the future, provide limits to the function.

            clim: Variable limits (This should be varlim, while clim referred to
                the limits of the colors in the color map)

            cbar: If True, it shows the color bar.

            clabel: Label attached to the color bar.

            norm: Norm of the color bar. Can be either linear or log.

            lw: Linewidth of the grid outline.
                If zero, a thin outline of the grid will be shown.
                If 0.75,e.g, b2plot style is achieved.

            use_grid: Toggles grid. Default to True (lw = 0.0).
                If False, then lw=0.75.

            fig_labels: Figure labels R[m] and z[m]

            separatrix: If True, print the field line of the separatrix.

            separatrix_color: Color of the field line of the separatrix.
                            If 'special', then white over black line.

            separatrix_lw: Line width of the field line of the separatrix.

            plot_vessel: Plot vessel automatically in canvas. Default to True.


        To be done
        ----------
        Add computational grid option

        """

        #General options
        prettify = priority('prettify', kwargs, {})
        computational = priority('computational', kwargs, prettify, False)
        index = priority(['index','ind','dimension', 'dim','ns','natm','nmol','nion'],
                kwargs, prettify, None)
        title = priority('title', kwargs, '')

        #Mask
        mask = priority('mask', kwargs, prettify, None)
        color_mask = priority(['color_mask','mask_color'], kwargs, prettify,
                'lightgrey')

        #Color map
        cmap = priority(['cmap','color_map'], kwargs, prettify, plt.cm.hot)
        if type(cmap) == str:
            try:
                cmap = plt.cm.__dict__[cmap]
            except:
                cmap = plt.cm.hot
        clim = priority(['clim', 'color_lim','color_limits',
            'colorbar_lim','colorbar_limits'],kwargs, prettify, None)
        cbar = priority(['cbar','color_bar','colorbar'], kwargs, prettify, True)
        clabel = priority('clabel', kwargs, prettify, '')
        norm = priority('norm', kwargs, prettify, None)

        #Add possibility so users can give exact value of center
        cmap_center = priority(['cmap_center','color_map_center',
            'center_cmap','center_color_map'], kwargs, prettify, None)
        cmap_shift = priority(['cmap_shift','shift_cmap',
            'color_map_shift','shift_color_map'], kwargs, prettify, None)
        #cmap_center has priority over cmap_shift
        if cmap_shift and not cmap_center:
            cmap = shifted_color_map(cmap, midpoint=cmap_shift)

        #Grid
        lw  = priority(['lw','grid_lw', 'lw_grid'], kwargs, prettify, 0.00)
        use_grid = priority('use_grid',kwargs, prettify, True)
        edgecolor = priority('edgecolor', kwargs, prettify, 'face')

        #Figure
        fig_size = priority(['figsize','fig_size', 'size_fig'], kwargs, prettify, (8,6))
        fig_labels = priority(['labels_fig','fig_labels'], kwargs, prettify, True)
        separatrix = priority('separatrix', kwargs, prettify, False)
        separatrix_color = priority('separatrix_color', kwargs, prettify, 'special')
        separatrix_lw = priority('separatrix_lw', kwargs, prettify, 1.0)

        plot_vessel = priority(['plot_vessel','vessel'], kwargs, prettify, True)

        vessel_kwargs = priority(['vessel_kwargs','vessel_prettify',
            'kwargs_vessel','prettify_vessel', 'kvessel'], kwargs, prettify, {})
        if ('fig_size' not in vessel_kwargs and
            'figsize'  not in vessel_kwargs and
            'size_fig' not in vessel_kwargs):
            vessel_kwargs['fig_size'] = fig_size
        xlim = priority('xlim', kwargs, prettify, None)
        ylim = priority('ylim', kwargs, prettify, None)
        if xlim is not None:
            vessel_kwargs['xlim'] = xlim
        if ylim is not None:
            vessel_kwargs['ylim'] = ylim



        #Create a canvas in case none is provided
        #Error: If canvas is provided, plot does NOT preserve proportionality
        # if plot_vessel was not called before.
        if canvas is None and not computational:
            #This is reversed as how it should be
            if plot_vessel:
                self.vessel(canvas=canvas,**vessel_kwargs)
            else:
                fig = plt.figure(figsize=fig_size)
                canvas = fig.add_subplot(1,1,1)
            #ATTENTION:
            #Change this to the new return from vessel and canvas.get_fig
            fig= plt.gcf()
            canvas = plt.gca()
        elif canvas is None:
            fig = plt.figure()
            canvas = fig.add_subplot(1,1,1)
        elif canvas and plot_vessel:
            #self.plot_vessel(canvas = canvas, linewidth = 1)
            self.vessel(canvas=canvas, **vessel_kwargs)

        plt.title(title)

        #If var is grid, plot simply the grid
        #ATTENTION: Remove or substitute for region?
        if type(var) == str and var == 'grid':
            for i in range(self._mother.nx):
                for k in range(self._mother.ny):
                    canvas.add_patch(Polygon(self._mother.grid[i,k], True,
                           linewidth = 0.5, edgecolor = 'gray', fill = False))

        #If var is a string, try to get a real var (using index)
        if type(var) == str and var != 'grid':
            try:
                if index is not None:
                    try:
                        var = getattr(self._mother, var)[:,:,index]
                    except:
                        self._mother.log.error("Index is not valid")
                else:
                    var = getattr(self._mother, var)
            except:
                self._mother.log.error("Variable unknown")

        #ATTENTION: substitute this if for a creation or a true (nx,ny) or
        # an exit if that cannot be achieved.
        #If var is a [nx,ny] quantity
        if (type(var) == np.ndarray and var.ndim == 2 and
            var.shape[0]==self._mother.nx and var.shape[1]==self._mother.ny):

            #Create a PatchCollection with all the cells in the grid
            #ATTENTION: Separate this as a _function, to be called
            # independently.
            gres = []
            vgres = []
            if not computational:
                for i in range(self._mother.nx):
                  for k in range(self._mother.ny):
                    gres.append(Polygon(self._mother.grid[i,k]))
                    vgres.append(var[i,k])
            else:
                return #For now
                #for i in range(self.nx):
                #  for k in range(self.ny):
                #    gres.append(Square())
                #    vgres.append(var[i,k])

            vgres = np.array(vgres)
            p = PatchCollection(gres)

            if mask is not None:
                #If mask only delimits the poloidal region, expand to grid
                if (mask.ndim == 1) and (mask.shape == self._mother.nx).all():
                    bad = (np.array([[mask[i]]*self._mother.ny
                           for i in range(self._mother.nx)]) == False)
                elif (mask.ndim == 2) and (mask.shape == ( self._mother.nx,self._mother.ny)):
                    bad = (mask == False)
                cmap.set_bad(color=color_mask)
                bad = bad.reshape(self._mother.nx*self._mother.ny)
                vgres = np.ma.masked_where(bad,vgres)
                p.set_clim((np.min(vgres),np.max(vgres)))


            if clim is not None:
                p.set_clim(clim)
                aux_cmap = plt.cm.get_cmap(cmap)
                cmap.set_under(aux_cmap(0.0))
                cmap.set_over(aux_cmap(1.0))


            #ATTENTION: Does not work with clims yet
            if cmap_center is not None:
                #Approximation between max and min of data to be used
                mx = np.max(vgres) if clim is None else clim[1]
                mn = np.min(vgres) if clim is None else clim[0]
                a = mx-mn
                b = mn
                center_fraction = (cmap_center-b)/a
                cmap = shifted_color_map(cmap, name=cmap.name, midpoint=center_fraction)


            #Linear is the default, but if log, clim has to be set again inside
            #ATTENTION:
            #center_cmap won't probably work with this, but
            #center_cmap is more important
            #for divergent color maps, which are most likely negative
            #(and thus, not suitable for log scale)
            if norm == 'log':
              if clim is not None:
                p.set_norm(colors.LogNorm(vmin = clim[0], vmax = clim[1]))
              else:
                p.set_norm(colors.LogNorm())

            if use_grid:
                p.set(array=vgres, cmap =cmap, linewidth=lw)
            else:
                p.set(array=vgres, cmap =cmap, linewidth=0.75)
            p.set_edgecolor(edgecolor)
            canvas.add_collection(p)

            #Aesthetics for the color bar
            #Should be plt. or canvas.?
            if cbar:
                if clim is not None and clabel:
                    plt.colorbar(p, extend = 'both', label=clabel)
                elif clim is not None:
                    plt.colorbar(p, extend = 'both')
                elif clabel:
                    plt.colorbar(p, label=clabel)
                else:
                    plt.colorbar(p)

            #Print separatrix or not
            if separatrix and not computational:
                self.fieldline(canvas=canvas, color=separatrix_color,
                                    lw=separatrix_lw)
            elif separatrix and computational:
                pass #Plot a straight line of the patches

            #Show figure labels R[m] and z[m]
            if fig_labels and not computational:
                plt.xlabel('R [m]')
                plt.ylabel('z [m]')

        else:
            self._mother.log.error("Data should be [nx,ny] or [nx,ny,index],"+
                           " if index was specified")



    def gdata(self, var = 'grid', canvas = None, **kwargs):
        """ Plot a quantity in the b2 grid.

        Parameters
        ----------
        var: str or numpy array
            Quantity to be plotted. It can be passed as the name of an
            attribute of self or a numpy array.
            Defaults to 'grid' which will simply plot the grid.

        canvas: matplotlib canvas object (ax, fig, etc), optional
            The vessel and the grid will be plotted in the given canvas,
            which can be an axis of a figure, a whole figure, etc.
            Defaults to creating a new plt.figure object.

        prettify: dictionary, optional.
            Allow to pass all the other aesthetic variables in
            dictionary form. This helps to call this function more than once
            using the same dictionary instead of re-writting the kwargs.
            Locally defined kwargs will overwrite the ones on prettify.

        The possible options for kwargs and prettify are:
            computational: If True, show computational grid instead (Not yet ready)
            title: str
                Title of the plot.

            mask: A mask which will cover False labelled cells with light Grey.

            cmap: Preferred color map of the type given in matplotlib.cm or plt.cm.
                 Can be either the object or the name of matplotlib color map.

            cmap_shift: Moves the center of the color map from 0.5 to cmap_shift.
                 Then, 0 is bottom of color bar and 1 is top.
                 In the future, provide limits to the function.

            clim: Variable limits (This should be varlim, while clim referred to
                the limits of the colors in the color map)

            cbar: If True, it shows the color bar.

            clabel: Label attached to the color bar.

            norm: Norm of the color bar. Can be either linear or log.

            lw: Linewidth of the grid outline.
                If zero, a thin outline of the grid will be shown.
                If 0.75,e.g, b2plot style is achieved.

            use_grid: Toggles grid. Default to True (lw = 0.0).
                If False, then lw=0.75.

            fig_labels: Figure labels R[m] and z[m]

            separatrix: If True, print the field line of the separatrix.

            separatrix_color: Color of the field line of the separatrix.
                            If 'special', then white over black line.

            separatrix_lw: Line width of the field line of the separatrix.

            plot_vessel: Plot vessel automatically in canvas. Default to True.


        To be done
        ----------
        Add computational grid option

        """
        config = read_config()['Plots']['gdata']
        prettify = priority('prettify', kwargs, {})

        ##---------------------------------------------------------------------
        ## General options.
        # Computational grid or real grid.
        computational = priority('computational', kwargs, prettify,
                config['computational'])

        # Title of the figure
        title = priority('title', kwargs, prettify, '')



        ##---------------------------------------------------------------------
        ## Mask options.
        # Mask to apply to data. True are values shown; False, not shown.
        mask = priority('mask', kwargs, prettify, config['mask'])

        # Color of the masked values.
        color_mask = priority(['color_mask','mask_color'], kwargs, prettify,
                config['color_mask'])



        ##---------------------------------------------------------------------
        ## Color map options.
        # Map color from matplotlib or user defined one with same signature
        cmap = priority(['cmap','color_map'], kwargs, prettify, 'hot')
        # If a str is given, check if it is a real matplotlib color map.
        if type(cmap) == str:
            try:
                cmap = getattr(plt.cm, cmap)
            except:
                cmap = plt.cm.hot

        # Norm of the color map
        norm = priority('norm', kwargs, prettify, None)

        # Limits of the color map, i.e, of the variable
        clim = priority(['clim', 'color_lim','color_limits',
            'colorbar_lim','colorbar_limits', 'zlim', 'var_lim'],
            kwargs, prettify, config['clim'])

        # Show colorbar?
        cbar = priority(['cbar','colorbar','color_bar'], kwargs, prettify,
                config['cbar'])

        # Label of the color bar
        clabel = priority(['clabel','colorbar_label'], kwargs, prettify,
                config['clabel'])


        #Add possibility so users can give exact value of center

        #cmap_center defines the exact value to be the center.
        cmap_center = priority(['cmap_center','color_map_center',
            'center_cmap','center_color_map'], kwargs, prettify,
            config['cmap_center'])

        #cmap_shitf shifts the value by the given amount.
        cmap_shift = priority(['cmap_shift','shift_cmap',
            'color_map_shift','shift_color_map'], kwargs, prettify,
            config['cmap_shift'])

        #cmap_center has priority over cmap_shift
        if cmap_shift and not cmap_center:
            cmap = shifted_color_map(cmap, midpoint=cmap_shift)



        ##---------------------------------------------------------------------
        ## Grid
        # Use grid?
        use_grid = priority('use_grid',kwargs, prettify, True)

        # Line width of the grid
        lw  = priority(['lw', 'linewidth', 'grid_lw', 'lw_grid'],
                kwargs, prettify, config['lw'])

        # Edge color. If 'face', then same as facecolor.
        edgecolor = priority(['edgecolor','ec'], kwargs, prettify,
                config['edgecolor'])


        ##---------------------------------------------------------------------
        ## Figure. Some should be passed down to figure creator
        # Size of the figure if it is to be created anew.
        figsize = priority(['figsize','fig_size', 'size_fig'], kwargs, prettify,
                config['figsize'])

        # Print the labels of the figure?
        fig_labels = priority(['labels_fig','fig_labels'], kwargs, prettify, True)

        # Plot the separatrix?
        separatrix = priority('separatrix', kwargs, prettify, False)

        # If true, what colour? 'special' means black over white.
        separatrix_color = priority('separatrix_color', kwargs, prettify, 'special')

        # If true, what line width?
        separatrix_lw = priority('separatrix_lw', kwargs, prettify, 1.0)


        # Plot the vessel?
        plot_vessel = priority(['plot_vessel','vessel'], kwargs, prettify, True)

        vessel_kwargs = priority(['vessel_kwargs','vessel_prettify',
            'kwargs_vessel','prettify_vessel', 'kvessel'], kwargs, prettify, {})
        if ('figsize' not in vessel_kwargs and
            'figsize'  not in vessel_kwargs and
            'size_fig' not in vessel_kwargs):
            vessel_kwargs['figsize'] = figsize
        xlim = priority('xlim', kwargs, prettify, None)
        ylim = priority('ylim', kwargs, prettify, None)
        if xlim is not None:
            vessel_kwargs['xlim'] = xlim
        if ylim is not None:
            vessel_kwargs['ylim'] = ylim



        ##---------------------------------------------------------------------
        #Create a canvas in case none is provided
        #Error: If canvas is provided, plot does NOT preserve proportionality
        # if plot_vessel was not called before.
        if canvas is None and not computational:
            #This is reversed as how it should be
            if plot_vessel:
                self.vessel(canvas=canvas,**vessel_kwargs)
            else:
                fig = plt.figure(figsize=figsize)
                canvas = fig.add_subplot(1,1,1)
            #ATTENTION:
            #Change this to the new return from vessel and canvas.get_fig
            fig= plt.gcf()
            canvas = plt.gca()
        elif canvas is None:
            fig = plt.figure()
            canvas = fig.add_subplot(1,1,1)
        elif canvas and plot_vessel:
            #self.plot_vessel(canvas = canvas, linewidth = 1)
            self.vessel(canvas=canvas, **vessel_kwargs)

        plt.title(title)

        #If var is grid, plot simply the grid
        #ATTENTION: Remove or substitute for region?
        if type(var) == str and var == 'grid':
            for i in range(self._mother.nx):
                for k in range(self._mother.ny):
                    canvas.add_patch(Polygon(self._mother.grid[i,k], True,
                           linewidth = 0.5, edgecolor = 'gray', fill = False))

        #If var is a string, try to get a real var (using index)
        if type(var) == str and var != 'grid':
            try:
                if index is not None:
                    try:
                        var = getattr(self._mother, var)[:,:,index]
                    except:
                        self._mother.log.error("Index is not valid")
                else:
                    var = getattr(self._mother, var)
            except:
                self._mother.log.error("Variable unknown")

        #ATTENTION: substitute this if for a creation or a true (nx,ny) or
        # an exit if that cannot be achieved.
        #If var is a [nx,ny] quantity
        if (type(var) == np.ndarray and var.ndim == 2 and
            var.shape[0]==self._mother.nx and var.shape[1]==self._mother.ny):

            #Create a PatchCollection with all the cells in the grid
            #ATTENTION: Separate this as a _function, to be called
            # independently.
            gres = []
            vgres = []
            if not computational:
                for i in range(self._mother.nx):
                  for k in range(self._mother.ny):
                    gres.append(Polygon(self._mother.grid[i,k]))
                    vgres.append(var[i,k])
            else:
                return #For now
                #for i in range(self.nx):
                #  for k in range(self.ny):
                #    gres.append(Square())
                #    vgres.append(var[i,k])

            vgres = np.array(vgres)
            p = PatchCollection(gres)

            if mask is not None:
                #If mask only delimits the poloidal region, expand to grid
                if (mask.ndim == 1) and (mask.shape == self._mother.nx).all():
                    bad = (np.array([[mask[i]]*self._mother.ny
                           for i in range(self._mother.nx)]) == False)
                elif (mask.ndim == 2) and (mask.shape == ( self._mother.nx,self._mother.ny)):
                    bad = (mask == False)
                cmap.set_bad(color=color_mask)
                bad = bad.reshape(self._mother.nx*self._mother.ny)
                vgres = np.ma.masked_where(bad,vgres)
                p.set_clim((np.min(vgres),np.max(vgres)))


            if clim is not None:
                p.set_clim(clim)
                aux_cmap = plt.cm.get_cmap(cmap)
                cmap.set_under(aux_cmap(0.0))
                cmap.set_over(aux_cmap(1.0))


            #ATTENTION: Does not work with clims yet
            if cmap_center is not None:
                #Approximation between max and min of data to be used
                mx = np.max(vgres) if clim is None else clim[1]
                mn = np.min(vgres) if clim is None else clim[0]
                a = mx-mn
                b = mn
                center_fraction = (cmap_center-b)/a
                cmap = shifted_color_map(cmap, name=cmap.name, midpoint=center_fraction)


            #Linear is the default, but if log, clim has to be set again inside
            #ATTENTION:
            #center_cmap won't probably work with this, but
            #center_cmap is more important
            #for divergent color maps, which are most likely negative
            #(and thus, not suitable for log scale)
            if norm == 'log':
              if clim is not None:
                p.set_norm(colors.LogNorm(vmin = clim[0], vmax = clim[1]))
              else:
                p.set_norm(colors.LogNorm())

            if use_grid:
                p.set(array=vgres, cmap =cmap, linewidth=lw)
            else:
                p.set(array=vgres, cmap =cmap, linewidth=0.75)
            p.set_edgecolor(edgecolor)
            canvas.add_collection(p)

            #Aesthetics for the color bar
            #Should be plt. or canvas.?
            if cbar:
                if clim is not None and clabel:
                    plt.colorbar(p, extend = 'both', label=clabel)
                elif clim is not None:
                    plt.colorbar(p, extend = 'both')
                elif clabel:
                    plt.colorbar(p, label=clabel)
                else:
                    plt.colorbar(p)

            #Print separatrix or not
            if separatrix and not computational:
                self.fieldline(canvas=canvas, color=separatrix_color,
                                    lw=separatrix_lw)
            elif separatrix and computational:
                pass #Plot a straight line of the patches

            #Show figure labels R[m] and z[m]
            if fig_labels and not computational:
                plt.xlabel('R [m]')
                plt.ylabel('z [m]')

        else:
            self._mother.log.error("Data should be [nx,ny] or [nx,ny,index],"+
                           " if index was specified")



    #ATTENTION: Fix this
    def fieldline(self, canvas=None, iy=None, face=None,
                       ixrange=None, **kwargs):
        """ybf is the cell center which face will be the field line
        Consider adding automatically the bottom/top face when the opposite
        face option is activated (or else the users will need to remember to add
        +1/-1 to their ixrange)
        """

        color = 'special' if 'color' not in kwargs else kwargs['color']
        lw = 1.0 if 'lw' not in kwargs else kwargs['lw']

        if iy is None:
            iy = self._mother.sep
        if face is None:
            face = 'bottom'

        if (face=='bottom') or (face=='b'):
            iyy=iy-1
        if (face=='top') or (face=='t'):
            iyy=iy+1

        if ixrange is None:
            ixrange = range(self._mother.nx)

        fieldline = np.zeros((len(ixrange),2))
        for i,ix in enumerate(ixrange):
            fieldline[i] = cellface(self._mother.grid[ix,iy],
                    self._mother.grid[ix,iyy])[0]

        #Last one nx+1
        #fieldline[-1] = cellface(self.grid[i,iy], self.grid[i,iyy])[1]

        if canvas is None:
            return fieldline
        elif canvas is not None:
            if color == 'special':
                canvas.plot(fieldline[:,0],fieldline[:,1],color='black',lw=lw+2.0)
                canvas.plot(fieldline[:,0],fieldline[:,1],color='white',lw=lw)
            else:
                canvas.plot(fieldline[:,0],fieldline[:,1],color=color,lw=lw)






    def region(self, **kwargs):
        """ Highlight the requested grid subregion
        In the future, maybe add the possibility of different colors for different regions
        """

        #General options
        prettify = priority('prettify', kwargs, {})
        canvas = priority(['canvas','fig','figure'], kwargs, None)

        #Region
        iy = priority(['iy','yrange'],kwargs,[])
        ix = priority(['ix','xrange'],kwargs,[])
        mask = priority(['mask','region'], kwargs, None)

        #Grid
        grid_facecolor = priority(['grid_facecolor','facecolor_grid','gfc'],
                kwargs, prettify, 'white')
        grid_edgecolor = priority(['grid_edgecolor','edgecolor_grid','gec'],
                kwargs, prettify, 'lightgrey')
        grid_alpha = priority(['grid_alpha','alpha_grid'], kwargs, prettify, 1.0)
        grid_lw = priority(['grid_lw','lw_grid','grid_linewidth','linewidth_grid'],
                kwargs, prettify, 0.25)

        #Highlights
        hl_color = priority(['hl_color','color_hl','highlight_color',
            'color_highlight','color'], kwargs, prettify, 'yellow')
        hl_alpha = priority(['hl_alpha','alpha_hl'], kwargs, prettify, 1.0)

        #Figure
        computational = priority('computational', kwargs, prettify, False)
        fig_size = priority(['figsize','fig_size', 'size_fig'], kwargs,
                prettify, (8,6))
        fig_labels = priority(['labels_fig','fig_labels'], kwargs, prettify, True)
        plot_vessel = priority('plot_vessel', kwargs, prettify, True)

        vessel_kwargs = priority(['vessel_kwargs','vessel_prettify',
            'kwargs_vessel','prettify_vessel', 'kvessel'], kwargs, prettify, {})
        if ('fig_size' not in vessel_kwargs and
            'figsize'  not in vessel_kwargs and
            'size_fig' not in vessel_kwargs):
            vessel_kwargs['fig_size'] = fig_size
        xlim = priority('xlim', kwargs, prettify, None)
        ylim = priority('ylim', kwargs, prettify, None)
        if xlim is not None:
            vessel_kwargs['xlim'] = xlim
        if ylim is not None:
            vessel_kwargs['ylim'] = ylim

        #Expand ix and iy into a mask, if the lattest does not exist
        #For now it only accepts integers or lists
        if ix is not [] and type(ix) is not list: ix = [ix]
        if iy is not [] and type(iy) is not list: iy = [iy]


        if mask is None and (ix or iy):
            mask1 = np.zeros((self._mother.nx, self._mother.ny))
            mask2 = np.zeros((self._mother.nx, self._mother.ny))
            for i in xrange(self._mother.nx):
                for k in xrange(self._mother.ny):
                    if (i in ix):
                        mask1[i,k] = 1
                    if (k in iy):
                        mask2[i,k] = 1
            if ix and iy:
                mask = np.logical_and(mask1,mask2)
            elif ix:
                mask = mask1
            elif iy:
                mask = mask2



        #Create a PatchCollection with all the cells in the grid
        gres = []
        if grid_facecolor.lower() == 'none':
            cgres = ['none']*(self._mother.nx*self._mother.ny)
        else:
            cgres = [grid_facecolor]*(self._mother.nx*self._mother.ny)
        l=0
        if not computational:
            for i in xrange(self._mother.nx):
                for k in xrange(self._mother.ny):
                    gres.append(Polygon(self._mother.grid[i,k]))
                    if mask is not None and mask[i,k]:
                        cgres[l] = hl_color
                    l+=1
        else:
            return #For now
            #for i in range(self.nx):
            #  for k in range(self.ny):
            #    gres.append(Square())

        p = PatchCollection(gres)
        p.set(edgecolor=grid_edgecolor, facecolor=cgres, linewidth=grid_lw)

        if canvas is None and not computational and plot_vessel:
            fig = plt.figure(figsize=fig_size)
            canvas = fig.add_subplot(1,1,1)
            self.vessel(canvas=canvas,**vessel_kwargs)
        elif canvas is None and computational:
            fig = plt.figure()
            canvas = fig.add_subplot(1,1,1)
        try:
            canvas.add_collection(p)
        except:
            pass




    def evolution(self, canvas=None, **kwargs):
        """ This class animates the time evolution of a given quantity, e.g,
        a (ntime, nx,ny) array representing the grid data of a certain
        quantity or the (ntime, ny) array representing a target profile.

        Parameters
        ----------
        canvas: matplotlib axes, optional.

        prettify:

        figsize: tuple or list, optional.

        vessel: bool, optional.
            Plot the vessel or not.

        vessel_params: dict, optional.
            Arguments passed directly to vessel plotting function.

        gdata_params: dict, optional.
            Arguments passed directly to grid_data plotting function.
            May include itself a vessel_params.


        """
        # Unpack kwargs
        prettify = kwargs['prettify'] if 'prettify' in kwargs else {}

        vessel = priority('vessel', prettify, kwargs, True)

        # Get base canvas
        if canvas is None and vessel == False:
            fig, canvas = plt.subplots(figsize=figsize)
        def init():
            #Call either self.vessel?
            return False

        def time_step(i):
            #Call either self.grid_data or self.profile?
            return False

        #anim = animation.funcAnimation(init_func=init, blit=True)









    @staticmethod
    def show(*args,**kwargs):
        plt.show(*args,**kwargs)






    @solps_property
    def pdfpages(self):
        #import pdb; pdb.set_trace()
        try:
            storagepath = self._mother.rundir
        except:
            storagepath = os.path.abspath('.')
        #self._pdfpages = self.Pdfpages(storagepath)
        self._pdfpages = self.Pdfpages(self)


    class Pdfpages(object):
        def __init__(self, mother):
            self._mother = mother
            self.set_storage_path(mother.storage_path)

        def set_storage_path(self,storagepath):
            storagepath = os.path.abspath(storagepath)
            if os.path.isdir(storagepath):
                self.storagepath = storagepath
            else:
                self._mother.log.error('Invalid path: %s',storagepath)

        def open(self,filename):
            """Opens an pdfpages object which allows to save all plots into
            a pdf file. If a pdfpages object was opened, all plots which are
            made are saved automatically."""
            self.close()
            if not filename.endswith('.pdf'): filename += '.pdf'
            filename = os.path.join(self.storagepath,filename)
            self.pdfpages = PdfPages(filename)

        def save(self):
            """Saves the current plot in the pdfpages object, if one was opened."""
            try:
                self.pdfpages.savefig()
            except:
                pass

        def close(self):
            """Closes the pdfpages object.
            REMARK: Don't forget this, or the pdf file will not be created!"""
            try:
                self.pdfpages.close()
                del self.pdfpages
            except:
                pass
