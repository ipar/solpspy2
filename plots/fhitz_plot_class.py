#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:
    Provides methods for plotting and visualization of data.
"""

# Common modules
from __future__ import print_function
import os
import sys
import logging
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.patches import Polygon,Circle,Arrow
from matplotlib.collections import PatchCollection
from subprocess import call
from collections import OrderedDict

# Special modules
from solpspy.tools.tools import priority, LinAlg2d
from solpspy.fhitz_plot_labels import PlotLabels
from solpspy.fhitz_force_analysis import ForceAnalysis,ForceAnalysisSubplots
from solpspy.fhitz_plot_profiles import xy, ProfileSubplots, PoloidalProfile, RadialProfile
from solpspy.fhitz_plot_tools import savefig_fhitz, convert_figsize, MarkerLine

# Debugging (remove later)
#import pdb; pdb.set_trace()
from IPython import embed

# =============================================================================

class PlotMethods(object):

    def __init__(self,solpspy_run):
        logging.basicConfig()
        self._log = logging.getLogger('PlotMethods Class')
        self._log.setLevel('INFO')
        self._run = solpspy_run
        self.plotLabels = PlotLabels(self._run)

    # =======================================================

    def ionization_front_2d(self,**kwargs):
        return IonizationFront2D(solpspy_run=self._run,**kwargs)

    def force_analysis(self,atom='N',fluxtubes=[0,2,4,6],**kwargs):
        f = ForceAnalysisSubplots(solpspy_run=self._run,atom=atom,
                                  fluxtubes=fluxtubes,**kwargs)
        return f

    def highlight_cell(self,nx,ny,**kwargs):
        kwargs['cmap']      = kwargs.pop('cmap',plt.cm.hot_r)
        kwargs['edgecolor'] = kwargs.pop('edgecolor','k')
        kwargs['lw']        = kwargs.pop('lw',0.05)
        cells = np.zeros((self._run.nx,self._run.ny))
        cells[nx,ny] = 1
        self.plot2d(cells,**kwargs)

    def plot2d(self,var,**kwargs):
        # Set defaults:
        xlim           = kwargs.pop('xlim'    , None)
        ylim           = kwargs.pop('ylim'    , None)
        showplot       = kwargs.pop('showplot', True)
        saveas         = kwargs.pop('saveas'  , None)
        kwargs['cmap'] = kwargs.pop('cmap'    , plt.cm.jet)

        # Additional surface color coding:
        color_coding   = kwargs.pop('color_coding',{2:'r',
                                                    3:'0.8',
                                                    4:'m',
                                                    5:'b',
                                                    6:'0.8' # 6:'c'
                                                    })
        try:
            vc     = self._run.vessel_colors
            colors = {i : color_coding.get(c,'k') for i,c in vc.items()}
            vessel_kwargs = {'color' : colors}
            kwargs['vessel_kwargs'] = vessel_kwargs
        except:
            pass

        # Plot:
        if var is None:
            self._run.plot.vessel(**vessel_kwargs)
        else:
            self._run.plot.grid_data(var=var,**kwargs)

        # Adjust plot:
        fig = plt.gcf()
        fig.set_size_inches(6.3,8,forward=True)

        if 'title' in kwargs:
            plt.title(kwargs['title'])

        #self.canvas.axis('equal')

        # Set plot limits:
        xmin=self._run.grid[:,:,:,0].min()
        xmax=self._run.grid[:,:,:,0].max()
        ymin=self._run.grid[:,:,:,1].min()
        ymax=self._run.grid[:,:,:,1].max()
        dy = (ymax - ymin)*0.05
        dx = (xmax - xmin)*0.05
        xmin=xmin-dx
        xmax=xmax+dx
        ymin=ymin-dy
        ymax=ymax+dy
        if xlim is not None:
            xmin=xlim[0]
            xmax=xlim[1]
        if ylim is not None:
            ymin=ylim[0]
            ymax=ylim[1]
        plt.axis([xmin, xmax, ymin, ymax])

        plt.tight_layout()

        savefig_fhitz(saveas)
        if showplot:
            plt.show()

        plt.close()

    # =======================================================

    def plot_force(self,force,species='N-N7+',fd=True,**kwargs):
        if fd:
            f = self._run.force_density(force,species)
        else:
            f = np.zeros((self._run.nx,self._run.ny))
            for ns in self._run.species_index(species):
                f += force[:,:,ns]

        if fd:
            cl = 'Force density [$\mathrm{Nm}^{-3}$]'
        else:
            cl = 'Force (on the whole cell volume) [$\mathrm{N}$]'
        kwargs['clabel'] = kwargs.get('clabel',cl)
        kwargs['clim'] = kwargs.get('clim',[-100,100])
        kwargs['cmap'] = kwargs.get('cmap',plt.cm.seismic)
        kwargs['cmap_center'] = kwargs.get('cmap_center',0)
        self.plot2d(f,**kwargs)

    # =======================================================
    # Dataset Plots:
    # ...
    #def D_fueling_scan(self):
    #    ...


#    # =======================================================
#    # Profiles:
#
#    def _create_profile_xyplot_lists(self,direction,varlist,poslist,
#                                     labellist=None,ylabellist=None,
#                                     comp=False):
#        # Preparation:
#        if type(varlist) != list: varlist = [varlist]
#        if type(poslist) != list: poslist = [poslist]
#        if  labellist is None:  labellist = len(varlist)*[None]
#        if ylabellist is None: ylabellist = len(varlist)*[None]
#        if (len(labellist) != len(varlist) 
#            and len(labellist) != len(varlist)*len(poslist)):
#            self._log.error('Incompatible size of labellist and varlist. '
#                            + '%s vs. %s.', len(labellist), len(varlist))
#            return
#        if (len(ylabellist) != len(varlist)
#            and len(labellist) != len(varlist)*len(poslist)):
#            self._log.error('Incompatible size of ylabellist and varlist: '
#                            + '%s vs. %s.', len(ylabellist), len(varlist))
#            return
#
#        if comp:
#            dx = np.array(self._run.ny*[range(self._run.nx)]).transpose()
#            dy = np.array(self._run.nx*[range(self._run.ny)])
#        else:
#            dx = self._run.domp
#            dy = self._run.ds
#
#        # For each list in xyplots a subplot will be created containing every
#        # xy-object in the list - so it's a list of lists of xy-objects.
#        xyplots = [[]] 
#
#        # A plot of different variables at different positions:
#        if len(varlist) > 1 and len(poslist) > 1:
#            for v,var in enumerate(varlist):
#
#                if len(xyplots[-1]) > 0:
#                    xyplots.append([])
#
#                for p,pos in enumerate(poslist):
#                    if type(var) == str:
#                        data   = self.plotLabels[var].data
#                        xyargs = self.plotLabels[var].return_xyargs()
#                    else:
#                        data = var
#                        xyargs = {}
#
#                    if  labellist[0] != None: 
#                        if len(labellist) == len(varlist)*len(poslist):
#                            xyargs['label'] =  labellist[len(poslist)*v+p]
#                        else:
#                            xyargs['label'] =  labellist[v]
#                    if ylabellist[v] != None: 
#                        xyargs['ylabel'] = ylabellist[v]
#
#                    if direction == 'x':
#                        x = dx[:,pos]
#                        y = data[:,pos]
#                        postxt = 'ny = ' + str(pos)
#                    elif direction == 'y':
#                        x = dy[pos,:]
#                        y = data[pos,:]
#                        postxt = 'nx = ' + str(pos)
#
#                    if 'label' in xyargs:
#                        if len(labellist) != len(varlist)*len(poslist):
#                            xyargs['label'] += ' (' + postxt + ')'
#                    else:
#                        xyargs['label'] = postxt
#
#                    xyplots[-1].append( xy(x,y,**xyargs) )
#
#        # A plot of different variables at one position:
#        if len(varlist) > 1 and len(poslist) == 1:
#
#            pos = poslist[0]
#            for v,var in enumerate(varlist):
#
#                if type(var) == str:
#                    data   = self.plotLabels[var].data
#                    xyargs = self.plotLabels[var].return_xyargs()
#                else:
#                    data = var
#                    xyargs = {}
#
#                if  labellist[v] != None: xyargs[ 'label'] =  labellist[v]
#                if ylabellist[v] != None: xyargs['ylabel'] = ylabellist[v]
#
#                if direction == 'x':
#                    x = dx[:,pos]
#                    y = data[:,pos]
#                elif direction == 'y':
#                    x = dy[pos,:]
#                    y = data[pos,:]
#
#                if len(xyplots[-1]) > 0:
#                    if 'ylabel' not in xyargs:
#                        xyplots.append([])
#                    elif xyargs['ylabel'] != xyplots[-1][-1].ylabel:
#                        xyplots.append([])
#
#                xyplots[-1].append( xy(x,y,**xyargs) )
#
#        # A plot of one variable for different positions:
#        if len(varlist) == 1 and len(poslist) > 1:
#
#            var = varlist[0]
#            for p,pos in enumerate(poslist):
#
#                if type(var) == str:
#                    data   = self.plotLabels[var].data
#                    xyargs = self.plotLabels[var].return_xyargs()
#                else:
#                    data = var
#                    xyargs = {}
#
#                if  labellist[0] != None: 
#                    if len(labellist) == len(varlist)*len(poslist):
#                        xyargs['label'] =  labellist[p]
#                    else:
#                        xyargs['label'] =  labellist[0]
#                if ylabellist[0] != None: xyargs['ylabel'] = ylabellist[0]
#
#                if direction == 'x':
#                    x = dx[:,pos]
#                    y = data[:,pos]
#                    postxt = 'ny = ' + str(pos)
#                elif direction == 'y':
#                    x = dy[pos,:]
#                    y = data[pos,:]
#                    postxt = 'nx = ' + str(pos)
#
#                if 'label' in xyargs:
#                    if len(labellist) != len(varlist)*len(poslist):
#                        xyargs['label'] += ' (' + postxt + ')'
#                else:
#                    xyargs['label'] = postxt
#
#                xyplots[-1].append( xy(x,y,**xyargs) )
#
#        # A single plot of one variable at one position:
#        if len(varlist) == 1 and len(poslist) == 1:
#            var = varlist[0]
#            pos = poslist[0]
#            if type(var) == str:
#                data   = self.plotLabels[var].data
#                xyargs = self.plotLabels[var].return_xyargs()
#            else:
#                data = var
#                xyargs = {}
#
#            if  labellist[0] != None: xyargs[ 'label'] =  labellist[0]
#            if ylabellist[0] != None: xyargs['ylabel'] = ylabellist[0]
#
#            if direction == 'x':
#                x = dx[:,pos]
#                y = data[:,pos]
#            elif direction == 'y':
#                x = dy[pos,:]
#                y = data[pos,:]
#
#            xyplots[-1].append( xy(x,y,**xyargs) )
#
#        return xyplots
#
#    def plot_radial_profile(self,var,**kwargs):
#        # Possible arguments and defaults:
#        pos      = kwargs.pop('pos'      , self._run.omp)
#        legend   = kwargs.pop('legend'   , None)
#        ylabel   = kwargs.pop('ylabel'   , None)
#        comp     = kwargs.pop('comp'     , False)
#
#        title   = kwargs.get('title'   , '')
#        xlabel  = kwargs.get('xlabel'  , '')
#        if xlabel == '':
#            if comp: xlabel = 'Radial Cell Index'
#            else:    xlabel = 'Radial Distance from the Separatrix [m]'
#        kwargs['xlabel'] = xlabel
#
#        if type(var) != list and len(var.shape) == 3:
#            var = [var[:,:,i] for i in range(var.shape[2])]
#
#        xyplots = self._create_profile_xyplot_lists('y',var,pos,legend,
#                                                    ylabel,comp)
#        self._do_subplots(xyplots,**kwargs)
#
#    def plot_poloidal_profile(self,var,**kwargs):
#        # Possible arguments and defaults:
#        pos     = kwargs.pop('pos'     , self._run.sep)
#        legend  = kwargs.pop('legend'  , None)
#        ylabel  = kwargs.pop('ylabel'  , None)
#        comp    = kwargs.pop('comp'    , False)
#
#        title   = kwargs.get('title'   , '')
#        xlabel  = kwargs.get('xlabel'  , '')
#        if xlabel == '':
#            if comp: xlabel = 'Poloidal Cell Index'
#            else:    xlabel = 'Poloidal Distance from the Outer Midplane [m]'
#        kwargs['xlabel'] = xlabel
#
#        xyplots = self._create_profile_xyplot_lists('x',var,pos,legend,
#                                                    ylabel,comp)
#        self._do_subplots(xyplots,**kwargs)

#    # =======================================================
#    # Time traces:
#
#    def plot_time_trace(self,var,title='',ylabel='',fit=None):
#        """Plot time traces like, e.g. self._run.timetrace.nesepm."""
#        x = self._run.timetrace.timesa
#        if var.shape != x.shape:
#            self._log.error('Invalid shape of time trace variable: %s',
#                            var.shape)
#            return
#        plt.plot(x,var)
#        if type(fit) is bool and fit is True:
#            fit = np.polyfit(x,var,1)
#        if fit is not None:
#            if type(fit) != np.ndarray: 
#                fit = np.polyfit(x,var,1)
#            y = np.zeros((len(var)))
#            for i,fp in enumerate(fit[::-1]): y += fp * x**i # [::-1] reverses
#            plt.plot(x,y)
#        plt.xlabel('Time [s]')
#        plt.ylabel(ylabel)
#        plt.title(title)
#        self._run.plot.pdfpages.save()
#        plt.show()
#        plt.close()

    # =======================================================

#    def plot_rad_prof_for_all_ns(self,var,**kwargs):
#        """Plots the radial profiles of 'var' for all species."""
#
#        if var.shape != (self._run.nx,self._run.ny,self._run.ns):
#            self._log.warning('Invalid variable shape: %s', var.shape)
#            return
#
#        xpos   = kwargs.pop('xpos'   , self._run.omp)
#        title  = kwargs.pop('title'  , '')
#        ylabel = kwargs.pop('ylabel' , '')
#
#        for ns in range(self._run.ns):
#            title_ns = title + ' (' + self._run.species_names[ns] + ')'
#            x = self._run.ds[xpos,:]
#            y = var[xpos,:,ns]
#            plt.plot(x,y)
#            plt.title(title_ns)
#            plt.xlabel('Distance from the outer midplane separatrix')
#            plt.ylabel(ylabel)
#            plt.grid(b=True, which='major', color='0.7', linestyle='-')
#            plt.tight_layout()
#            self._run.plot.pdfpages.save()
#            #plt.show()
#            plt.close()

    def plot_particle_balance(self):
        tt = self._run.timetrace
        x = tt.timesa

        plt.plot(x,tt.D_puff_total,color='r',lw=2,
                 label='Total influx',zorder=9)

        plt.plot(x,tt.D_puff      ,color='r',ls='--',marker='',
                 label='D puff')
        plt.plot(x,tt.D_puff_core ,color='r',ls=':' ,marker='',
                 label='D puff (core)')

        plt.plot(x,tt.D_pump_total,color='g',lw=2,
                 label='Total outflux',zorder=8)

        plt.plot(x,tt.D2_pump     ,color='g',ls='--',marker='',
                 label='D2 pump')
        plt.plot(x,tt.D_pump      ,color='g',ls='-.',marker='',
                 label='D pump')
        plt.plot(x,tt.D_pump_core ,color='g',ls=':' ,marker='',
                 label='D pump (core)')
        plt.plot(x,tt.D2_pump_core,color='g',ls=':' ,marker='',
                 label='D2 pump (core)')

        plt.title('Deuterium particle balance')
        plt.xlabel('Time [s]')
        plt.ylabel('Atomic flux [atoms/second]')
        plt.grid(b=True, which='major', color='0.7', linestyle='-')
        l = plt.legend(loc=5)
        l.set_zorder(20)
        plt.tight_layout()
        plt.show()


#    # =======================================================
#    # General methods:
#
#    def _do_subplots(self,xyplots,**kwargs):
#        """Plots a list of xyplots as a set of subplots."""
#
#        # Possible options and defaults:
#        title    = kwargs.pop('title'    , '')
#        legtitle = kwargs.pop('legtitle' , '')
#        xlabel   = kwargs.pop('xlabel'   , '')
##       lines    = kwargs.pop('lines'    , '-')
##       markers  = kwargs.pop('markers'  , '')
#        saveas   = kwargs.pop('saveas'   , None)
#        showplot = kwargs.pop('showplot' , True)
#        for kw in kwargs:
#            print('Invalid option:',kw)
#            return
#        
#        # Preparation:
##        if type(lines) != list:
##            lines   = len(xyplots)*[lines]
##        if type(markes) != list:
##            markers = len(xyplots)*[markers]
#
#        if saveas is not None:
#            saveas = os.path.join(self._run.rundir,saveas)
#
#	n = len(xyplots)
#
#	# Plot:
#        
#        height = 11.69
#        if n == 1: height = height/2
#	fig = plt.figure(1,figsize=(8.27,height)) # DIN A4 format
#	fig.set_canvas(plt.gcf().canvas)
#
#	for i in range(len(xyplots)):
#            if i==0:
#        	ax1 = plt.subplot(n,1,1)
#        	plt.title(title, y=1.02)
#            else:
#        	plt.subplot(n,1,i+1,sharex=ax1)
#
#            if type(xyplots[i]) != list: xyplots[i] = [xyplots[i]]
#            for j in range(len(xyplots[i])):
#                plt.plot(xyplots[i][j].x,
#                         xyplots[i][j].y,
#                         label=xyplots[i][j].label)
#            plt.ylabel(xyplots[i][0].ylabel)
#            plt.legend(loc=0,title=legtitle)
#            plt.grid(b=True, which='major', color='0.5', linestyle='-')
#
#	plt.xlabel(xlabel)
#	plt.tight_layout()
#
#        self._run.plot.pdfpages.save()
#	savefig_fhitz(saveas)
#	if showplot:
#            plt.show()
#	plt.close()
#
#	if saveas != None : 
#            self._log.info('Created file: %s',saveas)
#            #call(["acroread",saveas])





    # =======================================================

    def gridData(self,var,**kwargs):
        GridData(self._run,var,**kwargs)

    def vectorPlot(self,var,**kwargs):
        VectorPlot(self._run,var,**kwargs)

    def poloidalProfile(self,vars,**kwargs):
        p = PoloidalProfile(self._run,vars,**kwargs)
        p.plot()
        return p

    def radialProfile(self,vars,**kwargs):
        p = RadialProfile(self._run,vars,**kwargs)
        p.plot()
        return p

    def compare_experiment_te(self,**kwargs):
        default  = self._run.augped_te
        kwargs['legend']     = priority('legend', kwargs, 'Simulation')
        kwargs['experiment'] = priority(['experiment','augped_file'], 
                                        kwargs, default)
        kwargs['ylabel']     = priority('ylabel', 
                                        kwargs, ('Electron temperature '
                                                 + '[$\mathrm{eV}$]'))
        return self.radialProfile(self._run.te,**kwargs)

    def compare_experiment_ne(self,**kwargs):
        default  = self._run.augped_ne
        kwargs['legend']     = priority('legend', kwargs, 'Simulation')
        kwargs['experiment'] = priority(['experiment','augped_file'], 
                                        kwargs, default)
        kwargs['ylabel']     = priority('ylabel', 
                                        kwargs, ('Electron density '
                                                 + '[$\mathrm{m}^{-3}$]'))
        return self.radialProfile(self._run.ne,**kwargs)

    def compare_experiment(self,ne=None,te=None,**kwargs):

        saveas   = kwargs.pop('saveas'  ,None)
        showplot = kwargs.pop('showplot',True)
        kwargs['saveas']   = None
        kwargs['showplot'] = False

        if ne is not None:
            kwargs['experiment'] = ne
        Pne = self.compare_experiment_ne(**kwargs)
        if te is not None:
            kwargs['experiment'] = te
        Pte = self.compare_experiment_te(**kwargs)

        kwargs['saveas']   = saveas
        kwargs['showplot'] = showplot
        P = ProfileSubplots(profiles=[Pne,Pte],**kwargs)
        P.plot()

class VectorPlot(object):
    """Creates a 2d plot of e.g. a vector field using colored arrows/vectors.
    
    Parameters
    ----------
        solpspy_run : solpspy run instance

        var : np.array(nx,ny) or np.array(nx,ny,2)
            The variable to be plotted. For the dirxy=='xy' option these
            have to be values where the vectors can be added, like, e.g.
            a force, or a flux density - but not a total flux!

        dirxy : string ('x' or 'y' or 'xy')
            The direction of the specified variable

        title : string
            Title written above the plot

        label : string
            The label of the color bar

        xlim : list (containing: two floats: [min,max])
        ylim : list (containing: two floats: [min,max])
        zlim : list (containing: two floats: [min,max])
            The plot limits/ranges in x, y and z direction

        width : float
            Width of the vectors

        length : float
            The length of the vectors is automatically adjusted such,
            that they fit in the cell. With this parameter the length
            can additionally be adjusted.

        log : bool
            To switch the z axis to a logarithmic scale

        cmap : matplotlib.colors.LinearSegmentedColormap
            Color Map for the color coding of the vectors

        map_to_cell_center : bool
            Map values which are stored on the cell faces to the cell centers

        cfd : bool
            Calculate flux density! (Use this if var is a total flux.)

    """
    def __init__(self,solpspy_run,var,**kwargs):

        # Special default color map for the vector plots:
        cm = colors.LinearSegmentedColormap.from_list("", 
                                                      ["white",
                                                       "aliceblue",
                                                       "lightskyblue",
                                                       "skyblue",
                                                       "lime",
                                                       "yellow",
                                                       "red",
                                                       "darkred"])
        self.run = solpspy_run
        self.var = var

        self.dirxy    = priority(['dir','dirxy','direction']   , kwargs, "xy")
        self.title    = priority('title'                       , kwargs, "")
        self.label    = priority(['label','clabel','zlabel']   , kwargs, "")
        self.xlim     = priority('xlim'                        , kwargs, None)
        self.ylim     = priority('ylim'                        , kwargs, None)
        self.zlim     = priority(['zlim','clim']               , kwargs, None)
        self.width    = priority('width'                       , kwargs, 0.02)
        self.length   = priority('length'                      , kwargs, 0.9)
        self.log      = priority('log'                         , kwargs, False)
        self.cmap     = priority('cmap'                        , kwargs, cm)
        self.showplot = priority(['showplot','show_plot']      , kwargs, True)
        self.saveas   = priority(['saveas','save_as']          , kwargs, None)
        self.mtcc     = priority(['mtcc' ,'map_to_cell_center'], kwargs, False)
        self.cfd      = priority('cfd'                         , kwargs, False)

        if self.mtcc:
            self.var = self.run.map_cell_faces_to_center(self.var,
                                                         direction=self.dirxy)

        # Check shape of var:
        if (self.var.shape != (self.run.nx,self.run.ny) and
            self.var.shape != (self.run.nx,self.run.ny,2)):
            print("Error: invalid shape pf 'var' for vectorPlot()",
                  "with direction '" + self.dirxy + "':",self.var.shape)
            return
        if self.dirxy == 'xy' and self.var.shape != (self.run.nx,self.run.ny,2):
            print("Error: invalid shape pf 'var' for vectorPlot()",
                  "with direction '" + self.dirxy + "':",self.var.shape)
            return

        if self.dirxy == 'x':
            if self.var.shape == (self.run.nx,self.run.ny,2):
                self.var = self.var[:,:,0]
            self.varx = self.var
            self.vary = np.zeros(self.var.shape)
        elif self.dirxy == 'y':
            if self.var.shape == (self.run.nx,self.run.ny,2):
                self.var = self.var[:,:,1]
            self.varx = np.zeros(self.var.shape)
            self.vary = self.var
        elif self.dirxy == 'xy':
            self.varx = self.var[...,0]
            self.vary = self.var[...,1]

        if self.cfd:
            self.varx = self.varx / self.run.sx
            self.vary = self.vary / self.run.sy

        # Create plot:
        self.create_figure() # Maybe make them general! They are also used
        self.plot_vessel()   # in various other places (e.g. in the chords
        self.plot_grid()     # class twice, and so on.)
                             # THEY ARE ALREADY IN IVANS NEW PLOT CLASS!!!

        self.calculate_vectors()
        self.plot_vectors()
        self.set_plot_options()

        #fig = plt.gcf()
        #fig.set_size_inches(6,8)

        self.saveas = savefig_fhitz(self.saveas)

        if self.showplot:
            plt.show()
        plt.close()

    def create_figure(self):
        self.fig = plt.figure(figsize=(9,11))
        self.canvas = self.fig.add_subplot(1,1,1)

    def plot_vessel(self):
        for i in xrange(self.run.vessel.shape[1]):
            R = np.array([self.run.vessel[0,i],self.run.vessel[2,i]])
            z = np.array([self.run.vessel[1,i],self.run.vessel[3,i]])
            self.canvas.plot(R,z,'k',linewidth = 1)

    def plot_grid(self):
        for nx in range(self.run.nx):
            for ny in range(self.run.ny):
                self.canvas.add_patch(Polygon(self.run.grid[nx,ny], True,
                                         linewidth=0.5, edgecolor='0.9',
                                         fill=False))

    def calculate_vectors(self):

        self.vectors = []
        self.vecvals = []

        # Simple version:
        # (In each cell an arrow in the center of the cell
        #  with dimensions that is just fits into the cell)

        for nx in range(1,self.run.nx-1):     # (Skipping Guard Cells)
            for ny in range(1,self.run.ny-1): # (Skipping Guard Cells)

                # Calculate value of vector:
                val = np.sqrt(self.varx[nx,ny]**2 + self.vary[nx,ny]**2)

                # Cell center:
                cc = [self.run.cr[nx,ny],self.run.cz[nx,ny]]


                # Grid Vertex Indizes:
                #
                #   3       2          
                #  -|-------|-     y ^ 
                #   |       |        |
                #  -|-------|-       |---->
                #   0       1             x

                # Calculate normalized direction:
                # x direction inside of the cell:
                ex = (self.run.grid[nx,ny,2] + 
                      self.run.grid[nx,ny,1] - 
                      self.run.grid[nx,ny,3] - 
                      self.run.grid[nx,ny,0]) / 2
                # y direction inside of the cell:
                ey = (self.run.grid[nx,ny,2] + 
                      self.run.grid[nx,ny,3] - 
                      self.run.grid[nx,ny,0] - 
                      self.run.grid[nx,ny,1]) / 2
                # Vector direction:
                dirxy = self.varx[nx,ny]*ex + self.vary[nx,ny]*ey
                dirxy = dirxy/np.linalg.norm(dirxy)

                # Calculate vector length:
                # First define the boundary lines/cell faces:
                faces = [LinAlg2d.Line(self.run.grid[nx,ny,1],
                                       self.run.grid[nx,ny,2]),
                         LinAlg2d.Line(self.run.grid[nx,ny,0],
                                       self.run.grid[nx,ny,3]),
                         LinAlg2d.Line(self.run.grid[nx,ny,2],
                                       self.run.grid[nx,ny,3]),
                         LinAlg2d.Line(self.run.grid[nx,ny,0],
                                       self.run.grid[nx,ny,1])]
                # Find the intersections of the boundaries with the vector:
                vec = LinAlg2d.LineDir(cc,dirxy)
                ints = []
                for f in faces:
                    int = LinAlg2d.find_intersection(vec,f)
                    # Only consider the two relevant
                    # intersections inside of the cell:
                    if LinAlg2d.point_inside_cell(self.run.grid[nx,ny],int):
                        ints.append(int)
                # Calculate the distance:
                d = LinAlg2d.calculate_distance(ints[0],ints[1])
                dirxy = d*dirxy*self.length

                # Shift vector to cell center:
                pos = cc-dirxy/2

                self.vectors.append(Arrow(pos  [0],pos  [1],
                                          dirxy[0],dirxy[1],
                                          self.width))
                self.vecvals.append(val)

        self.vecvals = np.array(self.vecvals)

#        # TODO:
#        # Advanced version:
#        # (Combine small cells in such a way, that all vectors have
#        #  approximately the same dimensions, without loosing to
#        #  much detail.)
#        pass

    def plot_vectors(self):
        p = PatchCollection(self.vectors,zorder=10)

        if self.zlim is not None:
            p.set_clim(self.zlim)
        if self.log:
            if self.zlim is not None:
                p.set_norm(colors.LogNorm(vmin = self.zlim[0], 
                                          vmax = self.zlim[1]))
            else:
                p.set_norm(colors.LogNorm())
        p.set(array=self.vecvals,cmap=self.cmap)
        p.set_edgecolor('face')
        self.canvas.add_collection(p)
        cbar = plt.colorbar(p)
        if self.label: cbar.set_label(self.label, labelpad=10)

    def set_plot_options(self):
        self.canvas.axis('equal')

        # Set plot limits:
        xmin=self.run.grid[:,:,:,0].min()
        xmax=self.run.grid[:,:,:,0].max()
        ymin=self.run.grid[:,:,:,1].min()
        ymax=self.run.grid[:,:,:,1].max()
        dy = (ymax - ymin)*0.05
        dx = (xmax - xmin)*0.05
        xmin=xmin-dx
        xmax=xmax+dx
        ymin=ymin-dy
        ymax=ymax+dy
        if self.xlim is not None:
            xmin=self.xlim[0]
            xmax=self.xlim[1]
        if self.ylim is not None:
            ymin=self.ylim[0]
            ymax=self.ylim[1]
        self.canvas.axis([xmin, xmax, ymin, ymax])

        # Title and x/y-labels:
        self.canvas.set_title(self.title)
        self.canvas.set_xlabel('R [m]')
        self.canvas.set_ylabel('z [m]')

class GridData(object):
    def __init__(self,solpspy_run,var,**kwargs):
        pass

# ==================================================================

class IonizationFront2D(object):
    def __init__(self,solpspy_run,**kwargs):
        self.run           = solpspy_run
        self.title         = priority('title'         , kwargs, None)
        self.titles        = priority('titles'        , kwargs, '')
        self.species       = priority('species'       , kwargs, 'D')
        self.normalization = priority('normalization' , kwargs, 'seperate')
        self.suppression   = priority('suppression'   , kwargs, 1.0)
        self.figsize       = priority('figsize'       , kwargs, (8,4))
        self.showplot      = priority('showplot'      , kwargs, True)
        self.saveas        = priority('saveas'        , kwargs, None)

        # Preparation:
        if type(self.species) != list:
            self.species = [self.species]
        if type(self.titles) != list:
            self.titles = len(self.species)*[self.titles]
        self.figsize = convert_figsize(self.figsize)
        self.ionization = self.normalized_ionization()

        # Create default kwargs for plotting:
        self.kwargs = {}
        xlim,ylim = self.focus_on_divertor()
        self.kwargs['xlim']       = priority('xlim'      , kwargs, xlim)
        self.kwargs['ylim']       = priority('ylim'      , kwargs, ylim)
        self.kwargs['cmap']       = priority('cmap'      , kwargs, plt.cm.hot_r)
        self.kwargs['cbar']       = priority('cbar'      , kwargs, False)
        self.kwargs['lw']         = priority('lw'        , kwargs, 0.02)
        self.kwargs['edgecolor']  = priority('edgecolor' , kwargs, 'k')
        self.kwargs['separatrix'] = priority('separatrix', kwargs, True)
        self.kwargs['separatrix_color'] = priority('separatrix_color', 
                                                   kwargs, 'k')
        self.kwargs['figsize']    = priority('figsize'   , kwargs, self.figsize)

        self.plot()

    def plot(self):
        self.do_subplots()
        plt.tight_layout()

        # Adjust positioning for titles:
        if self.title is not None:
            self.fig.subplots_adjust(top=0.95)
        if self.title is not None and self.titles[0] != '':
            self.fig.subplots_adjust(top=0.91)
        if self.titles[-1] != '':
            self.fig.subplots_adjust(hspace=0.3)

        self.saveas = savefig_fhitz(self.saveas)
	if self.showplot: plt.show()
        plt.close()

    def do_subplots(self):
	n = len(self.species)
        width  = self.figsize[0]
        height = n*self.figsize[1]
	self.fig = plt.figure(1,figsize=(width,height)) # DIN A4 format
	self.fig.set_canvas(plt.gcf().canvas)
	for i,spec in enumerate(self.species):
            ax = plt.subplot(n,1,i+1)
            var = self.ionization[...,self.run.atom_index(spec)]
            self.kwargs['title'] = self.titles[i]
            self.run.plot.grid_data(var,canvas=ax,**self.kwargs)
        if self.title is not None:
            self.fig.suptitle(self.title)

    def normalized_ionization(self):
        neutrals   = [self.run.species_index(a)[0] for a in self.run.atom_names]
        ionization = self.run.rsana[:,:,neutrals]/np.expand_dims(self.run.vol,2)

        if self.normalization == 'global':
            for i in range(ionization.shape[-1]):
                ionization[...,i] = ionization[...,i]/np.max(ionization[...,i])
        else:
            for i in range(ionization.shape[-1]):
                ion_inn = ionization[...,i] * self.run.masks.inndiv
                ion_out = ionization[...,i] * self.run.masks.outdiv
                ionization[...,i]  = ion_inn / np.max(ion_inn)
                ionization[...,i] += ion_out / np.max(ion_out)
        return ionization**self.suppression

    def focus_on_divertor(self):
        xlim = [np.min(self.run.cr[np.where(self.run.masks.div)]),
                np.max(self.run.cr[np.where(self.run.masks.div)])]
        ylim = [np.min(self.run.cz[np.where(self.run.masks.div)]),
                np.max(self.run.cz[np.where(self.run.masks.div)])]

        # Only go up to the X-point?!:
        if 1:
            ylim[1] = self.run.xpoint[1]

        margin = 0.1
        xlim = [xlim[0]-margin*(xlim[1]-xlim[0]),
                xlim[1]+margin*(xlim[1]-xlim[0])]
        ylim = [ylim[0]-margin*(ylim[1]-ylim[0]),
                ylim[1]+margin*(ylim[1]-ylim[0])]
        return xlim,ylim
